def check(cond):
    if cond:
        return '[OK]'
    return '[ERROR]'


vs = 3 #V
imax = 5 #A
#imin = 0.0025 #A 
pdmax = 0.25 #watt
gain = 50 #V/V 25, 50, 100, 200 or 500
unidirectional = True
vref = 0 if unidirectional else vs/2 # voltage applied to the REF pin.
ADC_ref = 2.5 #V maximal 

print('---- inputs:')
print('supply voltage:   ', vs, 'V', check(1.7 < vs and vs < 5.5))
print('max current:      ', imax, 'A')
# print('min current:      ', imin, 'A')
print('max power over R: ', pdmax, 'W')
print('gain:             ', gain, 'V/V')
print('vref:             ', vref, 'V ( 0V for unidirectional )')
print('exteral ADC ref:  ', ADC_ref, 'V ')
print('unidirectional' if unidirectional else 'bidirectional')
print()
print('---- outputs:')

if not unidirectional:
    raise Exception('bidirectional mode not implemented') 
    


rsens = pdmax / imax**2
print('Rsens is', rsens, 'Ohm (',pdmax,'W at',imax,'A )')

# maximum/minimum output range in V
vsp = vs - 40e-3
vsn = 1e-3
print('Maximal output range from', vsn, 'V to', vsp, 'V')

# maximum/minimum output range in A
i_min_measureable = (vsn - vref) / (rsens * gain )
i_max_measureable = (vsp - vref) / (rsens * gain )
print('Full output range is ', i_min_measureable, 'A to ', i_max_measureable, 'A')
print()

imin = i_min_measureable
max_swing = (imax * rsens * gain) + vref
min_swing = (imin * rsens * gain) + vref

print('Range is', imin,'A (', min_swing, 'V ) to', imax,'A (', max_swing, 'V )')

print('Using ',100*max_swing/ADC_ref,'% of ADC resolution')

print('    min output bigger than min output range: ', check(min_swing >= vsn))
print('    max output smaller than max output range:', check(max_swing <= vsp))
print('    max output smaller than ext. ADC range:  ', check(max_swing <= ADC_ref))
 

