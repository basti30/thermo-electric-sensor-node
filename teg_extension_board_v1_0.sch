<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="80" name="tRubout_CU_Freistellung" color="7" fill="1" visible="no" active="no"/>
<layer number="81" name="bRubout_CU_Freistellung" color="7" fill="1" visible="yes" active="yes"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="7" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="no"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="no"/>
<layer number="110" name="110" color="7" fill="1" visible="no" active="no"/>
<layer number="111" name="111" color="7" fill="1" visible="no" active="no"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="4" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="129" name="top_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="147" name="Heatsink-Milling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="247" name="wrappinf" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="no"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="11" visible="no" active="no"/>
<layer number="252" name="BR-BS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="253" name="Trafo_Layer2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="OrgLBR" color="7" fill="1" visible="no" active="no"/>
<layer number="255" name="Accent" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="DINA3_L" urn="urn:adsk.eagle:symbol:13868/1" library_version="1">
<frame x1="0" y1="0" x2="388.62" y2="264.16" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD" urn="urn:adsk.eagle:symbol:13864/1" library_version="1">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA3_L" urn="urn:adsk.eagle:component:13931/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, landscape with extra doc field</description>
<gates>
<gate name="G$1" symbol="DINA3_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="287.02" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="partLibrary">
<packages>
<package name="C0603">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.1444" y1="0.5992" x2="1.1444" y2="0.5992" width="0.0508" layer="39"/>
<wire x1="1.1444" y1="0.5992" x2="1.1444" y2="-0.5992" width="0.0508" layer="39"/>
<wire x1="1.1444" y1="-0.5992" x2="-1.1444" y2="-0.5992" width="0.0508" layer="39"/>
<wire x1="-1.1444" y1="-0.5992" x2="-1.1444" y2="0.5992" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="0.6" dy="0.9" layer="1"/>
<smd name="2" x="0.7" y="0" dx="0.6" dy="0.9" layer="1"/>
<text x="0" y="0.762" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.762" layer="27" font="vector" ratio="15" rot="R180" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-1.1968" y1="0.6492" x2="1.1968" y2="0.6492" width="0.15" layer="21"/>
<wire x1="1.1968" y1="0.6492" x2="1.1968" y2="-0.6492" width="0.15" layer="21"/>
<wire x1="1.1968" y1="-0.6492" x2="-1.1968" y2="-0.6492" width="0.15" layer="21"/>
<wire x1="-1.1968" y1="-0.6492" x2="-1.1968" y2="0.6492" width="0.15" layer="21"/>
</package>
<package name="C0402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-0.9071" y1="0.4068" x2="0.9071" y2="0.4068" width="0.0508" layer="39"/>
<wire x1="0.9071" y1="0.4068" x2="0.9071" y2="-0.4068" width="0.0508" layer="39"/>
<wire x1="0.9071" y1="-0.4068" x2="-0.9071" y2="-0.4068" width="0.0508" layer="39"/>
<wire x1="-0.9071" y1="-0.4068" x2="-0.9071" y2="0.4068" width="0.0508" layer="39"/>
<smd name="1" x="-0.508" y="0" dx="0.508" dy="0.508" layer="1"/>
<smd name="2" x="0.508" y="0" dx="0.508" dy="0.508" layer="1"/>
<text x="0" y="0.508" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.508" size="0.762" layer="27" font="vector" ratio="15" rot="R180" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1524" y1="-0.2794" x2="0.1524" y2="0.2794" layer="35"/>
<wire x1="-0.9398" y1="0.4318" x2="0.9398" y2="0.4318" width="0.127" layer="21"/>
<wire x1="0.9398" y1="0.4318" x2="0.9398" y2="-0.4318" width="0.127" layer="21"/>
<wire x1="0.9398" y1="-0.4318" x2="-0.9398" y2="-0.4318" width="0.127" layer="21"/>
<wire x1="-0.9398" y1="-0.4318" x2="-0.9398" y2="0.4318" width="0.127" layer="21"/>
</package>
<package name="C1812">
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="0" y="2.159" size="0.762" layer="25" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.159" size="0.762" layer="27" ratio="15" rot="R180" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="C0805">
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="0.02" y="1.07" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="-0.01" y="-1.04" size="0.762" layer="27" font="vector" ratio="15" rot="R180" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="0" y="1.143" size="0.762" layer="25" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.143" size="0.762" layer="27" ratio="15" rot="R180" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.423" y1="0.65" x2="1.423" y2="0.65" width="0.0508" layer="39"/>
<wire x1="1.423" y1="0.65" x2="1.423" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="1.423" y1="-0.65" x2="-1.423" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="-1.423" y1="-0.65" x2="-1.423" y2="0.65" width="0.0508" layer="39"/>
<smd name="1" x="-0.8" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.8" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="0" y="0.889" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.889" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-1.35" y1="0.7" x2="1.4" y2="0.7" width="0.15" layer="21"/>
<wire x1="1.4" y1="0.7" x2="1.4" y2="-0.7" width="0.15" layer="21"/>
<wire x1="1.4" y1="-0.7" x2="-1.4" y2="-0.7" width="0.15" layer="21"/>
<wire x1="-1.4" y1="-0.7" x2="-1.4" y2="0.7" width="0.15" layer="21"/>
</package>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-0.9206" y1="0.4576" x2="0.9206" y2="0.4576" width="0.0508" layer="39"/>
<wire x1="0.9206" y1="0.4576" x2="0.9206" y2="-0.4576" width="0.0508" layer="39"/>
<wire x1="0.9206" y1="-0.4576" x2="-0.9206" y2="-0.4576" width="0.0508" layer="39"/>
<wire x1="-0.9206" y1="-0.4576" x2="-0.9206" y2="0.4576" width="0.0508" layer="39"/>
<smd name="1" x="-0.5" y="0" dx="0.5" dy="0.6" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.5" dy="0.6" layer="1"/>
<text x="0" y="0.635" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.635" size="0.762" layer="27" font="vector" ratio="15" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.12" y1="-0.35" x2="0.12" y2="0.35" layer="35"/>
<wire x1="-0.9492" y1="0.4992" x2="0.9492" y2="0.4992" width="0.15" layer="21"/>
<wire x1="0.9492" y1="0.4992" x2="0.9492" y2="-0.4992" width="0.15" layer="21"/>
<wire x1="0.9492" y1="-0.4992" x2="-0.9492" y2="-0.4992" width="0.15" layer="21"/>
<wire x1="-0.9492" y1="-0.4992" x2="-0.9492" y2="0.4992" width="0.15" layer="21"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="0" y="1.016" size="0.762" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.016" size="0.762" layer="27" font="vector" ratio="15" rot="R180" align="bottom-center">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="Z0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-0.9206" y1="0.4576" x2="0.9206" y2="0.4576" width="0.0508" layer="39"/>
<wire x1="0.9206" y1="0.4576" x2="0.9206" y2="-0.4576" width="0.0508" layer="39"/>
<wire x1="0.9206" y1="-0.4576" x2="-0.9206" y2="-0.4576" width="0.0508" layer="39"/>
<wire x1="-0.9206" y1="-0.4576" x2="-0.9206" y2="0.4576" width="0.0508" layer="39"/>
<smd name="1" x="-0.5" y="0" dx="0.5" dy="0.6" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.5" dy="0.6" layer="1"/>
<text x="0" y="0.635" size="1.016" layer="25" font="vector" ratio="12" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.635" size="1.016" layer="27" font="vector" ratio="12" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.12" y1="-0.35" x2="0.12" y2="0.35" layer="35"/>
<wire x1="-0.9492" y1="0.4992" x2="0.9492" y2="0.4992" width="0.15" layer="21"/>
<wire x1="0.9492" y1="0.4992" x2="0.9492" y2="-0.4992" width="0.15" layer="21"/>
<wire x1="0.9492" y1="-0.4992" x2="-0.9492" y2="-0.4992" width="0.15" layer="21"/>
<wire x1="-0.9492" y1="-0.4992" x2="-0.9492" y2="0.4992" width="0.15" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="C-EU" library_version="2">
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0.508" y2="0" width="0.1524" layer="94"/>
<text x="0" y="1.905" size="1.27" layer="95" align="top-right">&gt;NAME</text>
<text x="0" y="-1.905" size="1.27" layer="96" align="bottom-right">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="3.81" y2="0.254" layer="94" rot="R90"/>
<rectangle x1="-1.27" y1="-0.254" x2="2.794" y2="0.254" layer="94" rot="R90"/>
<pin name="1" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="R-EU" library_version="2">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="0" y="1.27" size="1.27" layer="95" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.54" size="1.27" layer="96" align="bottom-center">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="Z1000" library_version="2">
<pin name="1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<text x="0" y="1.778" size="1.27" layer="95" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.778" size="1.27" layer="96" rot="R180" align="bottom-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="C-EU-?-*" prefix="C" uservalue="yes">
<description>SMD Capacitors</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="1U/16V">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics" constant="no"/>
<attribute name="MANUFACTURER_PN" value="CL10B105KO8NNNC" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="1276-1019-1-ND" constant="no"/>
<attribute name="VALUE" value="1u/16V" constant="no"/>
</technology>
<technology name="4U7/6V3">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="Samsung Electro-Mechanics" constant="no"/>
<attribute name="MANUFACTURER_PN" value="CL10B475KQ8NQNC" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="1276-2087-6-ND" constant="no"/>
<attribute name="VALUE" value="4.7u/6.3V" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="0P5/50V">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="Murata Electronics" constant="no"/>
<attribute name="MANUFACTURER_PN" value="GJM1555C1HR50WB01D" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="490-11250-6-ND" constant="no"/>
<attribute name="VALUE" value="0.5p/C0G" constant="no"/>
</technology>
<technology name="0P8/50V">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="Murata Electronics" constant="no"/>
<attribute name="MANUFACTURER_PN" value="GJM1555C1HR80WB01D" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="490-11253-6-ND" constant="no"/>
<attribute name="VALUE" value="0.8p/C0G" constant="no"/>
</technology>
<technology name="100N/50V">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="Murata Electronics" constant="no"/>
<attribute name="MANUFACTURER_PN" value="GRM155R71H104KE14J" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="490-13342-6-ND" constant="no"/>
<attribute name="VALUE" value="100n" constant="no"/>
</technology>
<technology name="100P/50V">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="Murata Electronics" constant="no"/>
<attribute name="MANUFACTURER_PN" value="GRM1555C1H101JA01J" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="490-7754-6-ND" constant="no"/>
<attribute name="VALUE" value="100p" constant="no"/>
</technology>
<technology name="10U/6V3">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="Murata Electronics" constant="no"/>
<attribute name="MANUFACTURER_PN" value="GRJ155R60J106ME11D" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="490-13211-6-ND" constant="no"/>
<attribute name="VALUE" value="10u/6.3V" constant="no"/>
</technology>
<technology name="12P/50V">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="Taiyo Yuden" constant="no"/>
<attribute name="MANUFACTURER_PN" value="UMK105CG120JV-F" constant="no"/>
<attribute name="SUPPLIER" value="Mouser" constant="no"/>
<attribute name="SUPPLIER_OC" value="963-UMK105CG120JV-F" constant="no"/>
<attribute name="VALUE" value="12p" constant="no"/>
</technology>
<technology name="1N/50V">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="Murata Electronics" constant="no"/>
<attribute name="MANUFACTURER_PN" value="GRM1555C1H102JA01D" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="490-3244-6-ND" constant="no"/>
<attribute name="VALUE" value="1n" constant="no"/>
</technology>
<technology name="1P2/50V">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="Johanson Technology" constant="no"/>
<attribute name="MANUFACTURER_PN" value="500R07S1R2BV4T" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="712-1268-6-ND" constant="no"/>
<attribute name="VALUE" value="1.2p/C0G" constant="no"/>
</technology>
<technology name="1U/10V">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="Murata Electronics" constant="no"/>
<attribute name="MANUFACTURER_PN" value="GRM155R61A105KE01D" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="490-12701-2-ND" constant="no"/>
<attribute name="VALUE" value="1u/10V" constant="no"/>
</technology>
<technology name="47N/50V">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER_PN" value="CC0402KRX7R9BB473" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="311-3815-6-ND" constant="no"/>
<attribute name="VALUE" value="47n" constant="no"/>
</technology>
<technology name="820P/50V">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER_PN" value="CC0402KRX7R9BB821" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="311-1727-6-ND" constant="no"/>
<attribute name="VALUE" value="820p" constant="no"/>
</technology>
<technology name="DNP">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="1U/200V">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="AVX" constant="no"/>
<attribute name="MANUFACTURER_PN" value="18122C105KAT2A" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="478-10705-6-ND" constant="no"/>
<attribute name="VALUE" value="1u/200V" constant="no"/>
</technology>
</technologies>
</device>
<device name="0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="10U/16V">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="Samsung" constant="no"/>
<attribute name="MANUFACTURER_PN" value="CL21B106KOQNNNE" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="1276-2872-6-ND" constant="no"/>
<attribute name="VALUE" value="10u/16V" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="220U/6V3">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="Murata Electronics" constant="no"/>
<attribute name="MANUFACTURER_PN" value="GRM31CR60J227ME11L" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="490-13970-6-ND" constant="no"/>
<attribute name="VALUE" value="220u/6V3" constant="no"/>
</technology>
<technology name="DNP">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R-EU-?-*" prefix="R" uservalue="yes">
<description>SMD Resistors&lt;br&gt;</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="0">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER_PN" value="RC0402JR-070RL" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="311-0.0JRDKR-ND" constant="no"/>
<attribute name="VALUE" value="0" constant="no"/>
</technology>
<technology name="100K/1P">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER_PN" value="RC0402FR-07100KL" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="311-100KLRTR-ND" constant="no"/>
<attribute name="VALUE" value="100k/1%" constant="no"/>
</technology>
<technology name="10R/1P">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER_PN" value="RC0402FR-0710RL" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="311-10.0LRTR-ND" constant="no"/>
<attribute name="VALUE" value="10/1%" constant="no"/>
</technology>
<technology name="1M/1P">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER_PN" value="RC0402FR-071ML" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="311-1.00MLRTR-ND" constant="no"/>
<attribute name="VALUE" value="1M/1%" constant="no"/>
</technology>
<technology name="21K/0P1">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER_PN" value="RT0402BRD0721KL" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="YAG1389DKR-ND" constant="no"/>
<attribute name="VALUE" value="21k/0.1%" constant="no"/>
</technology>
<technology name="30K/1P">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER_PN" value="RC0402FR-0730KL" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="311-30.0KLRDKR-ND" constant="no"/>
<attribute name="VALUE" value="30k/1%" constant="no"/>
</technology>
<technology name="43K/1P">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER_PN" value="RC0402FR-0743KL" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="311-43.0KLRDKR-ND" constant="no"/>
<attribute name="VALUE" value="43k/1%" constant="no"/>
</technology>
<technology name="45K3/1P">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER_PN" value="RC0402FR-0745K3L" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="311-45.3KLRDKR-ND" constant="no"/>
<attribute name="VALUE" value="45.3k/1%" constant="no"/>
</technology>
<technology name="47K/1P">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER_PN" value="RC0402FR-0747KL" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="311-47.0KLRDKR-ND" constant="no"/>
<attribute name="VALUE" value="47k/1%" constant="no"/>
</technology>
<technology name="78K7/0P1">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER_PN" value="RT0402BRD0778K7L" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="YAG4311DKR-ND" constant="no"/>
<attribute name="VALUE" value="78.7k/0.1%" constant="no"/>
</technology>
<technology name="80K6/0P1">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="Panasonic" constant="no"/>
<attribute name="MANUFACTURER_PN" value="ERA-2AEB8062X" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="P80.6KDCDKR-ND" constant="no"/>
<attribute name="VALUE" value="80.6k/0.1%" constant="no"/>
</technology>
<technology name="DNP">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
<device name="0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="0">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="Yageo" constant="no"/>
<attribute name="MANUFACTURER_PN" value="RC0805JR-070RL" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="311-0.0ARDKR-ND" constant="no"/>
<attribute name="VALUE" value="0" constant="no"/>
</technology>
<technology name="DNP">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="DNP" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="Z1000" prefix="Z">
<gates>
<gate name="G$1" symbol="Z1000" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="Z0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="1000/100MHZ">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="Murata Electronics" constant="no"/>
<attribute name="MANUFACTURER_PN" value="BLM15AX102SN1D" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="490-5442-6-ND" constant="no"/>
<attribute name="VALUE" value="Z1000" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="voest_library">
<packages>
<package name="VQFN32-5X5">
<smd name="EXP1" x="0" y="0" dx="2.1" dy="2.1" layer="1" thermals="no" cream="no"/>
<pad name="EXP6" x="-0.8" y="0" drill="0.2" diameter="0.3" stop="no" thermals="no"/>
<pad name="EXP3" x="0" y="0.8" drill="0.2" diameter="0.3" stop="no" thermals="no"/>
<pad name="EXP4" x="0.8" y="0" drill="0.2" diameter="0.3" stop="no" thermals="no"/>
<pad name="EXP5" x="0" y="-0.8" drill="0.2" diameter="0.3" stop="no" thermals="no"/>
<smd name="1" x="-2.4" y="1.75" dx="0.6" dy="0.25" layer="1" stop="no"/>
<pad name="EXP2" x="0" y="0" drill="0.2" diameter="0.3" stop="no" thermals="no"/>
<rectangle x1="-1" y1="0.2" x2="-0.2" y2="1" layer="31"/>
<rectangle x1="-1" y1="-1" x2="-0.2" y2="-0.2" layer="31"/>
<rectangle x1="0.2" y1="-1" x2="1" y2="-0.2" layer="31"/>
<rectangle x1="0.2" y1="0.2" x2="1" y2="1" layer="31"/>
<rectangle x1="-2.77" y1="1.555" x2="-2.03" y2="1.945" layer="29"/>
<smd name="2" x="-2.4" y="1.25" dx="0.6" dy="0.25" layer="1" stop="no"/>
<rectangle x1="-2.77" y1="1.055" x2="-2.03" y2="1.445" layer="29"/>
<smd name="3" x="-2.4" y="0.75" dx="0.6" dy="0.25" layer="1" stop="no"/>
<rectangle x1="-2.77" y1="0.555" x2="-2.03" y2="0.945" layer="29"/>
<smd name="4" x="-2.4" y="0.25" dx="0.6" dy="0.25" layer="1" stop="no"/>
<rectangle x1="-2.77" y1="0.055" x2="-2.03" y2="0.445" layer="29"/>
<smd name="5" x="-2.4" y="-0.25" dx="0.6" dy="0.25" layer="1" stop="no"/>
<rectangle x1="-2.77" y1="-0.445" x2="-2.03" y2="-0.055" layer="29"/>
<smd name="6" x="-2.4" y="-0.75" dx="0.6" dy="0.25" layer="1" stop="no"/>
<rectangle x1="-2.77" y1="-0.945" x2="-2.03" y2="-0.555" layer="29"/>
<smd name="7" x="-2.4" y="-1.25" dx="0.6" dy="0.25" layer="1" stop="no"/>
<rectangle x1="-2.77" y1="-1.445" x2="-2.03" y2="-1.055" layer="29"/>
<smd name="8" x="-2.4" y="-1.75" dx="0.6" dy="0.25" layer="1" stop="no"/>
<rectangle x1="-2.77" y1="-1.945" x2="-2.03" y2="-1.555" layer="29"/>
<smd name="9" x="-1.75" y="-2.4" dx="0.6" dy="0.25" layer="1" rot="R90" stop="no"/>
<rectangle x1="-2.12" y1="-2.595" x2="-1.38" y2="-2.205" layer="29" rot="R90"/>
<smd name="10" x="-1.25" y="-2.4" dx="0.6" dy="0.25" layer="1" rot="R90" stop="no"/>
<rectangle x1="-1.62" y1="-2.595" x2="-0.88" y2="-2.205" layer="29" rot="R90"/>
<smd name="11" x="-0.75" y="-2.4" dx="0.6" dy="0.25" layer="1" rot="R90" stop="no"/>
<rectangle x1="-1.12" y1="-2.595" x2="-0.38" y2="-2.205" layer="29" rot="R90"/>
<smd name="12" x="-0.25" y="-2.4" dx="0.6" dy="0.25" layer="1" rot="R90" stop="no"/>
<rectangle x1="-0.62" y1="-2.595" x2="0.12" y2="-2.205" layer="29" rot="R90"/>
<smd name="13" x="0.25" y="-2.4" dx="0.6" dy="0.25" layer="1" rot="R90" stop="no"/>
<rectangle x1="-0.12" y1="-2.595" x2="0.62" y2="-2.205" layer="29" rot="R90"/>
<smd name="14" x="0.75" y="-2.4" dx="0.6" dy="0.25" layer="1" rot="R90" stop="no"/>
<rectangle x1="0.38" y1="-2.595" x2="1.12" y2="-2.205" layer="29" rot="R90"/>
<smd name="15" x="1.25" y="-2.4" dx="0.6" dy="0.25" layer="1" rot="R90" stop="no"/>
<rectangle x1="0.88" y1="-2.595" x2="1.62" y2="-2.205" layer="29" rot="R90"/>
<smd name="16" x="1.75" y="-2.4" dx="0.6" dy="0.25" layer="1" rot="R90" stop="no"/>
<rectangle x1="1.38" y1="-2.595" x2="2.12" y2="-2.205" layer="29" rot="R90"/>
<smd name="17" x="2.4" y="-1.75" dx="0.6" dy="0.25" layer="1" rot="R180" stop="no"/>
<rectangle x1="2.03" y1="-1.945" x2="2.77" y2="-1.555" layer="29" rot="R180"/>
<smd name="18" x="2.4" y="-1.25" dx="0.6" dy="0.25" layer="1" rot="R180" stop="no"/>
<rectangle x1="2.03" y1="-1.445" x2="2.77" y2="-1.055" layer="29" rot="R180"/>
<smd name="19" x="2.4" y="-0.75" dx="0.6" dy="0.25" layer="1" rot="R180" stop="no"/>
<rectangle x1="2.03" y1="-0.945" x2="2.77" y2="-0.555" layer="29" rot="R180"/>
<smd name="20" x="2.4" y="-0.25" dx="0.6" dy="0.25" layer="1" rot="R180" stop="no"/>
<rectangle x1="2.03" y1="-0.445" x2="2.77" y2="-0.055" layer="29" rot="R180"/>
<smd name="21" x="2.4" y="0.25" dx="0.6" dy="0.25" layer="1" rot="R180" stop="no"/>
<rectangle x1="2.03" y1="0.055" x2="2.77" y2="0.445" layer="29" rot="R180"/>
<smd name="22" x="2.4" y="0.75" dx="0.6" dy="0.25" layer="1" rot="R180" stop="no"/>
<rectangle x1="2.03" y1="0.555" x2="2.77" y2="0.945" layer="29" rot="R180"/>
<smd name="23" x="2.4" y="1.25" dx="0.6" dy="0.25" layer="1" rot="R180" stop="no"/>
<rectangle x1="2.03" y1="1.055" x2="2.77" y2="1.445" layer="29" rot="R180"/>
<smd name="24" x="2.4" y="1.75" dx="0.6" dy="0.25" layer="1" rot="R180" stop="no"/>
<rectangle x1="2.03" y1="1.555" x2="2.77" y2="1.945" layer="29" rot="R180"/>
<smd name="25" x="1.75" y="2.4" dx="0.6" dy="0.25" layer="1" rot="R270" stop="no"/>
<rectangle x1="1.38" y1="2.205" x2="2.12" y2="2.595" layer="29" rot="R270"/>
<smd name="26" x="1.25" y="2.4" dx="0.6" dy="0.25" layer="1" rot="R270" stop="no"/>
<rectangle x1="0.88" y1="2.205" x2="1.62" y2="2.595" layer="29" rot="R270"/>
<smd name="27" x="0.75" y="2.4" dx="0.6" dy="0.25" layer="1" rot="R270" stop="no"/>
<rectangle x1="0.38" y1="2.205" x2="1.12" y2="2.595" layer="29" rot="R270"/>
<smd name="28" x="0.25" y="2.4" dx="0.6" dy="0.25" layer="1" rot="R270" stop="no"/>
<rectangle x1="-0.12" y1="2.205" x2="0.62" y2="2.595" layer="29" rot="R270"/>
<smd name="29" x="-0.25" y="2.4" dx="0.6" dy="0.25" layer="1" rot="R270" stop="no"/>
<rectangle x1="-0.62" y1="2.205" x2="0.12" y2="2.595" layer="29" rot="R270"/>
<smd name="30" x="-0.75" y="2.4" dx="0.6" dy="0.25" layer="1" rot="R270" stop="no"/>
<rectangle x1="-1.12" y1="2.205" x2="-0.38" y2="2.595" layer="29" rot="R270"/>
<smd name="31" x="-1.25" y="2.4" dx="0.6" dy="0.25" layer="1" rot="R270" stop="no"/>
<rectangle x1="-1.62" y1="2.205" x2="-0.88" y2="2.595" layer="29" rot="R270"/>
<smd name="32" x="-1.75" y="2.4" dx="0.6" dy="0.25" layer="1" rot="R270" stop="no"/>
<rectangle x1="-2.12" y1="2.205" x2="-1.38" y2="2.595" layer="29" rot="R270"/>
<wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.1" layer="51"/>
<wire x1="2.5" y1="-2.5" x2="2.5" y2="2.5" width="0.1" layer="51"/>
<wire x1="2.5" y1="2.5" x2="-2.5" y2="2.5" width="0.1" layer="51"/>
<wire x1="-2.5" y1="2.5" x2="-2.5" y2="-2.5" width="0.1" layer="51"/>
<rectangle x1="-2.5" y1="0.5" x2="-0.5" y2="2.5" layer="51"/>
<text x="-0.7" y="-1.5" size="0.254" layer="25">&gt;NAME</text>
<text x="-0.7" y="-1.9" size="0.254" layer="27">&gt;VALUE</text>
</package>
<package name="THERMOCOUPLES_CONNECTOR">
<pad name="1" x="-17.145" y="0" drill="1.4"/>
<pad name="2" x="-13.335" y="0" drill="1.4"/>
<pad name="3" x="-9.525" y="0" drill="1.4"/>
<pad name="4" x="-5.715" y="0" drill="1.4"/>
<pad name="5" x="-1.905" y="0" drill="1.4"/>
<pad name="6" x="1.905" y="0" drill="1.4"/>
<pad name="7" x="5.715" y="0" drill="1.4"/>
<pad name="8" x="9.525" y="0" drill="1.4"/>
<pad name="9" x="13.335" y="0" drill="1.4"/>
<pad name="10" x="17.145" y="0" drill="1.4"/>
<pad name="NC@1" x="-21.545" y="0" drill="3"/>
<pad name="NC@2" x="21.545" y="0" drill="3"/>
<wire x1="24.2" y1="-3" x2="24.2" y2="4.2" width="0.2" layer="21"/>
<wire x1="24.2" y1="4.2" x2="-24.2" y2="4.2" width="0.2" layer="21"/>
<wire x1="-24.2" y1="4.2" x2="-24.2" y2="-3" width="0.2" layer="21"/>
<wire x1="-24.2" y1="-3" x2="24.2" y2="-3" width="0.2" layer="21"/>
<rectangle x1="-24.2" y1="2.7" x2="-19.2" y2="4.2" layer="21"/>
<rectangle x1="19.2" y1="2.7" x2="24.2" y2="4.2" layer="21"/>
<wire x1="-17.145" y1="-3.9290625" x2="-15.24" y2="-5.476875" width="0.2" layer="21"/>
<wire x1="-15.24" y1="-5.476875" x2="-13.335" y2="-3.9290625" width="0.2" layer="21"/>
<wire x1="-17.145" y1="-3.33375" x2="-17.145" y2="-3.9290625" width="0.2" layer="21"/>
<wire x1="-13.335" y1="-3.33375" x2="-13.335" y2="-3.9290625" width="0.2" layer="21"/>
<circle x="-15.24" y="-5.476875" radius="0.238125" width="0.3" layer="21"/>
<wire x1="-9.525" y1="-3.9290625" x2="-7.62" y2="-5.476875" width="0.2" layer="21"/>
<wire x1="-7.62" y1="-5.476875" x2="-5.715" y2="-3.9290625" width="0.2" layer="21"/>
<wire x1="-9.525" y1="-3.33375" x2="-9.525" y2="-3.9290625" width="0.2" layer="21"/>
<wire x1="-5.715" y1="-3.33375" x2="-5.715" y2="-3.9290625" width="0.2" layer="21"/>
<circle x="-7.62" y="-5.476875" radius="0.238125" width="0.3" layer="21"/>
<wire x1="-1.905" y1="-3.9290625" x2="0" y2="-5.476875" width="0.2" layer="21"/>
<wire x1="0" y1="-5.476875" x2="1.905" y2="-3.9290625" width="0.2" layer="21"/>
<wire x1="-1.905" y1="-3.33375" x2="-1.905" y2="-3.9290625" width="0.2" layer="21"/>
<wire x1="1.905" y1="-3.33375" x2="1.905" y2="-3.9290625" width="0.2" layer="21"/>
<circle x="0" y="-5.476875" radius="0.238125" width="0.3" layer="21"/>
<wire x1="5.715" y1="-3.9290625" x2="7.62" y2="-5.476875" width="0.2" layer="21"/>
<wire x1="7.62" y1="-5.476875" x2="9.525" y2="-3.9290625" width="0.2" layer="21"/>
<wire x1="5.715" y1="-3.33375" x2="5.715" y2="-3.9290625" width="0.2" layer="21"/>
<wire x1="9.525" y1="-3.33375" x2="9.525" y2="-3.9290625" width="0.2" layer="21"/>
<circle x="7.62" y="-5.476875" radius="0.238125" width="0.3" layer="21"/>
<wire x1="13.335" y1="-3.9290625" x2="15.24" y2="-5.476875" width="0.2" layer="21"/>
<wire x1="15.24" y1="-5.476875" x2="17.145" y2="-3.9290625" width="0.2" layer="21"/>
<wire x1="13.335" y1="-3.33375" x2="13.335" y2="-3.9290625" width="0.2" layer="21"/>
<wire x1="17.145" y1="-3.33375" x2="17.145" y2="-3.9290625" width="0.2" layer="21"/>
<circle x="15.24" y="-5.476875" radius="0.238125" width="0.3" layer="21"/>
<wire x1="17.145" y1="5.1196875" x2="15.24" y2="6.6675" width="0.2" layer="21"/>
<wire x1="15.24" y1="6.6675" x2="13.335" y2="5.1196875" width="0.2" layer="21"/>
<wire x1="17.145" y1="4.524375" x2="17.145" y2="5.1196875" width="0.2" layer="21"/>
<wire x1="13.335" y1="4.524375" x2="13.335" y2="5.1196875" width="0.2" layer="21"/>
<circle x="15.24" y="6.6675" radius="0.238125" width="0.3" layer="21"/>
<wire x1="9.525" y1="5.1196875" x2="7.62" y2="6.6675" width="0.2" layer="21"/>
<wire x1="7.62" y1="6.6675" x2="5.715" y2="5.1196875" width="0.2" layer="21"/>
<wire x1="9.525" y1="4.524375" x2="9.525" y2="5.1196875" width="0.2" layer="21"/>
<wire x1="5.715" y1="4.524375" x2="5.715" y2="5.1196875" width="0.2" layer="21"/>
<circle x="7.62" y="6.6675" radius="0.238125" width="0.3" layer="21"/>
<wire x1="1.905" y1="5.1196875" x2="0" y2="6.6675" width="0.2" layer="21"/>
<wire x1="0" y1="6.6675" x2="-1.905" y2="5.1196875" width="0.2" layer="21"/>
<wire x1="1.905" y1="4.524375" x2="1.905" y2="5.1196875" width="0.2" layer="21"/>
<wire x1="-1.905" y1="4.524375" x2="-1.905" y2="5.1196875" width="0.2" layer="21"/>
<circle x="0" y="6.6675" radius="0.238125" width="0.3" layer="21"/>
<wire x1="-5.715" y1="5.1196875" x2="-7.62" y2="6.6675" width="0.2" layer="21"/>
<wire x1="-7.62" y1="6.6675" x2="-9.525" y2="5.1196875" width="0.2" layer="21"/>
<wire x1="-5.715" y1="4.524375" x2="-5.715" y2="5.1196875" width="0.2" layer="21"/>
<wire x1="-9.525" y1="4.524375" x2="-9.525" y2="5.1196875" width="0.2" layer="21"/>
<circle x="-7.62" y="6.6675" radius="0.238125" width="0.3" layer="21"/>
<wire x1="-13.335" y1="5.1196875" x2="-15.24" y2="6.6675" width="0.2" layer="21"/>
<wire x1="-15.24" y1="6.6675" x2="-17.145" y2="5.1196875" width="0.2" layer="21"/>
<wire x1="-13.335" y1="4.524375" x2="-13.335" y2="5.1196875" width="0.2" layer="21"/>
<wire x1="-17.145" y1="4.524375" x2="-17.145" y2="5.1196875" width="0.2" layer="21"/>
<circle x="-15.24" y="6.6675" radius="0.238125" width="0.3" layer="21"/>
<text x="0" y="2.8575" size="0.4064" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="1.905" size="0.4064" layer="27" align="bottom-center">&gt;VALUE</text>
<wire x1="-19.5" y1="-7" x2="19.5" y2="-7" width="0.2" layer="51"/>
<wire x1="19.5" y1="-7" x2="19.5" y2="5" width="0.2" layer="51"/>
<wire x1="19.5" y1="5" x2="-19.5" y2="5" width="0.2" layer="51"/>
<wire x1="-19.5" y1="5" x2="-19.5" y2="-7" width="0.2" layer="51"/>
<circle x="-21.55" y="0" radius="1.50083125" width="2" layer="40"/>
<circle x="21.55" y="0" radius="1.50083125" width="2" layer="40"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.423" y1="0.65" x2="1.423" y2="0.65" width="0.0508" layer="39"/>
<wire x1="1.423" y1="0.65" x2="1.423" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="1.423" y1="-0.65" x2="-1.423" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="-1.423" y1="-0.65" x2="-1.423" y2="0.65" width="0.0508" layer="39"/>
<smd name="1" x="-0.8" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.8" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="0" y="0.835" size="1.2" layer="25" font="vector" ratio="12" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.055" size="1.2" layer="27" font="vector" ratio="12" align="bottom-center">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-1.35" y1="0.7" x2="1.4" y2="0.7" width="0.15" layer="51"/>
<wire x1="1.4" y1="0.7" x2="1.4" y2="-0.7" width="0.15" layer="51"/>
<wire x1="1.4" y1="-0.7" x2="-1.4" y2="-0.7" width="0.15" layer="51"/>
<wire x1="-1.4" y1="-0.7" x2="-1.4" y2="0.7" width="0.15" layer="51"/>
</package>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-0.9206" y1="0.4576" x2="0.9206" y2="0.4576" width="0.0508" layer="39"/>
<wire x1="0.9206" y1="0.4576" x2="0.9206" y2="-0.4576" width="0.0508" layer="39"/>
<wire x1="0.9206" y1="-0.4576" x2="-0.9206" y2="-0.4576" width="0.0508" layer="39"/>
<wire x1="-0.9206" y1="-0.4576" x2="-0.9206" y2="0.4576" width="0.0508" layer="39"/>
<smd name="1" x="-0.5" y="0" dx="0.5" dy="0.6" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.5" dy="0.6" layer="1"/>
<text x="0" y="0.735" size="1.2" layer="25" font="vector" ratio="12" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.905" size="1.2" layer="27" font="vector" ratio="12" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.12" y1="-0.35" x2="0.12" y2="0.35" layer="35"/>
<wire x1="-0.9492" y1="0.4992" x2="0.9492" y2="0.4992" width="0.15" layer="51"/>
<wire x1="0.9492" y1="0.4992" x2="0.9492" y2="-0.4992" width="0.15" layer="51"/>
<wire x1="0.9492" y1="-0.4992" x2="-0.9492" y2="-0.4992" width="0.15" layer="51"/>
<wire x1="-0.9492" y1="-0.4992" x2="-0.9492" y2="0.4992" width="0.15" layer="51"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="0" y="1.27" size="0.254" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="0.254" layer="27" font="vector" ratio="15" rot="R180" align="bottom-center">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C0603">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.1444" y1="0.5992" x2="1.1444" y2="0.5992" width="0.0508" layer="39"/>
<wire x1="1.1444" y1="0.5992" x2="1.1444" y2="-0.5992" width="0.0508" layer="39"/>
<wire x1="1.1444" y1="-0.5992" x2="-1.1444" y2="-0.5992" width="0.0508" layer="39"/>
<wire x1="-1.1444" y1="-0.5992" x2="-1.1444" y2="0.5992" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="0.6" dy="0.9" layer="1"/>
<smd name="2" x="0.7" y="0" dx="0.6" dy="0.9" layer="1"/>
<text x="0" y="0.762" size="0.254" layer="25" font="vector" ratio="12" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.254" layer="27" font="vector" ratio="12" rot="R180" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-1.1968" y1="0.6492" x2="1.1968" y2="0.6492" width="0.15" layer="51"/>
<wire x1="1.1968" y1="0.6492" x2="1.1968" y2="-0.6492" width="0.15" layer="51"/>
<wire x1="1.1968" y1="-0.6492" x2="-1.1968" y2="-0.6492" width="0.15" layer="51"/>
<wire x1="-1.1968" y1="-0.6492" x2="-1.1968" y2="0.6492" width="0.15" layer="51"/>
</package>
<package name="C0402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-0.9071" y1="0.4068" x2="0.9071" y2="0.4068" width="0.0508" layer="39"/>
<wire x1="0.9071" y1="0.4068" x2="0.9071" y2="-0.4068" width="0.0508" layer="39"/>
<wire x1="0.9071" y1="-0.4068" x2="-0.9071" y2="-0.4068" width="0.0508" layer="39"/>
<wire x1="-0.9071" y1="-0.4068" x2="-0.9071" y2="0.4068" width="0.0508" layer="39"/>
<smd name="1" x="-0.508" y="0" dx="0.508" dy="0.508" layer="1"/>
<smd name="2" x="0.508" y="0" dx="0.508" dy="0.508" layer="1"/>
<text x="0" y="0.662" size="0.254" layer="25" font="vector" ratio="12" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.608" size="0.254" layer="27" font="vector" ratio="12" rot="R180" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1524" y1="-0.2794" x2="0.1524" y2="0.2794" layer="35"/>
<wire x1="-0.9398" y1="0.4318" x2="0.9398" y2="0.4318" width="0.127" layer="51"/>
<wire x1="0.9398" y1="0.4318" x2="0.9398" y2="-0.4318" width="0.127" layer="51"/>
<wire x1="0.9398" y1="-0.4318" x2="-0.9398" y2="-0.4318" width="0.127" layer="51"/>
<wire x1="-0.9398" y1="-0.4318" x2="-0.9398" y2="0.4318" width="0.127" layer="51"/>
</package>
<package name="C1812">
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="0" y="0.635" size="0.254" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.635" size="0.254" layer="27" rot="R180" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="C0805">
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="0.02" y="1.07" size="0.254" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="-0.01" y="-1.04" size="0.254" layer="27" font="vector" ratio="15" rot="R180" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="0" y="1.27" size="0.254" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="0.254" layer="27" rot="R180" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1210H">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="0" y="1.905" size="1.2" layer="25" font="vector" ratio="12" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.175" size="1.2" layer="27" font="vector" ratio="12" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-2.54" y1="1.651" x2="2.54" y2="1.651" width="0.15" layer="51"/>
<wire x1="2.54" y1="1.651" x2="2.54" y2="-1.651" width="0.15" layer="51"/>
<wire x1="2.54" y1="-1.651" x2="-2.54" y2="-1.651" width="0.15" layer="51"/>
<wire x1="-2.54" y1="-1.651" x2="-2.54" y2="1.651" width="0.15" layer="51"/>
</package>
<package name="BATTERY_HOLDER_AA">
<pad name="M1" x="-24" y="0" drill="1.8" diameter="4"/>
<pad name="M2" x="-17" y="0" drill="1.8" diameter="4"/>
<pad name="P2" x="24" y="0" drill="1.8" diameter="4"/>
<pad name="P1" x="17" y="0" drill="1.8" diameter="4"/>
<wire x1="-25.75" y1="-7.25" x2="24.75" y2="-7.25" width="0.1" layer="51"/>
<wire x1="24.75" y1="-7.25" x2="24.75" y2="7.25" width="0.1" layer="51"/>
<wire x1="24.75" y1="7.25" x2="-25.75" y2="7.25" width="0.1" layer="51"/>
<wire x1="-25.75" y1="7.25" x2="-25.75" y2="-7.25" width="0.1" layer="51"/>
<rectangle x1="-26.25" y1="-5.25" x2="-25.75" y2="5.25" layer="51"/>
<rectangle x1="24.75" y1="-2" x2="26.25" y2="2" layer="51"/>
<wire x1="-27" y1="8" x2="-27" y2="-8" width="0.1" layer="21"/>
<wire x1="-27" y1="-8" x2="27" y2="-8" width="0.1" layer="21"/>
<wire x1="27" y1="-8" x2="27" y2="8" width="0.1" layer="21"/>
<wire x1="27" y1="8" x2="-27" y2="8" width="0.1" layer="21"/>
<text x="11" y="2" size="5" layer="21">+</text>
<text x="11" y="-6.5" size="5" layer="21">+</text>
<text x="-14.5" y="-6" size="5" layer="21">-</text>
<text x="-14.5" y="2.5" size="5" layer="21">-</text>
<rectangle x1="-25.5" y1="6.5" x2="-16" y2="8" layer="51"/>
<rectangle x1="-25.5" y1="-8" x2="-16" y2="-6.5" layer="51"/>
<rectangle x1="15" y1="-8" x2="24.5" y2="-6.5" layer="51"/>
<rectangle x1="15" y1="6.5" x2="24.5" y2="8" layer="51"/>
<text x="0" y="1.27" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="1.27" layer="27" rot="R180" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-27" y1="-4.5" x2="-26.5" y2="5" layer="51"/>
<rectangle x1="26.5" y1="-5" x2="27" y2="4.5" layer="51"/>
</package>
<package name="SOD123">
<description>&lt;b&gt;Diode&lt;/b&gt;</description>
<wire x1="-1.1" y1="0.7" x2="1.1" y2="0.7" width="0.254" layer="51"/>
<wire x1="1.1" y1="0.7" x2="1.1" y2="-0.7" width="0.254" layer="51"/>
<wire x1="1.1" y1="-0.7" x2="-1.1" y2="-0.7" width="0.254" layer="51"/>
<wire x1="-1.1" y1="-0.7" x2="-1.1" y2="0.7" width="0.254" layer="51"/>
<smd name="C" x="-1.85" y="0" dx="1.2" dy="0.7" layer="1"/>
<smd name="A" x="1.85" y="0" dx="1.2" dy="0.7" layer="1"/>
<text x="-1.1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.1" y="-2.3" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.95" y1="-0.45" x2="-1.2" y2="0.4" layer="51"/>
<rectangle x1="1.2" y1="-0.45" x2="1.95" y2="0.4" layer="51"/>
<rectangle x1="-0.8" y1="-0.65" x2="-0.55" y2="0.65" layer="51"/>
</package>
<package name="FIDUCIAL_1MM">
<smd name="1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" stop="no" cream="no"/>
<polygon width="0.127" layer="29">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="0" y="1" curve="90"/>
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1"/>
<vertex x="0" y="-0.6" curve="-90"/>
<vertex x="-0.6" y="0" curve="-90"/>
<vertex x="0" y="0.6"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="0" y="1" curve="-90"/>
<vertex x="1" y="0" curve="-90"/>
<vertex x="0" y="-1"/>
<vertex x="0" y="-0.6" curve="90"/>
<vertex x="0.6" y="0" curve="90"/>
<vertex x="0" y="0.6"/>
</polygon>
</package>
<package name="LED0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<smd name="A" x="-1.175" y="0" dx="1.25" dy="1.1" layer="1"/>
<smd name="C" x="1.175" y="0" dx="1.25" dy="1.1" layer="1"/>
<text x="0" y="0.77" size="0.254" layer="25" font="vector" ratio="15" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.77" size="0.254" layer="27" font="vector" ratio="15" align="top-center">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<wire x1="0.2" y1="0.25" x2="0.2" y2="0" width="0.127" layer="51"/>
<wire x1="0.2" y1="0" x2="0.2" y2="-0.25" width="0.127" layer="51"/>
<wire x1="0.2" y1="0" x2="-0.2" y2="0.25" width="0.127" layer="51"/>
<wire x1="-0.2" y1="0.25" x2="-0.2" y2="0" width="0.127" layer="51"/>
<wire x1="-0.2" y1="0" x2="-0.2" y2="-0.25" width="0.127" layer="51"/>
<wire x1="-0.2" y1="-0.25" x2="0.2" y2="0" width="0.127" layer="51"/>
<wire x1="-0.5" y1="0" x2="-0.2" y2="0" width="0.127" layer="51"/>
<wire x1="0.2" y1="0" x2="0.5" y2="0" width="0.127" layer="51"/>
</package>
<package name="OSCILLATOR2520">
<smd name="1" x="-0.925" y="-0.725" dx="1.05" dy="0.95" layer="1"/>
<smd name="4" x="-0.925" y="0.725" dx="1.05" dy="0.95" layer="1"/>
<smd name="2" x="0.925" y="-0.725" dx="1.05" dy="0.95" layer="1"/>
<smd name="3" x="0.925" y="0.725" dx="1.05" dy="0.95" layer="1"/>
<wire x1="-1.25" y1="-1" x2="1.25" y2="-1" width="0.1" layer="51"/>
<wire x1="1.25" y1="-1" x2="1.25" y2="1" width="0.1" layer="51"/>
<wire x1="1.25" y1="1" x2="-1.25" y2="1" width="0.1" layer="51"/>
<wire x1="-1.25" y1="1" x2="-1.25" y2="-1" width="0.1" layer="51"/>
<text x="0" y="0.05" size="0.1" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.05" size="0.1" layer="27" align="top-center">&gt;VALUE</text>
<circle x="-0.8" y="-0.6" radius="0.15" width="0.1" layer="51"/>
</package>
<package name="RTC">
<smd name="1" x="-1.6" y="-1.1" dx="0.5" dy="0.8" layer="1" rot="R180"/>
<smd name="2" x="-0.8" y="-1.1" dx="0.5" dy="0.8" layer="1" rot="R180"/>
<smd name="3" x="0" y="-1.1" dx="0.5" dy="0.8" layer="1" rot="R180"/>
<smd name="4" x="0.8" y="-1.1" dx="0.5" dy="0.8" layer="1" rot="R180"/>
<smd name="5" x="1.6" y="-1.1" dx="0.5" dy="0.8" layer="1" rot="R180"/>
<smd name="6" x="1.6" y="1.1" dx="0.5" dy="0.8" layer="1"/>
<smd name="7" x="0.8" y="1.1" dx="0.5" dy="0.8" layer="1"/>
<smd name="8" x="0" y="1.1" dx="0.5" dy="0.8" layer="1"/>
<smd name="9" x="-0.8" y="1.1" dx="0.5" dy="0.8" layer="1"/>
<smd name="10" x="-1.6" y="1.1" dx="0.5" dy="0.8" layer="1"/>
<wire x1="-1.85" y1="-1.25" x2="1.85" y2="-1.25" width="0.1" layer="51"/>
<wire x1="1.85" y1="-1.25" x2="1.85" y2="1.25" width="0.1" layer="51"/>
<wire x1="1.85" y1="1.25" x2="-1.85" y2="1.25" width="0.1" layer="51"/>
<wire x1="-1.85" y1="1.25" x2="-1.85" y2="-1.25" width="0.1" layer="51"/>
<circle x="-1.4" y="-0.7" radius="0.180275" width="0.1" layer="51"/>
<text x="-0.7" y="0.1" size="0.254" layer="25">&gt;NAME</text>
<text x="-0.7" y="-0.3" size="0.254" layer="27">&gt;VALUE</text>
</package>
<package name="SOIC16W">
<description>&lt;b&gt;SMALL OUTLINE INTEGRATED CIRCUIT&lt;/b&gt;&lt;p&gt;
wide body 7.5 mm/JEDEC MS-013AA</description>
<wire x1="5.19" y1="-3.7" x2="-5.19" y2="-3.7" width="0.2032" layer="51"/>
<wire x1="-5.19" y1="-3.7" x2="-5.19" y2="-3.2" width="0.2032" layer="51"/>
<wire x1="-5.19" y1="-3.2" x2="-5.19" y2="3.7" width="0.2032" layer="51"/>
<wire x1="-5.19" y1="3.7" x2="5.19" y2="3.7" width="0.2032" layer="51"/>
<wire x1="5.19" y1="3.7" x2="5.19" y2="-3.2" width="0.2032" layer="51"/>
<wire x1="5.19" y1="-3.2" x2="5.19" y2="-3.7" width="0.2032" layer="51"/>
<smd name="2" x="-3.175" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="13" x="-0.635" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-4.445" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="-1.905" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="-0.635" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="14" x="-1.905" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="12" x="0.635" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="11" x="1.905" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="1.905" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="9" x="4.445" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="0.635" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="3.175" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="10" x="3.175" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="4.445" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="15" x="-3.175" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="16" x="-4.445" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<text x="0" y="0.2" size="0.8128" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.2" size="0.8128" layer="27" align="top-center">&gt;VALUE</text>
<rectangle x1="-4.6901" y1="-5.32" x2="-4.1999" y2="-3.8001" layer="51"/>
<rectangle x1="-3.4201" y1="-5.32" x2="-2.9299" y2="-3.8001" layer="51"/>
<rectangle x1="-2.1501" y1="-5.32" x2="-1.6599" y2="-3.8001" layer="51"/>
<rectangle x1="-0.8801" y1="-5.32" x2="-0.3899" y2="-3.8001" layer="51"/>
<rectangle x1="0.3899" y1="-5.32" x2="0.8801" y2="-3.8001" layer="51"/>
<rectangle x1="1.6599" y1="-5.32" x2="2.1501" y2="-3.8001" layer="51"/>
<rectangle x1="2.9299" y1="-5.32" x2="3.4201" y2="-3.8001" layer="51"/>
<rectangle x1="4.1999" y1="-5.32" x2="4.6901" y2="-3.8001" layer="51"/>
<rectangle x1="4.1999" y1="3.8001" x2="4.6901" y2="5.32" layer="51"/>
<rectangle x1="2.9299" y1="3.8001" x2="3.4201" y2="5.32" layer="51"/>
<rectangle x1="1.6599" y1="3.8001" x2="2.1501" y2="5.32" layer="51"/>
<rectangle x1="0.3899" y1="3.8001" x2="0.8801" y2="5.32" layer="51"/>
<rectangle x1="-0.8801" y1="3.8001" x2="-0.3899" y2="5.32" layer="51"/>
<rectangle x1="-2.1501" y1="3.8001" x2="-1.6599" y2="5.32" layer="51"/>
<rectangle x1="-3.4201" y1="3.8001" x2="-2.9299" y2="5.32" layer="51"/>
<rectangle x1="-4.6901" y1="3.8001" x2="-4.1999" y2="5.32" layer="51"/>
<rectangle x1="-5.1" y1="-3.6" x2="-2.2" y2="0" layer="51"/>
</package>
<package name="SOIC14">
<smd name="1" x="-3.81" y="-2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="2" x="-2.54" y="-2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="-1.27" y="-2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="4" x="0" y="-2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="5" x="1.27" y="-2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="6" x="2.54" y="-2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="7" x="3.81" y="-2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="14" x="-3.81" y="2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="13" x="-2.54" y="2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="12" x="-1.27" y="2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="11" x="0" y="2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="10" x="1.27" y="2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="9" x="2.54" y="2.7" dx="0.6" dy="1.55" layer="1"/>
<smd name="8" x="3.81" y="2.7" dx="0.6" dy="1.55" layer="1"/>
<wire x1="-4.325" y1="-1.95" x2="4.325" y2="-1.95" width="0.1" layer="51"/>
<wire x1="4.325" y1="-1.95" x2="4.325" y2="1.95" width="0.1" layer="51"/>
<wire x1="4.325" y1="1.95" x2="-4.325" y2="1.95" width="0.1" layer="51"/>
<wire x1="-4.325" y1="1.95" x2="-4.325" y2="-1.95" width="0.1" layer="51"/>
<text x="0" y="0.1" size="0.254" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.1" size="0.254" layer="27" rot="R180" align="bottom-center">&gt;VALUE</text>
<rectangle x1="-4.3" y1="-1.9" x2="-2.8" y2="-0.4" layer="51"/>
</package>
<package name="UFQFPN48">
<smd name="EXP" x="0" y="0" dx="5.6" dy="5.6" layer="1" thermals="no" cream="no"/>
<smd name="6" x="-3.375" y="0.25" dx="0.55" dy="0.3" layer="1" stop="no"/>
<rectangle x1="-3.72" y1="0.03" x2="-3.03" y2="0.47" layer="29"/>
<smd name="5" x="-3.375" y="0.75" dx="0.55" dy="0.3" layer="1" stop="no"/>
<rectangle x1="-3.72" y1="0.53" x2="-3.03" y2="0.97" layer="29"/>
<smd name="4" x="-3.375" y="1.25" dx="0.55" dy="0.3" layer="1" stop="no"/>
<rectangle x1="-3.72" y1="1.03" x2="-3.03" y2="1.47" layer="29"/>
<smd name="3" x="-3.375" y="1.75" dx="0.55" dy="0.3" layer="1" stop="no"/>
<rectangle x1="-3.72" y1="1.53" x2="-3.03" y2="1.97" layer="29"/>
<smd name="2" x="-3.375" y="2.25" dx="0.55" dy="0.3" layer="1" stop="no"/>
<rectangle x1="-3.72" y1="2.03" x2="-3.03" y2="2.47" layer="29"/>
<smd name="1" x="-3.375" y="2.75" dx="0.55" dy="0.3" layer="1" stop="no"/>
<rectangle x1="-3.72" y1="2.53" x2="-3.03" y2="2.97" layer="29"/>
<smd name="12" x="-3.375" y="-2.75" dx="0.55" dy="0.3" layer="1" stop="no"/>
<rectangle x1="-3.72" y1="-2.97" x2="-3.03" y2="-2.53" layer="29"/>
<smd name="11" x="-3.375" y="-2.25" dx="0.55" dy="0.3" layer="1" stop="no"/>
<rectangle x1="-3.72" y1="-2.47" x2="-3.03" y2="-2.03" layer="29"/>
<smd name="10" x="-3.375" y="-1.75" dx="0.55" dy="0.3" layer="1" stop="no"/>
<rectangle x1="-3.72" y1="-1.97" x2="-3.03" y2="-1.53" layer="29"/>
<smd name="9" x="-3.375" y="-1.25" dx="0.55" dy="0.3" layer="1" stop="no"/>
<rectangle x1="-3.72" y1="-1.47" x2="-3.03" y2="-1.03" layer="29"/>
<smd name="8" x="-3.375" y="-0.75" dx="0.55" dy="0.3" layer="1" stop="no"/>
<rectangle x1="-3.72" y1="-0.97" x2="-3.03" y2="-0.53" layer="29"/>
<smd name="7" x="-3.375" y="-0.25" dx="0.55" dy="0.3" layer="1" stop="no"/>
<rectangle x1="-3.72" y1="-0.47" x2="-3.03" y2="-0.03" layer="29"/>
<smd name="18" x="-0.25" y="-3.375" dx="0.55" dy="0.3" layer="1" rot="R90" stop="no"/>
<rectangle x1="-0.595" y1="-3.595" x2="0.095" y2="-3.155" layer="29" rot="R90"/>
<smd name="17" x="-0.75" y="-3.375" dx="0.55" dy="0.3" layer="1" rot="R90" stop="no"/>
<rectangle x1="-1.095" y1="-3.595" x2="-0.405" y2="-3.155" layer="29" rot="R90"/>
<smd name="16" x="-1.25" y="-3.375" dx="0.55" dy="0.3" layer="1" rot="R90" stop="no"/>
<rectangle x1="-1.595" y1="-3.595" x2="-0.905" y2="-3.155" layer="29" rot="R90"/>
<smd name="15" x="-1.75" y="-3.375" dx="0.55" dy="0.3" layer="1" rot="R90" stop="no"/>
<rectangle x1="-2.095" y1="-3.595" x2="-1.405" y2="-3.155" layer="29" rot="R90"/>
<smd name="14" x="-2.25" y="-3.375" dx="0.55" dy="0.3" layer="1" rot="R90" stop="no"/>
<rectangle x1="-2.595" y1="-3.595" x2="-1.905" y2="-3.155" layer="29" rot="R90"/>
<smd name="13" x="-2.75" y="-3.375" dx="0.55" dy="0.3" layer="1" rot="R90" stop="no"/>
<rectangle x1="-3.095" y1="-3.595" x2="-2.405" y2="-3.155" layer="29" rot="R90"/>
<smd name="24" x="2.75" y="-3.375" dx="0.55" dy="0.3" layer="1" rot="R90" stop="no"/>
<rectangle x1="2.405" y1="-3.595" x2="3.095" y2="-3.155" layer="29" rot="R90"/>
<smd name="23" x="2.25" y="-3.375" dx="0.55" dy="0.3" layer="1" rot="R90" stop="no"/>
<rectangle x1="1.905" y1="-3.595" x2="2.595" y2="-3.155" layer="29" rot="R90"/>
<smd name="22" x="1.75" y="-3.375" dx="0.55" dy="0.3" layer="1" rot="R90" stop="no"/>
<rectangle x1="1.405" y1="-3.595" x2="2.095" y2="-3.155" layer="29" rot="R90"/>
<smd name="21" x="1.25" y="-3.375" dx="0.55" dy="0.3" layer="1" rot="R90" stop="no"/>
<rectangle x1="0.905" y1="-3.595" x2="1.595" y2="-3.155" layer="29" rot="R90"/>
<smd name="20" x="0.75" y="-3.375" dx="0.55" dy="0.3" layer="1" rot="R90" stop="no"/>
<rectangle x1="0.405" y1="-3.595" x2="1.095" y2="-3.155" layer="29" rot="R90"/>
<smd name="19" x="0.25" y="-3.375" dx="0.55" dy="0.3" layer="1" rot="R90" stop="no"/>
<rectangle x1="-0.095" y1="-3.595" x2="0.595" y2="-3.155" layer="29" rot="R90"/>
<smd name="30" x="3.375" y="-0.25" dx="0.55" dy="0.3" layer="1" rot="R180" stop="no"/>
<rectangle x1="3.03" y1="-0.47" x2="3.72" y2="-0.03" layer="29" rot="R180"/>
<smd name="29" x="3.375" y="-0.75" dx="0.55" dy="0.3" layer="1" rot="R180" stop="no"/>
<rectangle x1="3.03" y1="-0.97" x2="3.72" y2="-0.53" layer="29" rot="R180"/>
<smd name="28" x="3.375" y="-1.25" dx="0.55" dy="0.3" layer="1" rot="R180" stop="no"/>
<rectangle x1="3.03" y1="-1.47" x2="3.72" y2="-1.03" layer="29" rot="R180"/>
<smd name="27" x="3.375" y="-1.75" dx="0.55" dy="0.3" layer="1" rot="R180" stop="no"/>
<rectangle x1="3.03" y1="-1.97" x2="3.72" y2="-1.53" layer="29" rot="R180"/>
<smd name="26" x="3.375" y="-2.25" dx="0.55" dy="0.3" layer="1" rot="R180" stop="no"/>
<rectangle x1="3.03" y1="-2.47" x2="3.72" y2="-2.03" layer="29" rot="R180"/>
<smd name="25" x="3.375" y="-2.75" dx="0.55" dy="0.3" layer="1" rot="R180" stop="no"/>
<rectangle x1="3.03" y1="-2.97" x2="3.72" y2="-2.53" layer="29" rot="R180"/>
<smd name="36" x="3.375" y="2.75" dx="0.55" dy="0.3" layer="1" rot="R180" stop="no"/>
<rectangle x1="3.03" y1="2.53" x2="3.72" y2="2.97" layer="29" rot="R180"/>
<smd name="35" x="3.375" y="2.25" dx="0.55" dy="0.3" layer="1" rot="R180" stop="no"/>
<rectangle x1="3.03" y1="2.03" x2="3.72" y2="2.47" layer="29" rot="R180"/>
<smd name="34" x="3.375" y="1.75" dx="0.55" dy="0.3" layer="1" rot="R180" stop="no"/>
<rectangle x1="3.03" y1="1.53" x2="3.72" y2="1.97" layer="29" rot="R180"/>
<smd name="33" x="3.375" y="1.25" dx="0.55" dy="0.3" layer="1" rot="R180" stop="no"/>
<rectangle x1="3.03" y1="1.03" x2="3.72" y2="1.47" layer="29" rot="R180"/>
<smd name="32" x="3.375" y="0.75" dx="0.55" dy="0.3" layer="1" rot="R180" stop="no"/>
<rectangle x1="3.03" y1="0.53" x2="3.72" y2="0.97" layer="29" rot="R180"/>
<smd name="31" x="3.375" y="0.25" dx="0.55" dy="0.3" layer="1" rot="R180" stop="no"/>
<rectangle x1="3.03" y1="0.03" x2="3.72" y2="0.47" layer="29" rot="R180"/>
<smd name="42" x="0.25" y="3.375" dx="0.55" dy="0.3" layer="1" rot="R270" stop="no"/>
<rectangle x1="-0.095" y1="3.155" x2="0.595" y2="3.595" layer="29" rot="R270"/>
<smd name="41" x="0.75" y="3.375" dx="0.55" dy="0.3" layer="1" rot="R270" stop="no"/>
<rectangle x1="0.405" y1="3.155" x2="1.095" y2="3.595" layer="29" rot="R270"/>
<smd name="40" x="1.25" y="3.375" dx="0.55" dy="0.3" layer="1" rot="R270" stop="no"/>
<rectangle x1="0.905" y1="3.155" x2="1.595" y2="3.595" layer="29" rot="R270"/>
<smd name="39" x="1.75" y="3.375" dx="0.55" dy="0.3" layer="1" rot="R270" stop="no"/>
<rectangle x1="1.405" y1="3.155" x2="2.095" y2="3.595" layer="29" rot="R270"/>
<smd name="38" x="2.25" y="3.375" dx="0.55" dy="0.3" layer="1" rot="R270" stop="no"/>
<rectangle x1="1.905" y1="3.155" x2="2.595" y2="3.595" layer="29" rot="R270"/>
<smd name="37" x="2.75" y="3.375" dx="0.55" dy="0.3" layer="1" rot="R270" stop="no"/>
<rectangle x1="2.405" y1="3.155" x2="3.095" y2="3.595" layer="29" rot="R270"/>
<smd name="48" x="-2.75" y="3.375" dx="0.55" dy="0.3" layer="1" rot="R270" stop="no"/>
<rectangle x1="-3.095" y1="3.155" x2="-2.405" y2="3.595" layer="29" rot="R270"/>
<smd name="47" x="-2.25" y="3.375" dx="0.55" dy="0.3" layer="1" rot="R270" stop="no"/>
<rectangle x1="-2.595" y1="3.155" x2="-1.905" y2="3.595" layer="29" rot="R270"/>
<smd name="46" x="-1.75" y="3.375" dx="0.55" dy="0.3" layer="1" rot="R270" stop="no"/>
<rectangle x1="-2.095" y1="3.155" x2="-1.405" y2="3.595" layer="29" rot="R270"/>
<smd name="45" x="-1.25" y="3.375" dx="0.55" dy="0.3" layer="1" rot="R270" stop="no"/>
<rectangle x1="-1.595" y1="3.155" x2="-0.905" y2="3.595" layer="29" rot="R270"/>
<smd name="44" x="-0.75" y="3.375" dx="0.55" dy="0.3" layer="1" rot="R270" stop="no"/>
<rectangle x1="-1.095" y1="3.155" x2="-0.405" y2="3.595" layer="29" rot="R270"/>
<smd name="43" x="-0.25" y="3.375" dx="0.55" dy="0.3" layer="1" rot="R270" stop="no"/>
<rectangle x1="-0.595" y1="3.155" x2="0.095" y2="3.595" layer="29" rot="R270"/>
<rectangle x1="-2.4" y1="0.4" x2="-0.6" y2="2.4" layer="31"/>
<rectangle x1="0.6" y1="0.4" x2="2.4" y2="2.4" layer="31"/>
<rectangle x1="-2.4" y1="-2.4" x2="-0.6" y2="-0.4" layer="31"/>
<rectangle x1="0.6" y1="-2.4" x2="2.4" y2="-0.4" layer="31"/>
<wire x1="-3.5" y1="3.5" x2="-3.5" y2="-3.5" width="0.1" layer="51"/>
<wire x1="-3.5" y1="-3.5" x2="3.5" y2="-3.5" width="0.1" layer="51"/>
<wire x1="3.5" y1="-3.5" x2="3.5" y2="3.5" width="0.1" layer="51"/>
<wire x1="3.5" y1="3.5" x2="-3.5" y2="3.5" width="0.1" layer="51"/>
<text x="0" y="0.2" size="0.4064" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.2" size="0.4064" layer="27" align="top-center">&gt;VALUE</text>
<rectangle x1="-3.5" y1="2.4" x2="-2.4" y2="3.5" layer="51"/>
</package>
<package name="SWITCH">
<pad name="2" x="-7" y="-3.4" drill="1.5"/>
<pad name="4" x="0" y="-3.4" drill="1.5"/>
<pad name="6" x="7" y="-3.4" drill="1.5"/>
<pad name="1" x="-7" y="3.4" drill="1.5"/>
<pad name="3" x="0" y="3.4" drill="1.5"/>
<pad name="5" x="7" y="3.4" drill="1.5"/>
<wire x1="-11.5" y1="-7.5" x2="-11.5" y2="7.5" width="0.1" layer="51"/>
<wire x1="-11.5" y1="7.5" x2="11.5" y2="7.5" width="0.1" layer="51"/>
<wire x1="11.5" y1="7.5" x2="11.5" y2="-7.5" width="0.1" layer="51"/>
<wire x1="11.5" y1="-7.5" x2="-11.5" y2="-7.5" width="0.1" layer="51"/>
<rectangle x1="-11.5" y1="-7.5" x2="-8" y2="7.5" layer="51"/>
<rectangle x1="8" y1="-7.5" x2="11.5" y2="7.5" layer="51"/>
<rectangle x1="-8.5" y1="-7.5" x2="8.5" y2="-4" layer="51"/>
<rectangle x1="-8.5" y1="4" x2="8.5" y2="7.5" layer="51"/>
<rectangle x1="0.5" y1="-3.5" x2="7.5" y2="3.5" layer="51"/>
<text x="-5.5" y="0.5" size="0.6096" layer="25">&gt;NAME</text>
<text x="-5.5" y="-1.5" size="0.6096" layer="27">&gt;VALUE</text>
<wire x1="-11.4" y1="-7.4" x2="-11.4" y2="7.4" width="0.3" layer="21"/>
<wire x1="-11.4" y1="7.4" x2="11.4" y2="7.4" width="0.3" layer="21"/>
<wire x1="11.4" y1="7.4" x2="11.4" y2="-7.4" width="0.3" layer="21"/>
<wire x1="11.4" y1="-7.4" x2="-11.4" y2="-7.4" width="0.3" layer="21"/>
</package>
<package name="B1,27">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<wire x1="-0.635" y1="0" x2="0.635" y2="0" width="0.0024" layer="37"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.0024" layer="37"/>
<smd name="TP" x="0" y="0" dx="1.27" dy="1.27" layer="1" roundness="100" cream="no"/>
<text x="0" y="1.016" size="0.254" layer="25" ratio="10" align="center">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.27" size="0.254" layer="37" align="center">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="PROGHEADER">
<smd name="2" x="-3.81" y="2" dx="3" dy="0.6" layer="1" rot="R90"/>
<smd name="1" x="-3.81" y="-2" dx="3" dy="0.6" layer="1" rot="R90"/>
<smd name="4" x="-2.54" y="2" dx="3" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="-2.54" y="-2" dx="3" dy="0.6" layer="1" rot="R90"/>
<smd name="6" x="-1.27" y="2" dx="3" dy="0.6" layer="1" rot="R90"/>
<smd name="5" x="-1.27" y="-2" dx="3" dy="0.6" layer="1" rot="R90"/>
<smd name="8" x="0" y="2" dx="3" dy="0.6" layer="1" rot="R90"/>
<smd name="7" x="0" y="-2" dx="3" dy="0.6" layer="1" rot="R90"/>
<smd name="10" x="1.27" y="2" dx="3" dy="0.6" layer="1" rot="R90"/>
<smd name="9" x="1.27" y="-2" dx="3" dy="0.6" layer="1" rot="R90"/>
<smd name="12" x="2.54" y="2" dx="3" dy="0.6" layer="1" rot="R90"/>
<smd name="11" x="2.54" y="-2" dx="3" dy="0.6" layer="1" rot="R90"/>
<smd name="14" x="3.81" y="2" dx="3" dy="0.6" layer="1" rot="R90"/>
<smd name="13" x="3.81" y="-2" dx="3" dy="0.6" layer="1" rot="R90"/>
<wire x1="-4.5" y1="1.8" x2="-4.5" y2="-1.8" width="0.1" layer="21"/>
<wire x1="-4.5" y1="-1.8" x2="4.5" y2="-1.8" width="0.1" layer="51"/>
<wire x1="4.5" y1="-1.8" x2="4.5" y2="1.8" width="0.1" layer="21"/>
<wire x1="4.5" y1="1.8" x2="-4.5" y2="1.8" width="0.1" layer="51"/>
<rectangle x1="-2.5" y1="1.8" x2="2.6" y2="2.5" layer="51"/>
<rectangle x1="-2.5" y1="-2.5" x2="-1" y2="-1.8" layer="51"/>
<rectangle x1="1" y1="-2.5" x2="2.5" y2="-1.8" layer="51"/>
<text x="0" y="0.1" size="0.254" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.1" size="0.254" layer="27" rot="R180" align="bottom-center">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="ADS114S08">
<pin name="AINCOM" x="25.4" y="-20.32" length="short" rot="R180"/>
<pin name="AIN0" x="25.4" y="17.78" length="short" rot="R180"/>
<pin name="AIN1" x="25.4" y="15.24" length="short" rot="R180"/>
<pin name="AIN2" x="25.4" y="12.7" length="short" rot="R180"/>
<pin name="AIN3" x="25.4" y="10.16" length="short" rot="R180"/>
<pin name="AIN4" x="25.4" y="7.62" length="short" rot="R180"/>
<pin name="AIN5" x="25.4" y="5.08" length="short" rot="R180"/>
<pin name="AIN6" x="25.4" y="2.54" length="short" rot="R180"/>
<pin name="AIN7" x="25.4" y="0" length="short" rot="R180"/>
<pin name="AIN8" x="25.4" y="-2.54" length="short" rot="R180"/>
<pin name="AIN9" x="25.4" y="-5.08" length="short" rot="R180"/>
<pin name="AIN10" x="25.4" y="-7.62" length="short" rot="R180"/>
<pin name="AIN11" x="25.4" y="-10.16" length="short" rot="R180"/>
<pin name="AVSS" x="10.16" y="-25.4" length="short" rot="R90"/>
<pin name="DGND" x="-12.7" y="-25.4" length="short" rot="R90"/>
<pin name="AVDD" x="5.08" y="22.86" length="short" rot="R270"/>
<pin name="AVSS-SW" x="7.62" y="-25.4" length="short" rot="R90"/>
<pin name="REFN0" x="-2.54" y="-25.4" length="short" rot="R90"/>
<pin name="REFP0" x="-5.08" y="-25.4" length="short" rot="R90"/>
<pin name="REFCOM" x="5.08" y="-25.4" length="short" rot="R90"/>
<pin name="REFOUT" x="12.7" y="22.86" length="short" rot="R270"/>
<pin name="DVDD" x="-12.7" y="22.86" length="short" rot="R270"/>
<pin name="IOVDD" x="-10.16" y="22.86" length="short" rot="R270"/>
<pin name="START/SYNC" x="-25.4" y="7.62" length="short"/>
<pin name="/RESET" x="-25.4" y="5.08" length="short"/>
<pin name="/CS" x="-25.4" y="-5.08" length="short"/>
<pin name="SCLK" x="-25.4" y="-7.62" length="short"/>
<pin name="DIN" x="-25.4" y="-12.7" length="short"/>
<pin name="DOUT//DRDY" x="-25.4" y="-10.16" length="short"/>
<pin name="/DRDY" x="-25.4" y="2.54" length="short"/>
<pin name="CLK" x="-25.4" y="-20.32" length="short"/>
<wire x1="-22.86" y1="-22.86" x2="22.86" y2="-22.86" width="0.254" layer="94"/>
<wire x1="22.86" y1="-22.86" x2="22.86" y2="20.32" width="0.254" layer="94"/>
<wire x1="22.86" y1="20.32" x2="-22.86" y2="20.32" width="0.254" layer="94"/>
<wire x1="-22.86" y1="20.32" x2="-22.86" y2="-22.86" width="0.254" layer="94"/>
<text x="-5.08" y="0" size="1.9304" layer="95">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.9304" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="THERMOCOUPLES_CONNECTOR">
<wire x1="-6.35" y1="-15.24" x2="1.27" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-15.24" x2="1.27" y2="12.7" width="0.4064" layer="94"/>
<wire x1="1.27" y1="12.7" x2="-6.35" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="12.7" x2="-6.35" y2="-15.24" width="0.4064" layer="94"/>
<text x="-6.35" y="13.335" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="7" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="9" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
<wire x1="2.54" y1="10.16" x2="3.81" y2="10.16" width="0.254" layer="94"/>
<wire x1="3.81" y1="10.16" x2="5.08" y2="8.89" width="0.254" layer="94"/>
<wire x1="5.08" y1="8.89" x2="3.81" y2="7.62" width="0.254" layer="94"/>
<wire x1="3.81" y1="7.62" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<circle x="5.08" y="8.89" radius="0.254" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="3.81" y2="5.08" width="0.254" layer="94"/>
<wire x1="3.81" y1="5.08" x2="5.08" y2="3.81" width="0.254" layer="94"/>
<wire x1="5.08" y1="3.81" x2="3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="3.81" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<circle x="5.08" y="3.81" radius="0.254" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="3.81" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="0" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="3.81" y2="-2.54" width="0.254" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<circle x="5.08" y="-1.27" radius="0.254" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="3.81" y2="-5.08" width="0.254" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="5.08" y2="-6.35" width="0.254" layer="94"/>
<wire x1="5.08" y1="-6.35" x2="3.81" y2="-7.62" width="0.254" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<circle x="5.08" y="-6.35" radius="0.254" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="3.81" y2="-10.16" width="0.254" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="5.08" y2="-11.43" width="0.254" layer="94"/>
<wire x1="5.08" y1="-11.43" x2="3.81" y2="-12.7" width="0.254" layer="94"/>
<wire x1="3.81" y1="-12.7" x2="2.54" y2="-12.7" width="0.254" layer="94"/>
<circle x="5.08" y="-11.43" radius="0.254" width="0.254" layer="94"/>
</symbol>
<symbol name="R-EU">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="0" y="1.27" size="1.27" layer="95" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.54" size="1.27" layer="96" align="bottom-center">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="C-EU">
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0.508" y2="0" width="0.1524" layer="94"/>
<text x="0" y="1.905" size="1.27" layer="95" align="top-right">&gt;NAME</text>
<text x="0" y="-1.905" size="1.27" layer="96" align="bottom-right">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="3.81" y2="0.254" layer="94" rot="R90"/>
<rectangle x1="-1.27" y1="-0.254" x2="2.794" y2="0.254" layer="94" rot="R90"/>
<pin name="1" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="Z1000">
<pin name="1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<text x="0" y="1.778" size="1.27" layer="95" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.778" size="1.27" layer="96" rot="R180" align="bottom-center">&gt;VALUE</text>
</symbol>
<symbol name="BATTERY">
<wire x1="-0.635" y1="-0.635" x2="0" y2="-0.635" width="0.4064" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="-0.635" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="0.635" x2="0" y2="0.635" width="0.4064" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="2.54" y2="0.635" width="0.4064" layer="94"/>
<text x="3.81" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="3.81" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<text x="-2.54" y="1.016" size="1.778" layer="94">+</text>
</symbol>
<symbol name="SCHOTTKY">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.016" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.286" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="FID">
<text x="0" y="3.81" size="1.27" layer="94" align="center">&gt;NAME</text>
<circle x="0" y="0" radius="1.27" width="0.254" layer="94"/>
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="2.54" y="-0.889" size="1.27" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.921" size="1.27" layer="96">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
<symbol name="OSCILLATOR">
<pin name="INH" x="10.16" y="2.54" length="short" rot="R180"/>
<pin name="GND" x="-10.16" y="-2.54" length="short"/>
<pin name="OUT" x="10.16" y="-2.54" length="short" rot="R180"/>
<pin name="VCC" x="-10.16" y="2.54" length="short"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-7.62" y="5.334" size="1.27" layer="95">&gt;NAME</text>
<text x="-7.62" y="-6.604" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="RTC">
<pin name="CLKOE" x="12.7" y="-7.62" length="short" rot="R180"/>
<pin name="VDD" x="0" y="12.7" visible="pad" length="short" rot="R270"/>
<pin name="CLKOUT" x="12.7" y="-5.08" length="short" rot="R180"/>
<pin name="SCL" x="12.7" y="0" length="short" rot="R180"/>
<pin name="SDA" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="GND" x="0" y="-12.7" visible="pad" length="short" rot="R90"/>
<pin name="/INT" x="12.7" y="7.62" length="short" rot="R180"/>
<pin name="VBACK" x="-12.7" y="7.62" length="short"/>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<text x="-10.16" y="10.668" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="MEMORY">
<pin name="/RESET" x="15.24" y="-5.08" length="short" rot="R180"/>
<pin name="SCK" x="15.24" y="7.62" length="short" rot="R180"/>
<pin name="/CS" x="15.24" y="10.16" length="short" rot="R180"/>
<pin name="SI" x="15.24" y="5.08" length="short" rot="R180"/>
<pin name="SO" x="15.24" y="2.54" length="short" rot="R180"/>
<pin name="/WP" x="15.24" y="-7.62" length="short" rot="R180"/>
<pin name="IO3//RESET" x="15.24" y="-10.16" length="short" rot="R180"/>
<pin name="VDD" x="-12.7" y="10.16" length="short"/>
<pin name="GND" x="-12.7" y="-2.54" length="short"/>
<pin name="NC" x="-12.7" y="-5.08" length="short"/>
<pin name="RFU" x="-12.7" y="-10.16" length="short"/>
<pin name="DNU" x="-12.7" y="-7.62" length="short"/>
<wire x1="-10.16" y1="12.7" x2="-10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="-12.7" x2="12.7" y2="12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="12.7" x2="-10.16" y2="12.7" width="0.254" layer="94"/>
<text x="-10.16" y="13.208" size="1.27" layer="95">&gt;NAME</text>
<text x="-10.16" y="-14.478" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="INV">
<wire x1="-2.54" y1="5.08" x2="5.08" y2="0" width="0.4064" layer="94"/>
<wire x1="5.08" y1="0" x2="-2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.4064" layer="94"/>
<text x="2.54" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="I" x="-5.08" y="0" visible="pad" length="short" direction="in"/>
<pin name="O" x="10.16" y="0" visible="pad" length="middle" direction="out" function="dot" rot="R180"/>
</symbol>
<symbol name="PWRN">
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.905" y="-6.35" size="1.27" layer="95" rot="R90">GND</text>
<text x="1.905" y="2.54" size="1.27" layer="95" rot="R90">VCC</text>
<pin name="GND" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="VCC" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
</symbol>
<symbol name="STM32L451CCU3">
<pin name="VBAT" x="-27.94" y="15.24" length="short"/>
<pin name="PC13" x="-27.94" y="12.7" length="short"/>
<pin name="PC14-OSC32_IN" x="-27.94" y="10.16" length="short"/>
<pin name="PC15-OSC32_OUT" x="-27.94" y="7.62" length="short"/>
<pin name="PH0-OSC_IN" x="-27.94" y="5.08" length="short"/>
<pin name="PH1-OSC_OUT" x="-27.94" y="2.54" length="short"/>
<pin name="NRST" x="-27.94" y="0" length="short"/>
<pin name="VSSA/VREF-" x="-27.94" y="-2.54" length="short"/>
<pin name="VDDA/VREF+" x="-27.94" y="-5.08" length="short"/>
<pin name="PA0" x="-27.94" y="-7.62" length="short"/>
<pin name="PA1" x="-27.94" y="-10.16" length="short"/>
<pin name="PA2" x="-27.94" y="-12.7" length="short"/>
<pin name="PA3" x="-12.7" y="-27.94" length="short" rot="R90"/>
<pin name="PA4" x="-10.16" y="-27.94" length="short" rot="R90"/>
<pin name="PA5" x="-7.62" y="-27.94" length="short" rot="R90"/>
<pin name="PA6" x="-5.08" y="-27.94" length="short" rot="R90"/>
<pin name="PA7" x="-2.54" y="-27.94" length="short" rot="R90"/>
<pin name="PB0" x="0" y="-27.94" length="short" rot="R90"/>
<pin name="PB1" x="2.54" y="-27.94" length="short" rot="R90"/>
<pin name="PB2" x="5.08" y="-27.94" length="short" rot="R90"/>
<pin name="PB10" x="7.62" y="-27.94" length="short" rot="R90"/>
<pin name="PB11" x="10.16" y="-27.94" length="short" rot="R90"/>
<pin name="VSS@1" x="12.7" y="-27.94" length="short" rot="R90"/>
<pin name="VDD@1" x="15.24" y="-27.94" length="short" rot="R90"/>
<pin name="PB12" x="27.94" y="-12.7" length="short" rot="R180"/>
<pin name="PB13" x="27.94" y="-10.16" length="short" rot="R180"/>
<pin name="PB14" x="27.94" y="-7.62" length="short" rot="R180"/>
<pin name="PB15" x="27.94" y="-5.08" length="short" rot="R180"/>
<pin name="PA8" x="27.94" y="-2.54" length="short" rot="R180"/>
<pin name="PA9" x="27.94" y="0" length="short" rot="R180"/>
<pin name="PA10" x="27.94" y="2.54" length="short" rot="R180"/>
<pin name="PA11" x="27.94" y="5.08" length="short" rot="R180"/>
<pin name="PA12" x="27.94" y="7.62" length="short" rot="R180"/>
<pin name="PA13" x="27.94" y="10.16" length="short" rot="R180"/>
<pin name="VSS@2" x="27.94" y="12.7" length="short" rot="R180"/>
<pin name="VDD@2" x="27.94" y="15.24" length="short" rot="R180"/>
<pin name="PA14" x="15.24" y="27.94" length="short" rot="R270"/>
<pin name="PA15" x="12.7" y="27.94" length="short" rot="R270"/>
<pin name="PB3" x="10.16" y="27.94" length="short" rot="R270"/>
<pin name="PB4" x="7.62" y="27.94" length="short" rot="R270"/>
<pin name="PB5" x="5.08" y="27.94" length="short" rot="R270"/>
<pin name="PB6" x="2.54" y="27.94" length="short" rot="R270"/>
<pin name="PB7" x="0" y="27.94" length="short" rot="R270"/>
<pin name="PH3-BOOT0" x="-2.54" y="27.94" length="short" rot="R270"/>
<pin name="PB8" x="-5.08" y="27.94" length="short" rot="R270"/>
<pin name="PB9" x="-7.62" y="27.94" length="short" rot="R270"/>
<pin name="VSS@3" x="-10.16" y="27.94" length="short" rot="R270"/>
<pin name="VDD@3" x="-12.7" y="27.94" length="short" rot="R270"/>
<wire x1="-25.4" y1="25.4" x2="-25.4" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-25.4" y1="-25.4" x2="25.4" y2="-25.4" width="0.254" layer="94"/>
<wire x1="25.4" y1="-25.4" x2="25.4" y2="25.4" width="0.254" layer="94"/>
<wire x1="25.4" y1="25.4" x2="-25.4" y2="25.4" width="0.254" layer="94"/>
<text x="-25.4" y="25.908" size="1.27" layer="95">&gt;NAME</text>
<text x="-25.4" y="-27.178" size="1.27" layer="96">&gt;VALUE</text>
<pin name="EXP" x="22.86" y="-27.94" length="short" rot="R90"/>
</symbol>
<symbol name="SWITCH">
<wire x1="0.381" y1="0" x2="2.032" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-2.54" width="0.254" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="-3.81" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-2.54" x2="-3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.413" x2="-2.54" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="2.413" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.413" x2="2.54" y2="2.032" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.413" x2="2.54" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.413" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.413" x2="-2.54" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.508" x2="-1.27" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.381" x2="-1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.127" x2="-1.27" y2="0.127" width="0.1524" layer="94"/>
<circle x="-2.54" y="1.651" radius="0.381" width="0.254" layer="94"/>
<circle x="0" y="1.651" radius="0.381" width="0.254" layer="94"/>
<circle x="2.54" y="1.651" radius="0.381" width="0.254" layer="94"/>
<circle x="-2.54" y="-1.651" radius="0.381" width="0.254" layer="94"/>
<circle x="0" y="-1.651" radius="0.381" width="0.254" layer="94"/>
<circle x="2.54" y="-1.651" radius="0.381" width="0.254" layer="94"/>
<text x="-4.191" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<rectangle x1="-1.524" y1="-3.048" x2="-1.016" y2="-0.254" layer="94" rot="R270"/>
<rectangle x1="-1.524" y1="0.254" x2="-1.016" y2="3.048" layer="94" rot="R90"/>
<pin name="1" x="-2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="3" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="5" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="2" x="-2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="4" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="6" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<polygon width="0.1524" layer="94">
<vertex x="0.381" y="0"/>
<vertex x="1.016" y="0.254"/>
<vertex x="1.016" y="-0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="2.159" y="0"/>
<vertex x="1.524" y="-0.254"/>
<vertex x="1.524" y="0.254"/>
</polygon>
<wire x1="-1.27" y1="1.143" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-1.143" width="0.1524" layer="94"/>
</symbol>
<symbol name="TP">
<wire x1="-0.762" y1="-0.762" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="-0.762" x2="0" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0" y1="-1.524" x2="-0.762" y2="-0.762" width="0.254" layer="94"/>
<text x="-0.02" y="1.02" size="1" layer="95" align="center">&gt;NAME</text>
<text x="1.27" y="-1.27" size="1" layer="97">&gt;TP_SIGNAL_NAME</text>
<pin name="TP" x="0" y="-2.54" visible="off" length="short" direction="in" rot="R90"/>
</symbol>
<symbol name="PINH2X7">
<wire x1="-6.35" y1="-10.16" x2="8.89" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-10.16" x2="8.89" y2="10.16" width="0.4064" layer="94"/>
<wire x1="8.89" y1="10.16" x2="-6.35" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="10.16" x2="-6.35" y2="-10.16" width="0.4064" layer="94"/>
<text x="-6.35" y="10.795" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="13" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ADS114S08" prefix="IC">
<gates>
<gate name="G$1" symbol="ADS114S08" x="0" y="0"/>
</gates>
<devices>
<device name="" package="VQFN32-5X5">
<connects>
<connect gate="G$1" pin="/CS" pad="9"/>
<connect gate="G$1" pin="/DRDY" pad="13"/>
<connect gate="G$1" pin="/RESET" pad="18"/>
<connect gate="G$1" pin="AIN0" pad="7"/>
<connect gate="G$1" pin="AIN1" pad="6"/>
<connect gate="G$1" pin="AIN10" pad="20"/>
<connect gate="G$1" pin="AIN11" pad="19"/>
<connect gate="G$1" pin="AIN2" pad="5"/>
<connect gate="G$1" pin="AIN3" pad="4"/>
<connect gate="G$1" pin="AIN4" pad="3"/>
<connect gate="G$1" pin="AIN5" pad="2"/>
<connect gate="G$1" pin="AIN6" pad="32"/>
<connect gate="G$1" pin="AIN7" pad="31"/>
<connect gate="G$1" pin="AIN8" pad="22"/>
<connect gate="G$1" pin="AIN9" pad="21"/>
<connect gate="G$1" pin="AINCOM" pad="1"/>
<connect gate="G$1" pin="AVDD" pad="26"/>
<connect gate="G$1" pin="AVSS" pad="25 27 EXP1 EXP2 EXP3 EXP4 EXP5 EXP6"/>
<connect gate="G$1" pin="AVSS-SW" pad="28"/>
<connect gate="G$1" pin="CLK" pad="17"/>
<connect gate="G$1" pin="DGND" pad="14"/>
<connect gate="G$1" pin="DIN" pad="10"/>
<connect gate="G$1" pin="DOUT//DRDY" pad="12"/>
<connect gate="G$1" pin="DVDD" pad="16"/>
<connect gate="G$1" pin="IOVDD" pad="15"/>
<connect gate="G$1" pin="REFCOM" pad="24"/>
<connect gate="G$1" pin="REFN0" pad="29"/>
<connect gate="G$1" pin="REFOUT" pad="23"/>
<connect gate="G$1" pin="REFP0" pad="30"/>
<connect gate="G$1" pin="SCLK" pad="11"/>
<connect gate="G$1" pin="START/SYNC" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="TI" constant="no"/>
<attribute name="MANUFACTURER_PN" value="ADS114S08IRHBT" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="296-47167-1-ND" constant="no"/>
<attribute name="VALUE" value="ADS114S08" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="THERMOCOUPLES_CONNECTOR" prefix="CON">
<gates>
<gate name="G$1" symbol="THERMOCOUPLES_CONNECTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="THERMOCOUPLES_CONNECTOR">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="RS" constant="no"/>
<attribute name="MANUFACTURER_PN" value="874-5281, 173-1618" constant="no"/>
<attribute name="SUPPLIER" value="RS" constant="no"/>
<attribute name="SUPPLIER_OC" value="874-5281, 173-1618" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" prefix="R" uservalue="yes">
<description>SMD Resistors&lt;br&gt;</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C" prefix="C" uservalue="yes">
<description>SMD Capacitors</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1210" package="C1210H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="Z1000" prefix="Z">
<gates>
<gate name="G$1" symbol="Z1000" x="0" y="0"/>
</gates>
<devices>
<device name="Z0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="TDK" constant="no"/>
<attribute name="MANUFACTURER_PN" value="MMZ1608B102CTD25" constant="no"/>
<attribute name="SUPPLIER" value="Farnell" constant="no"/>
<attribute name="SUPPLIER_OC" value="3516859RL" constant="no"/>
<attribute name="VALUE" value="Z1000" constant="no"/>
</technology>
<technology name="Z1000">
<attribute name="IN_STOCK" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BATTERY" prefix="BAT">
<gates>
<gate name="G$1" symbol="BATTERY" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BATTERY_HOLDER_AA">
<connects>
<connect gate="G$1" pin="+" pad="P1 P2"/>
<connect gate="G$1" pin="-" pad="M1 M2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="Keystone" constant="no"/>
<attribute name="MANUFACTURER_PN" value="92" constant="no"/>
<attribute name="SUPPLIER" value="RS" constant="no"/>
<attribute name="SUPPLIER_OC" value="172-6012" constant="no"/>
<attribute name="VALUE" value="AA/3.6V/1.8Ah/130°C" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="D-1N5819HW" prefix="D">
<gates>
<gate name="G$1" symbol="SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOD123">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="No" constant="no"/>
<attribute name="MANUFACTURER" value="DiodesZetex" constant="no"/>
<attribute name="MANUFACTURER_PN" value="1N5819HW-7-F" constant="no"/>
<attribute name="SUPPLIER" value="RS" constant="no"/>
<attribute name="SUPPLIER_OC" value="708-2197" constant="no"/>
<attribute name="VALUE" value="1N5819" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FIDUCIAL" prefix="FID">
<description>For use by pick and place machines&lt;br&gt;
to calibrate the vision/machine, 1mm&lt;br&gt;&lt;br&gt;

Place two in opposite corners of every layer to be placed</description>
<gates>
<gate name="FID$1" symbol="FID" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FIDUCIAL_1MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LED">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="LED0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MANUFACTURER_PN" value="" constant="no"/>
<attribute name="SUPPLIER" value="" constant="no"/>
<attribute name="SUPPLIER_OC" value="" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
<technology name="GREEN">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="Kingbright" constant="no"/>
<attribute name="MANUFACTURER_PN" value="APT2012LZGCK" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="754-1939-1-ND" constant="no"/>
<attribute name="VALUE" value="Green" constant="no"/>
</technology>
<technology name="RED">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="Kingbright" constant="no"/>
<attribute name="MANUFACTURER_PN" value="APTD2012LSURCK" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="754-2044-1-ND" constant="no"/>
<attribute name="VALUE" value="Red" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MC2520Z" prefix="IC">
<gates>
<gate name="G$1" symbol="OSCILLATOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="OSCILLATOR2520">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="INH" pad="1"/>
<connect gate="G$1" pin="OUT" pad="3"/>
<connect gate="G$1" pin="VCC" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="KYOCERA" constant="no"/>
<attribute name="MANUFACTURER_PN" value="MC2520Z4.09600C19XSH" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="1253-MC2520Z4.09600C19XSHCT-ND" constant="no"/>
<attribute name="VALUE" value="4.096MHz/30ppm" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RTC" prefix="RTC">
<gates>
<gate name="G$1" symbol="RTC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RTC">
<connects>
<connect gate="G$1" pin="/INT" pad="7"/>
<connect gate="G$1" pin="CLKOE" pad="1"/>
<connect gate="G$1" pin="CLKOUT" pad="3"/>
<connect gate="G$1" pin="GND" pad="6 8 10"/>
<connect gate="G$1" pin="SCL" pad="4"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="VBACK" pad="9"/>
<connect gate="G$1" pin="VDD" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="MC" constant="no"/>
<attribute name="MANUFACTURER_PN" value="RV-3129-C3" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="2195-RV-3129-C3-32.768KHZ-OPTION-A-TB-QACT-ND" constant="no"/>
<attribute name="VALUE" value="RV-3129-C3/8ppm" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="S25FL256LAGMFN000" prefix="IC">
<gates>
<gate name="G$1" symbol="MEMORY" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC16W">
<connects>
<connect gate="G$1" pin="/CS" pad="7"/>
<connect gate="G$1" pin="/RESET" pad="3"/>
<connect gate="G$1" pin="/WP" pad="9"/>
<connect gate="G$1" pin="DNU" pad="11 12 13"/>
<connect gate="G$1" pin="GND" pad="10"/>
<connect gate="G$1" pin="IO3//RESET" pad="1"/>
<connect gate="G$1" pin="NC" pad="4 5"/>
<connect gate="G$1" pin="RFU" pad="6 14"/>
<connect gate="G$1" pin="SCK" pad="16"/>
<connect gate="G$1" pin="SI" pad="15"/>
<connect gate="G$1" pin="SO" pad="8"/>
<connect gate="G$1" pin="VDD" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="Cypress Semiconductor" constant="no"/>
<attribute name="MANUFACTURER_PN" value="S25FL256LAGMFN000" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="428-3939-ND" constant="no"/>
<attribute name="VALUE" value="Flash-32MB" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SN74AHC04" prefix="IC">
<gates>
<gate name="-1" symbol="INV" x="-22.86" y="17.78"/>
<gate name="-2" symbol="INV" x="10.16" y="17.78"/>
<gate name="-3" symbol="INV" x="-22.86" y="0"/>
<gate name="-4" symbol="INV" x="10.16" y="0"/>
<gate name="-5" symbol="INV" x="-22.86" y="-17.78"/>
<gate name="-6" symbol="INV" x="10.16" y="-17.78"/>
<gate name="-7" symbol="PWRN" x="-22.86" y="38.1"/>
</gates>
<devices>
<device name="" package="SOIC14">
<connects>
<connect gate="-1" pin="I" pad="1"/>
<connect gate="-1" pin="O" pad="2"/>
<connect gate="-2" pin="I" pad="3"/>
<connect gate="-2" pin="O" pad="4"/>
<connect gate="-3" pin="I" pad="5"/>
<connect gate="-3" pin="O" pad="6"/>
<connect gate="-4" pin="I" pad="9"/>
<connect gate="-4" pin="O" pad="8"/>
<connect gate="-5" pin="I" pad="11"/>
<connect gate="-5" pin="O" pad="10"/>
<connect gate="-6" pin="I" pad="13"/>
<connect gate="-6" pin="O" pad="12"/>
<connect gate="-7" pin="GND" pad="7"/>
<connect gate="-7" pin="VCC" pad="14"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="TI" constant="no"/>
<attribute name="MANUFACTURER_PN" value="SN74AHC04DR" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="296-4516-1-ND" constant="no"/>
<attribute name="VALUE" value="SN74AHC04" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STM32L451CCU3" prefix="IC">
<gates>
<gate name="G$1" symbol="STM32L451CCU3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="UFQFPN48">
<connects>
<connect gate="G$1" pin="EXP" pad="EXP"/>
<connect gate="G$1" pin="NRST" pad="7"/>
<connect gate="G$1" pin="PA0" pad="10"/>
<connect gate="G$1" pin="PA1" pad="11"/>
<connect gate="G$1" pin="PA10" pad="31"/>
<connect gate="G$1" pin="PA11" pad="32"/>
<connect gate="G$1" pin="PA12" pad="33"/>
<connect gate="G$1" pin="PA13" pad="34"/>
<connect gate="G$1" pin="PA14" pad="37"/>
<connect gate="G$1" pin="PA15" pad="38"/>
<connect gate="G$1" pin="PA2" pad="12"/>
<connect gate="G$1" pin="PA3" pad="13"/>
<connect gate="G$1" pin="PA4" pad="14"/>
<connect gate="G$1" pin="PA5" pad="15"/>
<connect gate="G$1" pin="PA6" pad="16"/>
<connect gate="G$1" pin="PA7" pad="17"/>
<connect gate="G$1" pin="PA8" pad="29"/>
<connect gate="G$1" pin="PA9" pad="30"/>
<connect gate="G$1" pin="PB0" pad="18"/>
<connect gate="G$1" pin="PB1" pad="19"/>
<connect gate="G$1" pin="PB10" pad="21"/>
<connect gate="G$1" pin="PB11" pad="22"/>
<connect gate="G$1" pin="PB12" pad="25"/>
<connect gate="G$1" pin="PB13" pad="26"/>
<connect gate="G$1" pin="PB14" pad="27"/>
<connect gate="G$1" pin="PB15" pad="28"/>
<connect gate="G$1" pin="PB2" pad="20"/>
<connect gate="G$1" pin="PB3" pad="39"/>
<connect gate="G$1" pin="PB4" pad="40"/>
<connect gate="G$1" pin="PB5" pad="41"/>
<connect gate="G$1" pin="PB6" pad="42"/>
<connect gate="G$1" pin="PB7" pad="43"/>
<connect gate="G$1" pin="PB8" pad="45"/>
<connect gate="G$1" pin="PB9" pad="46"/>
<connect gate="G$1" pin="PC13" pad="2"/>
<connect gate="G$1" pin="PC14-OSC32_IN" pad="3"/>
<connect gate="G$1" pin="PC15-OSC32_OUT" pad="4"/>
<connect gate="G$1" pin="PH0-OSC_IN" pad="5"/>
<connect gate="G$1" pin="PH1-OSC_OUT" pad="6"/>
<connect gate="G$1" pin="PH3-BOOT0" pad="44"/>
<connect gate="G$1" pin="VBAT" pad="1"/>
<connect gate="G$1" pin="VDD@1" pad="24"/>
<connect gate="G$1" pin="VDD@2" pad="36"/>
<connect gate="G$1" pin="VDD@3" pad="48"/>
<connect gate="G$1" pin="VDDA/VREF+" pad="9"/>
<connect gate="G$1" pin="VSS@1" pad="23"/>
<connect gate="G$1" pin="VSS@2" pad="35"/>
<connect gate="G$1" pin="VSS@3" pad="47"/>
<connect gate="G$1" pin="VSSA/VREF-" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="STMicroelectronics" constant="no"/>
<attribute name="MANUFACTURER_PN" value="STM32L451CCU3" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="497-17972-ND" constant="no"/>
<attribute name="VALUE" value="STM32L451CCU3" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SWITCH" prefix="S">
<gates>
<gate name="G$1" symbol="SWITCH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SWITCH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="Everel" constant="no"/>
<attribute name="MANUFACTURER_PN" value="LF24A3000W" constant="no"/>
<attribute name="SUPPLIER" value="RS" constant="no"/>
<attribute name="SUPPLIER_OC" value="863-3548" constant="no"/>
<attribute name="VALUE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TP" prefix="TP">
<gates>
<gate name="G$1" symbol="TP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="B1,27">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<technologies>
<technology name="">
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PROGHEADER" prefix="PROG">
<gates>
<gate name="G$1" symbol="PINH2X7" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PROGHEADER">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="13" pad="13"/>
<connect gate="G$1" pin="14" pad="14"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="IN_STOCK" value="Yes" constant="no"/>
<attribute name="MANUFACTURER" value="Samtec" constant="no"/>
<attribute name="MANUFACTURER_PN" value="FTSH-107-01-L-DV-K" constant="no"/>
<attribute name="SUPPLIER" value="Digikey" constant="no"/>
<attribute name="SUPPLIER_OC" value="SAM9449-ND" constant="no"/>
<attribute name="VALUE" value="1.27mm" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SamacSys_Parts">
<description>&lt;b&gt;https://componentsearchengine.com&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="SOT95P280X145-5N">
<description>&lt;b&gt;SOT-23 DBV 5-1&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.25" y="0.95" dx="1.2" dy="0.6" layer="1"/>
<smd name="2" x="-1.25" y="0" dx="1.2" dy="0.6" layer="1"/>
<smd name="3" x="-1.25" y="-0.95" dx="1.2" dy="0.6" layer="1"/>
<smd name="4" x="1.25" y="-0.95" dx="1.2" dy="0.6" layer="1"/>
<smd name="5" x="1.25" y="0.95" dx="1.2" dy="0.6" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-2.1" y1="1.775" x2="2.1" y2="1.775" width="0.05" layer="51"/>
<wire x1="2.1" y1="1.775" x2="2.1" y2="-1.775" width="0.05" layer="51"/>
<wire x1="2.1" y1="-1.775" x2="-2.1" y2="-1.775" width="0.05" layer="51"/>
<wire x1="-2.1" y1="-1.775" x2="-2.1" y2="1.775" width="0.05" layer="51"/>
<wire x1="-0.8" y1="1.45" x2="0.8" y2="1.45" width="0.1" layer="51"/>
<wire x1="0.8" y1="1.45" x2="0.8" y2="-1.45" width="0.1" layer="51"/>
<wire x1="0.8" y1="-1.45" x2="-0.8" y2="-1.45" width="0.1" layer="51"/>
<wire x1="-0.8" y1="-1.45" x2="-0.8" y2="1.45" width="0.1" layer="51"/>
<wire x1="-0.8" y1="0.5" x2="0.15" y2="1.45" width="0.1" layer="51"/>
<wire x1="-0.3" y1="1.45" x2="0.3" y2="1.45" width="0.2" layer="21"/>
<wire x1="0.3" y1="1.45" x2="0.3" y2="-1.45" width="0.2" layer="21"/>
<wire x1="0.3" y1="-1.45" x2="-0.3" y2="-1.45" width="0.2" layer="21"/>
<wire x1="-0.3" y1="-1.45" x2="-0.3" y2="1.45" width="0.2" layer="21"/>
<wire x1="-1.85" y1="1.5" x2="-0.65" y2="1.5" width="0.2" layer="21"/>
</package>
<package name="TO254P483X1016X1994-3P">
<description>&lt;b&gt;TO-220AB&lt;/b&gt;&lt;br&gt;
</description>
<pad name="1" x="0" y="0" drill="1.38" diameter="2.07" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.38" diameter="2.07"/>
<pad name="3" x="5.08" y="0" drill="1.38" diameter="2.07"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.045" y1="3.475" x2="8.125" y2="3.475" width="0.05" layer="51"/>
<wire x1="8.125" y1="3.475" x2="8.125" y2="-1.855" width="0.05" layer="51"/>
<wire x1="8.125" y1="-1.855" x2="-3.045" y2="-1.855" width="0.05" layer="51"/>
<wire x1="-3.045" y1="-1.855" x2="-3.045" y2="3.475" width="0.05" layer="51"/>
<wire x1="-2.795" y1="3.225" x2="7.875" y2="3.225" width="0.1" layer="51"/>
<wire x1="7.875" y1="3.225" x2="7.875" y2="-1.605" width="0.1" layer="51"/>
<wire x1="7.875" y1="-1.605" x2="-2.795" y2="-1.605" width="0.1" layer="51"/>
<wire x1="-2.795" y1="-1.605" x2="-2.795" y2="3.225" width="0.1" layer="51"/>
<wire x1="-2.795" y1="1.955" x2="-1.525" y2="3.225" width="0.1" layer="51"/>
<wire x1="7.875" y1="-1.605" x2="7.875" y2="3.225" width="0.2" layer="21"/>
<wire x1="7.875" y1="3.225" x2="-2.795" y2="3.225" width="0.2" layer="21"/>
<wire x1="-2.795" y1="3.225" x2="-2.795" y2="0" width="0.2" layer="21"/>
</package>
<package name="15462152">
<description>&lt;b&gt;1546215-2-4&lt;/b&gt;&lt;br&gt;
</description>
<pad name="1" x="0" y="0" drill="1.2" diameter="1.8"/>
<pad name="2" x="2.54" y="0" drill="1.2" diameter="1.8"/>
<text x="1.27" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="1.27" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.5" y1="3.25" x2="4.04" y2="3.25" width="0.2" layer="51"/>
<wire x1="4.04" y1="3.25" x2="4.04" y2="-3.25" width="0.2" layer="51"/>
<wire x1="4.04" y1="-3.25" x2="-1.5" y2="-3.25" width="0.2" layer="51"/>
<wire x1="-1.5" y1="-3.25" x2="-1.5" y2="3.25" width="0.2" layer="51"/>
<wire x1="-1.5" y1="3.25" x2="4.04" y2="3.25" width="0.1" layer="21"/>
<wire x1="4.04" y1="3.25" x2="4.04" y2="-3.25" width="0.1" layer="21"/>
<wire x1="4.04" y1="-3.25" x2="-1.5" y2="-3.25" width="0.1" layer="21"/>
<wire x1="-1.5" y1="-3.25" x2="-1.5" y2="3.25" width="0.1" layer="21"/>
<wire x1="-2.5" y1="4.25" x2="5.04" y2="4.25" width="0.1" layer="51"/>
<wire x1="5.04" y1="4.25" x2="5.04" y2="-4.25" width="0.1" layer="51"/>
<wire x1="5.04" y1="-4.25" x2="-2.5" y2="-4.25" width="0.1" layer="51"/>
<wire x1="-2.5" y1="-4.25" x2="-2.5" y2="4.25" width="0.1" layer="51"/>
</package>
<package name="SOT65P280X110-8N">
<description>&lt;b&gt;DDF0008A&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.25" y="0.975" dx="1.2" dy="0.45" layer="1"/>
<smd name="2" x="-1.25" y="0.325" dx="1.2" dy="0.45" layer="1"/>
<smd name="3" x="-1.25" y="-0.325" dx="1.2" dy="0.45" layer="1"/>
<smd name="4" x="-1.25" y="-0.975" dx="1.2" dy="0.45" layer="1"/>
<smd name="5" x="1.25" y="-0.975" dx="1.2" dy="0.45" layer="1"/>
<smd name="6" x="1.25" y="-0.325" dx="1.2" dy="0.45" layer="1"/>
<smd name="7" x="1.25" y="0.325" dx="1.2" dy="0.45" layer="1"/>
<smd name="8" x="1.25" y="0.975" dx="1.2" dy="0.45" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-2.1" y1="1.725" x2="2.1" y2="1.725" width="0.05" layer="51"/>
<wire x1="2.1" y1="1.725" x2="2.1" y2="-1.725" width="0.05" layer="51"/>
<wire x1="2.1" y1="-1.725" x2="-2.1" y2="-1.725" width="0.05" layer="51"/>
<wire x1="-2.1" y1="-1.725" x2="-2.1" y2="1.725" width="0.05" layer="51"/>
<wire x1="-0.8" y1="1.45" x2="0.8" y2="1.45" width="0.1" layer="51"/>
<wire x1="0.8" y1="1.45" x2="0.8" y2="-1.45" width="0.1" layer="51"/>
<wire x1="0.8" y1="-1.45" x2="-0.8" y2="-1.45" width="0.1" layer="51"/>
<wire x1="-0.8" y1="-1.45" x2="-0.8" y2="1.45" width="0.1" layer="51"/>
<wire x1="-0.8" y1="0.8" x2="-0.15" y2="1.45" width="0.1" layer="51"/>
<wire x1="-0.3" y1="1.45" x2="0.3" y2="1.45" width="0.2" layer="21"/>
<wire x1="0.3" y1="1.45" x2="0.3" y2="-1.45" width="0.2" layer="21"/>
<wire x1="0.3" y1="-1.45" x2="-0.3" y2="-1.45" width="0.2" layer="21"/>
<wire x1="-0.3" y1="-1.45" x2="-0.3" y2="1.45" width="0.2" layer="21"/>
<wire x1="-1.85" y1="1.55" x2="-0.65" y2="1.55" width="0.2" layer="21"/>
</package>
<package name="C2012_COMMERCIAL">
<description>&lt;b&gt;C2012_Commercial&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-0.925" y="0" dx="1.05" dy="1.05" layer="1"/>
<smd name="2" x="0.925" y="0" dx="1.05" dy="1.05" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.2" layer="51"/>
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.2" layer="51"/>
<wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.2" layer="51"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.2" layer="51"/>
<circle x="-2.075" y="0" radius="0.05" width="0.2" layer="25"/>
<wire x1="-3.175" y1="1.825" x2="2.45" y2="1.825" width="0.05" layer="51"/>
<wire x1="2.45" y1="1.825" x2="2.45" y2="-1.825" width="0.05" layer="51"/>
<wire x1="2.45" y1="-1.825" x2="-3.175" y2="-1.825" width="0.05" layer="51"/>
<wire x1="-3.175" y1="-1.825" x2="-3.175" y2="1.825" width="0.05" layer="51"/>
</package>
<package name="CR32NP220KC">
<description>&lt;b&gt;CR32NP-220KC-2&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="1.275" y="1.45" dx="1.1" dy="0.75" layer="1" rot="R90"/>
<smd name="2" x="-1.275" y="1.45" dx="1.1" dy="0.75" layer="1" rot="R90"/>
<smd name="3" x="-1.275" y="-1.45" dx="1.1" dy="0.75" layer="1" rot="R90"/>
<smd name="4" x="1.275" y="-1.45" dx="1.1" dy="0.75" layer="1" rot="R90"/>
<text x="0" y="0.25" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0.25" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.9" y1="1.75" x2="1.9" y2="1.75" width="0.2" layer="51"/>
<wire x1="1.9" y1="1.75" x2="1.9" y2="-1.75" width="0.2" layer="51"/>
<wire x1="1.9" y1="-1.75" x2="-1.9" y2="-1.75" width="0.2" layer="51"/>
<wire x1="-1.9" y1="-1.75" x2="-1.9" y2="1.75" width="0.2" layer="51"/>
<wire x1="-2.9" y1="3.5" x2="2.9" y2="3.5" width="0.1" layer="51"/>
<wire x1="2.9" y1="3.5" x2="2.9" y2="-3" width="0.1" layer="51"/>
<wire x1="2.9" y1="-3" x2="-2.9" y2="-3" width="0.1" layer="51"/>
<wire x1="-2.9" y1="-3" x2="-2.9" y2="3.5" width="0.1" layer="51"/>
<wire x1="1.9" y1="0.9" x2="1.9" y2="-0.9" width="0.1" layer="21"/>
<wire x1="-1.9" y1="0.6" x2="-1.9" y2="-0.9" width="0.1" layer="21"/>
<wire x1="1.5" y1="2.4" x2="1.5" y2="2.4" width="0.3" layer="21"/>
<wire x1="1.5" y1="2.4" x2="1.5" y2="2.5" width="0.3" layer="21" curve="180"/>
<wire x1="1.5" y1="2.5" x2="1.5" y2="2.5" width="0.3" layer="21"/>
<wire x1="1.5" y1="2.5" x2="1.5" y2="2.4" width="0.3" layer="21" curve="180"/>
</package>
<package name="SOT65P210X110-6N">
<description>&lt;b&gt;DCK (R-PDSO-G6)&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-0.95" y="0.65" dx="1.2" dy="0.4" layer="1"/>
<smd name="2" x="-0.95" y="0" dx="1.2" dy="0.4" layer="1"/>
<smd name="3" x="-0.95" y="-0.65" dx="1.2" dy="0.4" layer="1"/>
<smd name="4" x="0.95" y="-0.65" dx="1.2" dy="0.4" layer="1"/>
<smd name="5" x="0.95" y="0" dx="1.2" dy="0.4" layer="1"/>
<smd name="6" x="0.95" y="0.65" dx="1.2" dy="0.4" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.8" y1="1.325" x2="1.8" y2="1.325" width="0.05" layer="51"/>
<wire x1="1.8" y1="1.325" x2="1.8" y2="-1.325" width="0.05" layer="51"/>
<wire x1="1.8" y1="-1.325" x2="-1.8" y2="-1.325" width="0.05" layer="51"/>
<wire x1="-1.8" y1="-1.325" x2="-1.8" y2="1.325" width="0.05" layer="51"/>
<wire x1="-0.625" y1="1" x2="0.625" y2="1" width="0.1" layer="51"/>
<wire x1="0.625" y1="1" x2="0.625" y2="-1" width="0.1" layer="51"/>
<wire x1="0.625" y1="-1" x2="-0.625" y2="-1" width="0.1" layer="51"/>
<wire x1="-0.625" y1="-1" x2="-0.625" y2="1" width="0.1" layer="51"/>
<wire x1="-0.625" y1="0.35" x2="0.025" y2="1" width="0.1" layer="51"/>
<wire x1="-1.55" y1="1.2" x2="-0.35" y2="1.2" width="0.2" layer="21"/>
</package>
<package name="GMR100HTBFA10L0">
<description>&lt;b&gt;GMR100HTBFA10L0-1&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.1" y="0" dx="3.6" dy="3.25" layer="1" rot="R90"/>
<smd name="2" x="2.1" y="0" dx="3.6" dy="3.25" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.2" y1="1.6" x2="3.2" y2="1.6" width="0.2" layer="51"/>
<wire x1="3.2" y1="1.6" x2="3.2" y2="-1.6" width="0.2" layer="51"/>
<wire x1="3.2" y1="-1.6" x2="-3.2" y2="-1.6" width="0.2" layer="51"/>
<wire x1="-3.2" y1="-1.6" x2="-3.2" y2="1.6" width="0.2" layer="51"/>
<wire x1="-4.725" y1="2.8" x2="4.725" y2="2.8" width="0.1" layer="51"/>
<wire x1="4.725" y1="2.8" x2="4.725" y2="-2.8" width="0.1" layer="51"/>
<wire x1="4.725" y1="-2.8" x2="-4.725" y2="-2.8" width="0.1" layer="51"/>
<wire x1="-4.725" y1="-2.8" x2="-4.725" y2="2.8" width="0.1" layer="51"/>
<wire x1="-0.3" y1="1.6" x2="0.3" y2="1.6" width="0.1" layer="21"/>
<wire x1="-0.3" y1="-1.6" x2="0.3" y2="-1.6" width="0.1" layer="21"/>
</package>
<package name="QFN50P400X400X95-25N">
<description>&lt;b&gt;ICM-20689&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.95" y="1.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="2" x="-1.95" y="0.75" dx="0.8" dy="0.3" layer="1"/>
<smd name="3" x="-1.95" y="0.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="4" x="-1.95" y="-0.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="5" x="-1.95" y="-0.75" dx="0.8" dy="0.3" layer="1"/>
<smd name="6" x="-1.95" y="-1.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="7" x="-1.25" y="-1.95" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="8" x="-0.75" y="-1.95" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="9" x="-0.25" y="-1.95" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="10" x="0.25" y="-1.95" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="11" x="0.75" y="-1.95" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="12" x="1.25" y="-1.95" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="13" x="1.95" y="-1.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="14" x="1.95" y="-0.75" dx="0.8" dy="0.3" layer="1"/>
<smd name="15" x="1.95" y="-0.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="16" x="1.95" y="0.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="17" x="1.95" y="0.75" dx="0.8" dy="0.3" layer="1"/>
<smd name="18" x="1.95" y="1.25" dx="0.8" dy="0.3" layer="1"/>
<smd name="19" x="1.25" y="1.95" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="20" x="0.75" y="1.95" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="21" x="0.25" y="1.95" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="22" x="-0.25" y="1.95" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="23" x="-0.75" y="1.95" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="24" x="-1.25" y="1.95" dx="0.8" dy="0.3" layer="1" rot="R90"/>
<smd name="25" x="0" y="0" dx="2.75" dy="2.65" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-2.6" y1="2.6" x2="2.6" y2="2.6" width="0.05" layer="51"/>
<wire x1="2.6" y1="2.6" x2="2.6" y2="-2.6" width="0.05" layer="51"/>
<wire x1="2.6" y1="-2.6" x2="-2.6" y2="-2.6" width="0.05" layer="51"/>
<wire x1="-2.6" y1="-2.6" x2="-2.6" y2="2.6" width="0.05" layer="51"/>
<wire x1="-2" y1="2" x2="2" y2="2" width="0.1" layer="51"/>
<wire x1="2" y1="2" x2="2" y2="-2" width="0.1" layer="51"/>
<wire x1="2" y1="-2" x2="-2" y2="-2" width="0.1" layer="51"/>
<wire x1="-2" y1="-2" x2="-2" y2="2" width="0.1" layer="51"/>
<wire x1="-2" y1="1.5" x2="-1.5" y2="2" width="0.1" layer="51"/>
<circle x="-2.35" y="2" radius="0.125" width="0.25" layer="25"/>
</package>
<package name="PQFN50P400X400X146-24N">
<wire x1="-2.05" y1="2.05" x2="2.05" y2="2.05" width="0.127" layer="51"/>
<wire x1="2.05" y1="2.05" x2="2.05" y2="-2.05" width="0.127" layer="51"/>
<wire x1="2.05" y1="-2.05" x2="-2.05" y2="-2.05" width="0.127" layer="51"/>
<wire x1="-2.05" y1="-2.05" x2="-2.05" y2="2.05" width="0.127" layer="51"/>
<wire x1="-1.61" y1="2.05" x2="-2.05" y2="2.05" width="0.127" layer="21"/>
<wire x1="-2.05" y1="2.05" x2="-2.05" y2="1.61" width="0.127" layer="21"/>
<wire x1="-2.05" y1="-1.61" x2="-2.05" y2="-2.05" width="0.127" layer="21"/>
<wire x1="-2.05" y1="-2.05" x2="-1.61" y2="-2.05" width="0.127" layer="21"/>
<wire x1="1.61" y1="-2.05" x2="2.05" y2="-2.05" width="0.127" layer="21"/>
<wire x1="2.05" y1="-2.05" x2="2.05" y2="-1.61" width="0.127" layer="21"/>
<wire x1="2.05" y1="1.61" x2="2.05" y2="2.05" width="0.127" layer="21"/>
<wire x1="2.05" y1="2.05" x2="1.61" y2="2.05" width="0.127" layer="21"/>
<wire x1="-2.24" y1="2.24" x2="2.24" y2="2.24" width="0.05" layer="39"/>
<wire x1="2.24" y1="2.24" x2="2.24" y2="-2.24" width="0.05" layer="39"/>
<wire x1="2.24" y1="-2.24" x2="-2.24" y2="-2.24" width="0.05" layer="39"/>
<wire x1="-2.24" y1="-2.24" x2="-2.24" y2="2.24" width="0.05" layer="39"/>
<circle x="-3" y="1.5" radius="0.1" width="0.2" layer="21"/>
<text x="-2.512340625" y="2.763590625" size="1.021009375" layer="25">&gt;NAME</text>
<text x="-2.52026875" y="-4.032440625" size="1.024240625" layer="27">&gt;VALUE</text>
<rectangle x1="-1.3" y1="-1.3" x2="1.3" y2="1.3" layer="41"/>
<rectangle x1="-1.3" y1="-1.3" x2="1.3" y2="1.3" layer="43"/>
<smd name="3" x="-1.765" y="0.25" dx="0.45" dy="0.35" layer="1" roundness="25"/>
<smd name="4" x="-1.765" y="-0.25" dx="0.45" dy="0.35" layer="1" roundness="25"/>
<smd name="5" x="-1.765" y="-0.75" dx="0.45" dy="0.35" layer="1" roundness="25"/>
<smd name="6" x="-1.765" y="-1.25" dx="0.45" dy="0.35" layer="1" roundness="25"/>
<smd name="2" x="-1.765" y="0.75" dx="0.45" dy="0.35" layer="1" roundness="25"/>
<smd name="1" x="-1.765" y="1.25" dx="0.45" dy="0.35" layer="1" roundness="25"/>
<smd name="16" x="1.765" y="0.25" dx="0.45" dy="0.35" layer="1" roundness="25"/>
<smd name="17" x="1.765" y="0.75" dx="0.45" dy="0.35" layer="1" roundness="25"/>
<smd name="18" x="1.765" y="1.25" dx="0.45" dy="0.35" layer="1" roundness="25"/>
<smd name="15" x="1.765" y="-0.25" dx="0.45" dy="0.35" layer="1" roundness="25"/>
<smd name="14" x="1.765" y="-0.75" dx="0.45" dy="0.35" layer="1" roundness="25"/>
<smd name="13" x="1.765" y="-1.25" dx="0.45" dy="0.35" layer="1" roundness="25"/>
<smd name="9" x="-0.25" y="-1.765" dx="0.45" dy="0.35" layer="1" roundness="25" rot="R90"/>
<smd name="10" x="0.25" y="-1.765" dx="0.45" dy="0.35" layer="1" roundness="25" rot="R90"/>
<smd name="11" x="0.75" y="-1.765" dx="0.45" dy="0.35" layer="1" roundness="25" rot="R90"/>
<smd name="12" x="1.25" y="-1.765" dx="0.45" dy="0.35" layer="1" roundness="25" rot="R90"/>
<smd name="8" x="-0.75" y="-1.765" dx="0.45" dy="0.35" layer="1" roundness="25" rot="R90"/>
<smd name="7" x="-1.25" y="-1.765" dx="0.45" dy="0.35" layer="1" roundness="25" rot="R90"/>
<smd name="21" x="0.25" y="1.765" dx="0.45" dy="0.35" layer="1" roundness="25" rot="R90"/>
<smd name="20" x="0.75" y="1.765" dx="0.45" dy="0.35" layer="1" roundness="25" rot="R90"/>
<smd name="19" x="1.25" y="1.765" dx="0.45" dy="0.35" layer="1" roundness="25" rot="R90"/>
<smd name="22" x="-0.25" y="1.765" dx="0.45" dy="0.35" layer="1" roundness="25" rot="R90"/>
<smd name="23" x="-0.75" y="1.765" dx="0.45" dy="0.35" layer="1" roundness="25" rot="R90"/>
<smd name="24" x="-1.25" y="1.765" dx="0.45" dy="0.35" layer="1" roundness="25" rot="R90"/>
</package>
<package name="MTXDOT-EU1-A01-100">
<smd name="P$1" x="0" y="-0.889" dx="0.7112" dy="2.4892" layer="1"/>
<smd name="P$2" x="0" y="-3.556" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$3" x="0" y="-5.334" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$4" x="0" y="-7.112" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$5" x="0" y="-8.89" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$6" x="0" y="-10.668" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$7" x="0" y="-12.446" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$8" x="0" y="-14.224" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$9" x="0" y="-16.002" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$10" x="0" y="-17.78" dx="0.7112" dy="0.7112" layer="1"/>
<wire x1="-1.143" y1="1.143" x2="-1.143" y2="-22.606" width="0.127" layer="21"/>
<smd name="P$11" x="0" y="-19.558" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$12" x="0" y="-21.336" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$13" x="1.778" y="-21.336" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$14" x="3.556" y="-21.336" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$15" x="5.334" y="-21.336" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$16" x="7.112" y="-21.336" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$17" x="8.89" y="-21.336" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$18" x="10.668" y="-21.336" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$19" x="12.446" y="-21.336" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$20" x="14.224" y="-21.336" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$21" x="16.002" y="-21.336" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$22" x="17.78" y="-21.336" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$23" x="19.558" y="-21.336" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$24" x="21.336" y="-21.336" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$25" x="21.336" y="-19.558" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$26" x="21.336" y="-17.78" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$27" x="21.336" y="-16.002" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$28" x="21.336" y="-14.224" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$29" x="21.336" y="-12.446" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$30" x="21.336" y="-10.668" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$31" x="21.336" y="-8.89" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$32" x="21.336" y="-7.112" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$33" x="21.336" y="-5.334" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$34" x="21.336" y="-3.556" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$35" x="21.336" y="-1.778" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$36" x="21.336" y="0" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$37" x="19.558" y="0" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$38" x="17.78" y="0" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$39" x="16.002" y="0" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$40" x="14.224" y="0" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$41" x="12.446" y="0" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$42" x="10.668" y="0" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$43" x="8.89" y="0" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$44" x="7.112" y="0" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$45" x="5.334" y="0" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$46" x="3.556" y="0" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$47" x="1.778" y="0" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$48" x="17.78" y="-1.778" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$49" x="3.556" y="-1.778" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$50" x="5.334" y="-5.334" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$51" x="10.668" y="-5.334" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$52" x="16.002" y="-5.334" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$53" x="5.334" y="-10.668" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$54" x="10.668" y="-10.668" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$55" x="16.002" y="-10.668" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$56" x="5.334" y="-16.002" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$57" x="10.668" y="-16.002" dx="0.7112" dy="0.7112" layer="1"/>
<smd name="P$58" x="16.002" y="-16.002" dx="0.7112" dy="0.7112" layer="1"/>
<wire x1="-1.143" y1="1.143" x2="22.86" y2="1.143" width="0.127" layer="21"/>
<wire x1="-1.143" y1="-22.606" x2="22.86" y2="-22.606" width="0.127" layer="21"/>
<wire x1="22.86" y1="1.143" x2="22.86" y2="-22.606" width="0.127" layer="21"/>
</package>
<package name="RM186-SM-02">
<smd name="P$1" x="-0.127" y="-5.08" dx="1.143" dy="1.016" layer="1"/>
<wire x1="0" y1="0" x2="0" y2="-4.572" width="0.127" layer="21"/>
<wire x1="0" y1="-4.572" x2="0" y2="-25.4" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="9.398" y2="0" width="0.127" layer="21"/>
<wire x1="25.4" y1="0" x2="9.398" y2="0" width="0.127" layer="21"/>
<wire x1="9.398" y1="0" x2="9.398" y2="-4.572" width="0.127" layer="21"/>
<wire x1="0" y1="-4.572" x2="9.398" y2="-4.572" width="0.127" layer="21"/>
<rectangle x1="-10.16" y1="-4.572" x2="9.398" y2="7.62" layer="39"/>
<rectangle x1="9.398" y1="0" x2="25.908" y2="7.62" layer="39"/>
<wire x1="25.4" y1="0" x2="25.4" y2="-25.4" width="0.127" layer="21"/>
<wire x1="0" y1="-25.4" x2="25.4" y2="-25.4" width="0.127" layer="21"/>
<smd name="P$2" x="-0.127" y="-6.604" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$3" x="-0.127" y="-8.128" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$4" x="-0.127" y="-9.652" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$5" x="-0.127" y="-11.176" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$6" x="-0.127" y="-12.7" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$7" x="-0.127" y="-14.224" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$8" x="-0.127" y="-15.748" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$9" x="-0.127" y="-17.272" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$10" x="-0.127" y="-18.796" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$11" x="-0.127" y="-20.32" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$12" x="-0.127" y="-21.844" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$24" x="25.527" y="-5.08" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$23" x="25.527" y="-6.604" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$22" x="25.527" y="-8.128" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$21" x="25.527" y="-9.652" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$20" x="25.527" y="-11.176" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$19" x="25.527" y="-12.7" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$18" x="25.527" y="-14.224" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$17" x="25.527" y="-15.748" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$16" x="25.527" y="-17.272" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$15" x="25.527" y="-18.796" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$14" x="25.527" y="-20.32" dx="1.143" dy="1.016" layer="1"/>
<smd name="P$13" x="25.527" y="-21.844" dx="1.143" dy="1.016" layer="1"/>
</package>
<package name="SOT95P280X145-6N">
<description>&lt;b&gt;sot-23&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-1.25" y="0.95" dx="1.25" dy="0.6" layer="1"/>
<smd name="2" x="-1.25" y="0" dx="1.25" dy="0.6" layer="1"/>
<smd name="3" x="-1.25" y="-0.95" dx="1.25" dy="0.6" layer="1"/>
<smd name="4" x="1.25" y="-0.95" dx="1.25" dy="0.6" layer="1"/>
<smd name="5" x="1.25" y="0" dx="1.25" dy="0.6" layer="1"/>
<smd name="6" x="1.25" y="0.95" dx="1.25" dy="0.6" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-2.125" y1="1.75" x2="2.125" y2="1.75" width="0.05" layer="51"/>
<wire x1="2.125" y1="1.75" x2="2.125" y2="-1.75" width="0.05" layer="51"/>
<wire x1="2.125" y1="-1.75" x2="-2.125" y2="-1.75" width="0.05" layer="51"/>
<wire x1="-2.125" y1="-1.75" x2="-2.125" y2="1.75" width="0.05" layer="51"/>
<wire x1="-0.812" y1="1.45" x2="0.812" y2="1.45" width="0.1" layer="51"/>
<wire x1="0.812" y1="1.45" x2="0.812" y2="-1.45" width="0.1" layer="51"/>
<wire x1="0.812" y1="-1.45" x2="-0.812" y2="-1.45" width="0.1" layer="51"/>
<wire x1="-0.812" y1="-1.45" x2="-0.812" y2="1.45" width="0.1" layer="51"/>
<wire x1="-0.812" y1="0.5" x2="0.138" y2="1.45" width="0.1" layer="51"/>
<wire x1="-0.275" y1="1.45" x2="0.275" y2="1.45" width="0.2" layer="21"/>
<wire x1="0.275" y1="1.45" x2="0.275" y2="-1.45" width="0.2" layer="21"/>
<wire x1="0.275" y1="-1.45" x2="-0.275" y2="-1.45" width="0.2" layer="21"/>
<wire x1="-0.275" y1="-1.45" x2="-0.275" y2="1.45" width="0.2" layer="21"/>
<wire x1="-1.875" y1="1.6" x2="-0.625" y2="1.6" width="0.2" layer="21"/>
</package>
<package name="JUMPER_CLOSED">
<smd name="1" x="-0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<smd name="2" x="0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<text x="-1.397" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.8" y="-0.4" size="0.01" layer="27">&gt;VALUE</text>
<rectangle x1="-0.508" y1="-0.1778" x2="0.508" y2="0.1778" layer="1"/>
<rectangle x1="-0.0762" y1="-0.889" x2="0.0762" y2="0.889" layer="29"/>
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.1524" layer="21"/>
</package>
<package name="SOP50P490X110-10N">
<description>&lt;b&gt;10 vssop&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.2" y="1" dx="1.4" dy="0.3" layer="1"/>
<smd name="2" x="-2.2" y="0.5" dx="1.4" dy="0.3" layer="1"/>
<smd name="3" x="-2.2" y="0" dx="1.4" dy="0.3" layer="1"/>
<smd name="4" x="-2.2" y="-0.5" dx="1.4" dy="0.3" layer="1"/>
<smd name="5" x="-2.2" y="-1" dx="1.4" dy="0.3" layer="1"/>
<smd name="6" x="2.2" y="-1" dx="1.4" dy="0.3" layer="1"/>
<smd name="7" x="2.2" y="-0.5" dx="1.4" dy="0.3" layer="1"/>
<smd name="8" x="2.2" y="0" dx="1.4" dy="0.3" layer="1"/>
<smd name="9" x="2.2" y="0.5" dx="1.4" dy="0.3" layer="1"/>
<smd name="10" x="2.2" y="1" dx="1.4" dy="0.3" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.15" y1="1.8" x2="3.15" y2="1.8" width="0.05" layer="51"/>
<wire x1="3.15" y1="1.8" x2="3.15" y2="-1.8" width="0.05" layer="51"/>
<wire x1="3.15" y1="-1.8" x2="-3.15" y2="-1.8" width="0.05" layer="51"/>
<wire x1="-3.15" y1="-1.8" x2="-3.15" y2="1.8" width="0.05" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.1" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.1" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.1" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="1.5" width="0.1" layer="51"/>
<wire x1="-1.5" y1="1" x2="-1" y2="1.5" width="0.1" layer="51"/>
<wire x1="-1.15" y1="1.5" x2="1.15" y2="1.5" width="0.2" layer="21"/>
<wire x1="1.15" y1="1.5" x2="1.15" y2="-1.5" width="0.2" layer="21"/>
<wire x1="1.15" y1="-1.5" x2="-1.15" y2="-1.5" width="0.2" layer="21"/>
<wire x1="-1.15" y1="-1.5" x2="-1.15" y2="1.5" width="0.2" layer="21"/>
<wire x1="-2.9" y1="1.5" x2="-1.5" y2="1.5" width="0.2" layer="21"/>
</package>
<package name="JUMPER_OPEN" urn="urn:adsk.eagle:footprint:15432/1" locally_modified="yes">
<description>&lt;b&gt;Solder jumper&lt;/b&gt;</description>
<wire x1="1.397" y1="-1.016" x2="-1.397" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.397" y1="1.016" x2="1.651" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.651" y1="0.762" x2="-1.397" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.651" y1="-0.762" x2="-1.397" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="1.397" y1="-1.016" x2="1.651" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.016" x2="1.397" y2="1.016" width="0.1524" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<smd name="2" x="0.762" y="0" dx="1.1684" dy="1.6002" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<rectangle x1="-0.0762" y1="-0.9144" x2="0.0762" y2="0.9144" layer="29"/>
</package>
</packages>
<packages3d>
<package3d name="SJ" urn="urn:adsk.eagle:package:15471/1" type="box">
<description>Solder jumper</description>
<packageinstances>
<packageinstance name="JUMPER_OPEN"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="IRFB7430PBF">
<wire x1="7.62" y1="2.54" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="5.842" y2="2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="5.842" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.842" y1="0" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="5.842" y1="5.588" x2="5.842" y2="4.572" width="0.254" layer="94"/>
<wire x1="5.842" y1="-0.508" x2="5.842" y2="0.508" width="0.254" layer="94"/>
<wire x1="5.842" y1="2.032" x2="5.842" y2="3.048" width="0.254" layer="94"/>
<circle x="6.35" y="2.54" radius="3.81" width="0.254" layer="94"/>
<text x="11.43" y="3.81" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="11.43" y="1.27" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="G" x="0" y="0" visible="pad" length="short"/>
<pin name="D" x="7.62" y="10.16" visible="pad" length="short" rot="R270"/>
<pin name="S" x="7.62" y="-5.08" visible="pad" length="short" rot="R90"/>
<polygon width="0.254" layer="94">
<vertex x="5.842" y="2.54"/>
<vertex x="6.858" y="3.048"/>
<vertex x="6.858" y="2.032"/>
</polygon>
</symbol>
<symbol name="1546215-2">
<wire x1="5.08" y1="2.54" x2="15.24" y2="2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="-5.08" x2="15.24" y2="2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<text x="16.51" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="16.51" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="0" y="0" length="middle"/>
<pin name="2" x="0" y="-2.54" length="middle"/>
</symbol>
<symbol name="TLV9102IDDFR">
<wire x1="5.08" y1="2.54" x2="22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="22.86" y1="-10.16" x2="22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="22.86" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<text x="24.13" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="24.13" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="OUT1" x="0" y="0" length="middle"/>
<pin name="IN1-" x="0" y="-2.54" length="middle"/>
<pin name="IN1+" x="0" y="-5.08" length="middle"/>
<pin name="V-" x="0" y="-7.62" length="middle"/>
<pin name="IN2+" x="27.94" y="0" length="middle" rot="R180"/>
<pin name="IN2-" x="27.94" y="-2.54" length="middle" rot="R180"/>
<pin name="OUT2" x="27.94" y="-5.08" length="middle" rot="R180"/>
<pin name="V+" x="27.94" y="-7.62" length="middle" rot="R180"/>
</symbol>
<symbol name="C2012X5R1E475K125AB">
<wire x1="5.588" y1="2.54" x2="5.588" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.588" y2="0" width="0.254" layer="94"/>
<wire x1="7.112" y1="0" x2="7.62" y2="0" width="0.254" layer="94"/>
<text x="8.89" y="6.35" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="8.89" y="3.81" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="0" y="0" visible="pad" length="middle"/>
<pin name="2" x="12.7" y="0" visible="pad" length="middle" rot="R180"/>
</symbol>
<symbol name="CR32NP-100KC">
<wire x1="5.08" y1="2.54" x2="15.24" y2="2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="-5.08" x2="15.24" y2="2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<text x="16.51" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="16.51" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="2" x="0" y="0" length="middle"/>
<pin name="3" x="0" y="-2.54" length="middle"/>
<pin name="4" x="20.32" y="-2.54" length="middle" rot="R180"/>
</symbol>
<symbol name="INA190A4QDCKRQ1">
<wire x1="5.08" y1="2.54" x2="20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="-7.62" x2="20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<text x="21.59" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="21.59" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="REF" x="0" y="0" length="middle"/>
<pin name="GND" x="0" y="-2.54" length="middle"/>
<pin name="VS" x="0" y="-5.08" length="middle"/>
<pin name="OUT" x="25.4" y="0" length="middle" rot="R180"/>
<pin name="IN-" x="25.4" y="-2.54" length="middle" rot="R180"/>
<pin name="IN+" x="25.4" y="-5.08" length="middle" rot="R180"/>
</symbol>
<symbol name="GMR100HTBFA10L0">
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.254" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.254" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<text x="13.97" y="6.35" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="13.97" y="3.81" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="0" y="0" visible="pad" length="middle"/>
<pin name="2" x="17.78" y="0" visible="pad" length="middle" rot="R180"/>
</symbol>
<symbol name="ICM-20789">
<wire x1="5.08" y1="12.7" x2="38.1" y2="12.7" width="0.254" layer="94"/>
<wire x1="38.1" y1="-25.4" x2="38.1" y2="12.7" width="0.254" layer="94"/>
<wire x1="38.1" y1="-25.4" x2="5.08" y2="-25.4" width="0.254" layer="94"/>
<wire x1="5.08" y1="12.7" x2="5.08" y2="-25.4" width="0.254" layer="94"/>
<text x="31.75" y="17.78" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="31.75" y="15.24" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="NC_1" x="0" y="0" length="middle"/>
<pin name="NC_X_2" x="0" y="-2.54" length="middle"/>
<pin name="NC_X_3" x="0" y="-5.08" length="middle"/>
<pin name="NC_X_4" x="0" y="-7.62" length="middle"/>
<pin name="NC_X_5" x="0" y="-10.16" length="middle"/>
<pin name="PR_DA" x="0" y="-12.7" length="middle"/>
<pin name="RP_CL" x="15.24" y="-30.48" length="middle" rot="R90"/>
<pin name="VDDIO" x="17.78" y="-30.48" length="middle" rot="R90"/>
<pin name="AD0/SDO" x="20.32" y="-30.48" length="middle" rot="R90"/>
<pin name="REGOUT" x="22.86" y="-30.48" length="middle" rot="R90"/>
<pin name="FSYNC" x="25.4" y="-30.48" length="middle" rot="R90"/>
<pin name="INT" x="27.94" y="-30.48" length="middle" rot="R90"/>
<pin name="GND" x="43.18" y="0" length="middle" rot="R180"/>
<pin name="NC_X_17" x="43.18" y="-2.54" length="middle" rot="R180"/>
<pin name="NC_X_16" x="43.18" y="-5.08" length="middle" rot="R180"/>
<pin name="NC_X_15" x="43.18" y="-7.62" length="middle" rot="R180"/>
<pin name="NC_X_14" x="43.18" y="-10.16" length="middle" rot="R180"/>
<pin name="VDD" x="43.18" y="-12.7" length="middle" rot="R180"/>
<pin name="SDA/SDI" x="15.24" y="17.78" length="middle" rot="R270"/>
<pin name="SCL/SCLK" x="17.78" y="17.78" length="middle" rot="R270"/>
<pin name="NCS" x="20.32" y="17.78" length="middle" rot="R270"/>
<pin name="NC_21" x="22.86" y="17.78" length="middle" rot="R270"/>
<pin name="NC_20" x="25.4" y="17.78" length="middle" rot="R270"/>
<pin name="NC_19" x="27.94" y="17.78" length="middle" rot="R270"/>
</symbol>
<symbol name="TLV70218DBVT">
<wire x1="5.08" y1="2.54" x2="20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="-7.62" x2="20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<text x="21.59" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="21.59" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="IN" x="0" y="0" length="middle"/>
<pin name="GND" x="0" y="-2.54" length="middle"/>
<pin name="EN" x="0" y="-5.08" length="middle"/>
<pin name="NC" x="25.4" y="0" length="middle" direction="nc" rot="R180"/>
<pin name="OUT" x="25.4" y="-2.54" length="middle" rot="R180"/>
</symbol>
<symbol name="MTXDOT-EU1-A01-100">
<wire x1="-20.32" y1="22.86" x2="-20.32" y2="-53.34" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-53.34" x2="10.16" y2="-53.34" width="0.254" layer="94"/>
<wire x1="10.16" y1="-53.34" x2="10.16" y2="22.86" width="0.254" layer="94"/>
<wire x1="10.16" y1="22.86" x2="-20.32" y2="22.86" width="0.254" layer="94"/>
<pin name="GND0" x="-25.4" y="20.32" length="middle"/>
<pin name="RFU1" x="-25.4" y="17.78" length="middle"/>
<pin name="VDD0" x="-25.4" y="15.24" length="middle"/>
<pin name="VDD1" x="-25.4" y="12.7" length="middle"/>
<pin name="GND1" x="-25.4" y="10.16" length="middle"/>
<pin name="RFU2" x="-25.4" y="7.62" length="middle"/>
<pin name="RFU3" x="-25.4" y="5.08" length="middle"/>
<pin name="RFU4" x="-25.4" y="2.54" length="middle"/>
<pin name="SPI_NSS" x="-25.4" y="0" length="middle"/>
<pin name="SPI_SCK" x="-25.4" y="-2.54" length="middle"/>
<pin name="SPI_MOSI" x="-25.4" y="-5.08" length="middle"/>
<pin name="UART1_TX" x="-25.4" y="-10.16" length="middle"/>
<pin name="UART1_RX" x="-25.4" y="-12.7" length="middle"/>
<pin name="UART0_TX" x="-25.4" y="-15.24" length="middle"/>
<pin name="UART0_RX" x="-25.4" y="-17.78" length="middle"/>
<pin name="GND2" x="-25.4" y="-20.32" length="middle"/>
<pin name="SPI_MISO" x="-25.4" y="-7.62" length="middle"/>
<pin name="RFU7" x="-25.4" y="-22.86" length="middle"/>
<pin name="RFU8" x="-25.4" y="-25.4" length="middle"/>
<pin name="GND3" x="-25.4" y="-27.94" length="middle"/>
<pin name="RFU5" x="-25.4" y="-30.48" length="middle"/>
<pin name="RFU6" x="-25.4" y="-33.02" length="middle"/>
<pin name="GPIO3" x="-25.4" y="-35.56" length="middle"/>
<pin name="GPIO2" x="-25.4" y="-38.1" length="middle"/>
<pin name="GPIO1" x="-25.4" y="-40.64" length="middle"/>
<pin name="GPIO0" x="-25.4" y="-43.18" length="middle"/>
<pin name="I2C0_SCL" x="-25.4" y="-45.72" length="middle"/>
<pin name="I2C0_SDA" x="-25.4" y="-48.26" length="middle"/>
<pin name="SWCLK" x="-25.4" y="-50.8" length="middle"/>
<pin name="SWDIO" x="15.24" y="20.32" length="middle" rot="R180"/>
<pin name="UART1_RTS" x="15.24" y="17.78" length="middle" rot="R180"/>
<pin name="UART1_CTS" x="15.24" y="15.24" length="middle" rot="R180"/>
<pin name="NRESET" x="15.24" y="12.7" length="middle" rot="R180"/>
<pin name="WAKE" x="15.24" y="10.16" length="middle" rot="R180"/>
<pin name="GND4" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="GND5" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="ANT1" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="GND6" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="GND7" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="GND8" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="GND9" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="GND10" x="15.24" y="-10.16" length="middle" rot="R180"/>
<pin name="GND11" x="15.24" y="-12.7" length="middle" rot="R180"/>
<pin name="GND12" x="15.24" y="-15.24" length="middle" rot="R180"/>
<pin name="GND13" x="15.24" y="-17.78" length="middle" rot="R180"/>
<pin name="GND14" x="15.24" y="-20.32" length="middle" rot="R180"/>
<pin name="ANT2" x="15.24" y="-22.86" length="middle" rot="R180"/>
<pin name="GND15" x="15.24" y="-25.4" length="middle" rot="R180"/>
<pin name="GND16" x="15.24" y="-27.94" length="middle" rot="R180"/>
<pin name="GND17" x="15.24" y="-30.48" length="middle" rot="R180"/>
<pin name="GND18" x="15.24" y="-33.02" length="middle" rot="R180"/>
<pin name="GND19" x="15.24" y="-35.56" length="middle" rot="R180"/>
<pin name="GND20" x="15.24" y="-38.1" length="middle" rot="R180"/>
<pin name="GND21" x="15.24" y="-40.64" length="middle" rot="R180"/>
<pin name="GND22" x="15.24" y="-43.18" length="middle" rot="R180"/>
<pin name="GND23" x="15.24" y="-45.72" length="middle" rot="R180"/>
<pin name="GND24" x="15.24" y="-48.26" length="middle" rot="R180"/>
<pin name="GND25" x="15.24" y="-50.8" length="middle" rot="R180"/>
</symbol>
<symbol name="RM186-SM-02">
<wire x1="0" y1="0" x2="40.64" y2="0" width="0.254" layer="94"/>
<wire x1="40.64" y1="0" x2="40.64" y2="-33.02" width="0.254" layer="94"/>
<wire x1="40.64" y1="-33.02" x2="0" y2="-33.02" width="0.254" layer="94"/>
<wire x1="0" y1="-33.02" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="GND_1" x="-5.08" y="-2.54" length="middle"/>
<pin name="SIO_21/UART_TX" x="-5.08" y="-5.08" length="middle"/>
<pin name="SIO_22/UART_RX" x="-5.08" y="-7.62" length="middle"/>
<pin name="SIO_23/UART_RTS" x="-5.08" y="-10.16" length="middle"/>
<pin name="SIO_24/UART_CTS" x="-5.08" y="-12.7" length="middle"/>
<pin name="SIO_25" x="-5.08" y="-15.24" length="middle"/>
<pin name="SIO_28" x="-5.08" y="-17.78" length="middle"/>
<pin name="GND_2" x="-5.08" y="-20.32" length="middle"/>
<pin name="SIO_29/SCL" x="-5.08" y="-22.86" length="middle"/>
<pin name="SIO_30/SDA" x="-5.08" y="-25.4" length="middle"/>
<pin name="GND_3" x="-5.08" y="-27.94" length="middle"/>
<pin name="VCC_BLE" x="-5.08" y="-30.48" length="middle"/>
<pin name="GND_4" x="45.72" y="-2.54" length="middle" rot="R180"/>
<pin name="NC/SWCLK" x="45.72" y="-5.08" length="middle" rot="R180"/>
<pin name="NRESET/SWDIO" x="45.72" y="-7.62" length="middle" rot="R180"/>
<pin name="GND_5" x="45.72" y="-10.16" length="middle" rot="R180"/>
<pin name="SIO_6/AIN" x="45.72" y="-12.7" length="middle" rot="R180"/>
<pin name="SIO_5/AIN" x="45.72" y="-15.24" length="middle" rot="R180"/>
<pin name="SIO_4/AIN" x="45.72" y="-17.78" length="middle" rot="R180"/>
<pin name="SIO_3/AIN/MOSI" x="45.72" y="-20.32" length="middle" rot="R180"/>
<pin name="SIO_17/MISO" x="45.72" y="-22.86" length="middle" rot="R180"/>
<pin name="SIO_00/CLK" x="45.72" y="-25.4" length="middle" rot="R180"/>
<pin name="GND_6" x="45.72" y="-27.94" length="middle" rot="R180"/>
<pin name="VCC_LORA" x="45.72" y="-30.48" length="middle" rot="R180"/>
</symbol>
<symbol name="MAX1605EUT+T">
<wire x1="5.08" y1="2.54" x2="22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="22.86" y1="-7.62" x2="22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="22.86" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<text x="24.13" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="24.13" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="!SHDN" x="0" y="0" length="middle" direction="in"/>
<pin name="VCC" x="0" y="-2.54" length="middle" direction="pwr"/>
<pin name="GND" x="0" y="-5.08" length="middle" direction="pwr"/>
<pin name="FB" x="27.94" y="0" length="middle" direction="in" rot="R180"/>
<pin name="LIM" x="27.94" y="-2.54" length="middle" direction="in" rot="R180"/>
<pin name="LX" x="27.94" y="-5.08" length="middle" rot="R180"/>
</symbol>
<symbol name="SJ_CLOSED">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.556" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<wire x1="-1.016" y1="0" x2="0.889" y2="0" width="0.6096" layer="94"/>
</symbol>
<symbol name="DAC80501ZDGST">
<wire x1="5.08" y1="2.54" x2="30.48" y2="2.54" width="0.254" layer="94"/>
<wire x1="30.48" y1="-12.7" x2="30.48" y2="2.54" width="0.254" layer="94"/>
<wire x1="30.48" y1="-12.7" x2="5.08" y2="-12.7" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-12.7" width="0.254" layer="94"/>
<text x="31.75" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="31.75" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="VDD" x="0" y="0" length="middle" direction="pwr"/>
<pin name="VOUT" x="0" y="-2.54" length="middle" direction="out"/>
<pin name="NC_1" x="0" y="-5.08" length="middle"/>
<pin name="AGND" x="0" y="-7.62" length="middle" direction="pwr"/>
<pin name="SPI2C" x="0" y="-10.16" length="middle" direction="in"/>
<pin name="VREFIO" x="35.56" y="0" length="middle" rot="R180"/>
<pin name="NC_2" x="35.56" y="-2.54" length="middle" rot="R180"/>
<pin name="SDIN/SDA" x="35.56" y="-5.08" length="middle" rot="R180"/>
<pin name="!SYNC!/A0" x="35.56" y="-7.62" length="middle" rot="R180"/>
<pin name="SCLK/SCL" x="35.56" y="-10.16" length="middle" rot="R180"/>
</symbol>
<symbol name="SJ_OPEN">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="IRFB7430PBF" prefix="Q">
<description>&lt;b&gt;Infineon IRFB7430PBF N-channel MOSFET, 195 A, 40 V StrongIRFET, 3-Pin TO-220AB&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/2/IRFB7430PBF.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="IRFB7430PBF" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO254P483X1016X1994-3P">
<connects>
<connect gate="G$1" pin="D" pad="2"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Infineon IRFB7430PBF N-channel MOSFET, 195 A, 40 V StrongIRFET, 3-Pin TO-220AB" constant="no"/>
<attribute name="HEIGHT" value="4.83mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Infineon" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="IRFB7430PBF" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="942-IRFB7430PBF" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.co.uk/ProductDetail/Infineon-IR/IRFB7430PBF?qs=PRpJrl6HpdwkCaEhLAouSA%3D%3D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1546215-2" prefix="J">
<description>&lt;b&gt;Fixed Terminal Blocks TERMI-BLOK PCB MOUNT, 90 2P.&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&amp;DocNm=1546215&amp;DocType=Customer Drawing&amp;DocLang=English"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="1546215-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="15462152">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Fixed Terminal Blocks TERMI-BLOK PCB MOUNT, 90 2P." constant="no"/>
<attribute name="HEIGHT" value="10.3mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="TE Connectivity" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="1546215-2" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="571-1546215-2" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.co.uk/ProductDetail/TE-Connectivity/1546215-2?qs=Lxq28tKK6BPmJFoAOY7Ljw%3D%3D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TLV9102IDDFR" prefix="IC">
<description>&lt;b&gt;Operational Amplifiers - Op Amps Dual 1MHz, 16-V rail-to-rail input/output, low-offset voltage, low-power op amp 8-SOT-23-THIN -40 to 125&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.ti.com/lit/gpn/tlv9102"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="TLV9102IDDFR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT65P280X110-8N">
<connects>
<connect gate="G$1" pin="IN1+" pad="3"/>
<connect gate="G$1" pin="IN1-" pad="2"/>
<connect gate="G$1" pin="IN2+" pad="5"/>
<connect gate="G$1" pin="IN2-" pad="6"/>
<connect gate="G$1" pin="OUT1" pad="1"/>
<connect gate="G$1" pin="OUT2" pad="7"/>
<connect gate="G$1" pin="V+" pad="8"/>
<connect gate="G$1" pin="V-" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Operational Amplifiers - Op Amps Dual 1MHz, 16-V rail-to-rail input/output, low-offset voltage, low-power op amp 8-SOT-23-THIN -40 to 125" constant="no"/>
<attribute name="HEIGHT" value="1.1mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Texas Instruments" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="TLV9102IDDFR" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="595-TLV9102IDDFR" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.co.uk/ProductDetail/Texas-Instruments/TLV9102IDDFR?qs=IS%252B4QmGtzzroGAqUkTxJrA%3D%3D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C2012X5R1E475K125AB" prefix="C">
<description>&lt;b&gt;TDK C2012 C 4.7uF Ceramic Multilayer Capacitor, 25 V dc, +85C, X5R Dielectric, +/-10% SMD&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://product.tdk.com/en/search/list#pn=C1608C0G1E103J080AA&amp;site=FBNXDO0R&amp;charset=UTF-8&amp;group=tdk_pdc_en&amp;design=producttdkcom-en&amp;fromsyncsearch=1&amp;_l=20&amp;_p=1&amp;_c=part_no-part_no&amp;_d=626"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="C2012X5R1E475K125AB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C2012_COMMERCIAL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="TDK C2012 C 4.7uF Ceramic Multilayer Capacitor, 25 V dc, +85C, X5R Dielectric, +/-10% SMD" constant="no"/>
<attribute name="HEIGHT" value="0.75mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="TDK" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="C2012X5R1E475K125AB" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="810-C2012X5R1E475K" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.co.uk/ProductDetail/TDK/C2012X5R1E475K125AB?qs=LcTL%2F5vFEzEfIWXeqp5s%252BQ%3D%3D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CR32NP-100KC" prefix="L">
<description>&lt;b&gt;Fixed Inductors 10uH 760A 230 ohms&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://products.sumida.com/products/pdf/CR32.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="CR32NP-100KC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CR32NP220KC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Fixed Inductors 10uH 760A 230 ohms" constant="no"/>
<attribute name="HEIGHT" value="3mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Sumida" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="CR32NP-100KC" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="851-CR32NP-100KC" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.co.uk/ProductDetail/Sumida/CR32NP-100KC?qs=LKl%2FVGYbYtgIyn7e61Bpog%3D%3D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="INA190A4QDCKRQ1" prefix="IC">
<description>&lt;b&gt;AEC-Q100 40V, bi-dir, low-/high-side, zero-drift, voltage output CSA w/low bias current and enable&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.ti.com/lit/gpn/INA190-Q1"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="INA190A4QDCKRQ1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT65P210X110-6N">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="IN+" pad="4"/>
<connect gate="G$1" pin="IN-" pad="5"/>
<connect gate="G$1" pin="OUT" pad="6"/>
<connect gate="G$1" pin="REF" pad="1"/>
<connect gate="G$1" pin="VS" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="AEC-Q100 40V, bi-dir, low-/high-side, zero-drift, voltage output CSA w/low bias current and enable" constant="no"/>
<attribute name="HEIGHT" value="1.1mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Texas Instruments" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="INA190A4QDCKRQ1" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="595-INA190A4QDCKRQ1" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.co.uk/ProductDetail/Texas-Instruments/INA190A4QDCKRQ1?qs=mAH9sUMRCtuy65K8jF1VBg%3D%3D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GMR100HTBFA10L0" prefix="R">
<description>&lt;b&gt;Current Sense Resistors - SMD 2512 0.01ohm 1% Shunt Res AEC-Q200&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://rohmfs.rohm.com/en/products/databook/datasheet/passive/resistor/chip_resistor/gmr-e.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="GMR100HTBFA10L0" x="0" y="0"/>
</gates>
<devices>
<device name="" package="GMR100HTBFA10L0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Current Sense Resistors - SMD 2512 0.01ohm 1% Shunt Res AEC-Q200" constant="no"/>
<attribute name="HEIGHT" value="0.55mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="ROHM Semiconductor" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="GMR100HTBFA10L0" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="755-GMR100HTBFA10L0" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.co.uk/ProductDetail/ROHM-Semiconductor/GMR100HTBFA10L0?qs=chTDxNqvsylQYSdLz9rpug%3D%3D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ICM-20789" prefix="IC">
<description>&lt;b&gt;IMUs - Inertial Measurement Units 6-Axis DMP-Enabled Drone/VR/IoT solution, Targeted For All Current MPU-6000/6050 Users&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.invensense.com/wp-content/uploads/2016/03/PB-000045-ICM-20689-v1.0.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ICM-20789" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN50P400X400X95-25N">
<connects>
<connect gate="G$1" pin="AD0/SDO" pad="9"/>
<connect gate="G$1" pin="FSYNC" pad="11"/>
<connect gate="G$1" pin="GND" pad="18"/>
<connect gate="G$1" pin="INT" pad="12"/>
<connect gate="G$1" pin="NCS" pad="22"/>
<connect gate="G$1" pin="NC_1" pad="1"/>
<connect gate="G$1" pin="NC_19" pad="19"/>
<connect gate="G$1" pin="NC_20" pad="20"/>
<connect gate="G$1" pin="NC_21" pad="21"/>
<connect gate="G$1" pin="NC_X_14" pad="14"/>
<connect gate="G$1" pin="NC_X_15" pad="15"/>
<connect gate="G$1" pin="NC_X_16" pad="16"/>
<connect gate="G$1" pin="NC_X_17" pad="17"/>
<connect gate="G$1" pin="NC_X_2" pad="2"/>
<connect gate="G$1" pin="NC_X_3" pad="3"/>
<connect gate="G$1" pin="NC_X_4" pad="4"/>
<connect gate="G$1" pin="NC_X_5" pad="5"/>
<connect gate="G$1" pin="PR_DA" pad="6"/>
<connect gate="G$1" pin="REGOUT" pad="10"/>
<connect gate="G$1" pin="RP_CL" pad="7"/>
<connect gate="G$1" pin="SCL/SCLK" pad="23"/>
<connect gate="G$1" pin="SDA/SDI" pad="24"/>
<connect gate="G$1" pin="VDD" pad="13"/>
<connect gate="G$1" pin="VDDIO" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="IMUs - Inertial Measurement Units 6-Axis DMP-Enabled Drone/VR/IoT solution, Targeted For All Current MPU-6000/6050 Users" constant="no"/>
<attribute name="HEIGHT" value="0.95mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="TDK" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="ICM-20689" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="410-ICM-20689" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.co.uk/ProductDetail/TDK-InvenSense/ICM-20689?qs=u4fy%2FsgLU9NcfLrncAE%252BeQ%3D%3D" constant="no"/>
</technology>
</technologies>
</device>
<device name="PQFN50" package="PQFN50P400X400X146-24N">
<connects>
<connect gate="G$1" pin="AD0/SDO" pad="9"/>
<connect gate="G$1" pin="FSYNC" pad="11"/>
<connect gate="G$1" pin="GND" pad="18"/>
<connect gate="G$1" pin="INT" pad="12"/>
<connect gate="G$1" pin="NCS" pad="22"/>
<connect gate="G$1" pin="NC_1" pad="1"/>
<connect gate="G$1" pin="NC_19" pad="19"/>
<connect gate="G$1" pin="NC_20" pad="20"/>
<connect gate="G$1" pin="NC_21" pad="21"/>
<connect gate="G$1" pin="NC_X_14" pad="14"/>
<connect gate="G$1" pin="NC_X_15" pad="15"/>
<connect gate="G$1" pin="NC_X_16" pad="16"/>
<connect gate="G$1" pin="NC_X_17" pad="17"/>
<connect gate="G$1" pin="NC_X_2" pad="2"/>
<connect gate="G$1" pin="NC_X_3" pad="3"/>
<connect gate="G$1" pin="NC_X_4" pad="4"/>
<connect gate="G$1" pin="NC_X_5" pad="5"/>
<connect gate="G$1" pin="PR_DA" pad="6"/>
<connect gate="G$1" pin="REGOUT" pad="10"/>
<connect gate="G$1" pin="RP_CL" pad="7"/>
<connect gate="G$1" pin="SCL/SCLK" pad="23"/>
<connect gate="G$1" pin="SDA/SDI" pad="24"/>
<connect gate="G$1" pin="VDD" pad="13"/>
<connect gate="G$1" pin="VDDIO" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TLV70218DBVT" prefix="IC">
<description>&lt;b&gt;Texas Instruments TLV70218DBVT, LDO Voltage Regulator, 300mA, 1.8 V 2%, 2  5.5 Vin, 5-Pin SOT-23&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.ti.com/lit/gpn/tlv702"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="TLV70218DBVT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT95P280X145-5N">
<connects>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="IN" pad="1"/>
<connect gate="G$1" pin="NC" pad="4"/>
<connect gate="G$1" pin="OUT" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Texas Instruments TLV70218DBVT, LDO Voltage Regulator, 300mA, 1.8 V 2%, 2  5.5 Vin, 5-Pin SOT-23" constant="no"/>
<attribute name="HEIGHT" value="1.45mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Texas Instruments" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="TLV70218DBVT" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="595-TLV70218DBVT" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.co.uk/ProductDetail/Texas-Instruments/TLV70218DBVT?qs=zQ2yIHQ6k3yCTbRGDS6zAA%3D%3D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MTXDOT-EU1-A01-100">
<gates>
<gate name="G$1" symbol="MTXDOT-EU1-A01-100" x="5.08" y="5.08"/>
</gates>
<devices>
<device name="" package="MTXDOT-EU1-A01-100">
<connects>
<connect gate="G$1" pin="ANT1" pad="P$37"/>
<connect gate="G$1" pin="ANT2" pad="P$47"/>
<connect gate="G$1" pin="GND0" pad="P$1"/>
<connect gate="G$1" pin="GND1" pad="P$5"/>
<connect gate="G$1" pin="GND10" pad="P$42"/>
<connect gate="G$1" pin="GND11" pad="P$43"/>
<connect gate="G$1" pin="GND12" pad="P$44"/>
<connect gate="G$1" pin="GND13" pad="P$45"/>
<connect gate="G$1" pin="GND14" pad="P$46"/>
<connect gate="G$1" pin="GND15" pad="P$48"/>
<connect gate="G$1" pin="GND16" pad="P$49"/>
<connect gate="G$1" pin="GND17" pad="P$50"/>
<connect gate="G$1" pin="GND18" pad="P$51"/>
<connect gate="G$1" pin="GND19" pad="P$52"/>
<connect gate="G$1" pin="GND2" pad="P$17"/>
<connect gate="G$1" pin="GND20" pad="P$53"/>
<connect gate="G$1" pin="GND21" pad="P$54"/>
<connect gate="G$1" pin="GND22" pad="P$55"/>
<connect gate="G$1" pin="GND23" pad="P$56"/>
<connect gate="G$1" pin="GND24" pad="P$57"/>
<connect gate="G$1" pin="GND25" pad="P$58"/>
<connect gate="G$1" pin="GND3" pad="P$20"/>
<connect gate="G$1" pin="GND4" pad="P$35"/>
<connect gate="G$1" pin="GND5" pad="P$36"/>
<connect gate="G$1" pin="GND6" pad="P$38"/>
<connect gate="G$1" pin="GND7" pad="P$39"/>
<connect gate="G$1" pin="GND8" pad="P$40"/>
<connect gate="G$1" pin="GND9" pad="P$41"/>
<connect gate="G$1" pin="GPIO0" pad="P$26"/>
<connect gate="G$1" pin="GPIO1" pad="P$25"/>
<connect gate="G$1" pin="GPIO2" pad="P$24"/>
<connect gate="G$1" pin="GPIO3" pad="P$23"/>
<connect gate="G$1" pin="I2C0_SCL" pad="P$27"/>
<connect gate="G$1" pin="I2C0_SDA" pad="P$28"/>
<connect gate="G$1" pin="NRESET" pad="P$33"/>
<connect gate="G$1" pin="RFU1" pad="P$2"/>
<connect gate="G$1" pin="RFU2" pad="P$6"/>
<connect gate="G$1" pin="RFU3" pad="P$7"/>
<connect gate="G$1" pin="RFU4" pad="P$8"/>
<connect gate="G$1" pin="RFU5" pad="P$18"/>
<connect gate="G$1" pin="RFU6" pad="P$19"/>
<connect gate="G$1" pin="RFU7" pad="P$21"/>
<connect gate="G$1" pin="RFU8" pad="P$22"/>
<connect gate="G$1" pin="SPI_MISO" pad="P$12"/>
<connect gate="G$1" pin="SPI_MOSI" pad="P$11"/>
<connect gate="G$1" pin="SPI_NSS" pad="P$9"/>
<connect gate="G$1" pin="SPI_SCK" pad="P$10"/>
<connect gate="G$1" pin="SWCLK" pad="P$29"/>
<connect gate="G$1" pin="SWDIO" pad="P$30"/>
<connect gate="G$1" pin="UART0_RX" pad="P$16"/>
<connect gate="G$1" pin="UART0_TX" pad="P$15"/>
<connect gate="G$1" pin="UART1_CTS" pad="P$32"/>
<connect gate="G$1" pin="UART1_RTS" pad="P$31"/>
<connect gate="G$1" pin="UART1_RX" pad="P$14"/>
<connect gate="G$1" pin="UART1_TX" pad="P$13"/>
<connect gate="G$1" pin="VDD0" pad="P$3"/>
<connect gate="G$1" pin="VDD1" pad="P$4"/>
<connect gate="G$1" pin="WAKE" pad="P$34"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RM186-SM-02">
<gates>
<gate name="G$1" symbol="RM186-SM-02" x="-15.24" y="15.24"/>
</gates>
<devices>
<device name="" package="RM186-SM-02">
<connects>
<connect gate="G$1" pin="GND_1" pad="P$1"/>
<connect gate="G$1" pin="GND_2" pad="P$8"/>
<connect gate="G$1" pin="GND_3" pad="P$11"/>
<connect gate="G$1" pin="GND_4" pad="P$24"/>
<connect gate="G$1" pin="GND_5" pad="P$21"/>
<connect gate="G$1" pin="GND_6" pad="P$14"/>
<connect gate="G$1" pin="NC/SWCLK" pad="P$23"/>
<connect gate="G$1" pin="NRESET/SWDIO" pad="P$22"/>
<connect gate="G$1" pin="SIO_00/CLK" pad="P$15"/>
<connect gate="G$1" pin="SIO_17/MISO" pad="P$16"/>
<connect gate="G$1" pin="SIO_21/UART_TX" pad="P$2"/>
<connect gate="G$1" pin="SIO_22/UART_RX" pad="P$3"/>
<connect gate="G$1" pin="SIO_23/UART_RTS" pad="P$4"/>
<connect gate="G$1" pin="SIO_24/UART_CTS" pad="P$5"/>
<connect gate="G$1" pin="SIO_25" pad="P$6"/>
<connect gate="G$1" pin="SIO_28" pad="P$7"/>
<connect gate="G$1" pin="SIO_29/SCL" pad="P$9"/>
<connect gate="G$1" pin="SIO_3/AIN/MOSI" pad="P$17"/>
<connect gate="G$1" pin="SIO_30/SDA" pad="P$10"/>
<connect gate="G$1" pin="SIO_4/AIN" pad="P$18"/>
<connect gate="G$1" pin="SIO_5/AIN" pad="P$19"/>
<connect gate="G$1" pin="SIO_6/AIN" pad="P$20"/>
<connect gate="G$1" pin="VCC_BLE" pad="P$12"/>
<connect gate="G$1" pin="VCC_LORA" pad="P$13"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MAX1605EUT+T" prefix="IC">
<description>&lt;b&gt;Switching Voltage Regulators 30V Internal Switch LCD Bias Supply&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.mouser.de/datasheet/2/256/MAX1605-1507063.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="MAX1605EUT+T" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT95P280X145-6N">
<connects>
<connect gate="G$1" pin="!SHDN" pad="1"/>
<connect gate="G$1" pin="FB" pad="6"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="LIM" pad="5"/>
<connect gate="G$1" pin="LX" pad="4"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Switching Voltage Regulators 30V Internal Switch LCD Bias Supply" constant="no"/>
<attribute name="HEIGHT" value="1.45mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Maxim Integrated" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="MAX1605EUT+T" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="700-MAX1605EUT+T" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.co.uk/ProductDetail/Maxim-Integrated/MAX1605EUT%2bT?qs=EJX7GXAF8YOjNXFDYRZMDA%3D%3D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JUMPER_CLOSED" prefix="J">
<gates>
<gate name="G$1" symbol="SJ_CLOSED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JUMPER_CLOSED">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DAC80501ZDGST" prefix="IC">
<description>&lt;b&gt;Digital to Analog Converters - DAC True 16-bit, 1-ch, SPI/I2C, voltage-output DAC in WSON package with precision internal reference 10-VSSOP -40 to 125&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.ti.com/lit/ds/symlink/dac60501.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="DAC80501ZDGST" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP50P490X110-10N">
<connects>
<connect gate="G$1" pin="!SYNC!/A0" pad="7"/>
<connect gate="G$1" pin="AGND" pad="4"/>
<connect gate="G$1" pin="NC_1" pad="3"/>
<connect gate="G$1" pin="NC_2" pad="9"/>
<connect gate="G$1" pin="SCLK/SCL" pad="6"/>
<connect gate="G$1" pin="SDIN/SDA" pad="8"/>
<connect gate="G$1" pin="SPI2C" pad="5"/>
<connect gate="G$1" pin="VDD" pad="1"/>
<connect gate="G$1" pin="VOUT" pad="2"/>
<connect gate="G$1" pin="VREFIO" pad="10"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Digital to Analog Converters - DAC True 16-bit, 1-ch, SPI/I2C, voltage-output DAC in WSON package with precision internal reference 10-VSSOP -40 to 125" constant="no"/>
<attribute name="HEIGHT" value="1.1mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Texas Instruments" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="DAC80501ZDGST" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="595-DAC80501ZDGST" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.co.uk/ProductDetail/Texas-Instruments/DAC80501ZDGST?qs=%252B6g0mu59x7JqdHgN5ZkaXA%3D%3D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JUMPER_OPEN" prefix="J" uservalue="yes">
<description>SMD solder &lt;b&gt;JUMPER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="SJ_OPEN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JUMPER_OPEN">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15471/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="33" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Diode">
<description>&lt;B&gt;PN Junction, BridgeRectifier, Zener, Schottky, Switching</description>
<packages>
<package name="SODFL3718X115" urn="urn:adsk.eagle:footprint:9427170/1">
<description>SODFL, 3.70 mm span, 2.80 X 1.80 X 1.15 mm body
&lt;p&gt;SODFL package with 3.70 mm span with body size 2.80 X 1.80 X 1.15 mm&lt;/p&gt;</description>
<wire x1="1.45" y1="0.9946" x2="-2.4717" y2="0.9946" width="0.12" layer="21"/>
<wire x1="-2.4717" y1="0.9946" x2="-2.4717" y2="-0.9946" width="0.12" layer="21"/>
<wire x1="-2.4717" y1="-0.9946" x2="1.45" y2="-0.9946" width="0.12" layer="21"/>
<wire x1="1.45" y1="-0.95" x2="-1.45" y2="-0.95" width="0.12" layer="51"/>
<wire x1="-1.45" y1="-0.95" x2="-1.45" y2="0.95" width="0.12" layer="51"/>
<wire x1="-1.45" y1="0.95" x2="1.45" y2="0.95" width="0.12" layer="51"/>
<wire x1="1.45" y1="0.95" x2="1.45" y2="-0.95" width="0.12" layer="51"/>
<smd name="1" x="-1.6004" y="0" dx="1.1146" dy="1.3612" layer="1"/>
<smd name="2" x="1.6004" y="0" dx="1.1146" dy="1.3612" layer="1"/>
<text x="0" y="1.6296" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6296" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="SODFL3718X115" urn="urn:adsk.eagle:package:9427153/2" type="model">
<description>SODFL, 3.70 mm span, 2.80 X 1.80 X 1.15 mm body
&lt;p&gt;SODFL package with 3.70 mm span with body size 2.80 X 1.80 X 1.15 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SODFL3718X115"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SCHOTTKY" urn="urn:adsk.eagle:symbol:16378173/3">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.016" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<text x="0" y="3.175" size="1.778" layer="95" align="center">&gt;NAME</text>
<text x="0" y="-3.429" size="1.778" layer="95" align="center">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SCHOTTKY_" urn="urn:adsk.eagle:component:16494885/5" prefix="D">
<description>&lt;B&gt;Schottky Diode - Popular parts</description>
<gates>
<gate name="G$1" symbol="SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="DO-219" package="SODFL3718X115">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9427153/2"/>
</package3dinstances>
<technologies>
<technology name="MBR0520LT1G">
<attribute name="CATEGORY" value="Diode" constant="no"/>
<attribute name="DESCRIPTION" value="DIODE SCHOTTKY 20V 500MA SOD123" constant="no"/>
<attribute name="FORWARD_CURRENT" value="" constant="no"/>
<attribute name="MANUFACTURER" value="ON Semiconductor" constant="no"/>
<attribute name="MPN" value="MBR0520LT1G" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="REVERSE_VOLTAGE" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="Schottky" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VALUE" value="MBR0520LT1G" constant="no"/>
<attribute name="ZENER_VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="testpad" urn="urn:adsk.eagle:library:385">
<description>&lt;b&gt;Test Pins/Pads&lt;/b&gt;&lt;p&gt;
Cream on SMD OFF.&lt;br&gt;
new: Attribute TP_SIGNAL_NAME&lt;br&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="B1,27" urn="urn:adsk.eagle:footprint:27900/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<wire x1="-0.635" y1="0" x2="0.635" y2="0" width="0.0024" layer="37"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.0024" layer="37"/>
<smd name="TP" x="0" y="0" dx="1.27" dy="1.27" layer="1" roundness="100" cream="no"/>
<text x="-0.635" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-0.635" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="B2,54" urn="urn:adsk.eagle:footprint:27901/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<wire x1="-0.635" y1="0" x2="0.635" y2="0" width="0.0024" layer="37"/>
<wire x1="0" y1="-0.635" x2="0" y2="0.635" width="0.0024" layer="37"/>
<circle x="0" y="0" radius="0.635" width="0.254" layer="37"/>
<smd name="TP" x="0" y="0" dx="2.54" dy="2.54" layer="1" roundness="100" cream="no"/>
<text x="-1.27" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.397" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-3.175" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="P1-13" urn="urn:adsk.eagle:footprint:27902/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="TP" x="0" y="0" drill="1.3208" diameter="2.159" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
<rectangle x1="-0.3302" y1="-0.3302" x2="0.3302" y2="0.3302" layer="51"/>
</package>
<package name="P1-13Y" urn="urn:adsk.eagle:footprint:27903/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="TP" x="0" y="0" drill="1.3208" diameter="1.905" shape="long" rot="R90"/>
<text x="-0.889" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-3.81" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
<rectangle x1="-0.3302" y1="-0.3302" x2="0.3302" y2="0.3302" layer="51"/>
</package>
<package name="P1-17" urn="urn:adsk.eagle:footprint:27904/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.8128" width="0.1524" layer="51"/>
<pad name="TP" x="0" y="0" drill="1.7018" diameter="2.54" shape="octagon"/>
<text x="-1.143" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-3.175" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
<rectangle x1="-0.3302" y1="-0.3302" x2="0.3302" y2="0.3302" layer="51"/>
</package>
<package name="P1-17Y" urn="urn:adsk.eagle:footprint:27905/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.8128" width="0.1524" layer="51"/>
<pad name="TP" x="0" y="0" drill="1.7018" diameter="2.1208" shape="long" rot="R90"/>
<text x="-1.143" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-3.81" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
<rectangle x1="-0.3302" y1="-0.3302" x2="0.3302" y2="0.3302" layer="51"/>
</package>
<package name="P1-20" urn="urn:adsk.eagle:footprint:27906/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<circle x="0" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="TP" x="0" y="0" drill="2.0066" diameter="3.1496" shape="octagon"/>
<text x="-1.524" y="1.778" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-3.175" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
<rectangle x1="-0.3302" y1="-0.3302" x2="0.3302" y2="0.3302" layer="51"/>
</package>
<package name="P1-20Y" urn="urn:adsk.eagle:footprint:27907/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<circle x="0" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="TP" x="0" y="0" drill="2.0066" diameter="2.54" shape="long" rot="R90"/>
<text x="-1.27" y="2.794" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-4.445" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
<rectangle x1="-0.3302" y1="-0.3302" x2="0.3302" y2="0.3302" layer="51"/>
</package>
<package name="TP06R" urn="urn:adsk.eagle:footprint:27908/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.6" dy="0.6" layer="1" roundness="100" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.254" y="-0.381" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP06SQ" urn="urn:adsk.eagle:footprint:27909/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.5996" dy="0.5996" layer="1" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.254" y="-0.381" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP07R" urn="urn:adsk.eagle:footprint:27910/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.7" dy="0.7" layer="1" roundness="100" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.254" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP07SQ" urn="urn:adsk.eagle:footprint:27911/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.7" dy="0.7" layer="1" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.381" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP08R" urn="urn:adsk.eagle:footprint:27912/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.8" dy="0.8" layer="1" roundness="100" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.381" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP08SQ" urn="urn:adsk.eagle:footprint:27913/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.8" dy="0.8" layer="1" cream="no"/>
<text x="-0.3" y="0.4001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP09R" urn="urn:adsk.eagle:footprint:27914/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.9" dy="0.9" layer="1" roundness="100" cream="no"/>
<text x="-0.4501" y="0.5001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP09SQ" urn="urn:adsk.eagle:footprint:27915/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="0.8998" dy="0.8998" layer="1" cream="no"/>
<text x="-0.4501" y="0.5001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP10R" urn="urn:adsk.eagle:footprint:27916/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" cream="no"/>
<text x="-0.5001" y="0.5499" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP10SQ" urn="urn:adsk.eagle:footprint:27917/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1" dy="1" layer="1" cream="no"/>
<text x="-0.5001" y="0.5499" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.635" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP11R" urn="urn:adsk.eagle:footprint:27918/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.1" dy="1.1" layer="1" roundness="100" cream="no"/>
<text x="-0.5499" y="0.5999" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.508" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-1.905" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP11SQ" urn="urn:adsk.eagle:footprint:27919/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.1" dy="1.1" layer="1" cream="no"/>
<text x="-0.5499" y="0.5999" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.635" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP12SQ" urn="urn:adsk.eagle:footprint:27920/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.1998" dy="1.1998" layer="1" cream="no"/>
<text x="-0.5999" y="0.65" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.635" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP12R" urn="urn:adsk.eagle:footprint:27921/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.2" dy="1.2" layer="1" roundness="100" cream="no"/>
<text x="-0.5999" y="0.65" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.635" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP13R" urn="urn:adsk.eagle:footprint:27922/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.3" dy="1.3" layer="1" roundness="100" cream="no"/>
<text x="-0.65" y="0.7" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.635" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP14R" urn="urn:adsk.eagle:footprint:27923/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.4" dy="1.4" layer="1" roundness="100" cream="no"/>
<text x="-0.7" y="0.7501" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.508" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP15R" urn="urn:adsk.eagle:footprint:27924/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.5" dy="1.5" layer="1" roundness="100" cream="no"/>
<text x="-0.7501" y="0.8001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP16R" urn="urn:adsk.eagle:footprint:27925/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.6" dy="1.6" layer="1" roundness="100" cream="no"/>
<text x="-0.8001" y="0.8499" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP17R" urn="urn:adsk.eagle:footprint:27926/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.7" dy="1.7" layer="1" roundness="100" cream="no"/>
<text x="-0.8499" y="0.8999" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP18R" urn="urn:adsk.eagle:footprint:27927/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.8" dy="1.8" layer="1" roundness="100" cream="no"/>
<text x="-0.8999" y="0.95" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP19R" urn="urn:adsk.eagle:footprint:27928/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100" cream="no"/>
<text x="-0.95" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP20R" urn="urn:adsk.eagle:footprint:27929/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="2" dy="2" layer="1" roundness="100" cream="no"/>
<text x="-1" y="1.05" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-1.016" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP13SQ" urn="urn:adsk.eagle:footprint:27930/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.3" dy="1.3" layer="1" cream="no"/>
<text x="-0.65" y="0.7" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP14SQ" urn="urn:adsk.eagle:footprint:27931/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.4" dy="1.4" layer="1" cream="no"/>
<text x="-0.7" y="0.7501" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-0.762" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP15SQ" urn="urn:adsk.eagle:footprint:27932/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.5" dy="1.5" layer="1" cream="no"/>
<text x="-0.7501" y="0.8001" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP16SQ" urn="urn:adsk.eagle:footprint:27933/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.5996" dy="1.5996" layer="1" cream="no"/>
<text x="-0.8001" y="0.8499" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP17SQ" urn="urn:adsk.eagle:footprint:27934/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.7" dy="1.7" layer="1" cream="no"/>
<text x="-0.8499" y="0.8999" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-0.889" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP18SQ" urn="urn:adsk.eagle:footprint:27935/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.8" dy="1.8" layer="1" cream="no"/>
<text x="-0.8999" y="0.95" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-1.016" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP19SQ" urn="urn:adsk.eagle:footprint:27936/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="1.8998" dy="1.8998" layer="1" cream="no"/>
<text x="-0.95" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-1.016" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="TP20SQ" urn="urn:adsk.eagle:footprint:27937/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<smd name="TP" x="0" y="0" dx="2" dy="2" layer="1" cream="no"/>
<text x="-1" y="1.05" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.0254" layer="27">&gt;VALUE</text>
<text x="0" y="-2.54" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
</packages>
<packages3d>
<package3d name="B1,27" urn="urn:adsk.eagle:package:27944/2" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="B1,27"/>
</packageinstances>
</package3d>
<package3d name="B2,54" urn="urn:adsk.eagle:package:27948/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="B2,54"/>
</packageinstances>
</package3d>
<package3d name="P1-13" urn="urn:adsk.eagle:package:27946/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="P1-13"/>
</packageinstances>
</package3d>
<package3d name="P1-13Y" urn="urn:adsk.eagle:package:27947/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="P1-13Y"/>
</packageinstances>
</package3d>
<package3d name="P1-17" urn="urn:adsk.eagle:package:27949/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="P1-17"/>
</packageinstances>
</package3d>
<package3d name="P1-17Y" urn="urn:adsk.eagle:package:27953/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="P1-17Y"/>
</packageinstances>
</package3d>
<package3d name="P1-20" urn="urn:adsk.eagle:package:27950/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="P1-20"/>
</packageinstances>
</package3d>
<package3d name="P1-20Y" urn="urn:adsk.eagle:package:27951/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="P1-20Y"/>
</packageinstances>
</package3d>
<package3d name="TP06R" urn="urn:adsk.eagle:package:27954/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP06R"/>
</packageinstances>
</package3d>
<package3d name="TP06SQ" urn="urn:adsk.eagle:package:27952/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP06SQ"/>
</packageinstances>
</package3d>
<package3d name="TP07R" urn="urn:adsk.eagle:package:27970/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP07R"/>
</packageinstances>
</package3d>
<package3d name="TP07SQ" urn="urn:adsk.eagle:package:27955/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP07SQ"/>
</packageinstances>
</package3d>
<package3d name="TP08R" urn="urn:adsk.eagle:package:27956/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP08R"/>
</packageinstances>
</package3d>
<package3d name="TP08SQ" urn="urn:adsk.eagle:package:27960/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP08SQ"/>
</packageinstances>
</package3d>
<package3d name="TP09R" urn="urn:adsk.eagle:package:27958/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP09R"/>
</packageinstances>
</package3d>
<package3d name="TP09SQ" urn="urn:adsk.eagle:package:27957/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP09SQ"/>
</packageinstances>
</package3d>
<package3d name="TP10R" urn="urn:adsk.eagle:package:27959/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP10R"/>
</packageinstances>
</package3d>
<package3d name="TP10SQ" urn="urn:adsk.eagle:package:27962/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP10SQ"/>
</packageinstances>
</package3d>
<package3d name="TP11R" urn="urn:adsk.eagle:package:27961/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP11R"/>
</packageinstances>
</package3d>
<package3d name="TP11SQ" urn="urn:adsk.eagle:package:27965/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP11SQ"/>
</packageinstances>
</package3d>
<package3d name="TP12SQ" urn="urn:adsk.eagle:package:27964/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP12SQ"/>
</packageinstances>
</package3d>
<package3d name="TP12R" urn="urn:adsk.eagle:package:27963/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP12R"/>
</packageinstances>
</package3d>
<package3d name="TP13R" urn="urn:adsk.eagle:package:27967/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP13R"/>
</packageinstances>
</package3d>
<package3d name="TP14R" urn="urn:adsk.eagle:package:27966/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP14R"/>
</packageinstances>
</package3d>
<package3d name="TP15R" urn="urn:adsk.eagle:package:27968/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP15R"/>
</packageinstances>
</package3d>
<package3d name="TP16R" urn="urn:adsk.eagle:package:27969/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP16R"/>
</packageinstances>
</package3d>
<package3d name="TP17R" urn="urn:adsk.eagle:package:27971/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP17R"/>
</packageinstances>
</package3d>
<package3d name="TP18R" urn="urn:adsk.eagle:package:27981/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP18R"/>
</packageinstances>
</package3d>
<package3d name="TP19R" urn="urn:adsk.eagle:package:27972/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP19R"/>
</packageinstances>
</package3d>
<package3d name="TP20R" urn="urn:adsk.eagle:package:27973/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP20R"/>
</packageinstances>
</package3d>
<package3d name="TP13SQ" urn="urn:adsk.eagle:package:27974/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP13SQ"/>
</packageinstances>
</package3d>
<package3d name="TP14SQ" urn="urn:adsk.eagle:package:27984/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP14SQ"/>
</packageinstances>
</package3d>
<package3d name="TP15SQ" urn="urn:adsk.eagle:package:27975/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP15SQ"/>
</packageinstances>
</package3d>
<package3d name="TP16SQ" urn="urn:adsk.eagle:package:27976/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP16SQ"/>
</packageinstances>
</package3d>
<package3d name="TP17SQ" urn="urn:adsk.eagle:package:27977/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP17SQ"/>
</packageinstances>
</package3d>
<package3d name="TP18SQ" urn="urn:adsk.eagle:package:27979/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP18SQ"/>
</packageinstances>
</package3d>
<package3d name="TP19SQ" urn="urn:adsk.eagle:package:27978/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP19SQ"/>
</packageinstances>
</package3d>
<package3d name="TP20SQ" urn="urn:adsk.eagle:package:27980/1" type="box" library_version="3">
<description>TEST PAD</description>
<packageinstances>
<packageinstance name="TP20SQ"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="TP" urn="urn:adsk.eagle:symbol:27940/1" library_version="3">
<wire x1="-0.762" y1="-0.762" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="-0.762" x2="0" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0" y1="-1.524" x2="-0.762" y2="-0.762" width="0.254" layer="94"/>
<text x="-1.27" y="1.27" size="1.778" layer="95">&gt;NAME</text>
<text x="1.27" y="-1.27" size="1.778" layer="97">&gt;TP_SIGNAL_NAME</text>
<pin name="TP" x="0" y="-2.54" visible="off" length="short" direction="in" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TP" urn="urn:adsk.eagle:component:27992/3" prefix="TP" library_version="3">
<description>&lt;b&gt;Test pad&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="TP" x="0" y="0"/>
</gates>
<devices>
<device name="B1,27" package="B1,27">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27944/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="19" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="B2,54" package="B2,54">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27948/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="PAD1-13" package="P1-13">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27946/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="12" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="PAD1-13Y" package="P1-13Y">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27947/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="PAD1-17" package="P1-17">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27949/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="PAD1-17Y" package="P1-17Y">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27953/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="PAD1-20" package="P1-20">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27950/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="PAD1-20Y" package="P1-20Y">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27951/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP06R" package="TP06R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27954/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP06SQ" package="TP06SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27952/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP07R" package="TP07R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27970/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP07SQ" package="TP07SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27955/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP08R" package="TP08R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27956/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP08SQ" package="TP08SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27960/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP09R" package="TP09R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27958/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP09SQ" package="TP09SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27957/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP10R" package="TP10R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27959/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP10SQ" package="TP10SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27962/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="4" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP11R" package="TP11R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27961/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP11SQ" package="TP11SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27965/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP12SQ" package="TP12SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27964/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP12R" package="TP12R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27963/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP13R" package="TP13R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27967/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP14R" package="TP14R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27966/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP15R" package="TP15R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27968/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP16R" package="TP16R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27969/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP17R" package="TP17R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27971/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP18R" package="TP18R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27981/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP19R" package="TP19R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27972/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP20R" package="TP20R">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27973/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP13SQ" package="TP13SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27974/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP14SQ" package="TP14SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27984/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP15SQ" package="TP15SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27975/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP16SQ" package="TP16SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27976/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP17SQ" package="TP17SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27977/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP18SQ" package="TP18SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27979/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP19SQ" package="TP19SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27978/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TP20SQ" package="TP20SQ">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:27980/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
<attribute name="TP_SIGNAL_NAME" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-amp-quick" urn="urn:adsk.eagle:library:125">
<description>&lt;b&gt;AMP Connectors, Type QUICK&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="04P" urn="urn:adsk.eagle:footprint:5913/1" library_version="2">
<description>&lt;b&gt;AMP QUICK CONNECTOR&lt;/b&gt;</description>
<wire x1="-2.921" y1="1.778" x2="-2.794" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="1.651" x2="-2.54" y2="1.397" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.397" x2="-2.286" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.651" x2="-2.159" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="-2.159" x2="-2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="-2.286" x2="-4.826" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-2.159" x2="-2.794" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-2.159" x2="-2.286" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.54" x2="-4.826" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="1.778" x2="-4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.778" x2="-4.826" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.778" x2="-4.445" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.778" x2="-3.175" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.778" x2="-4.445" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.524" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.778" x2="-1.905" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.524" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.524" x2="-2.794" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.524" x2="-1.905" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.524" x2="-4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.524" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="-4.826" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.54" x2="-4.064" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="2.54" x2="-4.064" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="2.54" x2="-3.556" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="2.667" x2="-4.064" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.54" x2="-1.524" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.54" x2="-1.016" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="2.413" x2="-2.286" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.413" x2="-2.794" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-2.159" x2="-0.254" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-2.159" x2="0.254" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-2.286" x2="0.254" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.778" x2="1.905" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.778" x2="-0.635" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.524" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.778" x2="0.635" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.524" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.524" x2="-0.254" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-1.524" x2="0.254" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-1.524" x2="0.635" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.524" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.54" x2="1.016" y2="2.667" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.54" x2="2.286" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.54" x2="1.524" y2="2.667" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.667" x2="1.016" y2="2.667" width="0.1524" layer="21"/>
<wire x1="0.254" y1="2.413" x2="0.254" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.254" y1="2.54" x2="1.016" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.254" y1="2.413" x2="-0.254" y2="2.413" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="2.413" x2="-0.254" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.54" x2="-0.254" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.667" x2="-1.524" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="2.54" x2="-1.524" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.413" x2="-2.286" y2="2.413" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.54" x2="-3.556" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.778" x2="-4.826" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.778" x2="-0.254" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.778" x2="0.254" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.651" x2="-2.286" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="1.651" x2="-2.794" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="1.778" x2="-0.254" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="0.254" y1="1.778" x2="0.254" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.778" x2="-3.175" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-2.286" x2="-0.254" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.778" x2="0.635" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-2.159" x2="-0.254" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.778" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-1.778" x2="4.826" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.794" y1="-2.159" x2="2.286" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-2.159" x2="2.286" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.794" y1="-2.159" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.286" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-1.778" x2="4.445" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.778" x2="4.445" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.524" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.778" x2="3.175" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.524" x2="2.286" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.524" x2="2.794" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.794" y1="-1.524" x2="3.175" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.524" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.794" y1="1.651" x2="2.794" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.651" x2="2.54" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.651" x2="2.286" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.778" x2="2.286" y2="1.651" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.397" x2="2.921" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.778" x2="4.826" y2="1.778" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.778" x2="3.175" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-1.778" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.286" y1="2.413" x2="2.286" y2="2.54" width="0.1524" layer="21"/>
<wire x1="2.794" y1="2.413" x2="2.286" y2="2.413" width="0.1524" layer="21"/>
<wire x1="2.794" y1="2.54" x2="3.556" y2="2.54" width="0.1524" layer="21"/>
<wire x1="2.794" y1="2.413" x2="2.794" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.556" y1="2.54" x2="3.556" y2="2.667" width="0.1524" layer="21"/>
<wire x1="4.064" y1="2.667" x2="3.556" y2="2.667" width="0.1524" layer="21"/>
<wire x1="4.064" y1="2.54" x2="4.064" y2="2.667" width="0.1524" layer="21"/>
<wire x1="4.064" y1="2.54" x2="4.826" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.54" x2="4.826" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.254" y1="1.778" x2="2.159" y2="1.778" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="0.9144" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="0.9144" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="0.9144" shape="long" rot="R90"/>
<text x="-3.302" y="0.9398" size="0.9906" layer="21" ratio="12" rot="R90">1</text>
<text x="-4.826" y="2.9464" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.8354" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.1402" y1="-0.3302" x2="-3.4798" y2="0.3302" layer="51"/>
<rectangle x1="-1.6002" y1="-0.3302" x2="-0.9398" y2="0.3302" layer="51"/>
<rectangle x1="0.9398" y1="-0.3302" x2="1.6002" y2="0.3302" layer="51"/>
<rectangle x1="3.4798" y1="-0.3302" x2="4.1402" y2="0.3302" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="04P" urn="urn:adsk.eagle:package:5958/1" type="box" library_version="2">
<description>AMP QUICK CONNECTOR</description>
<packageinstances>
<packageinstance name="04P"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="M04" urn="urn:adsk.eagle:symbol:5918/1" library_version="2">
<wire x1="1.27" y1="-5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<text x="-5.08" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="M04" urn="urn:adsk.eagle:component:5989/2" prefix="SL" uservalue="yes" library_version="2">
<description>&lt;b&gt;AMP QUICK CONNECTOR&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="M04" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="04P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5958/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
<attribute name="POPULARITY" value="19" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead" urn="urn:adsk.eagle:library:325">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1X02" urn="urn:adsk.eagle:footprint:22309/1" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
<package name="1X02/90" urn="urn:adsk.eagle:footprint:22310/1" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-3.175" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="4.445" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="1X02" urn="urn:adsk.eagle:package:22435/2" type="model" library_version="4">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="1X02"/>
</packageinstances>
</package3d>
<package3d name="1X02/90" urn="urn:adsk.eagle:package:22437/2" type="model" library_version="4">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="1X02/90"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PINHD2" urn="urn:adsk.eagle:symbol:22308/1" library_version="4">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X2" urn="urn:adsk.eagle:component:22516/4" prefix="JP" uservalue="yes" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22435/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="98" constant="no"/>
</technology>
</technologies>
</device>
<device name="/90" package="1X02/90">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22437/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="24" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="holes" urn="urn:adsk.eagle:library:237">
<description>&lt;b&gt;Mounting Holes and Pads&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="2,8-PAD" urn="urn:adsk.eagle:footprint:14250/1" library_version="2">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 2.8 mm, round</description>
<wire x1="0" y1="2.921" x2="0" y2="2.667" width="0.0508" layer="21"/>
<wire x1="0" y1="-2.667" x2="0" y2="-2.921" width="0.0508" layer="21"/>
<wire x1="-1.778" y1="0" x2="0" y2="-1.778" width="2.286" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="1.778" x2="1.778" y2="0" width="2.286" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="0.635" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="2.921" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.8128" layer="39"/>
<circle x="0" y="0" radius="3.175" width="0.8128" layer="40"/>
<circle x="0" y="0" radius="3.175" width="0.8128" layer="43"/>
<circle x="0" y="0" radius="1.5" width="0.2032" layer="21"/>
<pad name="B2,8" x="0" y="0" drill="2.8" diameter="5.334"/>
</package>
<package name="3,0-PAD" urn="urn:adsk.eagle:footprint:14251/1" library_version="2">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.0 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="39"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.6" width="0.2032" layer="21"/>
<pad name="B3,0" x="0" y="0" drill="3" diameter="5.842"/>
<text x="-1.27" y="-3.81" size="1.27" layer="48">3,0</text>
</package>
<package name="3,2-PAD" urn="urn:adsk.eagle:footprint:14252/1" library_version="2">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.2 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="39"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.7" width="0.1524" layer="21"/>
<pad name="B3,2" x="0" y="0" drill="3.2" diameter="5.842"/>
<text x="-1.27" y="-3.81" size="1.27" layer="48">3,2</text>
</package>
<package name="3,3-PAD" urn="urn:adsk.eagle:footprint:14253/1" library_version="2">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.3 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="39"/>
<circle x="0" y="0" radius="3.683" width="1.27" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.7" width="0.2032" layer="21"/>
<pad name="B3,3" x="0" y="0" drill="3.3" diameter="5.842"/>
</package>
<package name="3,6-PAD" urn="urn:adsk.eagle:footprint:14254/1" library_version="2">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 3.6 mm, round</description>
<wire x1="-2.159" y1="0" x2="0" y2="-2.159" width="2.4892" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.159" x2="2.159" y2="0" width="2.4892" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="3.429" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="3.683" width="1.397" layer="39"/>
<circle x="0" y="0" radius="3.683" width="1.397" layer="40"/>
<circle x="0" y="0" radius="3.556" width="1.016" layer="43"/>
<circle x="0" y="0" radius="1.9" width="0.2032" layer="21"/>
<pad name="B3,6" x="0" y="0" drill="3.6" diameter="5.842"/>
</package>
<package name="4,1-PAD" urn="urn:adsk.eagle:footprint:14255/1" library_version="2">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 4.1 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="5.08" width="2" layer="43"/>
<circle x="0" y="0" radius="2.15" width="0.2032" layer="21"/>
<pad name="B4,1" x="0" y="0" drill="4.1" diameter="8"/>
</package>
<package name="4,3-PAD" urn="urn:adsk.eagle:footprint:14256/1" library_version="2">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 4.3 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="4.4958" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="5.588" width="2" layer="43"/>
<circle x="0" y="0" radius="5.588" width="2" layer="39"/>
<circle x="0" y="0" radius="5.588" width="2" layer="40"/>
<circle x="0" y="0" radius="2.25" width="0.1524" layer="21"/>
<pad name="B4,3" x="0" y="0" drill="4.3" diameter="9"/>
</package>
<package name="4,5-PAD" urn="urn:adsk.eagle:footprint:14257/1" library_version="2">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 4.5 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="4.4958" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="5.588" width="2" layer="43"/>
<circle x="0" y="0" radius="5.588" width="2" layer="39"/>
<circle x="0" y="0" radius="5.588" width="2" layer="40"/>
<circle x="0" y="0" radius="2.35" width="0.1524" layer="21"/>
<pad name="B4,5" x="0" y="0" drill="4.5" diameter="9"/>
</package>
<package name="5,0-PAD" urn="urn:adsk.eagle:footprint:14258/1" library_version="2">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 5.0 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="4.4958" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="5.588" width="2" layer="43"/>
<circle x="0" y="0" radius="5.588" width="2" layer="39"/>
<circle x="0" y="0" radius="5.588" width="2" layer="40"/>
<circle x="0" y="0" radius="2.6" width="0.1524" layer="21"/>
<pad name="B5" x="0" y="0" drill="5" diameter="9"/>
</package>
<package name="5,5-PAD" urn="urn:adsk.eagle:footprint:14259/1" library_version="2">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt; 5.5 mm, round</description>
<wire x1="-2.54" y1="0" x2="0" y2="-2.54" width="3.9116" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="3.9116" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="4.4958" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.762" width="0.4572" layer="51"/>
<circle x="0" y="0" radius="5.588" width="2" layer="43"/>
<circle x="0" y="0" radius="5.588" width="2" layer="39"/>
<circle x="0" y="0" radius="5.588" width="2" layer="40"/>
<circle x="0" y="0" radius="2.85" width="0.1524" layer="21"/>
<pad name="B5,5" x="0" y="0" drill="5.5" diameter="9"/>
</package>
</packages>
<packages3d>
<package3d name="2,8-PAD" urn="urn:adsk.eagle:package:14281/1" type="box" library_version="2">
<description>MOUNTING PAD 2.8 mm, round</description>
<packageinstances>
<packageinstance name="2,8-PAD"/>
</packageinstances>
</package3d>
<package3d name="3,0-PAD" urn="urn:adsk.eagle:package:14280/1" type="box" library_version="2">
<description>MOUNTING PAD 3.0 mm, round</description>
<packageinstances>
<packageinstance name="3,0-PAD"/>
</packageinstances>
</package3d>
<package3d name="3,2-PAD" urn="urn:adsk.eagle:package:14282/1" type="box" library_version="2">
<description>MOUNTING PAD 3.2 mm, round</description>
<packageinstances>
<packageinstance name="3,2-PAD"/>
</packageinstances>
</package3d>
<package3d name="3,3-PAD" urn="urn:adsk.eagle:package:14283/1" type="box" library_version="2">
<description>MOUNTING PAD 3.3 mm, round</description>
<packageinstances>
<packageinstance name="3,3-PAD"/>
</packageinstances>
</package3d>
<package3d name="3,6-PAD" urn="urn:adsk.eagle:package:14284/1" type="box" library_version="2">
<description>MOUNTING PAD 3.6 mm, round</description>
<packageinstances>
<packageinstance name="3,6-PAD"/>
</packageinstances>
</package3d>
<package3d name="4,1-PAD" urn="urn:adsk.eagle:package:14285/1" type="box" library_version="2">
<description>MOUNTING PAD 4.1 mm, round</description>
<packageinstances>
<packageinstance name="4,1-PAD"/>
</packageinstances>
</package3d>
<package3d name="4,3-PAD" urn="urn:adsk.eagle:package:14286/1" type="box" library_version="2">
<description>MOUNTING PAD 4.3 mm, round</description>
<packageinstances>
<packageinstance name="4,3-PAD"/>
</packageinstances>
</package3d>
<package3d name="4,5-PAD" urn="urn:adsk.eagle:package:14287/1" type="box" library_version="2">
<description>MOUNTING PAD 4.5 mm, round</description>
<packageinstances>
<packageinstance name="4,5-PAD"/>
</packageinstances>
</package3d>
<package3d name="5,0-PAD" urn="urn:adsk.eagle:package:14288/1" type="box" library_version="2">
<description>MOUNTING PAD 5.0 mm, round</description>
<packageinstances>
<packageinstance name="5,0-PAD"/>
</packageinstances>
</package3d>
<package3d name="5,5-PAD" urn="urn:adsk.eagle:package:14291/1" type="box" library_version="2">
<description>MOUNTING PAD 5.5 mm, round</description>
<packageinstances>
<packageinstance name="5,5-PAD"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="MOUNT-PAD" urn="urn:adsk.eagle:symbol:14249/1" library_version="2">
<wire x1="0.254" y1="2.032" x2="2.032" y2="0.254" width="1.016" layer="94" curve="-75.749967" cap="flat"/>
<wire x1="-2.032" y1="0.254" x2="-0.254" y2="2.032" width="1.016" layer="94" curve="-75.749967" cap="flat"/>
<wire x1="-2.032" y1="-0.254" x2="-0.254" y2="-2.032" width="1.016" layer="94" curve="75.749967" cap="flat"/>
<wire x1="0.254" y1="-2.032" x2="2.032" y2="-0.254" width="1.016" layer="94" curve="75.749967" cap="flat"/>
<circle x="0" y="0" radius="1.524" width="0.0508" layer="94"/>
<text x="2.794" y="0.5842" size="1.778" layer="95">&gt;NAME</text>
<text x="2.794" y="-2.4638" size="1.778" layer="96">&gt;VALUE</text>
<pin name="MOUNT" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MOUNT-PAD-ROUND" urn="urn:adsk.eagle:component:14303/2" prefix="H" library_version="2">
<description>&lt;b&gt;MOUNTING PAD&lt;/b&gt;, round</description>
<gates>
<gate name="G$1" symbol="MOUNT-PAD" x="0" y="0"/>
</gates>
<devices>
<device name="2.8" package="2,8-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B2,8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14281/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="6" constant="no"/>
</technology>
</technologies>
</device>
<device name="3.0" package="3,0-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,0"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14280/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="17" constant="no"/>
</technology>
</technologies>
</device>
<device name="3.2" package="3,2-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14282/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="4" constant="no"/>
</technology>
</technologies>
</device>
<device name="3.3" package="3,3-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14283/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
</technology>
</technologies>
</device>
<device name="3.6" package="3,6-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B3,6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14284/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="4.1" package="4,1-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B4,1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14285/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="4.3" package="4,3-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B4,3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14286/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="4.5" package="4,5-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B4,5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14287/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="5.0" package="5,0-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14288/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="5.5" package="5,5-PAD">
<connects>
<connect gate="G$1" pin="MOUNT" pad="B5,5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:14291/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-subd" urn="urn:adsk.eagle:library:189">
<description>&lt;b&gt;SUB-D Connectors&lt;/b&gt;&lt;p&gt;
Harting&lt;br&gt;
NorComp&lt;br&gt;
&lt;p&gt;
PREFIX :&lt;br&gt;
H = High density&lt;br&gt;
F = Female&lt;br&gt;
M = Male&lt;p&gt;
NUMBER: Number of pins&lt;p&gt;
SUFFIX :&lt;br&gt;
H = Horizontal&lt;br&gt;
V = Vertical &lt;br&gt;
P = Shield pin on housing&lt;br&gt;
B = No mounting holes&lt;br&gt;
S = Pins individually placeable (Single)&lt;br&gt;
D = Direct mounting &lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="F09VP" urn="urn:adsk.eagle:footprint:10103/1" library_version="2">
<description>&lt;b&gt;SUB-D&lt;/b&gt;</description>
<wire x1="-7.5184" y1="-2.9464" x2="-8.3058" y2="2.3368" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-3.937" x2="7.5317" y2="-2.905" width="0.1524" layer="21" curve="76.489196"/>
<wire x1="6.985" y1="3.937" x2="8.3005" y2="2.3038" width="0.1524" layer="21" curve="-102.298925"/>
<wire x1="8.3058" y1="2.3114" x2="7.5184" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.937" x2="6.985" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.3051" y1="2.3268" x2="-6.985" y2="3.937" width="0.1524" layer="21" curve="-101.30773"/>
<wire x1="-7.5259" y1="-2.9295" x2="-6.223" y2="-3.937" width="0.1524" layer="21" curve="75.428151"/>
<wire x1="-6.223" y1="-3.937" x2="6.223" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-6.223" x2="-12.7" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-6.223" x2="-10.16" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-6.096" x2="-10.16" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-6.096" x2="-10.16" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-6.223" x2="10.16" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-14.859" y1="-6.223" x2="-12.7" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="-5.588" x2="-14.859" y2="-6.223" width="0.1524" layer="21" curve="90"/>
<wire x1="14.859" y1="-6.223" x2="15.494" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="14.859" y1="6.223" x2="12.7" y2="6.223" width="0.1524" layer="21"/>
<wire x1="15.494" y1="5.588" x2="15.494" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="14.859" y1="6.223" x2="15.494" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-15.494" y1="5.588" x2="-15.494" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.494" y1="5.588" x2="-14.859" y2="6.223" width="0.1524" layer="21" curve="-90"/>
<wire x1="10.16" y1="-6.223" x2="10.16" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-6.223" x2="12.7" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-6.096" x2="12.7" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-6.096" x2="12.7" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-6.223" x2="14.859" y2="-6.223" width="0.1524" layer="21"/>
<wire x1="10.16" y1="6.223" x2="10.16" y2="6.096" width="0.1524" layer="21"/>
<wire x1="10.16" y1="6.223" x2="-10.16" y2="6.223" width="0.1524" layer="21"/>
<wire x1="10.16" y1="6.096" x2="12.7" y2="6.096" width="0.1524" layer="21"/>
<wire x1="12.7" y1="6.096" x2="12.7" y2="6.223" width="0.1524" layer="21"/>
<wire x1="12.7" y1="6.223" x2="10.16" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="6.223" x2="-12.7" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="6.223" x2="-14.859" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="6.096" x2="-10.16" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="6.096" x2="-10.16" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="6.223" x2="-12.7" y2="6.223" width="0.1524" layer="21"/>
<circle x="1.3716" y="-1.4224" radius="0.762" width="0.254" layer="51"/>
<circle x="2.7432" y="1.4224" radius="0.762" width="0.254" layer="51"/>
<circle x="4.1148" y="-1.4224" radius="0.762" width="0.254" layer="51"/>
<circle x="5.4864" y="1.4224" radius="0.762" width="0.254" layer="51"/>
<circle x="0" y="1.4224" radius="0.762" width="0.254" layer="51"/>
<circle x="-1.3716" y="-1.4224" radius="0.762" width="0.254" layer="51"/>
<circle x="-2.7432" y="1.4224" radius="0.762" width="0.254" layer="51"/>
<circle x="-4.1148" y="-1.4224" radius="0.762" width="0.254" layer="51"/>
<circle x="-5.4864" y="1.4224" radius="0.762" width="0.254" layer="51"/>
<circle x="-12.5222" y="0" radius="1.651" width="0.1524" layer="21"/>
<circle x="12.5222" y="0" radius="1.651" width="0.1524" layer="21"/>
<pad name="1" x="5.4864" y="1.4224" drill="1.016" shape="octagon"/>
<pad name="2" x="2.7432" y="1.4224" drill="1.016" shape="octagon"/>
<pad name="3" x="0" y="1.4224" drill="1.016" shape="octagon"/>
<pad name="4" x="-2.7432" y="1.4224" drill="1.016" shape="octagon"/>
<pad name="5" x="-5.4864" y="1.4224" drill="1.016" shape="octagon"/>
<pad name="6" x="4.1148" y="-1.4224" drill="1.016" shape="octagon"/>
<pad name="7" x="1.3716" y="-1.4224" drill="1.016" shape="octagon"/>
<pad name="8" x="-1.3716" y="-1.4224" drill="1.016" shape="octagon"/>
<pad name="9" x="-4.1148" y="-1.4224" drill="1.016" shape="octagon"/>
<pad name="G1" x="-12.5222" y="0" drill="3.302" diameter="5.08"/>
<pad name="G2" x="12.5222" y="0" drill="3.302" diameter="5.08"/>
<text x="5.08" y="2.54" size="0.9906" layer="21" ratio="12">1</text>
<text x="2.286" y="2.54" size="0.9906" layer="21" ratio="12">2</text>
<text x="-0.508" y="2.54" size="0.9906" layer="21" ratio="12">3</text>
<text x="-3.175" y="2.54" size="0.9906" layer="21" ratio="12">4</text>
<text x="-5.969" y="2.54" size="0.9906" layer="21" ratio="12">5</text>
<text x="3.81" y="-3.556" size="0.9906" layer="21" ratio="12">6</text>
<text x="1.016" y="-3.556" size="0.9906" layer="21" ratio="12">7</text>
<text x="-1.778" y="-3.556" size="0.9906" layer="21" ratio="12">8</text>
<text x="-4.572" y="-3.556" size="0.9906" layer="21" ratio="12">9</text>
<text x="-15.367" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="6.985" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="F09VP" urn="urn:adsk.eagle:package:10230/1" type="box" library_version="2">
<description>SUB-D</description>
<packageinstances>
<packageinstance name="F09VP"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="F09G" urn="urn:adsk.eagle:symbol:10101/1" library_version="2">
<wire x1="-1.651" y1="3.429" x2="-1.651" y2="1.651" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="1.524" y1="1.651" x2="1.524" y2="3.429" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-1.651" y1="5.969" x2="-1.651" y2="4.191" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="1.524" y1="-3.429" x2="1.524" y2="-1.651" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-1.651" y1="0.889" x2="-1.651" y2="-0.889" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="1.524" y1="-0.889" x2="1.524" y2="0.889" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-1.651" y1="-1.651" x2="-1.651" y2="-3.429" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="1.524" y1="-5.969" x2="1.524" y2="-4.191" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-1.651" y1="-4.191" x2="-1.651" y2="-5.969" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-4.064" y1="6.9312" x2="-2.5226" y2="8.172" width="0.4064" layer="94" curve="-102.324066" cap="flat"/>
<wire x1="-2.5226" y1="8.1718" x2="0" y2="7.62" width="0.4064" layer="94"/>
<wire x1="0" y1="7.62" x2="3.0654" y2="6.9494" width="0.4064" layer="94"/>
<wire x1="3.0654" y1="6.9495" x2="4.0642" y2="5.7088" width="0.4064" layer="94" curve="-77.655139" cap="flat"/>
<wire x1="4.064" y1="-5.7088" x2="4.064" y2="5.7088" width="0.4064" layer="94"/>
<wire x1="3.0654" y1="-6.9494" x2="4.064" y2="-5.7088" width="0.4064" layer="94" curve="77.657889"/>
<wire x1="-4.064" y1="-6.9312" x2="-4.064" y2="6.9312" width="0.4064" layer="94"/>
<wire x1="-2.5226" y1="-8.1718" x2="0" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="0" y1="-7.62" x2="3.0654" y2="-6.9494" width="0.4064" layer="94"/>
<wire x1="-4.064" y1="-6.9312" x2="-2.5226" y2="-8.1719" width="0.4064" layer="94" curve="102.337599" cap="flat"/>
<wire x1="2.54" y1="7.62" x2="0" y2="7.62" width="0.1524" layer="94"/>
<wire x1="0" y1="-7.62" x2="2.54" y2="-7.62" width="0.1524" layer="94"/>
<circle x="0" y="7.62" radius="0.254" width="0.6096" layer="94"/>
<circle x="0" y="-7.62" radius="0.254" width="0.6096" layer="94"/>
<text x="-3.81" y="-10.795" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="8.89" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas"/>
<pin name="6" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="2" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas"/>
<pin name="7" x="7.62" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="3" x="-7.62" y="0" visible="pad" length="middle" direction="pas"/>
<pin name="8" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="4" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas"/>
<pin name="9" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="5" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas"/>
<pin name="G1" x="7.62" y="7.62" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="G2" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="F09VP" urn="urn:adsk.eagle:component:10327/2" prefix="X" uservalue="yes" library_version="2">
<description>&lt;b&gt;SUB-D&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="F09G" x="0" y="0"/>
</gates>
<devices>
<device name="" package="F09VP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
<connect gate="G$1" pin="G1" pad="G1"/>
<connect gate="G$1" pin="G2" pad="G2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:10230/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
<attribute name="POPULARITY" value="2" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="customParts" urn="urn:adsk.wipprod:fs.file:vf.SX5B0h59S26sooymxZ0hvA">
<packages>
<package name="2X06" urn="urn:adsk.eagle:footprint:22364/1" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-7.62" y1="-1.905" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-2.54" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-2.54" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="-1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="11" x="6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="12" x="6.35" y="1.27" drill="1.016" shape="octagon"/>
<text x="-7.62" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-7.62" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
</package>
<package name="2X06/90" urn="urn:adsk.eagle:footprint:22365/1" library_version="4">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-7.62" y1="-1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="6.985" x2="-6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="6.985" x2="6.35" y2="1.27" width="0.762" layer="21"/>
<pad name="2" x="-6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="4" x="-3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="6" x="-1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="8" x="1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="10" x="3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="12" x="6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="1" x="-6.35" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="3" x="-3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="5" x="-1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="7" x="1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="9" x="3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="11" x="6.35" y="-6.35" drill="1.016" shape="octagon"/>
<text x="-8.255" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="9.525" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-6.731" y1="0.635" x2="-5.969" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.731" y2="1.143" layer="21"/>
<rectangle x1="-6.731" y1="-2.921" x2="-5.969" y2="-1.905" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-6.731" y1="-5.461" x2="-5.969" y2="-4.699" layer="21"/>
<rectangle x1="-6.731" y1="-4.699" x2="-5.969" y2="-2.921" layer="51"/>
<rectangle x1="-4.191" y1="-4.699" x2="-3.429" y2="-2.921" layer="51"/>
<rectangle x1="-4.191" y1="-5.461" x2="-3.429" y2="-4.699" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-5.461" x2="-0.889" y2="-4.699" layer="21"/>
<rectangle x1="-1.651" y1="-4.699" x2="-0.889" y2="-2.921" layer="51"/>
<rectangle x1="0.889" y1="-4.699" x2="1.651" y2="-2.921" layer="51"/>
<rectangle x1="0.889" y1="-5.461" x2="1.651" y2="-4.699" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
<rectangle x1="5.969" y1="-2.921" x2="6.731" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-5.461" x2="4.191" y2="-4.699" layer="21"/>
<rectangle x1="3.429" y1="-4.699" x2="4.191" y2="-2.921" layer="51"/>
<rectangle x1="5.969" y1="-4.699" x2="6.731" y2="-2.921" layer="51"/>
<rectangle x1="5.969" y1="-5.461" x2="6.731" y2="-4.699" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="2X06" urn="urn:adsk.eagle:package:22474/2" locally_modified="yes" type="model" library_version="4">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="2X06"/>
</packageinstances>
</package3d>
<package3d name="2X06/90" urn="urn:adsk.eagle:package:22480/2" type="model" library_version="4">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="2X06/90"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PINH2X6" library_version="67">
<wire x1="-6.35" y1="-10.16" x2="8.89" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-10.16" x2="8.89" y2="7.62" width="0.4064" layer="94"/>
<wire x1="8.89" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-10.16" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-2X6" prefix="JP" uservalue="yes" library_version="67">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X6" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X06">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22474/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="8" constant="no"/>
</technology>
</technologies>
</device>
<device name="/90" package="2X06/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22480/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="3" name="lora_line" width="0.185025" drill="0">
<clearance class="3" value="0.1016"/>
</class>
</classes>
<parts>
<part name="FRAME2" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA3_L" device="" value="LPSNP"/>
<part name="IC5" library="voest_library" deviceset="ADS114S08" device="" value="ADS114S08"/>
<part name="CON1" library="voest_library" deviceset="THERMOCOUPLES_CONNECTOR" device="" value=""/>
<part name="GND44" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R1" library="voest_library" deviceset="R" device="0603" value="10"/>
<part name="R2" library="voest_library" deviceset="R" device="0603" value="10"/>
<part name="R5" library="voest_library" deviceset="R" device="0603" value="10"/>
<part name="R17" library="voest_library" deviceset="R" device="0603" value="10"/>
<part name="R20" library="voest_library" deviceset="R" device="0603" value="10"/>
<part name="R19" library="voest_library" deviceset="R" device="0603" value="10"/>
<part name="R22" library="voest_library" deviceset="R" device="0603" value="10"/>
<part name="R21" library="voest_library" deviceset="R" device="0603" value="10"/>
<part name="R24" library="voest_library" deviceset="R" device="0603" value="10"/>
<part name="R23" library="voest_library" deviceset="R" device="0603" value="10"/>
<part name="R26" library="voest_library" deviceset="R" device="0603" value="10"/>
<part name="R28" library="voest_library" deviceset="R" device="0603" value="10"/>
<part name="R30" library="voest_library" deviceset="R" device="0603" value="10"/>
<part name="R32" library="voest_library" deviceset="R" device="0603" value="10"/>
<part name="R34" library="voest_library" deviceset="R" device="0603" value="10"/>
<part name="GND45" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R14" library="voest_library" deviceset="R" device="0603" value="2.2k/1%"/>
<part name="R13" library="voest_library" deviceset="R" device="0603" value="NTC/10k"/>
<part name="GND46" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND47" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND48" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND49" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R10" library="voest_library" deviceset="R" device="0603" value="10"/>
<part name="GND51" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="Z1" library="voest_library" deviceset="Z1000" device="Z0603" value="Z1000"/>
<part name="Z3" library="voest_library" deviceset="Z1000" device="Z0603" value="Z1000"/>
<part name="Z5" library="voest_library" deviceset="Z1000" device="Z0603" value="Z1000"/>
<part name="Z7" library="voest_library" deviceset="Z1000" device="Z0603" value="Z1000"/>
<part name="Z8" library="voest_library" deviceset="Z1000" device="Z0603" value="Z1000"/>
<part name="Z9" library="voest_library" deviceset="Z1000" device="Z0603" value="Z1000"/>
<part name="Z10" library="voest_library" deviceset="Z1000" device="Z0603" value="Z1000"/>
<part name="Z11" library="voest_library" deviceset="Z1000" device="Z0603" value="Z1000"/>
<part name="Z12" library="voest_library" deviceset="Z1000" device="Z0603" value="Z1000"/>
<part name="Z13" library="voest_library" deviceset="Z1000" device="Z0603" value="Z1000"/>
<part name="Z15" library="voest_library" deviceset="Z1000" device="Z0603" value="Z1000"/>
<part name="Z17" library="voest_library" deviceset="Z1000" device="Z0603" value="Z1000"/>
<part name="Z19" library="voest_library" deviceset="Z1000" device="Z0603" value="Z1000"/>
<part name="Z21" library="voest_library" deviceset="Z1000" device="Z0603" value="Z1000"/>
<part name="Z23" library="voest_library" deviceset="Z1000" device="Z0603" value="Z1000"/>
<part name="Z24" library="voest_library" deviceset="Z1000" device="Z0603" value="Z1000"/>
<part name="C24" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C25" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C26" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C27" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C28" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C29" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C30" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C31" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C32" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C33" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C22" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C21" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C19" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="P+19" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="R42" library="partLibrary" deviceset="R-EU-?-*" device="0402" technology="0" value="1.5M"/>
<part name="R43" library="partLibrary" deviceset="R-EU-?-*" device="0402" technology="0" value="180k"/>
<part name="GND16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C4" library="partLibrary" deviceset="C-EU-?-*" device="0603" value="10p"/>
<part name="GND18" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="D2" library="Diode" deviceset="SCHOTTKY_" device="DO-219" package3d_urn="urn:adsk.eagle:package:9427153/2" technology="MBR0520LT1G" value="MBR0520LT1G"/>
<part name="GND19" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="Q1" library="SamacSys_Parts" deviceset="IRFB7430PBF" device=""/>
<part name="CON2" library="SamacSys_Parts" deviceset="1546215-2" device=""/>
<part name="GND21" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND22" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND23" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND26" library="supply1" deviceset="GND" device=""/>
<part name="GND27" library="supply1" deviceset="GND" device=""/>
<part name="IC7" library="SamacSys_Parts" deviceset="TLV9102IDDFR" device=""/>
<part name="R7" library="partLibrary" deviceset="R-EU-?-*" device="0402" value="470k"/>
<part name="R6" library="partLibrary" deviceset="R-EU-?-*" device="0402" value="200k"/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="C8" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C7" library="SamacSys_Parts" deviceset="C2012X5R1E475K125AB" device=""/>
<part name="L1" library="SamacSys_Parts" deviceset="CR32NP-100KC" device=""/>
<part name="IC3" library="SamacSys_Parts" deviceset="INA190A4QDCKRQ1" device=""/>
<part name="C9" library="partLibrary" deviceset="C-EU-?-*" device="0402" technology="100N/50V" value="DNP"/>
<part name="R49" library="SamacSys_Parts" deviceset="GMR100HTBFA10L0" device=""/>
<part name="R50" library="partLibrary" deviceset="R-EU-?-*" device="0402" technology="0" value="DNP"/>
<part name="R51" library="partLibrary" deviceset="R-EU-?-*" device="0402" technology="0" value="DNP"/>
<part name="P+13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="R44" library="partLibrary" deviceset="R-EU-?-*" device="0402" technology="0" value="180k"/>
<part name="R46" library="partLibrary" deviceset="R-EU-?-*" device="0402" technology="0" value="180k"/>
<part name="R33" library="voest_library" deviceset="R" device="0603" value="680k"/>
<part name="R45" library="partLibrary" deviceset="R-EU-?-*" device="0402" technology="0" value="180k"/>
<part name="TP3" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP4" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP5" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP6" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP7" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="C12" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="IC6" library="SamacSys_Parts" deviceset="ICM-20789" device="PQFN50"/>
<part name="GND32" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND33" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND34" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="IC8" library="SamacSys_Parts" deviceset="TLV70218DBVT" device=""/>
<part name="GND35" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R3" library="partLibrary" deviceset="R-EU-?-*" device="0402" technology="0" value="10k"/>
<part name="GND36" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R4" library="partLibrary" deviceset="R-EU-?-*" device="0402" technology="0" value="10k"/>
<part name="GND37" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C15" library="partLibrary" deviceset="C-EU-?-*" device="0603" value="10u"/>
<part name="GND38" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND39" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C18" library="partLibrary" deviceset="C-EU-?-*" device="0402" technology="100N/50V" value="100n"/>
<part name="C13" library="partLibrary" deviceset="C-EU-?-*" device="0402" technology="100N/50V" value="100n"/>
<part name="C11" library="partLibrary" deviceset="C-EU-?-*" device="0402" technology="100N/50V" value="100n"/>
<part name="C16" library="partLibrary" deviceset="C-EU-?-*" device="0402" technology="100N/50V" value="100n"/>
<part name="C20" library="partLibrary" deviceset="C-EU-?-*" device="0402" technology="100N/50V" value="DNP"/>
<part name="C23" library="partLibrary" deviceset="C-EU-?-*" device="0402" technology="100N/50V" value="DNP"/>
<part name="Z16" library="partLibrary" deviceset="Z1000" device="0402" technology="1000/100MHZ" value="Z1000"/>
<part name="C35" library="partLibrary" deviceset="C-EU-?-*" device="0402" technology="100N/50V" value="100n"/>
<part name="GND40" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="R27" library="voest_library" deviceset="R" device="0603" value="680k"/>
<part name="GND24" library="supply1" deviceset="GND" device=""/>
<part name="GND25" library="supply1" deviceset="GND" device=""/>
<part name="R25" library="partLibrary" deviceset="R-EU-?-*" device="0402" technology="0" value="180k"/>
<part name="R31" library="partLibrary" deviceset="R-EU-?-*" device="0402" technology="0" value="180k"/>
<part name="R29" library="voest_library" deviceset="R" device="0603" value="680k"/>
<part name="C17" library="partLibrary" deviceset="C-EU-?-*" device="0402" technology="100N/50V" value="DNP"/>
<part name="C6" library="partLibrary" deviceset="C-EU-?-*" device="0402" technology="100N/50V" value="DNP"/>
<part name="GND41" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND42" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND43" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND53" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND55" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND56" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND57" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND58" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C14" library="partLibrary" deviceset="C-EU-?-*" device="0402" technology="100N/50V" value="100n"/>
<part name="GND61" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C34" library="partLibrary" deviceset="C-EU-?-*" device="0402" technology="100N/50V" value="100n"/>
<part name="GND63" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="Z18" library="partLibrary" deviceset="Z1000" device="0402" technology="1000/100MHZ" value="Z1000"/>
<part name="P+18" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND64" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U$2" library="SamacSys_Parts" deviceset="MTXDOT-EU1-A01-100" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="Z2" library="partLibrary" deviceset="Z1000" device="0402" technology="1000/100MHZ" value="Z1000"/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SL1" library="con-amp-quick" library_urn="urn:adsk.eagle:library:125" deviceset="M04" device="" package3d_urn="urn:adsk.eagle:package:5958/1" value="0"/>
<part name="SL2" library="con-amp-quick" library_urn="urn:adsk.eagle:library:125" deviceset="M04" device="" package3d_urn="urn:adsk.eagle:package:5958/1" value="0"/>
<part name="GND15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C2" library="partLibrary" deviceset="C-EU-?-*" device="0402" technology="100N/50V" value="100n"/>
<part name="U$1" library="SamacSys_Parts" deviceset="RM186-SM-02" device=""/>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="DINA3_L" device=""/>
<part name="BAT2" library="voest_library" deviceset="BATTERY" device="" value="AA/3.6V/1.8Ah/130°C"/>
<part name="D1" library="voest_library" deviceset="D-1N5819HW" device="" value="1N5819"/>
<part name="FID1" library="voest_library" deviceset="FIDUCIAL" device=""/>
<part name="FID2" library="voest_library" deviceset="FIDUCIAL" device=""/>
<part name="LED1" library="voest_library" deviceset="LED" device="0805" technology="GREEN" value="Green"/>
<part name="IC4" library="voest_library" deviceset="MC2520Z" device="" value="4.096MHz/30ppm"/>
<part name="RTC1" library="voest_library" deviceset="RTC" device="" value="RV-3129-C3/8ppm"/>
<part name="IC9" library="voest_library" deviceset="S25FL256LAGMFN000" device="" value="Flash-32MB"/>
<part name="IC10" library="voest_library" deviceset="SN74AHC04" device="" value="SN74AHC04"/>
<part name="IC11" library="voest_library" deviceset="STM32L451CCU3" device="" value="STM32L451CCU3"/>
<part name="S1" library="voest_library" deviceset="SWITCH" device="" value=""/>
<part name="TP35" library="voest_library" deviceset="TP" device=""/>
<part name="GND29" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="JP1" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-1X2" device="" package3d_urn="urn:adsk.eagle:package:22435/2"/>
<part name="D3" library="voest_library" deviceset="D-1N5819HW" device="" value="1N5819"/>
<part name="GND52" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="TP36" library="voest_library" deviceset="TP" device=""/>
<part name="GND54" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="TP37" library="voest_library" deviceset="TP" device=""/>
<part name="GND59" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R11" library="voest_library" deviceset="R" device="0603" value="0"/>
<part name="R12" library="voest_library" deviceset="R" device="0603" value="0"/>
<part name="GND60" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND62" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND65" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND66" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND67" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R35" library="voest_library" deviceset="R" device="0603" value="0"/>
<part name="R36" library="voest_library" deviceset="R" device="0603" value="DNP"/>
<part name="P+9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND68" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND69" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R37" library="voest_library" deviceset="R" device="0603" value="430"/>
<part name="GND71" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R39" library="voest_library" deviceset="R" device="0603" value="10k"/>
<part name="R40" library="voest_library" deviceset="R" device="0603" value="2.7k"/>
<part name="R41" library="voest_library" deviceset="R" device="0603" value="2.7k"/>
<part name="PROG1" library="voest_library" deviceset="PROGHEADER" device="" value="1.27mm"/>
<part name="GND72" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C3" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C5" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C37" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C38" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C39" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C40" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C41" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C42" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C43" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C44" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="IC12" library="voest_library" deviceset="SN74AHC04" device="" value="SN74AHC04"/>
<part name="GND73" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C45" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="P+15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND74" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND75" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C46" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="GND76" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+20" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C47" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="GND77" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+21" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C48" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="GND78" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+22" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C49" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="GND79" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+23" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+25" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND81" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C50" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="H1" library="holes" library_urn="urn:adsk.eagle:library:237" deviceset="MOUNT-PAD-ROUND" device="3.0" package3d_urn="urn:adsk.eagle:package:14280/1"/>
<part name="H2" library="holes" library_urn="urn:adsk.eagle:library:237" deviceset="MOUNT-PAD-ROUND" device="3.0" package3d_urn="urn:adsk.eagle:package:14280/1"/>
<part name="H3" library="holes" library_urn="urn:adsk.eagle:library:237" deviceset="MOUNT-PAD-ROUND" device="3.0" package3d_urn="urn:adsk.eagle:package:14280/1"/>
<part name="H4" library="holes" library_urn="urn:adsk.eagle:library:237" deviceset="MOUNT-PAD-ROUND" device="3.0" package3d_urn="urn:adsk.eagle:package:14280/1"/>
<part name="TP38" library="voest_library" deviceset="TP" device=""/>
<part name="D4" library="voest_library" deviceset="D-1N5819HW" device="" value="1N5819"/>
<part name="BAT1" library="voest_library" deviceset="BATTERY" device="" value="DNP"/>
<part name="X1" library="con-subd" library_urn="urn:adsk.eagle:library:189" deviceset="F09VP" device="" package3d_urn="urn:adsk.eagle:package:10230/1"/>
<part name="GND82" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND83" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="H5" library="holes" library_urn="urn:adsk.eagle:library:237" deviceset="MOUNT-PAD-ROUND" device="3.0" package3d_urn="urn:adsk.eagle:package:14280/1"/>
<part name="H6" library="holes" library_urn="urn:adsk.eagle:library:237" deviceset="MOUNT-PAD-ROUND" device="3.0" package3d_urn="urn:adsk.eagle:package:14280/1"/>
<part name="H7" library="holes" library_urn="urn:adsk.eagle:library:237" deviceset="MOUNT-PAD-ROUND" device="3.0" package3d_urn="urn:adsk.eagle:package:14280/1"/>
<part name="H8" library="holes" library_urn="urn:adsk.eagle:library:237" deviceset="MOUNT-PAD-ROUND" device="3.0" package3d_urn="urn:adsk.eagle:package:14280/1"/>
<part name="H9" library="holes" library_urn="urn:adsk.eagle:library:237" deviceset="MOUNT-PAD-ROUND" device="3.0" package3d_urn="urn:adsk.eagle:package:14280/1"/>
<part name="FID3" library="voest_library" deviceset="FIDUCIAL" device=""/>
<part name="FID4" library="voest_library" deviceset="FIDUCIAL" device=""/>
<part name="TP39" library="voest_library" deviceset="TP" device=""/>
<part name="TP40" library="voest_library" deviceset="TP" device=""/>
<part name="TP41" library="voest_library" deviceset="TP" device=""/>
<part name="TP42" library="voest_library" deviceset="TP" device=""/>
<part name="C65" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C66" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C67" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C68" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C69" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C70" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="GND93" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND94" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND95" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND96" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="H10" library="holes" library_urn="urn:adsk.eagle:library:237" deviceset="MOUNT-PAD-ROUND" device="3.0" package3d_urn="urn:adsk.eagle:package:14280/1"/>
<part name="H11" library="holes" library_urn="urn:adsk.eagle:library:237" deviceset="MOUNT-PAD-ROUND" device="3.0" package3d_urn="urn:adsk.eagle:package:14280/1"/>
<part name="H12" library="holes" library_urn="urn:adsk.eagle:library:237" deviceset="MOUNT-PAD-ROUND" device="3.0" package3d_urn="urn:adsk.eagle:package:14280/1"/>
<part name="GND97" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="JP2" library="customParts" library_urn="urn:adsk.wipprod:fs.file:vf.SX5B0h59S26sooymxZ0hvA" deviceset="PINHD-2X6" device="" package3d_urn="urn:adsk.eagle:package:22474/2" value="Extension Header"/>
<part name="R52" library="partLibrary" deviceset="R-EU-?-*" device="0402" technology="0" value="0"/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C10" library="partLibrary" deviceset="C-EU-?-*" device="0603" value="10u"/>
<part name="GND28" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="TP1" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="GND31" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="TP31" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP32" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP33" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP34" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP44" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP45" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP11" library="voest_library" deviceset="TP" device=""/>
<part name="R47" library="voest_library" deviceset="R" device="0603" value="10"/>
<part name="R48" library="voest_library" deviceset="R" device="0603" value="10"/>
<part name="Z4" library="voest_library" deviceset="Z1000" device="Z0603" value="Z1000"/>
<part name="C51" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="C53" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="R56" library="partLibrary" deviceset="R-EU-?-*" device="0402" technology="0" value="180k"/>
<part name="GND30" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C52" library="partLibrary" deviceset="C-EU-?-*" device="0603" value="10p"/>
<part name="GND84" library="supply1" deviceset="GND" device=""/>
<part name="Z6" library="voest_library" deviceset="Z1000" device="Z0603" value="Z1000"/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C36" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="IC13" library="SamacSys_Parts" deviceset="MAX1605EUT+T" device=""/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="J1" library="SamacSys_Parts" deviceset="JUMPER_CLOSED" device=""/>
<part name="J3" library="SamacSys_Parts" deviceset="JUMPER_CLOSED" device=""/>
<part name="J2" library="SamacSys_Parts" deviceset="JUMPER_CLOSED" device=""/>
<part name="IC2" library="SamacSys_Parts" deviceset="DAC80501ZDGST" device=""/>
<part name="R8" library="partLibrary" deviceset="R-EU-?-*" device="0402" technology="0" value="10k"/>
<part name="R15" library="partLibrary" deviceset="R-EU-?-*" device="0402" technology="0" value="10k"/>
<part name="C54" library="partLibrary" deviceset="C-EU-?-*" device="0402" technology="100N/50V" value="100n"/>
<part name="GND85" library="supply1" deviceset="GND" device=""/>
<part name="J5" library="SamacSys_Parts" deviceset="JUMPER_OPEN" device="" package3d_urn="urn:adsk.eagle:package:15471/1"/>
<part name="GND86" library="supply1" deviceset="GND" device=""/>
<part name="J4" library="SamacSys_Parts" deviceset="JUMPER_CLOSED" device=""/>
<part name="TP2" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="C1" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C55" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="C56" library="voest_library" deviceset="C" device="0603" value="10u/X7S"/>
<part name="J6" library="SamacSys_Parts" deviceset="JUMPER_OPEN" device="" package3d_urn="urn:adsk.eagle:package:15471/1"/>
<part name="J7" library="SamacSys_Parts" deviceset="JUMPER_OPEN" device="" package3d_urn="urn:adsk.eagle:package:15471/1"/>
<part name="J10" library="SamacSys_Parts" deviceset="JUMPER_OPEN" device="" package3d_urn="urn:adsk.eagle:package:15471/1"/>
<part name="J9" library="SamacSys_Parts" deviceset="JUMPER_OPEN" device="" package3d_urn="urn:adsk.eagle:package:15471/1"/>
<part name="J8" library="SamacSys_Parts" deviceset="JUMPER_OPEN" device="" package3d_urn="urn:adsk.eagle:package:15471/1"/>
<part name="J11" library="SamacSys_Parts" deviceset="JUMPER_OPEN" device="" package3d_urn="urn:adsk.eagle:package:15471/1"/>
<part name="J13" library="SamacSys_Parts" deviceset="JUMPER_OPEN" device="" package3d_urn="urn:adsk.eagle:package:15471/1"/>
<part name="J12" library="SamacSys_Parts" deviceset="JUMPER_OPEN" device="" package3d_urn="urn:adsk.eagle:package:15471/1"/>
<part name="J15" library="SamacSys_Parts" deviceset="JUMPER_OPEN" device="" package3d_urn="urn:adsk.eagle:package:15471/1"/>
<part name="J14" library="SamacSys_Parts" deviceset="JUMPER_OPEN" device="" package3d_urn="urn:adsk.eagle:package:15471/1"/>
<part name="J16" library="SamacSys_Parts" deviceset="JUMPER_OPEN" device="" package3d_urn="urn:adsk.eagle:package:15471/1"/>
<part name="J17" library="SamacSys_Parts" deviceset="JUMPER_OPEN" device="" package3d_urn="urn:adsk.eagle:package:15471/1"/>
<part name="J18" library="SamacSys_Parts" deviceset="JUMPER_OPEN" device="" package3d_urn="urn:adsk.eagle:package:15471/1"/>
<part name="J19" library="SamacSys_Parts" deviceset="JUMPER_OPEN" device="" package3d_urn="urn:adsk.eagle:package:15471/1"/>
<part name="J20" library="SamacSys_Parts" deviceset="JUMPER_OPEN" device="" package3d_urn="urn:adsk.eagle:package:15471/1"/>
<part name="GND70" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND87" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND80" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J21" library="SamacSys_Parts" deviceset="JUMPER_OPEN" device="" package3d_urn="urn:adsk.eagle:package:15471/1"/>
<part name="J22" library="SamacSys_Parts" deviceset="JUMPER_CLOSED" device=""/>
<part name="R9" library="voest_library" deviceset="R" device="0603" value="2.7k"/>
<part name="R16" library="voest_library" deviceset="R" device="0603" value="2.7k"/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="TP9" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP10" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP12" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP13" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP14" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP15" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP16" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP17" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP18" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP19" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP20" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP21" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP22" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="TP23" library="testpad" library_urn="urn:adsk.eagle:library:385" deviceset="TP" device="TP20R" package3d_urn="urn:adsk.eagle:package:27973/1"/>
<part name="J23" library="SamacSys_Parts" deviceset="JUMPER_OPEN" device="" package3d_urn="urn:adsk.eagle:package:15471/1"/>
<part name="J24" library="SamacSys_Parts" deviceset="JUMPER_CLOSED" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-25.4" y="7.62" size="5.08" layer="94">V1</text>
<text x="-99.06" y="27.94" size="3.81" layer="94">Thermo electric sensor node</text>
<text x="-294.64" y="243.84" size="1.778" layer="97">Switch OFF/ON</text>
<text x="-309.88" y="182.88" size="1.778" layer="97">~1 minute time retention without battery</text>
<wire x1="-381" y1="7.62" x2="-381" y2="114.3" width="0.254" layer="97" style="longdash"/>
<wire x1="-381" y1="114.3" x2="-279.4" y2="114.3" width="0.254" layer="97" style="longdash"/>
<wire x1="-279.4" y1="114.3" x2="-279.4" y2="7.62" width="0.254" layer="97" style="longdash"/>
<wire x1="-279.4" y1="7.62" x2="-381" y2="7.62" width="0.254" layer="97" style="longdash"/>
<text x="-381" y="116.84" size="3.81" layer="97">Clock</text>
<wire x1="-381" y1="248.92" x2="-198.12" y2="248.92" width="0.254" layer="97" style="longdash"/>
<wire x1="-198.12" y1="248.92" x2="-198.12" y2="180.34" width="0.254" layer="97" style="longdash"/>
<wire x1="-198.12" y1="180.34" x2="-381" y2="180.34" width="0.254" layer="97" style="longdash"/>
<wire x1="-381" y1="180.34" x2="-381" y2="248.92" width="0.254" layer="97" style="longdash"/>
<text x="-381" y="251.46" size="3.81" layer="97">Supply</text>
<text x="-330.2" y="208.28" size="1.778" layer="97" rot="R90">3,1-3,7V</text>
<text x="-342.9" y="231.14" size="1.778" layer="97">2,8-3,6V</text>
<text x="-223.52" y="231.14" size="1.778" layer="97">2,8-3,6V</text>
<wire x1="-381" y1="167.64" x2="-269.24" y2="167.64" width="0.254" layer="97" style="longdash"/>
<wire x1="-269.24" y1="167.64" x2="-269.24" y2="127" width="0.254" layer="97" style="longdash"/>
<wire x1="-269.24" y1="127" x2="-381" y2="127" width="0.254" layer="97" style="longdash"/>
<wire x1="-381" y1="127" x2="-381" y2="167.64" width="0.254" layer="97" style="longdash"/>
<text x="-381" y="170.18" size="3.81" layer="97">Flash</text>
<wire x1="-137.16" y1="147.32" x2="-5.08" y2="147.32" width="0.254" layer="97" style="longdash"/>
<wire x1="-5.08" y1="147.32" x2="-5.08" y2="40.64" width="0.254" layer="97" style="longdash"/>
<wire x1="-5.08" y1="40.64" x2="-137.16" y2="40.64" width="0.254" layer="97" style="longdash"/>
<wire x1="-137.16" y1="40.64" x2="-137.16" y2="147.32" width="0.254" layer="97" style="longdash"/>
<text x="-137.16" y="149.86" size="3.81" layer="97">uC</text>
<wire x1="-274.32" y1="7.62" x2="-274.32" y2="106.68" width="0.254" layer="97" style="longdash"/>
<wire x1="-274.32" y1="106.68" x2="-190.5" y2="106.68" width="0.254" layer="97" style="longdash"/>
<wire x1="-190.5" y1="106.68" x2="-190.5" y2="7.62" width="0.254" layer="97" style="longdash"/>
<wire x1="-190.5" y1="7.62" x2="-274.32" y2="7.62" width="0.254" layer="97" style="longdash"/>
<text x="-274.32" y="109.22" size="3.81" layer="97">Connectors</text>
<wire x1="-185.42" y1="106.68" x2="-142.24" y2="106.68" width="0.254" layer="97" style="longdash"/>
<wire x1="-142.24" y1="106.68" x2="-142.24" y2="35.56" width="0.254" layer="97" style="longdash"/>
<wire x1="-142.24" y1="35.56" x2="-116.84" y2="35.56" width="0.254" layer="97" style="longdash"/>
<wire x1="-116.84" y1="35.56" x2="-116.84" y2="7.62" width="0.254" layer="97" style="longdash"/>
<wire x1="-116.84" y1="7.62" x2="-185.42" y2="7.62" width="0.254" layer="97" style="longdash"/>
<wire x1="-185.42" y1="7.62" x2="-185.42" y2="106.68" width="0.254" layer="97" style="longdash"/>
<text x="-313.182" y="128.524" size="1.778" layer="97">Estimation of the memory requirements:
1 SPS; 5 hours

60x2 byte thermocouples
6       byte timestamp
4       byte counter
6x2   byte internal temperature
6x2   byte external temperature
6x2   byte offset control
6x2   byte supply voltage control
-------------------------------------------
178   byte

178 * 60 * 60 * 5 = 3.1MB</text>
<text x="-270.002" y="37.084" size="1.778" layer="97">UART
Only 0V - 3.3V</text>
<text x="-269.24" y="96.52" size="3.81" layer="97">Interface</text>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="-388.62" y="0" smashed="yes"/>
<instance part="FRAME1" gate="G$2" x="-101.6" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="-88.9" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="-15.24" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="-83.82" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="BAT2" gate="G$1" x="-337.82" y="213.36" smashed="yes">
<attribute name="NAME" x="-340.995" y="209.55" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-332.74" y="201.93" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D1" gate="G$1" x="-350.52" y="223.52" smashed="yes" rot="R90">
<attribute name="NAME" x="-354.965" y="221.234" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-352.171" y="218.694" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="FID1" gate="FID$1" x="-139.7" y="27.94" smashed="yes">
<attribute name="NAME" x="-139.7" y="31.75" size="1.27" layer="94" align="center"/>
</instance>
<instance part="FID2" gate="FID$1" x="-139.7" y="15.24" smashed="yes">
<attribute name="NAME" x="-139.7" y="19.05" size="1.27" layer="94" align="center"/>
</instance>
<instance part="LED1" gate="G$1" x="-27.94" y="60.96" smashed="yes">
<attribute name="NAME" x="-38.1" y="60.071" size="1.27" layer="95"/>
<attribute name="VALUE" x="-38.1" y="58.039" size="1.27" layer="96"/>
</instance>
<instance part="IC4" gate="G$1" x="-355.6" y="96.52" smashed="yes">
<attribute name="NAME" x="-363.22" y="101.854" size="1.27" layer="95"/>
<attribute name="VALUE" x="-363.22" y="89.916" size="1.27" layer="96"/>
</instance>
<instance part="RTC1" gate="G$1" x="-284.48" y="205.74" smashed="yes">
<attribute name="NAME" x="-294.64" y="216.408" size="1.778" layer="95"/>
<attribute name="VALUE" x="-295.402" y="191.262" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="IC9" gate="G$1" x="-347.98" y="147.32" smashed="yes">
<attribute name="NAME" x="-358.14" y="160.528" size="1.27" layer="95"/>
<attribute name="VALUE" x="-358.14" y="132.842" size="1.27" layer="96"/>
</instance>
<instance part="IC10" gate="-1" x="-309.88" y="81.28" smashed="yes">
<attribute name="NAME" x="-307.34" y="84.455" size="1.778" layer="95"/>
<attribute name="VALUE" x="-307.34" y="76.2" size="1.778" layer="96"/>
</instance>
<instance part="IC10" gate="-2" x="-309.88" y="68.58" smashed="yes">
<attribute name="NAME" x="-307.34" y="71.755" size="1.778" layer="95"/>
<attribute name="VALUE" x="-307.34" y="63.5" size="1.778" layer="96"/>
</instance>
<instance part="IC10" gate="-3" x="-309.88" y="55.88" smashed="yes">
<attribute name="NAME" x="-307.34" y="59.055" size="1.778" layer="95"/>
<attribute name="VALUE" x="-307.34" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="IC10" gate="-4" x="-309.88" y="43.18" smashed="yes">
<attribute name="NAME" x="-307.34" y="46.355" size="1.778" layer="95"/>
<attribute name="VALUE" x="-307.34" y="38.1" size="1.778" layer="96"/>
</instance>
<instance part="IC10" gate="-5" x="-309.88" y="30.48" smashed="yes">
<attribute name="NAME" x="-307.34" y="33.655" size="1.778" layer="95"/>
<attribute name="VALUE" x="-307.34" y="25.4" size="1.778" layer="96"/>
</instance>
<instance part="IC10" gate="-6" x="-309.88" y="17.78" smashed="yes">
<attribute name="NAME" x="-307.34" y="20.955" size="1.778" layer="95"/>
<attribute name="VALUE" x="-307.34" y="12.7" size="1.778" layer="96"/>
</instance>
<instance part="IC10" gate="-7" x="-365.76" y="30.48" smashed="yes">
<attribute name="NAME" x="-366.395" y="29.845" size="1.778" layer="95"/>
</instance>
<instance part="IC11" gate="G$1" x="-68.58" y="91.44" smashed="yes">
<attribute name="NAME" x="-68.58" y="94.488" size="1.27" layer="95"/>
<attribute name="VALUE" x="-73.66" y="87.122" size="1.27" layer="96"/>
</instance>
<instance part="S1" gate="G$1" x="-287.02" y="236.22" smashed="yes">
<attribute name="NAME" x="-291.211" y="233.68" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="TP35" gate="G$1" x="-350.52" y="233.68" smashed="yes">
<attribute name="NAME" x="-350.54" y="234.7" size="1" layer="95" align="center"/>
<attribute name="TP_SIGNAL_NAME" x="-349.25" y="232.41" size="1" layer="97"/>
</instance>
<instance part="GND29" gate="1" x="-350.52" y="200.66" smashed="yes"/>
<instance part="P+7" gate="VCC" x="-203.2" y="233.68" smashed="yes"/>
<instance part="JP1" gate="G$1" x="-373.38" y="226.06" smashed="yes" rot="MR0">
<attribute name="NAME" x="-374.65" y="233.807" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="-367.03" y="220.98" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="D3" gate="G$1" x="-302.26" y="220.98" smashed="yes" rot="R270">
<attribute name="NAME" x="-300.355" y="223.266" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="-305.689" y="223.266" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND52" gate="1" x="-302.26" y="193.04" smashed="yes"/>
<instance part="TP36" gate="G$1" x="-302.26" y="233.68" smashed="yes">
<attribute name="NAME" x="-302.28" y="234.7" size="1" layer="95" align="center"/>
<attribute name="TP_SIGNAL_NAME" x="-300.99" y="232.41" size="1" layer="97"/>
</instance>
<instance part="GND54" gate="1" x="-269.24" y="220.98" smashed="yes" rot="R90"/>
<instance part="TP37" gate="G$1" x="-208.28" y="233.68" smashed="yes">
<attribute name="NAME" x="-208.3" y="234.7" size="1" layer="95" align="center"/>
<attribute name="TP_SIGNAL_NAME" x="-207.01" y="232.41" size="1" layer="97"/>
</instance>
<instance part="GND59" gate="1" x="-284.48" y="187.96" smashed="yes"/>
<instance part="R11" gate="G$1" x="-360.68" y="228.6" smashed="yes">
<attribute name="NAME" x="-360.68" y="229.87" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-360.68" y="226.06" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="R12" gate="G$1" x="-322.58" y="228.6" smashed="yes">
<attribute name="NAME" x="-322.58" y="229.87" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-322.58" y="226.06" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="GND60" gate="1" x="-218.44" y="213.36" smashed="yes"/>
<instance part="GND62" gate="1" x="-213.36" y="213.36" smashed="yes"/>
<instance part="GND65" gate="1" x="-208.28" y="213.36" smashed="yes"/>
<instance part="GND66" gate="1" x="-203.2" y="213.36" smashed="yes"/>
<instance part="P+8" gate="VCC" x="-373.38" y="104.14" smashed="yes"/>
<instance part="GND67" gate="1" x="-373.38" y="81.28" smashed="yes"/>
<instance part="R35" gate="G$1" x="-332.74" y="109.22" smashed="yes">
<attribute name="NAME" x="-335.28" y="110.49" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-330.2" y="111.76" size="1.27" layer="96" rot="R180" align="bottom-center"/>
</instance>
<instance part="R36" gate="G$1" x="-332.74" y="104.14" smashed="yes">
<attribute name="NAME" x="-335.28" y="105.41" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-330.2" y="106.68" size="1.27" layer="96" rot="R180" align="bottom-center"/>
</instance>
<instance part="P+9" gate="VCC" x="-322.58" y="109.22" smashed="yes" rot="R270"/>
<instance part="P+10" gate="VCC" x="-365.76" y="45.72" smashed="yes"/>
<instance part="GND68" gate="1" x="-365.76" y="15.24" smashed="yes"/>
<instance part="GND69" gate="1" x="-27.94" y="50.8" smashed="yes"/>
<instance part="R37" gate="G$1" x="-27.94" y="68.58" smashed="yes" rot="R90">
<attribute name="NAME" x="-29.21" y="68.58" size="1.27" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="-25.4" y="68.58" size="1.27" layer="96" rot="R90" align="bottom-center"/>
</instance>
<instance part="GND71" gate="1" x="-269.24" y="193.04" smashed="yes"/>
<instance part="R39" gate="G$1" x="-264.16" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="-265.43" y="218.44" size="1.27" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="-266.7" y="223.52" size="1.27" layer="96" rot="R270" align="bottom-center"/>
</instance>
<instance part="R40" gate="G$1" x="-259.08" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="-260.35" y="218.44" size="1.27" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="-261.62" y="223.52" size="1.27" layer="96" rot="R270" align="bottom-center"/>
</instance>
<instance part="R41" gate="G$1" x="-254" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="-255.27" y="218.44" size="1.27" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="-256.54" y="223.52" size="1.27" layer="96" rot="R270" align="bottom-center"/>
</instance>
<instance part="PROG1" gate="G$1" x="-213.36" y="25.4" smashed="yes">
<attribute name="NAME" x="-219.71" y="36.195" size="1.778" layer="95"/>
<attribute name="VALUE" x="-219.71" y="12.7" size="1.778" layer="96"/>
</instance>
<instance part="GND72" gate="1" x="-226.06" y="25.4" smashed="yes" rot="R270"/>
<instance part="P+11" gate="VCC" x="-223.52" y="38.1" smashed="yes"/>
<instance part="C3" gate="G$1" x="-218.44" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="-220.345" y="220.98" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-216.535" y="220.98" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="C5" gate="G$1" x="-213.36" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="-215.265" y="220.98" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-211.455" y="220.98" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="C37" gate="G$1" x="-208.28" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="-210.185" y="220.98" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-206.375" y="220.98" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="C38" gate="G$1" x="-203.2" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="-205.105" y="220.98" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-201.295" y="220.98" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="C39" gate="G$1" x="-312.42" y="205.74" smashed="yes" rot="R90">
<attribute name="NAME" x="-314.325" y="205.74" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-310.515" y="205.74" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="C40" gate="G$1" x="-307.34" y="205.74" smashed="yes" rot="R90">
<attribute name="NAME" x="-309.245" y="205.74" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-305.435" y="205.74" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="C41" gate="G$1" x="-302.26" y="205.74" smashed="yes" rot="R90">
<attribute name="NAME" x="-304.165" y="205.74" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-300.355" y="205.74" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="C42" gate="G$1" x="-276.86" y="220.98" smashed="yes" rot="R180">
<attribute name="NAME" x="-276.86" y="219.075" size="1.27" layer="95" rot="R180" align="top-right"/>
<attribute name="VALUE" x="-276.86" y="222.885" size="1.27" layer="96" rot="R180" align="bottom-right"/>
</instance>
<instance part="C43" gate="G$1" x="-373.38" y="91.44" smashed="yes" rot="R90">
<attribute name="NAME" x="-375.285" y="91.44" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-371.475" y="91.44" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="C44" gate="G$1" x="-355.6" y="30.48" smashed="yes" rot="R90">
<attribute name="NAME" x="-357.505" y="30.48" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-353.695" y="30.48" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="IC12" gate="-1" x="-335.28" y="93.98" smashed="yes">
<attribute name="NAME" x="-332.74" y="97.155" size="1.778" layer="95"/>
<attribute name="VALUE" x="-332.74" y="88.9" size="1.778" layer="96"/>
</instance>
<instance part="IC12" gate="-2" x="-309.88" y="93.98" smashed="yes">
<attribute name="NAME" x="-307.34" y="97.155" size="1.778" layer="95"/>
<attribute name="VALUE" x="-307.34" y="88.9" size="1.778" layer="96"/>
</instance>
<instance part="IC12" gate="-3" x="-335.28" y="68.58" smashed="yes">
<attribute name="NAME" x="-332.74" y="71.755" size="1.778" layer="95"/>
<attribute name="VALUE" x="-332.74" y="63.5" size="1.778" layer="96"/>
</instance>
<instance part="IC12" gate="-4" x="-335.28" y="55.88" smashed="yes">
<attribute name="NAME" x="-332.74" y="59.055" size="1.778" layer="95"/>
<attribute name="VALUE" x="-332.74" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="IC12" gate="-5" x="-335.28" y="43.18" smashed="yes">
<attribute name="NAME" x="-332.74" y="46.355" size="1.778" layer="95"/>
<attribute name="VALUE" x="-332.74" y="38.1" size="1.778" layer="96"/>
</instance>
<instance part="IC12" gate="-6" x="-335.28" y="30.48" smashed="yes">
<attribute name="NAME" x="-332.74" y="33.655" size="1.778" layer="95"/>
<attribute name="VALUE" x="-332.74" y="25.4" size="1.778" layer="96"/>
</instance>
<instance part="IC12" gate="-7" x="-365.76" y="63.5" smashed="yes">
<attribute name="NAME" x="-366.395" y="62.865" size="1.778" layer="95"/>
</instance>
<instance part="GND73" gate="1" x="-342.9" y="22.86" smashed="yes"/>
<instance part="C45" gate="G$1" x="-355.6" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="-357.505" y="63.5" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-353.695" y="63.5" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="P+15" gate="VCC" x="-365.76" y="78.74" smashed="yes"/>
<instance part="GND74" gate="1" x="-365.76" y="48.26" smashed="yes"/>
<instance part="GND75" gate="1" x="-45.72" y="58.42" smashed="yes"/>
<instance part="P+17" gate="VCC" x="-99.06" y="111.76" smashed="yes"/>
<instance part="C46" gate="G$1" x="-124.46" y="86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="-126.365" y="86.36" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-122.555" y="86.36" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="GND76" gate="1" x="-124.46" y="76.2" smashed="yes"/>
<instance part="P+20" gate="VCC" x="-124.46" y="99.06" smashed="yes"/>
<instance part="C47" gate="G$1" x="-96.52" y="127" smashed="yes" rot="R180">
<attribute name="NAME" x="-96.52" y="125.095" size="1.27" layer="95" rot="R180" align="top-right"/>
<attribute name="VALUE" x="-96.52" y="128.905" size="1.27" layer="96" rot="R180" align="bottom-right"/>
</instance>
<instance part="GND77" gate="1" x="-91.44" y="124.46" smashed="yes"/>
<instance part="P+21" gate="VCC" x="-104.14" y="132.08" smashed="yes"/>
<instance part="C48" gate="G$1" x="-25.4" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="-27.305" y="109.22" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-23.495" y="109.22" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="GND78" gate="1" x="-25.4" y="99.06" smashed="yes"/>
<instance part="P+22" gate="VCC" x="-25.4" y="121.92" smashed="yes"/>
<instance part="C49" gate="G$1" x="-50.8" y="50.8" smashed="yes">
<attribute name="NAME" x="-50.8" y="52.705" size="1.27" layer="95" align="top-right"/>
<attribute name="VALUE" x="-50.8" y="48.895" size="1.27" layer="96" align="bottom-right"/>
</instance>
<instance part="GND79" gate="1" x="-60.96" y="50.8" smashed="yes" rot="R270"/>
<instance part="P+23" gate="VCC" x="-38.1" y="50.8" smashed="yes" rot="R270"/>
<instance part="P+25" gate="VCC" x="-365.76" y="162.56" smashed="yes"/>
<instance part="GND81" gate="1" x="-365.76" y="132.08" smashed="yes"/>
<instance part="C50" gate="G$1" x="-365.76" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="-368.935" y="147.32" size="1.27" layer="95" rot="R270" align="top-right"/>
<attribute name="VALUE" x="-362.585" y="147.32" size="1.27" layer="96" rot="R270" align="bottom-right"/>
</instance>
<instance part="H1" gate="G$1" x="-175.26" y="101.6" smashed="yes">
<attribute name="NAME" x="-173.228" y="102.1842" size="1.778" layer="95"/>
<attribute name="VALUE" x="-173.228" y="99.1362" size="1.778" layer="96"/>
</instance>
<instance part="H2" gate="G$1" x="-175.26" y="93.98" smashed="yes">
<attribute name="NAME" x="-173.228" y="94.5642" size="1.778" layer="95"/>
<attribute name="VALUE" x="-173.228" y="91.5162" size="1.778" layer="96"/>
</instance>
<instance part="H3" gate="G$1" x="-175.26" y="86.36" smashed="yes">
<attribute name="NAME" x="-173.228" y="86.9442" size="1.778" layer="95"/>
<attribute name="VALUE" x="-173.228" y="83.8962" size="1.778" layer="96"/>
</instance>
<instance part="H4" gate="G$1" x="-175.26" y="78.74" smashed="yes">
<attribute name="NAME" x="-173.228" y="79.3242" size="1.778" layer="95"/>
<attribute name="VALUE" x="-173.228" y="76.2762" size="1.778" layer="96"/>
</instance>
<instance part="TP38" gate="G$1" x="-317.5" y="99.06" smashed="yes">
<attribute name="NAME" x="-317.52" y="100.08" size="1" layer="95" align="center"/>
<attribute name="TP_SIGNAL_NAME" x="-316.23" y="97.79" size="1" layer="97"/>
</instance>
<instance part="D4" gate="G$1" x="-337.82" y="223.52" smashed="yes" rot="R90">
<attribute name="NAME" x="-342.265" y="221.234" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-339.471" y="218.694" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="BAT1" gate="G$1" x="-350.52" y="213.36" smashed="yes">
<attribute name="NAME" x="-353.695" y="209.55" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-345.44" y="209.55" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="X1" gate="G$1" x="-251.46" y="25.4" smashed="yes">
<attribute name="VALUE" x="-255.27" y="14.605" size="1.778" layer="96"/>
<attribute name="NAME" x="-255.27" y="34.29" size="1.778" layer="95"/>
</instance>
<instance part="GND82" gate="1" x="-241.3" y="12.7" smashed="yes"/>
<instance part="GND83" gate="1" x="-261.62" y="12.7" smashed="yes"/>
<instance part="H5" gate="G$1" x="-175.26" y="71.12" smashed="yes">
<attribute name="NAME" x="-173.228" y="71.7042" size="1.778" layer="95"/>
<attribute name="VALUE" x="-173.228" y="68.6562" size="1.778" layer="96"/>
</instance>
<instance part="H6" gate="G$1" x="-175.26" y="63.5" smashed="yes">
<attribute name="NAME" x="-173.228" y="64.0842" size="1.778" layer="95"/>
<attribute name="VALUE" x="-173.228" y="61.0362" size="1.778" layer="96"/>
</instance>
<instance part="H7" gate="G$1" x="-175.26" y="55.88" smashed="yes">
<attribute name="NAME" x="-173.228" y="56.4642" size="1.778" layer="95"/>
<attribute name="VALUE" x="-173.228" y="53.4162" size="1.778" layer="96"/>
</instance>
<instance part="H8" gate="G$1" x="-175.26" y="48.26" smashed="yes">
<attribute name="NAME" x="-173.228" y="48.8442" size="1.778" layer="95"/>
<attribute name="VALUE" x="-173.228" y="45.7962" size="1.778" layer="96"/>
</instance>
<instance part="H9" gate="G$1" x="-175.26" y="40.64" smashed="yes">
<attribute name="NAME" x="-173.228" y="41.2242" size="1.778" layer="95"/>
<attribute name="VALUE" x="-173.228" y="38.1762" size="1.778" layer="96"/>
</instance>
<instance part="FID3" gate="FID$1" x="-127" y="27.94" smashed="yes">
<attribute name="NAME" x="-127" y="31.75" size="1.27" layer="94" align="center"/>
</instance>
<instance part="FID4" gate="FID$1" x="-127" y="15.24" smashed="yes">
<attribute name="NAME" x="-127" y="19.05" size="1.27" layer="94" align="center"/>
</instance>
<instance part="TP39" gate="G$1" x="-25.4" y="91.44" smashed="yes">
<attribute name="NAME" x="-25.42" y="92.46" size="1" layer="95" align="center"/>
<attribute name="TP_SIGNAL_NAME" x="-24.13" y="90.17" size="1" layer="97"/>
</instance>
<instance part="TP40" gate="G$1" x="-22.86" y="91.44" smashed="yes">
<attribute name="NAME" x="-22.88" y="92.46" size="1" layer="95" align="center"/>
<attribute name="TP_SIGNAL_NAME" x="-21.59" y="90.17" size="1" layer="97"/>
</instance>
<instance part="TP41" gate="G$1" x="-20.32" y="91.44" smashed="yes">
<attribute name="NAME" x="-20.34" y="92.46" size="1" layer="95" align="center"/>
<attribute name="TP_SIGNAL_NAME" x="-19.05" y="90.17" size="1" layer="97"/>
</instance>
<instance part="TP42" gate="G$1" x="-342.9" y="88.9" smashed="yes" rot="R180">
<attribute name="NAME" x="-342.88" y="87.88" size="1" layer="95" rot="R180" align="center"/>
<attribute name="TP_SIGNAL_NAME" x="-344.17" y="90.17" size="1" layer="97" rot="R180"/>
</instance>
<instance part="C65" gate="G$1" x="-317.5" y="205.74" smashed="yes" rot="R90">
<attribute name="NAME" x="-319.405" y="205.74" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-315.595" y="205.74" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="C66" gate="G$1" x="-322.58" y="205.74" smashed="yes" rot="R90">
<attribute name="NAME" x="-324.485" y="205.74" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-320.675" y="205.74" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="C67" gate="G$1" x="-223.52" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="-225.425" y="220.98" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-221.615" y="220.98" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="C68" gate="G$1" x="-228.6" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="-230.505" y="220.98" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-226.695" y="220.98" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="C69" gate="G$1" x="-233.68" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="-235.585" y="220.98" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-231.775" y="220.98" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="C70" gate="G$1" x="-238.76" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="-240.665" y="220.98" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-236.855" y="220.98" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="GND93" gate="1" x="-223.52" y="213.36" smashed="yes"/>
<instance part="GND94" gate="1" x="-228.6" y="213.36" smashed="yes"/>
<instance part="GND95" gate="1" x="-233.68" y="213.36" smashed="yes"/>
<instance part="GND96" gate="1" x="-238.76" y="213.36" smashed="yes"/>
<instance part="H10" gate="G$1" x="-175.26" y="33.02" smashed="yes">
<attribute name="NAME" x="-173.228" y="33.6042" size="1.778" layer="95"/>
<attribute name="VALUE" x="-173.228" y="30.5562" size="1.778" layer="96"/>
</instance>
<instance part="H11" gate="G$1" x="-175.26" y="25.4" smashed="yes">
<attribute name="NAME" x="-173.228" y="25.9842" size="1.778" layer="95"/>
<attribute name="VALUE" x="-173.228" y="22.9362" size="1.778" layer="96"/>
</instance>
<instance part="H12" gate="G$1" x="-175.26" y="17.78" smashed="yes">
<attribute name="NAME" x="-173.228" y="18.3642" size="1.778" layer="95"/>
<attribute name="VALUE" x="-173.228" y="15.3162" size="1.778" layer="96"/>
</instance>
<instance part="GND97" gate="1" x="-180.34" y="12.7" smashed="yes"/>
<instance part="GND20" gate="1" x="-213.36" y="68.58" smashed="yes" rot="MR0"/>
<instance part="JP2" gate="A" x="-226.06" y="81.28" smashed="yes">
<attribute name="NAME" x="-232.41" y="89.535" size="1.778" layer="95"/>
<attribute name="VALUE" x="-240.03" y="68.58" size="1.778" layer="96"/>
</instance>
<instance part="R52" gate="G$1" x="-246.38" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="-246.38" y="72.39" size="1.27" layer="95" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="-246.38" y="76.2" size="1.27" layer="96" rot="R180" align="bottom-center"/>
</instance>
<instance part="P+1" gate="VCC" x="-266.7" y="83.82" smashed="yes">
<attribute name="VALUE" x="-269.24" y="84.836" size="1.778" layer="96"/>
</instance>
<instance part="C10" gate="G$1" x="-266.7" y="71.12" smashed="yes" rot="R270">
<attribute name="NAME" x="-265.176" y="71.501" size="1.778" layer="95"/>
<attribute name="VALUE" x="-265.176" y="66.421" size="1.778" layer="96"/>
</instance>
<instance part="GND28" gate="1" x="-266.7" y="60.96" smashed="yes" rot="MR0"/>
<instance part="TP1" gate="G$1" x="-198.12" y="73.66" smashed="yes">
<attribute name="NAME" x="-199.39" y="74.93" size="1.778" layer="95"/>
<attribute name="TP_SIGNAL_NAME" x="-196.85" y="72.39" size="1.778" layer="97"/>
</instance>
<instance part="GND31" gate="1" x="-198.12" y="66.04" smashed="yes" rot="MR0"/>
<instance part="TP31" gate="G$1" x="-246.38" y="86.36" smashed="yes" rot="R90">
<attribute name="NAME" x="-247.65" y="87.63" size="1.778" layer="95" rot="R180"/>
<attribute name="TP_SIGNAL_NAME" x="-245.11" y="87.63" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="TP32" gate="G$1" x="-246.38" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="-247.65" y="85.09" size="1.778" layer="95" rot="R180"/>
<attribute name="TP_SIGNAL_NAME" x="-245.11" y="85.09" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="TP33" gate="G$1" x="-246.38" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="-247.65" y="82.55" size="1.778" layer="95" rot="R180"/>
<attribute name="TP_SIGNAL_NAME" x="-245.11" y="82.55" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="TP34" gate="G$1" x="-246.38" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="-247.65" y="80.01" size="1.778" layer="95" rot="R180"/>
<attribute name="TP_SIGNAL_NAME" x="-245.11" y="80.01" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="TP44" gate="G$1" x="-246.38" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="-247.65" y="77.47" size="1.778" layer="95" rot="R180"/>
<attribute name="TP_SIGNAL_NAME" x="-245.11" y="77.47" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="TP45" gate="G$1" x="-208.28" y="76.2" smashed="yes" rot="R270">
<attribute name="NAME" x="-209.55" y="74.93" size="1.778" layer="95" rot="R270"/>
<attribute name="TP_SIGNAL_NAME" x="-209.55" y="74.93" size="1.778" layer="97" rot="R270"/>
</instance>
<instance part="TP11" gate="G$1" x="-17.78" y="91.44" smashed="yes">
<attribute name="NAME" x="-17.8" y="92.46" size="1" layer="95" align="center"/>
<attribute name="TP_SIGNAL_NAME" x="-16.51" y="90.17" size="1" layer="97"/>
</instance>
<instance part="TP2" gate="G$1" x="-259.08" y="76.2" smashed="yes">
<attribute name="NAME" x="-259.08" y="77.216" size="1.778" layer="95" rot="R90"/>
<attribute name="TP_SIGNAL_NAME" x="-257.81" y="74.93" size="1.778" layer="97"/>
</instance>
<instance part="R9" gate="G$1" x="-66.04" y="139.7" smashed="yes">
<attribute name="NAME" x="-68.58" y="140.97" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-63.5" y="142.24" size="1.27" layer="96" rot="R180" align="bottom-center"/>
</instance>
<instance part="R16" gate="G$1" x="-66.04" y="134.62" smashed="yes">
<attribute name="NAME" x="-68.58" y="135.89" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-63.5" y="137.16" size="1.27" layer="96" rot="R180" align="bottom-center"/>
</instance>
<instance part="P+4" gate="VCC" x="-55.88" y="144.78" smashed="yes"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND29" gate="1" pin="GND"/>
<wire x1="-350.52" y1="203.2" x2="-350.52" y2="205.74" width="0.1524" layer="91"/>
<pinref part="JP1" gate="G$1" pin="2"/>
<wire x1="-370.84" y1="226.06" x2="-365.76" y2="226.06" width="0.1524" layer="91"/>
<wire x1="-365.76" y1="226.06" x2="-365.76" y2="205.74" width="0.1524" layer="91"/>
<wire x1="-365.76" y1="205.74" x2="-350.52" y2="205.74" width="0.1524" layer="91"/>
<junction x="-350.52" y="205.74"/>
<wire x1="-350.52" y1="205.74" x2="-337.82" y2="205.74" width="0.1524" layer="91"/>
<wire x1="-337.82" y1="205.74" x2="-337.82" y2="208.28" width="0.1524" layer="91"/>
<pinref part="BAT2" gate="G$1" pin="-"/>
<pinref part="BAT1" gate="G$1" pin="-"/>
<wire x1="-350.52" y1="208.28" x2="-350.52" y2="205.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND52" gate="1" pin="GND"/>
<wire x1="-302.26" y1="203.2" x2="-302.26" y2="198.12" width="0.1524" layer="91"/>
<wire x1="-302.26" y1="198.12" x2="-302.26" y2="195.58" width="0.1524" layer="91"/>
<wire x1="-312.42" y1="203.2" x2="-312.42" y2="198.12" width="0.1524" layer="91"/>
<wire x1="-312.42" y1="198.12" x2="-307.34" y2="198.12" width="0.1524" layer="91"/>
<junction x="-302.26" y="198.12"/>
<wire x1="-307.34" y1="198.12" x2="-302.26" y2="198.12" width="0.1524" layer="91"/>
<wire x1="-307.34" y1="203.2" x2="-307.34" y2="198.12" width="0.1524" layer="91"/>
<junction x="-307.34" y="198.12"/>
<pinref part="C39" gate="G$1" pin="1"/>
<pinref part="C40" gate="G$1" pin="1"/>
<pinref part="C41" gate="G$1" pin="1"/>
<pinref part="C66" gate="G$1" pin="1"/>
<wire x1="-322.58" y1="203.2" x2="-322.58" y2="198.12" width="0.1524" layer="91"/>
<wire x1="-322.58" y1="198.12" x2="-317.5" y2="198.12" width="0.1524" layer="91"/>
<pinref part="C65" gate="G$1" pin="1"/>
<wire x1="-317.5" y1="198.12" x2="-317.5" y2="203.2" width="0.1524" layer="91"/>
<wire x1="-317.5" y1="198.12" x2="-312.42" y2="198.12" width="0.1524" layer="91"/>
<junction x="-317.5" y="198.12"/>
<junction x="-312.42" y="198.12"/>
</segment>
<segment>
<pinref part="GND54" gate="1" pin="GND"/>
<wire x1="-274.32" y1="220.98" x2="-271.78" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C42" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="RTC1" gate="G$1" pin="GND"/>
<pinref part="GND59" gate="1" pin="GND"/>
<wire x1="-284.48" y1="193.04" x2="-284.48" y2="190.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND60" gate="1" pin="GND"/>
<wire x1="-218.44" y1="218.44" x2="-218.44" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND62" gate="1" pin="GND"/>
<wire x1="-213.36" y1="218.44" x2="-213.36" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND65" gate="1" pin="GND"/>
<wire x1="-208.28" y1="218.44" x2="-208.28" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C37" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND66" gate="1" pin="GND"/>
<wire x1="-203.2" y1="218.44" x2="-203.2" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="IC4" gate="G$1" pin="GND"/>
<wire x1="-365.76" y1="93.98" x2="-368.3" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-368.3" y1="93.98" x2="-368.3" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-368.3" y1="86.36" x2="-373.38" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-373.38" y1="86.36" x2="-373.38" y2="88.9" width="0.1524" layer="91"/>
<pinref part="GND67" gate="1" pin="GND"/>
<wire x1="-373.38" y1="86.36" x2="-373.38" y2="83.82" width="0.1524" layer="91"/>
<junction x="-373.38" y="86.36"/>
<pinref part="C43" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="IC10" gate="-7" pin="GND"/>
<pinref part="GND68" gate="1" pin="GND"/>
<wire x1="-365.76" y1="22.86" x2="-365.76" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-365.76" y1="20.32" x2="-365.76" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-365.76" y1="20.32" x2="-355.6" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-355.6" y1="20.32" x2="-355.6" y2="27.94" width="0.1524" layer="91"/>
<junction x="-365.76" y="20.32"/>
<pinref part="C44" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND69" gate="1" pin="GND"/>
<pinref part="LED1" gate="G$1" pin="C"/>
<wire x1="-27.94" y1="53.34" x2="-27.94" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="RTC1" gate="G$1" pin="CLKOE"/>
<pinref part="GND71" gate="1" pin="GND"/>
<wire x1="-271.78" y1="198.12" x2="-269.24" y2="198.12" width="0.1524" layer="91"/>
<wire x1="-269.24" y1="198.12" x2="-269.24" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND72" gate="1" pin="GND"/>
<wire x1="-223.52" y1="25.4" x2="-220.98" y2="25.4" width="0.1524" layer="91"/>
<pinref part="PROG1" gate="G$1" pin="5"/>
<wire x1="-220.98" y1="25.4" x2="-220.98" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-220.98" y1="27.94" x2="-215.9" y2="27.94" width="0.1524" layer="91"/>
<pinref part="PROG1" gate="G$1" pin="7"/>
<wire x1="-215.9" y1="25.4" x2="-220.98" y2="25.4" width="0.1524" layer="91"/>
<junction x="-220.98" y="25.4"/>
</segment>
<segment>
<pinref part="IC12" gate="-3" pin="I"/>
<pinref part="GND73" gate="1" pin="GND"/>
<wire x1="-340.36" y1="68.58" x2="-342.9" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-342.9" y1="68.58" x2="-342.9" y2="55.88" width="0.1524" layer="91"/>
<pinref part="IC12" gate="-4" pin="I"/>
<wire x1="-342.9" y1="55.88" x2="-342.9" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-342.9" y1="43.18" x2="-342.9" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-342.9" y1="30.48" x2="-342.9" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-340.36" y1="55.88" x2="-342.9" y2="55.88" width="0.1524" layer="91"/>
<junction x="-342.9" y="55.88"/>
<pinref part="IC12" gate="-5" pin="I"/>
<wire x1="-340.36" y1="43.18" x2="-342.9" y2="43.18" width="0.1524" layer="91"/>
<junction x="-342.9" y="43.18"/>
<pinref part="IC12" gate="-6" pin="I"/>
<wire x1="-340.36" y1="30.48" x2="-342.9" y2="30.48" width="0.1524" layer="91"/>
<junction x="-342.9" y="30.48"/>
</segment>
<segment>
<pinref part="GND74" gate="1" pin="GND"/>
<pinref part="IC12" gate="-7" pin="GND"/>
<wire x1="-365.76" y1="50.8" x2="-365.76" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C45" gate="G$1" pin="1"/>
<wire x1="-365.76" y1="53.34" x2="-365.76" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-365.76" y1="53.34" x2="-355.6" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-355.6" y1="53.34" x2="-355.6" y2="60.96" width="0.1524" layer="91"/>
<junction x="-365.76" y="53.34"/>
</segment>
<segment>
<pinref part="IC11" gate="G$1" pin="EXP"/>
<wire x1="-45.72" y1="63.5" x2="-45.72" y2="60.96" width="0.1524" layer="91"/>
<pinref part="GND75" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND77" gate="1" pin="GND"/>
<pinref part="C47" gate="G$1" pin="1"/>
<wire x1="-93.98" y1="127" x2="-91.44" y2="127" width="0.1524" layer="91"/>
<pinref part="IC11" gate="G$1" pin="VSS@3"/>
<wire x1="-78.74" y1="119.38" x2="-78.74" y2="127" width="0.1524" layer="91"/>
<wire x1="-78.74" y1="127" x2="-91.44" y2="127" width="0.1524" layer="91"/>
<junction x="-91.44" y="127"/>
</segment>
<segment>
<pinref part="C48" gate="G$1" pin="1"/>
<pinref part="GND78" gate="1" pin="GND"/>
<wire x1="-25.4" y1="106.68" x2="-25.4" y2="104.14" width="0.1524" layer="91"/>
<pinref part="IC11" gate="G$1" pin="VSS@2"/>
<wire x1="-25.4" y1="104.14" x2="-25.4" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="104.14" x2="-25.4" y2="104.14" width="0.1524" layer="91"/>
<junction x="-25.4" y="104.14"/>
</segment>
<segment>
<pinref part="C49" gate="G$1" pin="1"/>
<pinref part="GND79" gate="1" pin="GND"/>
<wire x1="-53.34" y1="50.8" x2="-55.88" y2="50.8" width="0.1524" layer="91"/>
<pinref part="IC11" gate="G$1" pin="VSS@1"/>
<wire x1="-55.88" y1="50.8" x2="-58.42" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="63.5" x2="-55.88" y2="50.8" width="0.1524" layer="91"/>
<junction x="-55.88" y="50.8"/>
</segment>
<segment>
<pinref part="C46" gate="G$1" pin="1"/>
<pinref part="GND76" gate="1" pin="GND"/>
<wire x1="-124.46" y1="83.82" x2="-124.46" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-124.46" y1="81.28" x2="-124.46" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-124.46" y1="81.28" x2="-116.84" y2="81.28" width="0.1524" layer="91"/>
<junction x="-124.46" y="81.28"/>
<wire x1="-116.84" y1="81.28" x2="-116.84" y2="88.9" width="0.1524" layer="91"/>
<pinref part="IC11" gate="G$1" pin="VSSA/VREF-"/>
<wire x1="-116.84" y1="88.9" x2="-96.52" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND81" gate="1" pin="GND"/>
<pinref part="C50" gate="G$1" pin="1"/>
<wire x1="-365.76" y1="134.62" x2="-365.76" y2="142.24" width="0.1524" layer="91"/>
<pinref part="IC9" gate="G$1" pin="GND"/>
<wire x1="-365.76" y1="142.24" x2="-365.76" y2="144.78" width="0.1524" layer="91"/>
<wire x1="-365.76" y1="144.78" x2="-365.76" y2="147.32" width="0.1524" layer="91"/>
<wire x1="-360.68" y1="144.78" x2="-365.76" y2="144.78" width="0.1524" layer="91"/>
<junction x="-365.76" y="144.78"/>
<pinref part="IC9" gate="G$1" pin="NC"/>
<wire x1="-360.68" y1="142.24" x2="-365.76" y2="142.24" width="0.1524" layer="91"/>
<junction x="-365.76" y="142.24"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="G2"/>
<pinref part="GND82" gate="1" pin="GND"/>
<wire x1="-243.84" y1="17.78" x2="-241.3" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-241.3" y1="17.78" x2="-241.3" y2="15.24" width="0.1524" layer="91"/>
<junction x="-241.3" y="17.78"/>
<pinref part="X1" gate="G$1" pin="G1"/>
<wire x1="-243.84" y1="33.02" x2="-241.3" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-241.3" y1="33.02" x2="-241.3" y2="17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND83" gate="1" pin="GND"/>
<wire x1="-261.62" y1="15.24" x2="-261.62" y2="20.32" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="5"/>
<wire x1="-261.62" y1="20.32" x2="-259.08" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C70" gate="G$1" pin="1"/>
<pinref part="GND96" gate="1" pin="GND"/>
<wire x1="-238.76" y1="218.44" x2="-238.76" y2="215.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C69" gate="G$1" pin="1"/>
<pinref part="GND95" gate="1" pin="GND"/>
<wire x1="-233.68" y1="218.44" x2="-233.68" y2="215.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C68" gate="G$1" pin="1"/>
<pinref part="GND94" gate="1" pin="GND"/>
<wire x1="-228.6" y1="218.44" x2="-228.6" y2="215.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C67" gate="G$1" pin="1"/>
<pinref part="GND93" gate="1" pin="GND"/>
<wire x1="-223.52" y1="218.44" x2="-223.52" y2="215.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND97" gate="1" pin="GND"/>
<wire x1="-180.34" y1="15.24" x2="-180.34" y2="17.78" width="0.1524" layer="91"/>
<pinref part="H1" gate="G$1" pin="MOUNT"/>
<wire x1="-180.34" y1="17.78" x2="-180.34" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="25.4" x2="-180.34" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="33.02" x2="-180.34" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="40.64" x2="-180.34" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="48.26" x2="-180.34" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="55.88" x2="-180.34" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="63.5" x2="-180.34" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="71.12" x2="-180.34" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="78.74" x2="-180.34" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="86.36" x2="-180.34" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="93.98" x2="-180.34" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="101.6" x2="-177.8" y2="101.6" width="0.1524" layer="91"/>
<pinref part="H2" gate="G$1" pin="MOUNT"/>
<wire x1="-177.8" y1="93.98" x2="-180.34" y2="93.98" width="0.1524" layer="91"/>
<junction x="-180.34" y="93.98"/>
<pinref part="H3" gate="G$1" pin="MOUNT"/>
<wire x1="-177.8" y1="86.36" x2="-180.34" y2="86.36" width="0.1524" layer="91"/>
<junction x="-180.34" y="86.36"/>
<pinref part="H4" gate="G$1" pin="MOUNT"/>
<wire x1="-177.8" y1="78.74" x2="-180.34" y2="78.74" width="0.1524" layer="91"/>
<junction x="-180.34" y="78.74"/>
<pinref part="H5" gate="G$1" pin="MOUNT"/>
<wire x1="-177.8" y1="71.12" x2="-180.34" y2="71.12" width="0.1524" layer="91"/>
<junction x="-180.34" y="71.12"/>
<pinref part="H6" gate="G$1" pin="MOUNT"/>
<wire x1="-177.8" y1="63.5" x2="-180.34" y2="63.5" width="0.1524" layer="91"/>
<junction x="-180.34" y="63.5"/>
<pinref part="H7" gate="G$1" pin="MOUNT"/>
<wire x1="-177.8" y1="55.88" x2="-180.34" y2="55.88" width="0.1524" layer="91"/>
<junction x="-180.34" y="55.88"/>
<pinref part="H8" gate="G$1" pin="MOUNT"/>
<wire x1="-177.8" y1="48.26" x2="-180.34" y2="48.26" width="0.1524" layer="91"/>
<junction x="-180.34" y="48.26"/>
<pinref part="H9" gate="G$1" pin="MOUNT"/>
<wire x1="-177.8" y1="40.64" x2="-180.34" y2="40.64" width="0.1524" layer="91"/>
<junction x="-180.34" y="40.64"/>
<pinref part="H10" gate="G$1" pin="MOUNT"/>
<wire x1="-177.8" y1="33.02" x2="-180.34" y2="33.02" width="0.1524" layer="91"/>
<junction x="-180.34" y="33.02"/>
<pinref part="H11" gate="G$1" pin="MOUNT"/>
<wire x1="-177.8" y1="25.4" x2="-180.34" y2="25.4" width="0.1524" layer="91"/>
<junction x="-180.34" y="25.4"/>
<pinref part="H12" gate="G$1" pin="MOUNT"/>
<wire x1="-177.8" y1="17.78" x2="-180.34" y2="17.78" width="0.1524" layer="91"/>
<junction x="-180.34" y="17.78"/>
</segment>
<segment>
<wire x1="-220.98" y1="73.66" x2="-213.36" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-213.36" y1="73.66" x2="-213.36" y2="71.12" width="0.1524" layer="91"/>
<pinref part="GND20" gate="1" pin="GND"/>
<pinref part="JP2" gate="A" pin="12"/>
</segment>
<segment>
<pinref part="GND28" gate="1" pin="GND"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="-266.7" y1="63.5" x2="-266.7" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND31" gate="1" pin="GND"/>
<pinref part="TP1" gate="G$1" pin="TP"/>
<wire x1="-198.12" y1="71.12" x2="-198.12" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="TP35" gate="G$1" pin="TP"/>
<wire x1="-350.52" y1="228.6" x2="-350.52" y2="231.14" width="0.1524" layer="91"/>
<wire x1="-350.52" y1="228.6" x2="-337.82" y2="228.6" width="0.1524" layer="91"/>
<junction x="-350.52" y="228.6"/>
<wire x1="-350.52" y1="226.06" x2="-350.52" y2="228.6" width="0.1524" layer="91"/>
<wire x1="-337.82" y1="226.06" x2="-337.82" y2="228.6" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="-355.6" y1="228.6" x2="-350.52" y2="228.6" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="C"/>
<pinref part="D4" gate="G$1" pin="C"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="-337.82" y1="228.6" x2="-327.66" y2="228.6" width="0.1524" layer="91"/>
<junction x="-337.82" y="228.6"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="-337.82" y1="218.44" x2="-337.82" y2="220.98" width="0.1524" layer="91"/>
<pinref part="BAT2" gate="G$1" pin="+"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="RTC1" gate="G$1" pin="VBACK"/>
<wire x1="-302.26" y1="213.36" x2="-302.26" y2="210.82" width="0.1524" layer="91"/>
<wire x1="-297.18" y1="213.36" x2="-302.26" y2="213.36" width="0.1524" layer="91"/>
<junction x="-302.26" y="213.36"/>
<wire x1="-307.34" y1="210.82" x2="-307.34" y2="213.36" width="0.1524" layer="91"/>
<wire x1="-307.34" y1="213.36" x2="-302.26" y2="213.36" width="0.1524" layer="91"/>
<wire x1="-312.42" y1="210.82" x2="-312.42" y2="213.36" width="0.1524" layer="91"/>
<wire x1="-312.42" y1="213.36" x2="-307.34" y2="213.36" width="0.1524" layer="91"/>
<junction x="-307.34" y="213.36"/>
<pinref part="C39" gate="G$1" pin="2"/>
<pinref part="C40" gate="G$1" pin="2"/>
<pinref part="C41" gate="G$1" pin="2"/>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="-302.26" y1="218.44" x2="-302.26" y2="213.36" width="0.1524" layer="91"/>
<wire x1="-312.42" y1="213.36" x2="-317.5" y2="213.36" width="0.1524" layer="91"/>
<junction x="-312.42" y="213.36"/>
<pinref part="C66" gate="G$1" pin="2"/>
<wire x1="-317.5" y1="213.36" x2="-322.58" y2="213.36" width="0.1524" layer="91"/>
<wire x1="-322.58" y1="213.36" x2="-322.58" y2="210.82" width="0.1524" layer="91"/>
<pinref part="C65" gate="G$1" pin="2"/>
<wire x1="-317.5" y1="210.82" x2="-317.5" y2="213.36" width="0.1524" layer="91"/>
<junction x="-317.5" y="213.36"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="1"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="-370.84" y1="228.6" x2="-365.76" y2="228.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="4"/>
<wire x1="-287.02" y1="228.6" x2="-287.02" y2="231.14" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="-317.5" y1="228.6" x2="-302.26" y2="228.6" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="-302.26" y1="228.6" x2="-287.02" y2="228.6" width="0.1524" layer="91"/>
<wire x1="-302.26" y1="223.52" x2="-302.26" y2="228.6" width="0.1524" layer="91"/>
<junction x="-302.26" y="228.6"/>
<pinref part="TP36" gate="G$1" pin="TP"/>
<wire x1="-302.26" y1="228.6" x2="-302.26" y2="231.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="R35" gate="G$1" pin="2"/>
<pinref part="P+9" gate="VCC" pin="VCC"/>
<wire x1="-327.66" y1="109.22" x2="-325.12" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+10" gate="VCC" pin="VCC"/>
<pinref part="IC10" gate="-7" pin="VCC"/>
<wire x1="-365.76" y1="43.18" x2="-365.76" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-365.76" y1="40.64" x2="-365.76" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-365.76" y1="40.64" x2="-355.6" y2="40.64" width="0.1524" layer="91"/>
<junction x="-365.76" y="40.64"/>
<wire x1="-355.6" y1="40.64" x2="-355.6" y2="35.56" width="0.1524" layer="91"/>
<pinref part="C44" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="PROG1" gate="G$1" pin="3"/>
<wire x1="-215.9" y1="30.48" x2="-223.52" y2="30.48" width="0.1524" layer="91"/>
<pinref part="P+11" gate="VCC" pin="VCC"/>
<wire x1="-223.52" y1="30.48" x2="-223.52" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+15" gate="VCC" pin="VCC"/>
<pinref part="IC12" gate="-7" pin="VCC"/>
<wire x1="-365.76" y1="76.2" x2="-365.76" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-365.76" y1="73.66" x2="-365.76" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-365.76" y1="73.66" x2="-355.6" y2="73.66" width="0.1524" layer="91"/>
<junction x="-365.76" y="73.66"/>
<pinref part="C45" gate="G$1" pin="2"/>
<wire x1="-355.6" y1="73.66" x2="-355.6" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC11" gate="G$1" pin="VBAT"/>
<wire x1="-96.52" y1="106.68" x2="-99.06" y2="106.68" width="0.1524" layer="91"/>
<pinref part="P+17" gate="VCC" pin="VCC"/>
<wire x1="-99.06" y1="106.68" x2="-99.06" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+21" gate="VCC" pin="VCC"/>
<pinref part="C47" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="129.54" x2="-104.14" y2="127" width="0.1524" layer="91"/>
<pinref part="IC11" gate="G$1" pin="VDD@3"/>
<wire x1="-104.14" y1="127" x2="-101.6" y2="127" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="119.38" x2="-81.28" y2="121.92" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="121.92" x2="-104.14" y2="121.92" width="0.1524" layer="91"/>
<wire x1="-104.14" y1="121.92" x2="-104.14" y2="127" width="0.1524" layer="91"/>
<junction x="-104.14" y="127"/>
</segment>
<segment>
<pinref part="P+22" gate="VCC" pin="VCC"/>
<pinref part="C48" gate="G$1" pin="2"/>
<wire x1="-25.4" y1="119.38" x2="-25.4" y2="116.84" width="0.1524" layer="91"/>
<pinref part="IC11" gate="G$1" pin="VDD@2"/>
<wire x1="-25.4" y1="116.84" x2="-25.4" y2="114.3" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="106.68" x2="-30.48" y2="106.68" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="106.68" x2="-30.48" y2="116.84" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="116.84" x2="-25.4" y2="116.84" width="0.1524" layer="91"/>
<junction x="-25.4" y="116.84"/>
</segment>
<segment>
<pinref part="P+23" gate="VCC" pin="VCC"/>
<pinref part="C49" gate="G$1" pin="2"/>
<wire x1="-40.64" y1="50.8" x2="-43.18" y2="50.8" width="0.1524" layer="91"/>
<pinref part="IC11" gate="G$1" pin="VDD@1"/>
<wire x1="-43.18" y1="50.8" x2="-45.72" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="63.5" x2="-53.34" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="55.88" x2="-43.18" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="55.88" x2="-43.18" y2="50.8" width="0.1524" layer="91"/>
<junction x="-43.18" y="50.8"/>
</segment>
<segment>
<pinref part="P+20" gate="VCC" pin="VCC"/>
<pinref part="C46" gate="G$1" pin="2"/>
<wire x1="-124.46" y1="96.52" x2="-124.46" y2="93.98" width="0.1524" layer="91"/>
<pinref part="IC11" gate="G$1" pin="VDDA/VREF+"/>
<wire x1="-124.46" y1="93.98" x2="-124.46" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="86.36" x2="-119.38" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-119.38" y1="86.36" x2="-119.38" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-119.38" y1="93.98" x2="-124.46" y2="93.98" width="0.1524" layer="91"/>
<junction x="-124.46" y="93.98"/>
</segment>
<segment>
<pinref part="IC4" gate="G$1" pin="VCC"/>
<wire x1="-365.76" y1="99.06" x2="-373.38" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-373.38" y1="99.06" x2="-373.38" y2="96.52" width="0.1524" layer="91"/>
<wire x1="-373.38" y1="101.6" x2="-373.38" y2="99.06" width="0.1524" layer="91"/>
<junction x="-373.38" y="99.06"/>
<pinref part="C43" gate="G$1" pin="2"/>
<pinref part="P+8" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="S1" gate="G$1" pin="6"/>
<wire x1="-284.48" y1="228.6" x2="-284.48" y2="231.14" width="0.1524" layer="91"/>
<wire x1="-284.48" y1="228.6" x2="-264.16" y2="228.6" width="0.1524" layer="91"/>
<wire x1="-264.16" y1="228.6" x2="-259.08" y2="228.6" width="0.1524" layer="91"/>
<wire x1="-259.08" y1="228.6" x2="-254" y2="228.6" width="0.1524" layer="91"/>
<wire x1="-254" y1="228.6" x2="-238.76" y2="228.6" width="0.1524" layer="91"/>
<wire x1="-238.76" y1="228.6" x2="-233.68" y2="228.6" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="228.6" x2="-228.6" y2="228.6" width="0.1524" layer="91"/>
<wire x1="-228.6" y1="228.6" x2="-223.52" y2="228.6" width="0.1524" layer="91"/>
<wire x1="-223.52" y1="228.6" x2="-218.44" y2="228.6" width="0.1524" layer="91"/>
<wire x1="-218.44" y1="228.6" x2="-213.36" y2="228.6" width="0.1524" layer="91"/>
<wire x1="-213.36" y1="228.6" x2="-208.28" y2="228.6" width="0.1524" layer="91"/>
<wire x1="-208.28" y1="228.6" x2="-203.2" y2="228.6" width="0.1524" layer="91"/>
<wire x1="-203.2" y1="228.6" x2="-203.2" y2="226.06" width="0.1524" layer="91"/>
<wire x1="-208.28" y1="226.06" x2="-208.28" y2="228.6" width="0.1524" layer="91"/>
<junction x="-208.28" y="228.6"/>
<wire x1="-213.36" y1="226.06" x2="-213.36" y2="228.6" width="0.1524" layer="91"/>
<junction x="-213.36" y="228.6"/>
<wire x1="-218.44" y1="226.06" x2="-218.44" y2="228.6" width="0.1524" layer="91"/>
<junction x="-218.44" y="228.6"/>
<pinref part="TP37" gate="G$1" pin="TP"/>
<wire x1="-208.28" y1="231.14" x2="-208.28" y2="228.6" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="C37" gate="G$1" pin="2"/>
<pinref part="C38" gate="G$1" pin="2"/>
<wire x1="-203.2" y1="231.14" x2="-203.2" y2="228.6" width="0.1524" layer="91"/>
<junction x="-203.2" y="228.6"/>
<pinref part="P+7" gate="VCC" pin="VCC"/>
<pinref part="RTC1" gate="G$1" pin="VDD"/>
<pinref part="C42" gate="G$1" pin="2"/>
<wire x1="-281.94" y1="220.98" x2="-284.48" y2="220.98" width="0.1524" layer="91"/>
<wire x1="-284.48" y1="220.98" x2="-284.48" y2="218.44" width="0.1524" layer="91"/>
<wire x1="-284.48" y1="220.98" x2="-284.48" y2="228.6" width="0.1524" layer="91"/>
<junction x="-284.48" y="220.98"/>
<junction x="-284.48" y="228.6"/>
<pinref part="R39" gate="G$1" pin="2"/>
<wire x1="-264.16" y1="226.06" x2="-264.16" y2="228.6" width="0.1524" layer="91"/>
<junction x="-264.16" y="228.6"/>
<pinref part="R40" gate="G$1" pin="2"/>
<wire x1="-259.08" y1="226.06" x2="-259.08" y2="228.6" width="0.1524" layer="91"/>
<junction x="-259.08" y="228.6"/>
<pinref part="R41" gate="G$1" pin="2"/>
<wire x1="-254" y1="226.06" x2="-254" y2="228.6" width="0.1524" layer="91"/>
<junction x="-254" y="228.6"/>
<pinref part="C67" gate="G$1" pin="2"/>
<wire x1="-223.52" y1="226.06" x2="-223.52" y2="228.6" width="0.1524" layer="91"/>
<junction x="-223.52" y="228.6"/>
<pinref part="C68" gate="G$1" pin="2"/>
<wire x1="-228.6" y1="226.06" x2="-228.6" y2="228.6" width="0.1524" layer="91"/>
<junction x="-228.6" y="228.6"/>
<pinref part="C69" gate="G$1" pin="2"/>
<wire x1="-233.68" y1="226.06" x2="-233.68" y2="228.6" width="0.1524" layer="91"/>
<junction x="-233.68" y="228.6"/>
<pinref part="C70" gate="G$1" pin="2"/>
<wire x1="-238.76" y1="226.06" x2="-238.76" y2="228.6" width="0.1524" layer="91"/>
<junction x="-238.76" y="228.6"/>
</segment>
<segment>
<pinref part="P+25" gate="VCC" pin="VCC"/>
<pinref part="C50" gate="G$1" pin="2"/>
<wire x1="-365.76" y1="160.02" x2="-365.76" y2="157.48" width="0.1524" layer="91"/>
<pinref part="IC9" gate="G$1" pin="VDD"/>
<wire x1="-365.76" y1="157.48" x2="-365.76" y2="154.94" width="0.1524" layer="91"/>
<wire x1="-360.68" y1="157.48" x2="-365.76" y2="157.48" width="0.1524" layer="91"/>
<junction x="-365.76" y="157.48"/>
</segment>
<segment>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<wire x1="-266.7" y1="73.66" x2="-266.7" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<junction x="-266.7" y="73.66"/>
<pinref part="R52" gate="G$1" pin="2"/>
<wire x1="-251.46" y1="73.66" x2="-259.08" y2="73.66" width="0.1524" layer="91"/>
<pinref part="TP2" gate="G$1" pin="TP"/>
<wire x1="-259.08" y1="73.66" x2="-266.7" y2="73.66" width="0.1524" layer="91"/>
<junction x="-259.08" y="73.66"/>
</segment>
<segment>
<pinref part="P+4" gate="VCC" pin="VCC"/>
<wire x1="-55.88" y1="142.24" x2="-55.88" y2="139.7" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="-60.96" y1="139.7" x2="-55.88" y2="139.7" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="-55.88" y1="139.7" x2="-55.88" y2="134.62" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="134.62" x2="-60.96" y2="134.62" width="0.1524" layer="91"/>
<junction x="-55.88" y="139.7"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="INH"/>
<wire x1="-345.44" y1="99.06" x2="-340.36" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-340.36" y1="99.06" x2="-340.36" y2="104.14" width="0.1524" layer="91"/>
<pinref part="R35" gate="G$1" pin="1"/>
<wire x1="-340.36" y1="104.14" x2="-340.36" y2="109.22" width="0.1524" layer="91"/>
<wire x1="-340.36" y1="109.22" x2="-337.82" y2="109.22" width="0.1524" layer="91"/>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="-337.82" y1="104.14" x2="-340.36" y2="104.14" width="0.1524" layer="91"/>
<junction x="-340.36" y="104.14"/>
</segment>
</net>
<net name="CLK-CTL" class="0">
<segment>
<pinref part="R36" gate="G$1" pin="2"/>
<wire x1="-327.66" y1="104.14" x2="-325.12" y2="104.14" width="0.1524" layer="91"/>
<label x="-325.12" y="104.14" size="0.8128" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-66.04" y1="63.5" x2="-66.04" y2="60.96" width="0.1524" layer="91"/>
<label x="-66.04" y="60.96" size="0.8128" layer="95" rot="R270" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PB1"/>
</segment>
</net>
<net name="CLK-ADC1-OUT" class="0">
<segment>
<pinref part="IC10" gate="-1" pin="O"/>
<wire x1="-299.72" y1="81.28" x2="-297.18" y2="81.28" width="0.1524" layer="91"/>
<label x="-297.18" y="81.28" size="0.8128" layer="95" xref="yes"/>
</segment>
</net>
<net name="CLK-ADC2-OUT" class="0">
<segment>
<wire x1="-299.72" y1="43.18" x2="-297.18" y2="43.18" width="0.1524" layer="91"/>
<label x="-297.18" y="43.18" size="0.8128" layer="95" xref="yes"/>
<pinref part="IC10" gate="-4" pin="O"/>
</segment>
</net>
<net name="CLK-ADC3-OUT" class="0">
<segment>
<wire x1="-299.72" y1="68.58" x2="-297.18" y2="68.58" width="0.1524" layer="91"/>
<label x="-297.18" y="68.58" size="0.8128" layer="95" xref="yes"/>
<pinref part="IC10" gate="-2" pin="O"/>
</segment>
</net>
<net name="CLK-ADC4-OUT" class="0">
<segment>
<wire x1="-299.72" y1="30.48" x2="-297.18" y2="30.48" width="0.1524" layer="91"/>
<label x="-297.18" y="30.48" size="0.8128" layer="95" xref="yes"/>
<pinref part="IC10" gate="-5" pin="O"/>
</segment>
</net>
<net name="CLK-ADC5-OUT" class="0">
<segment>
<wire x1="-299.72" y1="55.88" x2="-297.18" y2="55.88" width="0.1524" layer="91"/>
<label x="-297.18" y="55.88" size="0.8128" layer="95" xref="yes"/>
<pinref part="IC10" gate="-3" pin="O"/>
</segment>
</net>
<net name="CLK-ADC6-OUT" class="0">
<segment>
<wire x1="-299.72" y1="17.78" x2="-297.18" y2="17.78" width="0.1524" layer="91"/>
<label x="-297.18" y="17.78" size="0.8128" layer="95" xref="yes"/>
<pinref part="IC10" gate="-6" pin="O"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="A"/>
<pinref part="R37" gate="G$1" pin="1"/>
</segment>
</net>
<net name="RTC-SDA" class="0">
<segment>
<pinref part="RTC1" gate="G$1" pin="SDA"/>
<wire x1="-271.78" y1="208.28" x2="-259.08" y2="208.28" width="0.1524" layer="91"/>
<label x="-251.46" y="208.28" size="0.8128" layer="95" xref="yes"/>
<pinref part="R40" gate="G$1" pin="1"/>
<wire x1="-259.08" y1="208.28" x2="-251.46" y2="208.28" width="0.1524" layer="91"/>
<wire x1="-259.08" y1="215.9" x2="-259.08" y2="208.28" width="0.1524" layer="91"/>
<junction x="-259.08" y="208.28"/>
</segment>
<segment>
<label x="-58.42" y="60.96" size="0.8128" layer="95" rot="R270" xref="yes"/>
<wire x1="-58.42" y1="63.5" x2="-58.42" y2="60.96" width="0.1524" layer="91"/>
<pinref part="IC11" gate="G$1" pin="PB11"/>
</segment>
</net>
<net name="RTC-SCL" class="0">
<segment>
<pinref part="RTC1" gate="G$1" pin="SCL"/>
<wire x1="-271.78" y1="205.74" x2="-254" y2="205.74" width="0.1524" layer="91"/>
<label x="-251.46" y="205.74" size="0.8128" layer="95" xref="yes"/>
<pinref part="R41" gate="G$1" pin="1"/>
<wire x1="-254" y1="205.74" x2="-251.46" y2="205.74" width="0.1524" layer="91"/>
<wire x1="-254" y1="215.9" x2="-254" y2="205.74" width="0.1524" layer="91"/>
<junction x="-254" y="205.74"/>
</segment>
<segment>
<label x="-60.96" y="60.96" size="0.8128" layer="95" rot="R270" xref="yes"/>
<wire x1="-60.96" y1="63.5" x2="-60.96" y2="60.96" width="0.1524" layer="91"/>
<pinref part="IC11" gate="G$1" pin="PB10"/>
</segment>
</net>
<net name="RTC-/INT" class="0">
<segment>
<pinref part="RTC1" gate="G$1" pin="/INT"/>
<wire x1="-271.78" y1="213.36" x2="-264.16" y2="213.36" width="0.1524" layer="91"/>
<label x="-251.46" y="213.36" size="0.8128" layer="95" xref="yes"/>
<pinref part="R39" gate="G$1" pin="1"/>
<wire x1="-264.16" y1="213.36" x2="-251.46" y2="213.36" width="0.1524" layer="91"/>
<wire x1="-264.16" y1="215.9" x2="-264.16" y2="213.36" width="0.1524" layer="91"/>
<junction x="-264.16" y="213.36"/>
</segment>
<segment>
<label x="-63.5" y="60.96" size="0.8128" layer="95" rot="R270" xref="yes"/>
<wire x1="-63.5" y1="63.5" x2="-63.5" y2="60.96" width="0.1524" layer="91"/>
<pinref part="IC11" gate="G$1" pin="PB2"/>
</segment>
</net>
<net name="ADC-/RESET" class="0">
<segment>
<wire x1="-96.52" y1="104.14" x2="-99.06" y2="104.14" width="0.1524" layer="91"/>
<label x="-99.06" y="104.14" size="0.8128" layer="95" rot="R180" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PC13"/>
</segment>
</net>
<net name="ADC-START" class="0">
<segment>
<wire x1="-96.52" y1="101.6" x2="-99.06" y2="101.6" width="0.1524" layer="91"/>
<label x="-99.06" y="101.6" size="0.8128" layer="95" rot="R180" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PC14-OSC32_IN"/>
</segment>
</net>
<net name="ADC-SCK" class="0">
<segment>
<wire x1="-40.64" y1="81.28" x2="-20.32" y2="81.28" width="0.1524" layer="91"/>
<label x="-15.24" y="81.28" size="0.8128" layer="95" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PB13"/>
<pinref part="TP41" gate="G$1" pin="TP"/>
<wire x1="-20.32" y1="81.28" x2="-15.24" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="88.9" x2="-20.32" y2="81.28" width="0.1524" layer="91"/>
<junction x="-20.32" y="81.28"/>
</segment>
</net>
<net name="ADC1-/CS" class="0">
<segment>
<wire x1="-40.64" y1="78.74" x2="-17.78" y2="78.74" width="0.1524" layer="91"/>
<label x="-15.24" y="78.74" size="0.8128" layer="95" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PB12"/>
<pinref part="TP11" gate="G$1" pin="TP"/>
<wire x1="-17.78" y1="78.74" x2="-15.24" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="88.9" x2="-17.78" y2="78.74" width="0.1524" layer="91"/>
<junction x="-17.78" y="78.74"/>
</segment>
</net>
<net name="ADC-MISO" class="0">
<segment>
<wire x1="-40.64" y1="83.82" x2="-22.86" y2="83.82" width="0.1524" layer="91"/>
<label x="-15.24" y="83.82" size="0.8128" layer="95" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PB14"/>
<pinref part="TP40" gate="G$1" pin="TP"/>
<wire x1="-22.86" y1="83.82" x2="-15.24" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="88.9" x2="-22.86" y2="83.82" width="0.1524" layer="91"/>
<junction x="-22.86" y="83.82"/>
</segment>
</net>
<net name="ADC-MOSI" class="0">
<segment>
<wire x1="-40.64" y1="86.36" x2="-25.4" y2="86.36" width="0.1524" layer="91"/>
<label x="-15.24" y="86.36" size="0.8128" layer="95" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PB15"/>
<pinref part="TP39" gate="G$1" pin="TP"/>
<wire x1="-25.4" y1="86.36" x2="-15.24" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="88.9" x2="-25.4" y2="86.36" width="0.1524" layer="91"/>
<junction x="-25.4" y="86.36"/>
</segment>
</net>
<net name="ADC1-/DRDY" class="0">
<segment>
<wire x1="-96.52" y1="99.06" x2="-99.06" y2="99.06" width="0.1524" layer="91"/>
<label x="-99.06" y="99.06" size="0.8128" layer="95" rot="R180" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PC15-OSC32_OUT"/>
</segment>
</net>
<net name="SWDIO" class="0">
<segment>
<pinref part="PROG1" gate="G$1" pin="4"/>
<wire x1="-208.28" y1="30.48" x2="-203.2" y2="30.48" width="0.1524" layer="91"/>
<label x="-203.2" y="30.48" size="0.8128" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-40.64" y1="101.6" x2="-38.1" y2="101.6" width="0.1524" layer="91"/>
<pinref part="IC11" gate="G$1" pin="PA13"/>
<label x="-38.1" y="101.6" size="0.8128" layer="95" xref="yes"/>
</segment>
</net>
<net name="SWCLK" class="0">
<segment>
<pinref part="PROG1" gate="G$1" pin="6"/>
<wire x1="-208.28" y1="27.94" x2="-203.2" y2="27.94" width="0.1524" layer="91"/>
<label x="-203.2" y="27.94" size="0.8128" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-53.34" y1="119.38" x2="-53.34" y2="121.92" width="0.1524" layer="91"/>
<pinref part="IC11" gate="G$1" pin="PA14"/>
<label x="-53.34" y="121.92" size="0.8128" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<pinref part="PROG1" gate="G$1" pin="12"/>
<wire x1="-208.28" y1="20.32" x2="-203.2" y2="20.32" width="0.1524" layer="91"/>
<label x="-203.2" y="20.32" size="0.8128" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-99.06" y1="91.44" x2="-96.52" y2="91.44" width="0.1524" layer="91"/>
<label x="-99.06" y="91.44" size="0.8128" layer="95" rot="R180" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="NRST"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="OUT"/>
<pinref part="IC12" gate="-1" pin="I"/>
<wire x1="-345.44" y1="93.98" x2="-342.9" y2="93.98" width="0.1524" layer="91"/>
<pinref part="TP42" gate="G$1" pin="TP"/>
<wire x1="-342.9" y1="93.98" x2="-340.36" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-342.9" y1="91.44" x2="-342.9" y2="93.98" width="0.1524" layer="91"/>
<junction x="-342.9" y="93.98"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="IC12" gate="-1" pin="O"/>
<pinref part="IC12" gate="-2" pin="I"/>
<wire x1="-325.12" y1="93.98" x2="-317.5" y2="93.98" width="0.1524" layer="91"/>
<pinref part="IC10" gate="-1" pin="I"/>
<wire x1="-317.5" y1="93.98" x2="-314.96" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-314.96" y1="81.28" x2="-317.5" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-317.5" y1="81.28" x2="-317.5" y2="93.98" width="0.1524" layer="91"/>
<junction x="-317.5" y="93.98"/>
<pinref part="IC10" gate="-2" pin="I"/>
<wire x1="-314.96" y1="68.58" x2="-317.5" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-317.5" y1="68.58" x2="-317.5" y2="81.28" width="0.1524" layer="91"/>
<junction x="-317.5" y="81.28"/>
<pinref part="IC10" gate="-3" pin="I"/>
<wire x1="-314.96" y1="55.88" x2="-317.5" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-317.5" y1="55.88" x2="-317.5" y2="68.58" width="0.1524" layer="91"/>
<junction x="-317.5" y="68.58"/>
<pinref part="IC10" gate="-4" pin="I"/>
<wire x1="-314.96" y1="43.18" x2="-317.5" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-317.5" y1="43.18" x2="-317.5" y2="55.88" width="0.1524" layer="91"/>
<junction x="-317.5" y="55.88"/>
<pinref part="IC10" gate="-5" pin="I"/>
<wire x1="-314.96" y1="30.48" x2="-317.5" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-317.5" y1="30.48" x2="-317.5" y2="43.18" width="0.1524" layer="91"/>
<junction x="-317.5" y="43.18"/>
<pinref part="IC10" gate="-6" pin="I"/>
<wire x1="-314.96" y1="17.78" x2="-317.5" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-317.5" y1="17.78" x2="-317.5" y2="30.48" width="0.1524" layer="91"/>
<junction x="-317.5" y="30.48"/>
<pinref part="TP38" gate="G$1" pin="TP"/>
<wire x1="-317.5" y1="96.52" x2="-317.5" y2="93.98" width="0.1524" layer="91" style="longdash"/>
</segment>
</net>
<net name="CLK-UC" class="0">
<segment>
<pinref part="IC12" gate="-2" pin="O"/>
<wire x1="-299.72" y1="93.98" x2="-297.18" y2="93.98" width="0.1524" layer="91"/>
<label x="-297.18" y="93.98" size="0.8128" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-99.06" y1="96.52" x2="-96.52" y2="96.52" width="0.1524" layer="91"/>
<pinref part="IC11" gate="G$1" pin="PH0-OSC_IN"/>
<label x="-99.06" y="96.52" size="0.8128" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="FLASH-/CS" class="0">
<segment>
<pinref part="IC9" gate="G$1" pin="/CS"/>
<wire x1="-332.74" y1="157.48" x2="-330.2" y2="157.48" width="0.1524" layer="91"/>
<label x="-330.2" y="157.48" size="0.8128" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-68.58" y1="63.5" x2="-68.58" y2="60.96" width="0.1524" layer="91"/>
<label x="-68.58" y="60.96" size="0.8128" layer="95" rot="R270" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PB0"/>
</segment>
</net>
<net name="FLASH-SCK" class="0">
<segment>
<pinref part="IC9" gate="G$1" pin="SCK"/>
<wire x1="-332.74" y1="154.94" x2="-330.2" y2="154.94" width="0.1524" layer="91"/>
<label x="-330.2" y="154.94" size="0.8128" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-76.2" y1="63.5" x2="-76.2" y2="60.96" width="0.1524" layer="91"/>
<label x="-76.2" y="60.96" size="0.8128" layer="95" rot="R270" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PA5"/>
</segment>
</net>
<net name="FLASH-MOSI" class="0">
<segment>
<pinref part="IC9" gate="G$1" pin="SI"/>
<wire x1="-332.74" y1="152.4" x2="-330.2" y2="152.4" width="0.1524" layer="91"/>
<label x="-330.2" y="152.4" size="0.8128" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-71.12" y1="63.5" x2="-71.12" y2="60.96" width="0.1524" layer="91"/>
<label x="-71.12" y="60.96" size="0.8128" layer="95" rot="R270" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PA7"/>
</segment>
</net>
<net name="FLASH-MISO" class="0">
<segment>
<pinref part="IC9" gate="G$1" pin="SO"/>
<wire x1="-332.74" y1="149.86" x2="-330.2" y2="149.86" width="0.1524" layer="91"/>
<label x="-330.2" y="149.86" size="0.8128" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-73.66" y1="63.5" x2="-73.66" y2="60.96" width="0.1524" layer="91"/>
<label x="-73.66" y="60.96" size="0.8128" layer="95" rot="R270" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PA6"/>
</segment>
</net>
<net name="FLASH-/RESET" class="0">
<segment>
<pinref part="IC9" gate="G$1" pin="/RESET"/>
<wire x1="-332.74" y1="142.24" x2="-330.2" y2="142.24" width="0.1524" layer="91"/>
<label x="-330.2" y="142.24" size="0.8128" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-78.74" y1="63.5" x2="-78.74" y2="60.96" width="0.1524" layer="91"/>
<label x="-78.74" y="60.96" size="0.8128" layer="95" rot="R270" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PA4"/>
</segment>
</net>
<net name="FLASH-/WP" class="0">
<segment>
<pinref part="IC9" gate="G$1" pin="/WP"/>
<wire x1="-332.74" y1="139.7" x2="-330.2" y2="139.7" width="0.1524" layer="91"/>
<label x="-330.2" y="139.7" size="0.8128" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-96.52" y1="93.98" x2="-99.06" y2="93.98" width="0.1524" layer="91"/>
<label x="-99.06" y="93.98" size="0.8128" layer="95" rot="R180" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PH1-OSC_OUT"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="BAT1" gate="G$1" pin="+"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="-350.52" y1="218.44" x2="-350.52" y2="220.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="EXT_SDA" class="0">
<segment>
<wire x1="-220.98" y1="76.2" x2="-210.82" y2="76.2" width="0.1524" layer="91"/>
<pinref part="JP2" gate="A" pin="10"/>
<pinref part="TP45" gate="G$1" pin="TP"/>
<label x="-208.28" y="76.2" size="0.762" layer="95" xref="yes"/>
</segment>
</net>
<net name="P1.08" class="0">
<segment>
<pinref part="JP2" gate="A" pin="1"/>
<wire x1="-228.6" y1="86.36" x2="-243.84" y2="86.36" width="0.1524" layer="91"/>
<label x="-243.84" y="86.36" size="1.778" layer="95"/>
<pinref part="TP31" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="P0.26" class="0">
<segment>
<pinref part="JP2" gate="A" pin="7"/>
<wire x1="-228.6" y1="78.74" x2="-243.84" y2="78.74" width="0.1524" layer="91"/>
<label x="-243.84" y="78.74" size="1.778" layer="95"/>
<pinref part="TP34" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="P0.27" class="0">
<segment>
<pinref part="JP2" gate="A" pin="5"/>
<wire x1="-228.6" y1="81.28" x2="-243.84" y2="81.28" width="0.1524" layer="91"/>
<label x="-243.84" y="81.28" size="1.778" layer="95"/>
<pinref part="TP33" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="P1.09" class="0">
<segment>
<pinref part="JP2" gate="A" pin="3"/>
<wire x1="-228.6" y1="83.82" x2="-243.84" y2="83.82" width="0.1524" layer="91"/>
<label x="-243.84" y="83.82" size="1.778" layer="95"/>
<pinref part="TP32" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="R52" gate="G$1" pin="1"/>
<pinref part="JP2" gate="A" pin="11"/>
<wire x1="-241.3" y1="73.66" x2="-228.6" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="UART2_TX" class="0">
<segment>
<wire x1="-96.52" y1="78.74" x2="-99.06" y2="78.74" width="0.1524" layer="91"/>
<label x="-99.06" y="78.74" size="0.8128" layer="95" rot="R180" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PA2"/>
</segment>
<segment>
<wire x1="-259.08" y1="27.94" x2="-261.62" y2="27.94" width="0.1524" layer="91"/>
<label x="-261.62" y="27.94" size="0.8128" layer="95" rot="R180" xref="yes"/>
<pinref part="X1" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="PROG1" gate="G$1" pin="14"/>
<wire x1="-208.28" y1="17.78" x2="-203.2" y2="17.78" width="0.1524" layer="91"/>
<label x="-203.2" y="17.78" size="0.8128" layer="95" xref="yes"/>
</segment>
</net>
<net name="UART2_RX" class="0">
<segment>
<wire x1="-81.28" y1="60.96" x2="-81.28" y2="63.5" width="0.1524" layer="91"/>
<label x="-81.28" y="60.96" size="0.8128" layer="95" rot="R270" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PA3"/>
</segment>
<segment>
<pinref part="PROG1" gate="G$1" pin="13"/>
<wire x1="-215.9" y1="17.78" x2="-220.98" y2="17.78" width="0.1524" layer="91"/>
<label x="-220.98" y="17.78" size="0.8128" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-259.08" y1="25.4" x2="-261.62" y2="25.4" width="0.1524" layer="91"/>
<label x="-261.62" y="25.4" size="0.8128" layer="95" rot="R180" xref="yes"/>
<pinref part="X1" gate="G$1" pin="3"/>
</segment>
</net>
<net name="UART4_RTS" class="0">
<segment>
<pinref part="IC11" gate="G$1" pin="PA15"/>
<wire x1="-55.88" y1="119.38" x2="-55.88" y2="121.92" width="0.1524" layer="91"/>
<label x="-55.88" y="121.92" size="0.8128" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="UART4_CTS" class="0">
<segment>
<pinref part="IC11" gate="G$1" pin="PB7"/>
<wire x1="-68.58" y1="119.38" x2="-68.58" y2="121.92" width="0.1524" layer="91"/>
<label x="-68.58" y="121.92" size="0.8128" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
<net name="UART4_TX" class="0">
<segment>
<pinref part="IC11" gate="G$1" pin="PA0"/>
<wire x1="-96.52" y1="83.82" x2="-99.06" y2="83.82" width="0.1524" layer="91"/>
<label x="-99.06" y="83.82" size="0.8128" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="UART4_RX" class="0">
<segment>
<pinref part="IC11" gate="G$1" pin="PA1"/>
<wire x1="-96.52" y1="81.28" x2="-99.06" y2="81.28" width="0.1524" layer="91"/>
<label x="-99.06" y="81.28" size="0.8128" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="I2C_INT" class="0">
<segment>
<pinref part="R37" gate="G$1" pin="2"/>
<pinref part="IC11" gate="G$1" pin="PA8"/>
<wire x1="-27.94" y1="73.66" x2="-27.94" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="88.9" x2="-40.64" y2="88.9" width="0.1524" layer="91"/>
<label x="-27.94" y="73.66" size="0.8128" layer="95" xref="yes"/>
</segment>
</net>
<net name="EXT_CS" class="0">
<segment>
<pinref part="IC11" gate="G$1" pin="PB6"/>
<wire x1="-66.04" y1="119.38" x2="-66.04" y2="121.92" width="0.1524" layer="91"/>
<label x="-66.04" y="121.92" size="0.8128" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<wire x1="-220.98" y1="86.36" x2="-210.82" y2="86.36" width="0.1524" layer="91"/>
<label x="-210.82" y="86.36" size="0.8128" layer="95" xref="yes"/>
<pinref part="JP2" gate="A" pin="2"/>
</segment>
</net>
<net name="EXT_MOSI" class="0">
<segment>
<label x="-63.5" y="121.92" size="0.8128" layer="95" rot="R90" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PB5"/>
<wire x1="-63.5" y1="121.92" x2="-63.5" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="-210.82" y="78.74" size="0.8128" layer="95" xref="yes"/>
<wire x1="-210.82" y1="78.74" x2="-220.98" y2="78.74" width="0.1524" layer="91"/>
<pinref part="JP2" gate="A" pin="8"/>
</segment>
</net>
<net name="EXT_MISO" class="0">
<segment>
<label x="-60.96" y="121.92" size="0.8128" layer="95" rot="R90" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PB4"/>
<wire x1="-60.96" y1="121.92" x2="-60.96" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="-210.82" y="81.28" size="0.8128" layer="95" xref="yes"/>
<wire x1="-210.82" y1="81.28" x2="-220.98" y2="81.28" width="0.1524" layer="91"/>
<pinref part="JP2" gate="A" pin="6"/>
</segment>
</net>
<net name="EXT_SCLK" class="0">
<segment>
<label x="-58.42" y="121.92" size="0.8128" layer="95" rot="R90" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PB3"/>
<wire x1="-58.42" y1="121.92" x2="-58.42" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="-210.82" y="83.82" size="0.8128" layer="95" xref="yes"/>
<wire x1="-210.82" y1="83.82" x2="-220.98" y2="83.82" width="0.1524" layer="91"/>
<pinref part="JP2" gate="A" pin="4"/>
</segment>
</net>
<net name="EXT_SCL" class="0">
<segment>
<wire x1="-228.6" y1="76.2" x2="-243.84" y2="76.2" width="0.1524" layer="91"/>
<pinref part="JP2" gate="A" pin="9"/>
<label x="-243.84" y="76.2" size="1.778" layer="95"/>
<pinref part="TP44" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="UART1_RX" class="0">
<segment>
<wire x1="-38.1" y1="93.98" x2="-40.64" y2="93.98" width="0.1524" layer="91"/>
<label x="-38.1" y="93.98" size="0.8128" layer="95" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PA10"/>
</segment>
</net>
<net name="UART1_RTS" class="0">
<segment>
<wire x1="-38.1" y1="99.06" x2="-40.64" y2="99.06" width="0.1524" layer="91"/>
<label x="-38.1" y="99.06" size="0.8128" layer="95" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PA12"/>
</segment>
</net>
<net name="UART1_CTS" class="0">
<segment>
<wire x1="-40.64" y1="96.52" x2="-38.1" y2="96.52" width="0.1524" layer="91"/>
<label x="-38.1" y="96.52" size="0.8128" layer="95" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PA11"/>
</segment>
</net>
<net name="UART1_TX" class="0">
<segment>
<wire x1="-40.64" y1="91.44" x2="-38.1" y2="91.44" width="0.1524" layer="91"/>
<label x="-38.1" y="91.44" size="0.8128" layer="95" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PA9"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<wire x1="-73.66" y1="119.38" x2="-73.66" y2="139.7" width="0.1524" layer="91"/>
<label x="-78.74" y="139.7" size="0.8128" layer="95" rot="R180" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PB8"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="-71.12" y1="139.7" x2="-73.66" y2="139.7" width="0.1524" layer="91"/>
<junction x="-73.66" y="139.7"/>
<wire x1="-78.74" y1="139.7" x2="-73.66" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<wire x1="-76.2" y1="119.38" x2="-76.2" y2="134.62" width="0.1524" layer="91"/>
<label x="-78.74" y="134.62" size="0.8128" layer="95" rot="R180" xref="yes"/>
<pinref part="IC11" gate="G$1" pin="PB9"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="-71.12" y1="134.62" x2="-76.2" y2="134.62" width="0.1524" layer="91"/>
<wire x1="-78.74" y1="134.62" x2="-76.2" y2="134.62" width="0.1524" layer="91"/>
<junction x="-76.2" y="134.62"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="-101.6" y="25.4" size="1.778" layer="94">

Institute of Electrical Measurement and Sensor Systems</text>
<text x="-280.162" y="80.264" size="1.778" layer="97">Delta Sigma 16bit-ADC
-Input Filter fg=800Hz (-50db to fMod)
-20SPS
-fMod=256kHz
-50Hz line cycle rejection -95,4db
-Internal temperature mesurement
-Supply voltage monitoring</text>
<text x="-279.4" y="101.6" size="3.81" layer="97">ADC</text>
<text x="-213.36" y="101.6" size="1.778" layer="97">Temperature sensor</text>
<text x="-211.582" y="19.304" size="1.778" layer="97">VBias from ADC (AVDD/2).
Rout 350 Ohm 
CMRR: 120db</text>
<text x="-121.92" y="87.63" size="1.778" layer="95" align="center-left">CH01 HOT</text>
<text x="-381" y="170.18" size="3.81" layer="97">DAC</text>
<text x="-279.4" y="160.02" size="3.81" layer="97">Amplifier 3V to 10V</text>
<text x="-381" y="251.46" size="3.81" layer="97">Boost Converter</text>
<text x="-264.16" y="200.66" size="3.81" layer="97">TEG</text>
<text x="-269.24" y="254" size="3.81" layer="97">U / I Measurement</text>
<wire x1="-391.16" y1="182.88" x2="-292.1" y2="182.88" width="0.1524" layer="97"/>
<wire x1="-292.1" y1="172.72" x2="-292.1" y2="121.92" width="0.1524" layer="97"/>
<wire x1="-292.1" y1="116.84" x2="-391.16" y2="116.84" width="0.1524" layer="97"/>
<wire x1="-391.16" y1="116.84" x2="-391.16" y2="73.66" width="0.1524" layer="97"/>
<text x="-121.92" y="82.55" size="1.778" layer="95" align="center-left">CH02 COLD</text>
<text x="-121.92" y="77.47" size="1.778" layer="95" align="center-left">CH03 CASE</text>
<text x="-121.92" y="72.39" size="1.778" layer="95" align="center-left">CH04</text>
<text x="-121.92" y="67.31" size="1.778" layer="95" align="center-left">CH05</text>
<wire x1="-292.1" y1="121.92" x2="-292.1" y2="116.84" width="0.1524" layer="97"/>
<wire x1="-292.1" y1="182.88" x2="-292.1" y2="172.72" width="0.1524" layer="97"/>
<wire x1="-292.1" y1="182.88" x2="-292.1" y2="264.16" width="0.1524" layer="97"/>
<wire x1="-292.1" y1="172.72" x2="-180.34" y2="172.72" width="0.1524" layer="97"/>
<wire x1="-180.34" y1="172.72" x2="-180.34" y2="264.16" width="0.1524" layer="97"/>
<text x="-368.3" y="40.64" size="2.032" layer="97">0 to 12V</text>
<text x="-355.6" y="208.28" size="2.032" layer="97">0 to 12V</text>
<text x="-335.28" y="251.46" size="2.032" layer="81">configured for 12V output</text>
<text x="-375.92" y="45.72" size="2.032" layer="97">nice to have:</text>
<text x="-337.82" y="40.64" size="2.032" layer="97">0 to 5V</text>
<wire x1="-180.34" y1="172.72" x2="-180.34" y2="121.92" width="0.1524" layer="97"/>
<wire x1="-180.34" y1="121.92" x2="-292.1" y2="121.92" width="0.1524" layer="97"/>
<wire x1="-292.1" y1="116.84" x2="-292.1" y2="0" width="0.1524" layer="97"/>
<text x="-224.282" y="148.844" size="1.778" layer="97">Disabling unused OP-Amp
channel with ~VCC/2</text>
<text x="-162.56" y="254" size="3.81" layer="97">IMU</text>
<text x="-162.56" y="246.38" size="2.54" layer="97">I2C Communication, MCU at 3V</text>
<text x="-375.92" y="99.06" size="2.032" layer="97">ADC expects voltage between 0 and 2.5V</text>
<text x="-381" y="106.68" size="3.81" layer="97">Voltage dividers</text>
<text x="-368.3" y="88.9" size="2.032" layer="97">0 to 12V</text>
<text x="-327.66" y="88.9" size="2.032" layer="97">0 to 12V</text>
<wire x1="-180.34" y1="121.92" x2="-104.14" y2="121.92" width="0.1524" layer="97"/>
<wire x1="-104.14" y1="121.92" x2="-104.14" y2="264.16" width="0.1524" layer="97"/>
<wire x1="-104.14" y1="35.56" x2="-104.14" y2="121.92" width="0.1524" layer="97"/>
<text x="-63.5" y="81.28" size="1.778" layer="97">RM186-SM-02</text>
<wire x1="-104.14" y1="121.92" x2="-2.54" y2="121.92" width="0.1524" layer="97"/>
<text x="-99.06" y="111.76" size="3.81" layer="97">LORA MODUL RM186 </text>
<text x="-96.52" y="254" size="3.81" layer="97">LORA MODUL MTXDOT</text>
<text x="-96.52" y="246.38" size="2.54" layer="97">Using antenna connector on module</text>
<text x="-323.342" y="156.464" size="1.778" layer="97">I2C address 1001 001 </text>
<text x="-323.342" y="161.544" size="1.778" layer="97">I2C address 1001 000</text>
<text x="-282.702" y="16.764" size="1.778" layer="97">R11: Fallback 
solution if external 
clock is not used.</text>
</plain>
<instances>
<instance part="FRAME2" gate="G$1" x="-391.16" y="0" smashed="yes"/>
<instance part="FRAME2" gate="G$2" x="-104.14" y="0" smashed="yes">
<attribute name="LAST_DATE_TIME" x="-91.44" y="1.27" size="2.54" layer="94"/>
<attribute name="SHEET" x="-17.78" y="1.27" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="-86.36" y="19.05" size="2.54" layer="94"/>
</instance>
<instance part="IC5" gate="G$1" x="-231.14" y="50.8" smashed="yes">
<attribute name="NAME" x="-236.22" y="50.8" size="1.9304" layer="95"/>
<attribute name="VALUE" x="-236.22" y="48.26" size="1.9304" layer="96"/>
</instance>
<instance part="CON1" gate="G$1" x="-129.54" y="78.74" smashed="yes">
<attribute name="NAME" x="-135.89" y="92.075" size="1.778" layer="95"/>
<attribute name="VALUE" x="-120.65" y="62.992" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND44" gate="1" x="-172.72" y="22.86" smashed="yes"/>
<instance part="R1" gate="G$1" x="-147.32" y="86.36" smashed="yes">
<attribute name="NAME" x="-152.146" y="86.868" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-141.478" y="86.868" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="R2" gate="G$1" x="-147.32" y="88.9" smashed="yes">
<attribute name="NAME" x="-152.146" y="89.408" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-141.478" y="89.408" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="R5" gate="G$1" x="-147.32" y="81.28" smashed="yes">
<attribute name="NAME" x="-152.146" y="81.788" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-141.478" y="81.788" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="R17" gate="G$1" x="-147.32" y="83.82" smashed="yes">
<attribute name="NAME" x="-152.146" y="84.328" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-141.478" y="84.328" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="R20" gate="G$1" x="-147.32" y="76.2" smashed="yes">
<attribute name="NAME" x="-152.146" y="76.708" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-141.478" y="76.708" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="R19" gate="G$1" x="-147.32" y="78.74" smashed="yes">
<attribute name="NAME" x="-152.146" y="79.248" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-141.478" y="79.248" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="R22" gate="G$1" x="-147.32" y="71.12" smashed="yes">
<attribute name="NAME" x="-152.146" y="71.628" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-141.478" y="71.628" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="R21" gate="G$1" x="-147.32" y="73.66" smashed="yes">
<attribute name="NAME" x="-152.146" y="74.168" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-141.478" y="74.168" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="R24" gate="G$1" x="-147.32" y="66.04" smashed="yes">
<attribute name="NAME" x="-152.146" y="66.548" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-141.478" y="66.548" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="R23" gate="G$1" x="-147.32" y="68.58" smashed="yes">
<attribute name="NAME" x="-152.146" y="69.088" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-141.478" y="69.088" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="R26" gate="G$1" x="-147.32" y="53.34" smashed="yes">
<attribute name="NAME" x="-152.146" y="53.848" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-141.478" y="53.848" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="R28" gate="G$1" x="-147.32" y="48.26" smashed="yes">
<attribute name="NAME" x="-152.146" y="48.768" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-141.478" y="48.768" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="R30" gate="G$1" x="-147.32" y="43.18" smashed="yes">
<attribute name="NAME" x="-152.146" y="43.688" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-141.478" y="43.688" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="R32" gate="G$1" x="-147.32" y="38.1" smashed="yes">
<attribute name="NAME" x="-152.146" y="38.608" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-141.478" y="38.608" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="R34" gate="G$1" x="-147.32" y="33.02" smashed="yes">
<attribute name="NAME" x="-152.146" y="33.528" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-141.478" y="33.528" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="GND45" gate="1" x="-185.42" y="96.52" smashed="yes" rot="R90"/>
<instance part="R14" gate="G$1" x="-195.58" y="96.52" smashed="yes">
<attribute name="NAME" x="-195.58" y="97.79" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-195.58" y="93.98" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="R13" gate="G$1" x="-210.82" y="96.52" smashed="yes">
<attribute name="NAME" x="-210.82" y="97.79" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-210.82" y="93.98" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="GND46" gate="1" x="-213.36" y="76.2" smashed="yes"/>
<instance part="GND47" gate="1" x="-243.84" y="20.32" smashed="yes"/>
<instance part="P+2" gate="VCC" x="-241.3" y="81.28" smashed="yes"/>
<instance part="GND48" gate="1" x="-259.08" y="76.2" smashed="yes" rot="R270"/>
<instance part="GND49" gate="1" x="-223.52" y="17.78" smashed="yes"/>
<instance part="R10" gate="G$1" x="-246.38" y="99.06" smashed="yes">
<attribute name="NAME" x="-246.38" y="100.33" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-246.38" y="96.52" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="GND51" gate="1" x="-231.14" y="76.2" smashed="yes"/>
<instance part="Z1" gate="G$1" x="-165.1" y="88.9" smashed="yes">
<attribute name="NAME" x="-169.418" y="89.408" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-159.512" y="89.408" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="Z3" gate="G$1" x="-165.1" y="86.36" smashed="yes">
<attribute name="NAME" x="-169.418" y="86.868" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-159.512" y="86.868" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="Z5" gate="G$1" x="-165.1" y="83.82" smashed="yes">
<attribute name="NAME" x="-169.418" y="84.328" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-159.512" y="84.328" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="Z7" gate="G$1" x="-165.1" y="81.28" smashed="yes">
<attribute name="NAME" x="-169.418" y="81.788" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-159.512" y="81.788" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="Z8" gate="G$1" x="-165.1" y="78.74" smashed="yes">
<attribute name="NAME" x="-169.418" y="79.248" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-159.512" y="79.248" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="Z9" gate="G$1" x="-165.1" y="76.2" smashed="yes">
<attribute name="NAME" x="-169.418" y="76.708" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-159.512" y="76.708" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="Z10" gate="G$1" x="-165.1" y="73.66" smashed="yes">
<attribute name="NAME" x="-169.418" y="74.168" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-159.512" y="74.168" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="Z11" gate="G$1" x="-165.1" y="71.12" smashed="yes">
<attribute name="NAME" x="-169.418" y="71.628" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-159.512" y="71.628" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="Z12" gate="G$1" x="-165.1" y="68.58" smashed="yes">
<attribute name="NAME" x="-169.418" y="69.088" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-159.512" y="69.088" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="Z13" gate="G$1" x="-165.1" y="66.04" smashed="yes">
<attribute name="NAME" x="-169.418" y="66.548" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-159.512" y="66.548" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="Z15" gate="G$1" x="-165.1" y="53.34" smashed="yes">
<attribute name="NAME" x="-169.418" y="53.848" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-159.512" y="53.848" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="Z17" gate="G$1" x="-165.1" y="48.26" smashed="yes">
<attribute name="NAME" x="-169.418" y="48.768" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-159.512" y="48.768" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="Z19" gate="G$1" x="-165.1" y="43.18" smashed="yes">
<attribute name="NAME" x="-169.418" y="43.688" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-159.512" y="43.688" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="Z21" gate="G$1" x="-165.1" y="38.1" smashed="yes">
<attribute name="NAME" x="-169.418" y="38.608" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-159.512" y="38.608" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="Z23" gate="G$1" x="-165.1" y="33.02" smashed="yes">
<attribute name="NAME" x="-169.418" y="33.528" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-159.512" y="33.528" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="Z24" gate="G$1" x="-233.68" y="99.06" smashed="yes">
<attribute name="NAME" x="-233.68" y="100.838" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-233.68" y="97.282" size="1.27" layer="96" rot="R180" align="bottom-center"/>
</instance>
<instance part="C24" gate="G$1" x="-180.34" y="88.9" smashed="yes">
<attribute name="NAME" x="-177.8" y="89.535" size="1.27" layer="95" rot="R180" align="top-right"/>
<attribute name="VALUE" x="-180.34" y="89.535" size="1.27" layer="96" align="bottom-right"/>
</instance>
<instance part="C25" gate="G$1" x="-180.34" y="83.82" smashed="yes">
<attribute name="NAME" x="-177.8" y="84.455" size="1.27" layer="95" rot="R180" align="top-right"/>
<attribute name="VALUE" x="-180.34" y="84.455" size="1.27" layer="96" align="bottom-right"/>
</instance>
<instance part="C26" gate="G$1" x="-180.34" y="78.74" smashed="yes">
<attribute name="NAME" x="-177.8" y="79.375" size="1.27" layer="95" rot="R180" align="top-right"/>
<attribute name="VALUE" x="-180.34" y="79.375" size="1.27" layer="96" align="bottom-right"/>
</instance>
<instance part="C27" gate="G$1" x="-180.34" y="73.66" smashed="yes">
<attribute name="NAME" x="-177.8" y="74.295" size="1.27" layer="95" rot="R180" align="top-right"/>
<attribute name="VALUE" x="-180.34" y="74.295" size="1.27" layer="96" align="bottom-right"/>
</instance>
<instance part="C28" gate="G$1" x="-180.34" y="68.58" smashed="yes">
<attribute name="NAME" x="-177.8" y="69.215" size="1.27" layer="95" rot="R180" align="top-right"/>
<attribute name="VALUE" x="-180.34" y="69.215" size="1.27" layer="96" align="bottom-right"/>
</instance>
<instance part="C29" gate="G$1" x="-180.34" y="55.88" smashed="yes">
<attribute name="NAME" x="-177.8" y="56.515" size="1.27" layer="95" rot="R180" align="top-right"/>
<attribute name="VALUE" x="-180.34" y="56.515" size="1.27" layer="96" align="bottom-right"/>
</instance>
<instance part="C30" gate="G$1" x="-180.34" y="50.8" smashed="yes">
<attribute name="NAME" x="-177.8" y="51.435" size="1.27" layer="95" rot="R180" align="top-right"/>
<attribute name="VALUE" x="-180.34" y="51.435" size="1.27" layer="96" align="bottom-right"/>
</instance>
<instance part="C31" gate="G$1" x="-180.34" y="45.72" smashed="yes">
<attribute name="NAME" x="-177.8" y="46.355" size="1.27" layer="95" rot="R180" align="top-right"/>
<attribute name="VALUE" x="-180.34" y="46.355" size="1.27" layer="96" align="bottom-right"/>
</instance>
<instance part="C32" gate="G$1" x="-180.34" y="40.64" smashed="yes">
<attribute name="NAME" x="-177.8" y="41.275" size="1.27" layer="95" rot="R180" align="top-right"/>
<attribute name="VALUE" x="-180.34" y="41.275" size="1.27" layer="96" align="bottom-right"/>
</instance>
<instance part="C33" gate="G$1" x="-180.34" y="35.56" smashed="yes">
<attribute name="NAME" x="-177.8" y="36.195" size="1.27" layer="95" rot="R180" align="top-right"/>
<attribute name="VALUE" x="-180.34" y="36.195" size="1.27" layer="96" align="bottom-right"/>
</instance>
<instance part="C22" gate="G$1" x="-213.36" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="-215.265" y="83.82" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-211.455" y="83.82" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="C21" gate="G$1" x="-231.14" y="83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="-233.045" y="83.82" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-229.235" y="83.82" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="C19" gate="G$1" x="-251.46" y="76.2" smashed="yes">
<attribute name="NAME" x="-251.46" y="78.105" size="1.27" layer="95" align="top-right"/>
<attribute name="VALUE" x="-251.46" y="74.295" size="1.27" layer="96" align="bottom-right"/>
</instance>
<instance part="P+19" gate="VCC" x="-254" y="104.14" smashed="yes"/>
<instance part="GND2" gate="1" x="-360.68" y="121.92" smashed="yes" rot="MR0"/>
<instance part="GND6" gate="1" x="-254" y="134.62" smashed="yes" rot="MR0"/>
<instance part="R42" gate="G$1" x="-325.12" y="233.68" smashed="yes" rot="R270">
<attribute name="NAME" x="-323.85" y="233.68" size="1.27" layer="95" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="-327.66" y="233.68" size="1.27" layer="96" rot="R270" align="bottom-center"/>
</instance>
<instance part="R43" gate="G$1" x="-325.12" y="213.36" smashed="yes" rot="R270">
<attribute name="NAME" x="-323.85" y="213.36" size="1.27" layer="95" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="-327.66" y="213.36" size="1.27" layer="96" rot="R270" align="bottom-center"/>
</instance>
<instance part="GND16" gate="1" x="-325.12" y="205.74" smashed="yes" rot="MR0"/>
<instance part="C4" gate="G$1" x="-317.5" y="233.68" smashed="yes" rot="R90">
<attribute name="NAME" x="-319.405" y="233.68" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-315.595" y="233.68" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="GND18" gate="1" x="-309.88" y="205.74" smashed="yes" rot="MR0"/>
<instance part="D2" gate="G$1" x="-340.36" y="238.76" smashed="yes">
<attribute name="NAME" x="-340.36" y="244.475" size="1.778" layer="95" align="center"/>
<attribute name="VALUE" x="-340.36" y="241.427" size="1.778" layer="95" align="center"/>
</instance>
<instance part="GND19" gate="1" x="-386.08" y="210.82" smashed="yes" rot="MR0"/>
<instance part="Q1" gate="G$1" x="-210.82" y="187.96" smashed="yes">
<attribute name="NAME" x="-212.09" y="194.31" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="-222.25" y="196.85" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="CON2" gate="G$1" x="-246.38" y="190.5" smashed="yes" rot="MR0">
<attribute name="NAME" x="-252.73" y="198.12" size="1.778" layer="95" rot="MR0" align="center-left"/>
<attribute name="VALUE" x="-252.73" y="195.58" size="1.778" layer="96" rot="MR0" align="center-left"/>
</instance>
<instance part="GND21" gate="1" x="-243.84" y="177.8" smashed="yes" rot="MR0"/>
<instance part="GND22" gate="1" x="-203.2" y="177.8" smashed="yes" rot="MR0"/>
<instance part="P+6" gate="VCC" x="-287.02" y="246.38" smashed="yes">
<attribute name="VALUE" x="-289.56" y="247.396" size="1.778" layer="96"/>
</instance>
<instance part="GND23" gate="1" x="-276.86" y="238.76" smashed="yes" rot="MR0"/>
<instance part="GND26" gate="1" x="-355.6" y="10.16" smashed="yes" rot="MR0"/>
<instance part="GND27" gate="1" x="-325.12" y="10.16" smashed="yes" rot="MR0"/>
<instance part="IC7" gate="G$1" x="-251.46" y="144.78" smashed="yes">
<attribute name="NAME" x="-247.396" y="150.622" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="-247.65" y="148.59" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="R7" gate="G$1" x="-269.24" y="147.32" smashed="yes" rot="MR90">
<attribute name="NAME" x="-267.97" y="147.32" size="1.27" layer="95" rot="MR90" align="bottom-center"/>
<attribute name="VALUE" x="-271.78" y="147.32" size="1.27" layer="96" rot="MR90" align="bottom-center"/>
</instance>
<instance part="R6" gate="G$1" x="-269.24" y="137.16" smashed="yes" rot="MR90">
<attribute name="NAME" x="-267.97" y="137.16" size="1.27" layer="95" rot="MR90" align="bottom-center"/>
<attribute name="VALUE" x="-271.78" y="137.16" size="1.27" layer="96" rot="MR90" align="bottom-center"/>
</instance>
<instance part="GND7" gate="1" x="-269.24" y="129.54" smashed="yes" rot="MR0"/>
<instance part="C8" gate="G$1" x="-386.08" y="218.44" smashed="yes" rot="R90">
<attribute name="NAME" x="-387.985" y="218.44" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-384.175" y="218.44" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="C7" gate="G$1" x="-309.88" y="218.44" smashed="yes" rot="R90">
<attribute name="NAME" x="-306.07" y="222.25" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="-303.53" y="207.01" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="L1" gate="G$1" x="-373.38" y="241.3" smashed="yes">
<attribute name="NAME" x="-369.57" y="248.92" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="-369.57" y="246.38" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="IC3" gate="G$1" x="-251.46" y="241.3" smashed="yes">
<attribute name="NAME" x="-247.65" y="248.92" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="-247.65" y="246.38" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="C9" gate="G$1" x="-218.44" y="231.14" smashed="yes" rot="MR180">
<attribute name="NAME" x="-218.821" y="229.616" size="1.778" layer="95" rot="MR270"/>
<attribute name="VALUE" x="-213.741" y="229.616" size="1.778" layer="96" rot="MR270"/>
</instance>
<instance part="R49" gate="G$1" x="-226.06" y="208.28" smashed="yes">
<attribute name="NAME" x="-222.25" y="214.63" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="-222.25" y="212.09" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="R50" gate="G$1" x="-332.74" y="233.68" smashed="yes" rot="R270">
<attribute name="NAME" x="-331.47" y="233.68" size="1.27" layer="95" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="-335.28" y="233.68" size="1.27" layer="96" rot="R270" align="bottom-center"/>
</instance>
<instance part="R51" gate="G$1" x="-332.74" y="213.36" smashed="yes" rot="R270">
<attribute name="NAME" x="-331.47" y="213.36" size="1.27" layer="95" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="-335.28" y="213.36" size="1.27" layer="96" rot="R270" align="bottom-center"/>
</instance>
<instance part="P+13" gate="VCC" x="-325.12" y="38.1" smashed="yes">
<attribute name="VALUE" x="-327.66" y="39.116" size="1.778" layer="96"/>
</instance>
<instance part="R44" gate="G$1" x="-355.6" y="17.78" smashed="yes" rot="R270">
<attribute name="NAME" x="-354.33" y="17.78" size="1.27" layer="95" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="-358.14" y="17.78" size="1.27" layer="96" rot="R270" align="bottom-center"/>
</instance>
<instance part="R46" gate="G$1" x="-325.12" y="17.78" smashed="yes" rot="R270">
<attribute name="NAME" x="-323.85" y="17.78" size="1.27" layer="95" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="-327.66" y="17.78" size="1.27" layer="96" rot="R270" align="bottom-center"/>
</instance>
<instance part="R33" gate="G$1" x="-355.6" y="30.48" smashed="yes" rot="R90">
<attribute name="NAME" x="-359.156" y="30.48" size="1.27" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="-357.124" y="30.48" size="1.27" layer="96" rot="R90" align="bottom-center"/>
</instance>
<instance part="R45" gate="G$1" x="-325.12" y="30.48" smashed="yes" rot="R270">
<attribute name="NAME" x="-323.85" y="30.48" size="1.27" layer="95" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="-327.66" y="30.48" size="1.27" layer="96" rot="R270" align="bottom-center"/>
</instance>
<instance part="TP3" gate="G$1" x="-309.88" y="241.3" smashed="yes">
<attribute name="NAME" x="-311.15" y="242.57" size="1.778" layer="95"/>
<attribute name="TP_SIGNAL_NAME" x="-308.61" y="240.03" size="1.778" layer="97"/>
</instance>
<instance part="TP4" gate="G$1" x="-378.46" y="139.7" smashed="yes">
<attribute name="NAME" x="-379.73" y="140.97" size="1.778" layer="95"/>
<attribute name="TP_SIGNAL_NAME" x="-377.19" y="138.43" size="1.778" layer="97"/>
</instance>
<instance part="TP5" gate="G$1" x="-269.24" y="154.94" smashed="yes">
<attribute name="NAME" x="-270.51" y="156.21" size="1.778" layer="95"/>
<attribute name="TP_SIGNAL_NAME" x="-267.97" y="153.67" size="1.778" layer="97"/>
</instance>
<instance part="TP6" gate="G$1" x="-220.98" y="243.84" smashed="yes">
<attribute name="NAME" x="-222.25" y="245.11" size="1.778" layer="95"/>
<attribute name="TP_SIGNAL_NAME" x="-219.71" y="242.57" size="1.778" layer="97"/>
</instance>
<instance part="TP7" gate="G$1" x="-243.84" y="210.82" smashed="yes">
<attribute name="NAME" x="-245.11" y="212.09" size="1.778" layer="95"/>
<attribute name="TP_SIGNAL_NAME" x="-242.57" y="209.55" size="1.778" layer="97"/>
</instance>
<instance part="C12" gate="G$1" x="-114.3" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="-116.205" y="157.48" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-112.395" y="157.48" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="GND11" gate="1" x="-114.3" y="172.72" smashed="yes" rot="MR0"/>
<instance part="IC6" gate="G$1" x="-170.18" y="175.26" smashed="yes">
<attribute name="NAME" x="-138.43" y="193.04" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="-138.43" y="190.5" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="GND32" gate="1" x="-124.46" y="149.86" smashed="yes" rot="MR0"/>
<instance part="GND33" gate="1" x="-114.3" y="149.86" smashed="yes" rot="MR0"/>
<instance part="GND34" gate="1" x="-139.7" y="127" smashed="yes" rot="MR0"/>
<instance part="IC8" gate="G$1" x="-157.48" y="223.52" smashed="yes">
<attribute name="NAME" x="-151.13" y="231.14" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="-151.13" y="228.6" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="GND35" gate="1" x="-121.92" y="203.2" smashed="yes" rot="MR0"/>
<instance part="R3" gate="G$1" x="-114.3" y="213.36" smashed="yes" rot="R270">
<attribute name="NAME" x="-113.03" y="213.36" size="1.27" layer="95" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="-116.84" y="213.36" size="1.27" layer="96" rot="R270" align="bottom-center"/>
</instance>
<instance part="GND36" gate="1" x="-114.3" y="203.2" smashed="yes" rot="MR0"/>
<instance part="R4" gate="G$1" x="-149.86" y="134.62" smashed="yes" rot="R270">
<attribute name="NAME" x="-148.59" y="134.62" size="1.27" layer="95" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="-152.4" y="134.62" size="1.27" layer="96" rot="R270" align="bottom-center"/>
</instance>
<instance part="GND37" gate="1" x="-149.86" y="127" smashed="yes" rot="MR0"/>
<instance part="C15" gate="G$1" x="-129.54" y="213.36" smashed="yes" rot="R270">
<attribute name="NAME" x="-128.016" y="213.741" size="1.778" layer="95"/>
<attribute name="VALUE" x="-128.016" y="208.661" size="1.778" layer="96"/>
</instance>
<instance part="GND38" gate="1" x="-129.54" y="203.2" smashed="yes" rot="MR0"/>
<instance part="GND39" gate="1" x="-162.56" y="213.36" smashed="yes" rot="MR0"/>
<instance part="C18" gate="G$1" x="-139.7" y="134.62" smashed="yes" rot="R270">
<attribute name="NAME" x="-138.176" y="135.001" size="1.778" layer="95"/>
<attribute name="VALUE" x="-138.176" y="129.921" size="1.778" layer="96"/>
</instance>
<instance part="C13" gate="G$1" x="-121.92" y="213.36" smashed="yes" rot="R270">
<attribute name="NAME" x="-120.396" y="213.741" size="1.778" layer="95"/>
<attribute name="VALUE" x="-120.396" y="208.661" size="1.778" layer="96"/>
</instance>
<instance part="C11" gate="G$1" x="-124.46" y="160.02" smashed="yes" rot="R270">
<attribute name="NAME" x="-122.936" y="160.401" size="1.778" layer="95"/>
<attribute name="VALUE" x="-122.936" y="155.321" size="1.778" layer="96"/>
</instance>
<instance part="C16" gate="G$1" x="-259.08" y="236.22" smashed="yes" rot="R270">
<attribute name="NAME" x="-257.556" y="236.601" size="1.778" layer="95"/>
<attribute name="VALUE" x="-257.556" y="231.521" size="1.778" layer="96"/>
</instance>
<instance part="C20" gate="G$1" x="-347.98" y="20.32" smashed="yes" rot="MR270">
<attribute name="NAME" x="-344.424" y="20.701" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-341.884" y="15.621" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C23" gate="G$1" x="-317.5" y="20.32" smashed="yes" rot="MR270">
<attribute name="NAME" x="-313.944" y="20.701" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-311.404" y="15.621" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="Z16" gate="G$1" x="-172.72" y="233.68" smashed="yes" rot="R270">
<attribute name="NAME" x="-170.942" y="233.68" size="1.27" layer="95" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="-174.498" y="233.68" size="1.27" layer="96" rot="R90" align="bottom-center"/>
</instance>
<instance part="C35" gate="G$1" x="-172.72" y="223.52" smashed="yes" rot="R270">
<attribute name="NAME" x="-171.196" y="223.901" size="1.778" layer="95"/>
<attribute name="VALUE" x="-171.196" y="218.821" size="1.778" layer="96"/>
</instance>
<instance part="GND40" gate="1" x="-172.72" y="213.36" smashed="yes" rot="MR0"/>
<instance part="P+16" gate="VCC" x="-172.72" y="256.54" smashed="yes">
<attribute name="VALUE" x="-175.26" y="257.556" size="1.778" layer="96"/>
</instance>
<instance part="R27" gate="G$1" x="-358.14" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="-361.696" y="78.74" size="1.27" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="-359.664" y="78.74" size="1.27" layer="96" rot="R90" align="bottom-center"/>
</instance>
<instance part="GND24" gate="1" x="-358.14" y="58.42" smashed="yes" rot="MR0"/>
<instance part="GND25" gate="1" x="-317.5" y="58.42" smashed="yes" rot="MR0"/>
<instance part="R25" gate="G$1" x="-358.14" y="66.04" smashed="yes" rot="R270">
<attribute name="NAME" x="-356.87" y="66.04" size="1.27" layer="95" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="-360.68" y="66.04" size="1.27" layer="96" rot="R270" align="bottom-center"/>
</instance>
<instance part="R31" gate="G$1" x="-317.5" y="66.04" smashed="yes" rot="R270">
<attribute name="NAME" x="-316.23" y="66.04" size="1.27" layer="95" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="-320.04" y="66.04" size="1.27" layer="96" rot="R270" align="bottom-center"/>
</instance>
<instance part="R29" gate="G$1" x="-317.5" y="78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="-321.056" y="78.74" size="1.27" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="-319.024" y="78.74" size="1.27" layer="96" rot="R90" align="bottom-center"/>
</instance>
<instance part="C17" gate="G$1" x="-350.52" y="68.58" smashed="yes" rot="MR270">
<attribute name="NAME" x="-344.424" y="68.961" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-344.424" y="63.881" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C6" gate="G$1" x="-309.88" y="68.58" smashed="yes" rot="MR270">
<attribute name="NAME" x="-306.324" y="68.961" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-303.784" y="63.881" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND41" gate="1" x="-129.54" y="127" smashed="yes" rot="MR0"/>
<instance part="GND42" gate="1" x="-139.7" y="198.12" smashed="yes" rot="MR0"/>
<instance part="GND43" gate="1" x="-78.74" y="76.2" smashed="yes" rot="MR90"/>
<instance part="GND53" gate="1" x="-78.74" y="58.42" smashed="yes" rot="MR90"/>
<instance part="GND55" gate="1" x="-78.74" y="50.8" smashed="yes" rot="MR90"/>
<instance part="GND56" gate="1" x="-12.7" y="76.2" smashed="yes" rot="MR270"/>
<instance part="GND57" gate="1" x="-12.7" y="68.58" smashed="yes" rot="MR270"/>
<instance part="GND58" gate="1" x="-12.7" y="50.8" smashed="yes" rot="MR270"/>
<instance part="C14" gate="G$1" x="-78.74" y="45.72" smashed="yes" rot="R270">
<attribute name="NAME" x="-77.216" y="46.101" size="1.778" layer="95"/>
<attribute name="VALUE" x="-77.216" y="41.021" size="1.778" layer="96"/>
</instance>
<instance part="GND61" gate="1" x="-78.74" y="38.1" smashed="yes" rot="MR0"/>
<instance part="C34" gate="G$1" x="-15.24" y="45.72" smashed="yes" rot="R270">
<attribute name="NAME" x="-13.716" y="46.101" size="1.778" layer="95"/>
<attribute name="VALUE" x="-13.716" y="41.021" size="1.778" layer="96"/>
</instance>
<instance part="GND63" gate="1" x="-15.24" y="38.1" smashed="yes" rot="MR0"/>
<instance part="Z18" gate="G$1" x="-78.74" y="93.98" smashed="yes">
<attribute name="NAME" x="-78.74" y="95.758" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-78.74" y="92.202" size="1.27" layer="96" rot="R180" align="bottom-center"/>
</instance>
<instance part="P+18" gate="VCC" x="-99.06" y="99.06" smashed="yes">
<attribute name="VALUE" x="-101.6" y="100.076" size="1.778" layer="96"/>
</instance>
<instance part="GND64" gate="1" x="-71.12" y="81.28" smashed="yes" rot="MR0"/>
<instance part="U$2" gate="G$1" x="-43.18" y="193.04" smashed="yes"/>
<instance part="GND3" gate="1" x="-22.86" y="139.7" smashed="yes" rot="MR0"/>
<instance part="GND4" gate="1" x="-20.32" y="170.18" smashed="yes" rot="MR0"/>
<instance part="GND5" gate="1" x="-20.32" y="195.58" smashed="yes" rot="MR0"/>
<instance part="GND8" gate="1" x="-76.2" y="203.2" smashed="yes" rot="MR90"/>
<instance part="GND9" gate="1" x="-76.2" y="213.36" smashed="yes" rot="MR90"/>
<instance part="GND10" gate="1" x="-76.2" y="162.56" smashed="yes" rot="MR0"/>
<instance part="GND12" gate="1" x="-76.2" y="170.18" smashed="yes" rot="MR0"/>
<instance part="Z2" gate="G$1" x="-76.2" y="233.68" smashed="yes">
<attribute name="NAME" x="-76.2" y="235.458" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-76.2" y="231.902" size="1.27" layer="96" rot="R180" align="bottom-center"/>
</instance>
<instance part="P+5" gate="VCC" x="-96.52" y="238.76" smashed="yes">
<attribute name="VALUE" x="-99.06" y="239.776" size="1.778" layer="96"/>
</instance>
<instance part="GND13" gate="1" x="-68.58" y="220.98" smashed="yes" rot="MR0"/>
<instance part="SL1" gate="G$1" x="-33.02" y="231.14" smashed="yes">
<attribute name="NAME" x="-38.1" y="239.522" size="1.778" layer="95"/>
</instance>
<instance part="SL2" gate="G$1" x="-38.1" y="96.52" smashed="yes">
<attribute name="NAME" x="-43.18" y="104.902" size="1.778" layer="95"/>
</instance>
<instance part="GND15" gate="1" x="-25.4" y="101.6" smashed="yes" rot="MR270"/>
<instance part="GND17" gate="1" x="-20.32" y="236.22" smashed="yes" rot="MR270"/>
<instance part="C2" gate="G$1" x="-360.68" y="129.54" smashed="yes" rot="R90">
<attribute name="NAME" x="-362.204" y="129.159" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-362.204" y="134.239" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="U$1" gate="G$1" x="-66.04" y="78.74" smashed="yes"/>
<instance part="R47" gate="G$1" x="-213.36" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="-214.63" y="220.98" size="1.27" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="-210.82" y="220.98" size="1.27" layer="96" rot="R90" align="bottom-center"/>
</instance>
<instance part="R48" gate="G$1" x="-220.98" y="220.98" smashed="yes" rot="R90">
<attribute name="NAME" x="-222.25" y="220.98" size="1.27" layer="95" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="-218.44" y="220.98" size="1.27" layer="96" rot="R90" align="bottom-center"/>
</instance>
<instance part="Z4" gate="G$1" x="-276.86" y="231.14" smashed="yes">
<attribute name="NAME" x="-276.86" y="232.918" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-276.86" y="229.362" size="1.27" layer="96" rot="R180" align="bottom-center"/>
</instance>
<instance part="C51" gate="G$1" x="-266.7" y="236.22" smashed="yes" rot="R90">
<attribute name="NAME" x="-268.605" y="236.22" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-264.795" y="236.22" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="GND1" gate="1" x="-276.86" y="129.54" smashed="yes" rot="MR0"/>
<instance part="C53" gate="G$1" x="-276.86" y="134.62" smashed="yes" rot="R90">
<attribute name="NAME" x="-277.495" y="137.16" size="1.27" layer="95" rot="R270" align="top-right"/>
<attribute name="VALUE" x="-280.035" y="137.16" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="R56" gate="G$1" x="-215.9" y="182.88" smashed="yes" rot="R270">
<attribute name="NAME" x="-214.63" y="182.88" size="1.27" layer="95" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="-218.44" y="182.88" size="1.27" layer="96" rot="R270" align="bottom-center"/>
</instance>
<instance part="GND30" gate="1" x="-215.9" y="175.26" smashed="yes" rot="MR0"/>
<instance part="C52" gate="G$1" x="-220.98" y="132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="-222.885" y="132.08" size="1.27" layer="95" rot="R90" align="top-right"/>
<attribute name="VALUE" x="-219.075" y="132.08" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="GND84" gate="1" x="-220.98" y="127" smashed="yes" rot="MR0"/>
<instance part="Z6" gate="G$1" x="-365.76" y="149.86" smashed="yes">
<attribute name="NAME" x="-365.76" y="151.638" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-365.76" y="148.082" size="1.27" layer="96" rot="R180" align="bottom-center"/>
</instance>
<instance part="P+3" gate="VCC" x="-383.54" y="157.48" smashed="yes">
<attribute name="VALUE" x="-386.08" y="158.496" size="1.778" layer="96"/>
</instance>
<instance part="C36" gate="G$1" x="-71.12" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="-71.755" y="91.44" size="1.27" layer="95" rot="R270" align="top-right"/>
<attribute name="VALUE" x="-71.755" y="88.9" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="IC13" gate="G$1" x="-378.46" y="228.6" smashed="yes">
<attribute name="NAME" x="-374.65" y="218.44" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="-374.65" y="215.9" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="GND14" gate="1" x="-378.46" y="210.82" smashed="yes" rot="MR0"/>
<instance part="P+12" gate="VCC" x="-386.08" y="256.54" smashed="yes">
<attribute name="VALUE" x="-388.62" y="257.556" size="1.778" layer="96"/>
</instance>
<instance part="J1" gate="G$1" x="-386.08" y="246.38" smashed="yes" rot="R90">
<attribute name="NAME" x="-388.62" y="241.3" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="J3" gate="G$1" x="-378.46" y="149.86" smashed="yes" rot="R180">
<attribute name="NAME" x="-373.38" y="147.32" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="J2" gate="G$1" x="-304.8" y="238.76" smashed="yes" rot="R180">
<attribute name="NAME" x="-302.26" y="243.84" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="IC2" gate="G$1" x="-345.44" y="139.7" smashed="yes">
<attribute name="NAME" x="-339.09" y="147.32" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="-339.09" y="144.78" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="R8" gate="G$1" x="-353.06" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="-351.79" y="144.78" size="1.27" layer="95" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="-355.6" y="144.78" size="1.27" layer="96" rot="R270" align="bottom-center"/>
</instance>
<instance part="R15" gate="G$1" x="-317.5" y="149.86" smashed="yes">
<attribute name="NAME" x="-317.5" y="151.13" size="1.27" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="-317.5" y="147.32" size="1.27" layer="96" align="bottom-center"/>
</instance>
<instance part="C54" gate="G$1" x="-302.26" y="139.7" smashed="yes">
<attribute name="NAME" x="-302.641" y="141.224" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-297.561" y="141.224" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND85" gate="1" x="-294.64" y="132.08" smashed="yes" rot="MR0"/>
<instance part="J5" gate="1" x="-330.2" y="162.56" smashed="yes">
<attribute name="NAME" x="-335.28" y="165.1" size="1.778" layer="95"/>
</instance>
<instance part="GND86" gate="1" x="-342.9" y="160.02" smashed="yes" rot="MR0"/>
<instance part="J4" gate="G$1" x="-330.2" y="157.48" smashed="yes">
<attribute name="NAME" x="-335.28" y="160.02" size="1.778" layer="95"/>
</instance>
<instance part="C1" gate="G$1" x="-68.58" y="228.6" smashed="yes" rot="R90">
<attribute name="NAME" x="-69.215" y="231.14" size="1.27" layer="95" rot="R270" align="top-right"/>
<attribute name="VALUE" x="-69.215" y="228.6" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="C55" gate="G$1" x="-63.5" y="228.6" smashed="yes" rot="R90">
<attribute name="NAME" x="-64.135" y="231.14" size="1.27" layer="95" rot="R270" align="top-right"/>
<attribute name="VALUE" x="-64.135" y="228.6" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="C56" gate="G$1" x="-66.04" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="-66.675" y="91.44" size="1.27" layer="95" rot="R270" align="top-right"/>
<attribute name="VALUE" x="-66.675" y="88.9" size="1.27" layer="96" rot="R90" align="bottom-right"/>
</instance>
<instance part="J6" gate="1" x="-172.72" y="246.38" smashed="yes" rot="R90">
<attribute name="NAME" x="-170.18" y="243.84" size="1.778" layer="95"/>
</instance>
<instance part="J7" gate="1" x="-160.02" y="203.2" smashed="yes">
<attribute name="NAME" x="-165.1" y="205.74" size="1.778" layer="95"/>
</instance>
<instance part="J10" gate="1" x="-165.1" y="139.7" smashed="yes" rot="R90">
<attribute name="NAME" x="-161.544" y="136.144" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="J9" gate="1" x="-170.18" y="139.7" smashed="yes" rot="R90">
<attribute name="NAME" x="-172.72" y="136.906" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="J8" gate="1" x="-160.02" y="198.12" smashed="yes" rot="R180">
<attribute name="NAME" x="-162.56" y="195.58" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="J11" gate="1" x="-88.9" y="233.68" smashed="yes" rot="R180">
<attribute name="NAME" x="-91.44" y="231.14" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="J13" gate="1" x="-83.82" y="180.34" smashed="yes" rot="R180">
<attribute name="NAME" x="-86.36" y="177.8" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="J12" gate="1" x="-83.82" y="182.88" smashed="yes" rot="R180">
<attribute name="NAME" x="-86.36" y="187.96" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="J15" gate="1" x="-20.32" y="208.28" smashed="yes" rot="R180">
<attribute name="NAME" x="-17.78" y="205.74" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="J14" gate="1" x="-20.32" y="210.82" smashed="yes" rot="R180">
<attribute name="NAME" x="-17.78" y="215.9" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="J16" gate="1" x="-91.44" y="93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="-93.98" y="91.44" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="J17" gate="1" x="-78.74" y="73.66" smashed="yes" rot="R180">
<attribute name="NAME" x="-83.82" y="73.66" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="J18" gate="1" x="-78.74" y="71.12" smashed="yes" rot="R180">
<attribute name="NAME" x="-83.82" y="71.12" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="J19" gate="1" x="-78.74" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="-83.82" y="68.58" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="J20" gate="1" x="-78.74" y="66.04" smashed="yes" rot="R180">
<attribute name="NAME" x="-83.82" y="66.04" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="GND70" gate="1" x="-66.04" y="81.28" smashed="yes" rot="MR0"/>
<instance part="GND87" gate="1" x="-63.5" y="220.98" smashed="yes" rot="MR0"/>
<instance part="GND80" gate="1" x="-259.08" y="12.7" smashed="yes"/>
<instance part="J21" gate="1" x="-259.08" y="22.86" smashed="yes" rot="R270">
<attribute name="NAME" x="-256.54" y="20.32" size="1.778" layer="95" rot="R270"/>
</instance>
<instance part="J22" gate="G$1" x="-266.7" y="30.48" smashed="yes">
<attribute name="NAME" x="-271.78" y="33.02" size="1.778" layer="95"/>
</instance>
<instance part="TP9" gate="G$1" x="-76.2" y="193.04" smashed="yes" rot="R90">
<attribute name="NAME" x="-77.47" y="194.31" size="1.778" layer="95" rot="R180"/>
<attribute name="TP_SIGNAL_NAME" x="-74.93" y="194.31" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="TP10" gate="G$1" x="-76.2" y="190.5" smashed="yes" rot="R90">
<attribute name="NAME" x="-77.47" y="191.77" size="1.778" layer="95" rot="R180"/>
<attribute name="TP_SIGNAL_NAME" x="-74.93" y="191.77" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="TP12" gate="G$1" x="-76.2" y="187.96" smashed="yes" rot="R90">
<attribute name="NAME" x="-77.47" y="189.23" size="1.778" layer="95" rot="R180"/>
<attribute name="TP_SIGNAL_NAME" x="-74.93" y="189.23" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="TP13" gate="G$1" x="-76.2" y="185.42" smashed="yes" rot="R90">
<attribute name="NAME" x="-77.47" y="186.69" size="1.778" layer="95" rot="R180"/>
<attribute name="TP_SIGNAL_NAME" x="-74.93" y="186.69" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="TP14" gate="G$1" x="-76.2" y="177.8" smashed="yes" rot="R90">
<attribute name="NAME" x="-77.47" y="179.07" size="1.778" layer="95" rot="R180"/>
<attribute name="TP_SIGNAL_NAME" x="-74.93" y="179.07" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="TP15" gate="G$1" x="-76.2" y="175.26" smashed="yes" rot="R90">
<attribute name="NAME" x="-77.47" y="176.53" size="1.778" layer="95" rot="R180"/>
<attribute name="TP_SIGNAL_NAME" x="-74.93" y="176.53" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="TP16" gate="G$1" x="-76.2" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="-77.47" y="148.59" size="1.778" layer="95" rot="R180"/>
<attribute name="TP_SIGNAL_NAME" x="-74.93" y="148.59" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="TP17" gate="G$1" x="-76.2" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="-77.47" y="146.05" size="1.778" layer="95" rot="R180"/>
<attribute name="TP_SIGNAL_NAME" x="-74.93" y="146.05" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="TP18" gate="G$1" x="-86.36" y="55.88" smashed="yes" rot="R90">
<attribute name="NAME" x="-87.63" y="57.15" size="1.778" layer="95" rot="R180"/>
<attribute name="TP_SIGNAL_NAME" x="-85.09" y="57.15" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="TP19" gate="G$1" x="-86.36" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="-87.63" y="54.61" size="1.778" layer="95" rot="R180"/>
<attribute name="TP_SIGNAL_NAME" x="-85.09" y="54.61" size="1.778" layer="97" rot="R90"/>
</instance>
<instance part="TP20" gate="G$1" x="-12.7" y="58.42" smashed="yes" rot="R270">
<attribute name="NAME" x="-11.43" y="57.15" size="1.778" layer="95"/>
<attribute name="TP_SIGNAL_NAME" x="-13.97" y="57.15" size="1.778" layer="97" rot="R270"/>
</instance>
<instance part="TP21" gate="G$1" x="-12.7" y="60.96" smashed="yes" rot="R270">
<attribute name="NAME" x="-11.43" y="59.69" size="1.778" layer="95"/>
<attribute name="TP_SIGNAL_NAME" x="-13.97" y="59.69" size="1.778" layer="97" rot="R270"/>
</instance>
<instance part="TP22" gate="G$1" x="-12.7" y="53.34" smashed="yes" rot="R270">
<attribute name="NAME" x="-11.43" y="52.07" size="1.778" layer="95"/>
<attribute name="TP_SIGNAL_NAME" x="-13.97" y="52.07" size="1.778" layer="97" rot="R270"/>
</instance>
<instance part="TP23" gate="G$1" x="-12.7" y="55.88" smashed="yes" rot="R270">
<attribute name="NAME" x="-11.43" y="54.61" size="1.778" layer="95"/>
<attribute name="TP_SIGNAL_NAME" x="-13.97" y="54.61" size="1.778" layer="97" rot="R270"/>
</instance>
<instance part="J23" gate="1" x="-134.62" y="142.24" smashed="yes" rot="R180">
<attribute name="NAME" x="-132.334" y="146.05" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="J24" gate="G$1" x="-287.02" y="236.22" smashed="yes" rot="R90">
<attribute name="NAME" x="-281.94" y="233.68" size="1.778" layer="95" rot="R90"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<pinref part="GND45" gate="1" pin="GND"/>
<wire x1="-190.5" y1="96.52" x2="-187.96" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND48" gate="1" pin="GND"/>
<wire x1="-256.54" y1="76.2" x2="-254" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="IC5" gate="G$1" pin="REFCOM"/>
<wire x1="-226.06" y1="25.4" x2="-226.06" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-226.06" y1="22.86" x2="-223.52" y2="22.86" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="AVSS-SW"/>
<wire x1="-223.52" y1="22.86" x2="-223.52" y2="25.4" width="0.1524" layer="91"/>
<pinref part="GND49" gate="1" pin="GND"/>
<wire x1="-223.52" y1="22.86" x2="-223.52" y2="20.32" width="0.1524" layer="91"/>
<junction x="-223.52" y="22.86"/>
<wire x1="-223.52" y1="22.86" x2="-220.98" y2="22.86" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="AVSS"/>
<wire x1="-220.98" y1="22.86" x2="-220.98" y2="25.4" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="REFP0"/>
<wire x1="-236.22" y1="25.4" x2="-236.22" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-236.22" y1="22.86" x2="-233.68" y2="22.86" width="0.1524" layer="91"/>
<junction x="-226.06" y="22.86"/>
<pinref part="IC5" gate="G$1" pin="REFN0"/>
<wire x1="-233.68" y1="22.86" x2="-226.06" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-233.68" y1="25.4" x2="-233.68" y2="22.86" width="0.1524" layer="91"/>
<junction x="-233.68" y="22.86"/>
</segment>
<segment>
<pinref part="GND47" gate="1" pin="GND"/>
<pinref part="IC5" gate="G$1" pin="DGND"/>
<wire x1="-243.84" y1="22.86" x2="-243.84" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND46" gate="1" pin="GND"/>
<wire x1="-213.36" y1="81.28" x2="-213.36" y2="78.74" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND51" gate="1" pin="GND"/>
<wire x1="-231.14" y1="78.74" x2="-231.14" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="-360.68" y1="127" x2="-360.68" y2="124.46" width="0.1524" layer="91"/>
<wire x1="-360.68" y1="124.46" x2="-355.6" y2="124.46" width="0.1524" layer="91"/>
<wire x1="-355.6" y1="124.46" x2="-355.6" y2="132.08" width="0.1524" layer="91"/>
<wire x1="-355.6" y1="132.08" x2="-345.44" y2="132.08" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="AGND"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="-254" y1="137.16" x2="-251.46" y2="137.16" width="0.1524" layer="91"/>
<pinref part="IC7" gate="G$1" pin="V-"/>
</segment>
<segment>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="R43" gate="G$1" pin="2"/>
<pinref part="R51" gate="G$1" pin="2"/>
<wire x1="-332.74" y1="208.28" x2="-325.12" y2="208.28" width="0.1524" layer="91"/>
<junction x="-325.12" y="208.28"/>
</segment>
<segment>
<wire x1="-309.88" y1="218.44" x2="-309.88" y2="208.28" width="0.1524" layer="91"/>
<pinref part="GND18" gate="1" pin="GND"/>
<pinref part="C7" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="-386.08" y1="213.36" x2="-386.08" y2="215.9" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND22" gate="1" pin="GND"/>
<pinref part="Q1" gate="G$1" pin="S"/>
<wire x1="-203.2" y1="180.34" x2="-203.2" y2="182.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="-276.86" y1="241.3" x2="-266.7" y2="241.3" width="0.1524" layer="91"/>
<wire x1="-266.7" y1="241.3" x2="-259.08" y2="241.3" width="0.1524" layer="91"/>
<wire x1="-259.08" y1="241.3" x2="-259.08" y2="238.76" width="0.1524" layer="91"/>
<wire x1="-259.08" y1="238.76" x2="-251.46" y2="238.76" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="GND"/>
<pinref part="IC3" gate="G$1" pin="REF"/>
<wire x1="-251.46" y1="241.3" x2="-259.08" y2="241.3" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="1"/>
<junction x="-259.08" y="238.76"/>
<junction x="-259.08" y="241.3"/>
<pinref part="C51" gate="G$1" pin="2"/>
<junction x="-266.7" y="241.3"/>
</segment>
<segment>
<pinref part="CON2" gate="G$1" pin="2"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="-246.38" y1="187.96" x2="-243.84" y2="187.96" width="0.1524" layer="91"/>
<wire x1="-243.84" y1="187.96" x2="-243.84" y2="180.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="R6" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R44" gate="G$1" pin="2"/>
<pinref part="GND26" gate="1" pin="GND"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="-347.98" y1="15.24" x2="-347.98" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-347.98" y1="12.7" x2="-355.6" y2="12.7" width="0.1524" layer="91"/>
<junction x="-355.6" y="12.7"/>
</segment>
<segment>
<pinref part="R46" gate="G$1" pin="2"/>
<pinref part="GND27" gate="1" pin="GND"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="-317.5" y1="15.24" x2="-317.5" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-317.5" y1="12.7" x2="-325.12" y2="12.7" width="0.1524" layer="91"/>
<junction x="-325.12" y="12.7"/>
</segment>
<segment>
<pinref part="GND32" gate="1" pin="GND"/>
<wire x1="-124.46" y1="152.4" x2="-124.46" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND33" gate="1" pin="GND"/>
<wire x1="-114.3" y1="152.4" x2="-114.3" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="IC6" gate="G$1" pin="GND"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="-127" y1="175.26" x2="-114.3" y2="175.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND35" gate="1" pin="GND"/>
<wire x1="-121.92" y1="205.74" x2="-121.92" y2="208.28" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND36" gate="1" pin="GND"/>
<wire x1="-114.3" y1="205.74" x2="-114.3" y2="208.28" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND37" gate="1" pin="GND"/>
<pinref part="R4" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND38" gate="1" pin="GND"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="-129.54" y1="205.74" x2="-129.54" y2="208.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND34" gate="1" pin="GND"/>
<pinref part="C18" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND40" gate="1" pin="GND"/>
<pinref part="C35" gate="G$1" pin="2"/>
<wire x1="-172.72" y1="215.9" x2="-172.72" y2="218.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R25" gate="G$1" pin="2"/>
<pinref part="GND24" gate="1" pin="GND"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="-350.52" y1="63.5" x2="-350.52" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-350.52" y1="60.96" x2="-358.14" y2="60.96" width="0.1524" layer="91"/>
<junction x="-358.14" y="60.96"/>
</segment>
<segment>
<pinref part="R31" gate="G$1" pin="2"/>
<pinref part="GND25" gate="1" pin="GND"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="-309.88" y1="63.5" x2="-309.88" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-309.88" y1="60.96" x2="-317.5" y2="60.96" width="0.1524" layer="91"/>
<junction x="-317.5" y="60.96"/>
</segment>
<segment>
<pinref part="IC6" gate="G$1" pin="FSYNC"/>
<wire x1="-144.78" y1="144.78" x2="-144.78" y2="139.7" width="0.1524" layer="91"/>
<wire x1="-144.78" y1="139.7" x2="-129.54" y2="139.7" width="0.1524" layer="91"/>
<wire x1="-129.54" y1="139.7" x2="-129.54" y2="129.54" width="0.1524" layer="91"/>
<pinref part="GND41" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND42" gate="1" pin="GND"/>
<wire x1="-144.78" y1="200.66" x2="-139.7" y2="200.66" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="NC_20"/>
<wire x1="-144.78" y1="200.66" x2="-144.78" y2="193.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND43" gate="1" pin="GND"/>
<wire x1="-76.2" y1="76.2" x2="-71.12" y2="76.2" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="GND_1"/>
</segment>
<segment>
<pinref part="GND53" gate="1" pin="GND"/>
<wire x1="-76.2" y1="58.42" x2="-71.12" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="GND_2"/>
</segment>
<segment>
<pinref part="GND55" gate="1" pin="GND"/>
<wire x1="-76.2" y1="50.8" x2="-71.12" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="GND_3"/>
</segment>
<segment>
<pinref part="GND58" gate="1" pin="GND"/>
<wire x1="-15.24" y1="50.8" x2="-20.32" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="GND_6"/>
</segment>
<segment>
<pinref part="GND57" gate="1" pin="GND"/>
<wire x1="-15.24" y1="68.58" x2="-20.32" y2="68.58" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="GND_5"/>
</segment>
<segment>
<pinref part="GND56" gate="1" pin="GND"/>
<wire x1="-15.24" y1="76.2" x2="-20.32" y2="76.2" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="GND_4"/>
</segment>
<segment>
<pinref part="GND61" gate="1" pin="GND"/>
<pinref part="C14" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND63" gate="1" pin="GND"/>
<pinref part="C34" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND64" gate="1" pin="GND"/>
<wire x1="-71.12" y1="83.82" x2="-71.12" y2="86.36" width="0.1524" layer="91"/>
<pinref part="C36" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="GND15"/>
<wire x1="-27.94" y1="167.64" x2="-25.4" y2="167.64" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="167.64" x2="-25.4" y2="165.1" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="165.1" x2="-25.4" y2="162.56" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="162.56" x2="-25.4" y2="160.02" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="160.02" x2="-25.4" y2="157.48" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="157.48" x2="-25.4" y2="154.94" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="154.94" x2="-25.4" y2="152.4" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="152.4" x2="-25.4" y2="149.86" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="149.86" x2="-25.4" y2="147.32" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="147.32" x2="-25.4" y2="144.78" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="144.78" x2="-25.4" y2="142.24" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="142.24" x2="-22.86" y2="142.24" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="GND25"/>
<wire x1="-27.94" y1="142.24" x2="-25.4" y2="142.24" width="0.1524" layer="91"/>
<junction x="-25.4" y="142.24"/>
<pinref part="U$2" gate="G$1" pin="GND24"/>
<wire x1="-27.94" y1="144.78" x2="-25.4" y2="144.78" width="0.1524" layer="91"/>
<junction x="-25.4" y="144.78"/>
<pinref part="U$2" gate="G$1" pin="GND23"/>
<wire x1="-27.94" y1="147.32" x2="-25.4" y2="147.32" width="0.1524" layer="91"/>
<junction x="-25.4" y="147.32"/>
<pinref part="U$2" gate="G$1" pin="GND22"/>
<wire x1="-27.94" y1="149.86" x2="-25.4" y2="149.86" width="0.1524" layer="91"/>
<junction x="-25.4" y="149.86"/>
<pinref part="U$2" gate="G$1" pin="GND21"/>
<wire x1="-27.94" y1="152.4" x2="-25.4" y2="152.4" width="0.1524" layer="91"/>
<junction x="-25.4" y="152.4"/>
<pinref part="U$2" gate="G$1" pin="GND20"/>
<wire x1="-27.94" y1="154.94" x2="-25.4" y2="154.94" width="0.1524" layer="91"/>
<junction x="-25.4" y="154.94"/>
<pinref part="U$2" gate="G$1" pin="GND19"/>
<wire x1="-27.94" y1="157.48" x2="-25.4" y2="157.48" width="0.1524" layer="91"/>
<junction x="-25.4" y="157.48"/>
<pinref part="U$2" gate="G$1" pin="GND18"/>
<wire x1="-27.94" y1="160.02" x2="-25.4" y2="160.02" width="0.1524" layer="91"/>
<junction x="-25.4" y="160.02"/>
<pinref part="U$2" gate="G$1" pin="GND17"/>
<wire x1="-27.94" y1="162.56" x2="-25.4" y2="162.56" width="0.1524" layer="91"/>
<junction x="-25.4" y="162.56"/>
<pinref part="U$2" gate="G$1" pin="GND16"/>
<wire x1="-27.94" y1="165.1" x2="-25.4" y2="165.1" width="0.1524" layer="91"/>
<junction x="-25.4" y="165.1"/>
<pinref part="GND3" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="GND6"/>
<wire x1="-27.94" y1="193.04" x2="-25.4" y2="193.04" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="193.04" x2="-25.4" y2="190.5" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="GND14"/>
<wire x1="-25.4" y1="190.5" x2="-25.4" y2="187.96" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="187.96" x2="-25.4" y2="185.42" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="185.42" x2="-25.4" y2="182.88" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="182.88" x2="-25.4" y2="180.34" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="180.34" x2="-25.4" y2="177.8" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="177.8" x2="-25.4" y2="175.26" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="175.26" x2="-25.4" y2="172.72" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="172.72" x2="-27.94" y2="172.72" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="GND13"/>
<wire x1="-27.94" y1="175.26" x2="-25.4" y2="175.26" width="0.1524" layer="91"/>
<junction x="-25.4" y="175.26"/>
<pinref part="U$2" gate="G$1" pin="GND12"/>
<wire x1="-27.94" y1="177.8" x2="-25.4" y2="177.8" width="0.1524" layer="91"/>
<junction x="-25.4" y="177.8"/>
<pinref part="U$2" gate="G$1" pin="GND11"/>
<wire x1="-27.94" y1="180.34" x2="-25.4" y2="180.34" width="0.1524" layer="91"/>
<junction x="-25.4" y="180.34"/>
<pinref part="U$2" gate="G$1" pin="GND10"/>
<wire x1="-27.94" y1="182.88" x2="-25.4" y2="182.88" width="0.1524" layer="91"/>
<junction x="-25.4" y="182.88"/>
<pinref part="U$2" gate="G$1" pin="GND9"/>
<wire x1="-27.94" y1="185.42" x2="-25.4" y2="185.42" width="0.1524" layer="91"/>
<junction x="-25.4" y="185.42"/>
<pinref part="U$2" gate="G$1" pin="GND8"/>
<wire x1="-27.94" y1="187.96" x2="-25.4" y2="187.96" width="0.1524" layer="91"/>
<junction x="-25.4" y="187.96"/>
<pinref part="U$2" gate="G$1" pin="GND7"/>
<wire x1="-27.94" y1="190.5" x2="-25.4" y2="190.5" width="0.1524" layer="91"/>
<junction x="-25.4" y="190.5"/>
<wire x1="-25.4" y1="172.72" x2="-20.32" y2="172.72" width="0.1524" layer="91"/>
<junction x="-25.4" y="172.72"/>
<pinref part="GND4" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="-25.4" y1="200.66" x2="-25.4" y2="198.12" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="198.12" x2="-27.94" y2="198.12" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="198.12" x2="-20.32" y2="198.12" width="0.1524" layer="91"/>
<junction x="-25.4" y="198.12"/>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="U$2" gate="G$1" pin="GND5"/>
<pinref part="U$2" gate="G$1" pin="GND4"/>
<wire x1="-25.4" y1="200.66" x2="-27.94" y2="200.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="GND1"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="-68.58" y1="203.2" x2="-73.66" y2="203.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="-68.58" y1="213.36" x2="-73.66" y2="213.36" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="GND0"/>
</segment>
<segment>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="-68.58" y1="165.1" x2="-76.2" y2="165.1" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="GND3"/>
</segment>
<segment>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="-68.58" y1="172.72" x2="-76.2" y2="172.72" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="GND2"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="-68.58" y1="223.52" x2="-68.58" y2="226.06" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="SL2" gate="G$1" pin="4"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="-33.02" y1="101.6" x2="-27.94" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-27.94" y1="236.22" x2="-22.86" y2="236.22" width="0.1524" layer="91"/>
<pinref part="GND17" gate="1" pin="GND"/>
<pinref part="SL1" gate="G$1" pin="4"/>
</segment>
<segment>
<pinref part="IC8" gate="G$1" pin="GND"/>
<wire x1="-157.48" y1="220.98" x2="-162.56" y2="220.98" width="0.1524" layer="91"/>
<pinref part="GND39" gate="1" pin="GND"/>
<wire x1="-162.56" y1="215.9" x2="-162.56" y2="220.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC5" gate="G$1" pin="AINCOM"/>
<wire x1="-205.74" y1="30.48" x2="-203.2" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-203.2" y1="30.48" x2="-172.72" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="35.56" x2="-175.26" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="30.48" x2="-172.72" y2="35.56" width="0.1524" layer="91"/>
<junction x="-172.72" y="30.48"/>
<junction x="-172.72" y="35.56"/>
<wire x1="-172.72" y1="40.64" x2="-175.26" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="35.56" x2="-172.72" y2="40.64" width="0.1524" layer="91"/>
<junction x="-172.72" y="40.64"/>
<wire x1="-172.72" y1="45.72" x2="-175.26" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="40.64" x2="-172.72" y2="45.72" width="0.1524" layer="91"/>
<junction x="-172.72" y="45.72"/>
<wire x1="-172.72" y1="50.8" x2="-175.26" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="45.72" x2="-172.72" y2="50.8" width="0.1524" layer="91"/>
<junction x="-172.72" y="50.8"/>
<wire x1="-172.72" y1="55.88" x2="-175.26" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="50.8" x2="-172.72" y2="55.88" width="0.1524" layer="91"/>
<junction x="-172.72" y="55.88"/>
<wire x1="-170.18" y1="68.58" x2="-172.72" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="68.58" x2="-175.26" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="55.88" x2="-172.72" y2="68.58" width="0.1524" layer="91"/>
<junction x="-172.72" y="68.58"/>
<wire x1="-170.18" y1="73.66" x2="-172.72" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="73.66" x2="-175.26" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="68.58" x2="-172.72" y2="73.66" width="0.1524" layer="91"/>
<junction x="-172.72" y="73.66"/>
<wire x1="-170.18" y1="78.74" x2="-172.72" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="78.74" x2="-175.26" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="73.66" x2="-172.72" y2="78.74" width="0.1524" layer="91"/>
<junction x="-172.72" y="78.74"/>
<wire x1="-170.18" y1="83.82" x2="-172.72" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="83.82" x2="-175.26" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="78.74" x2="-172.72" y2="83.82" width="0.1524" layer="91"/>
<junction x="-172.72" y="83.82"/>
<wire x1="-170.18" y1="88.9" x2="-172.72" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="88.9" x2="-175.26" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="83.82" x2="-172.72" y2="88.9" width="0.1524" layer="91"/>
<junction x="-172.72" y="88.9"/>
<pinref part="Z1" gate="G$1" pin="1"/>
<pinref part="Z5" gate="G$1" pin="1"/>
<pinref part="Z8" gate="G$1" pin="1"/>
<pinref part="Z10" gate="G$1" pin="1"/>
<pinref part="Z12" gate="G$1" pin="1"/>
<pinref part="C24" gate="G$1" pin="2"/>
<pinref part="C25" gate="G$1" pin="2"/>
<pinref part="C26" gate="G$1" pin="2"/>
<pinref part="C27" gate="G$1" pin="2"/>
<pinref part="C28" gate="G$1" pin="2"/>
<pinref part="C29" gate="G$1" pin="2"/>
<pinref part="C30" gate="G$1" pin="2"/>
<pinref part="C31" gate="G$1" pin="2"/>
<pinref part="C32" gate="G$1" pin="2"/>
<pinref part="C33" gate="G$1" pin="2"/>
<wire x1="-172.72" y1="30.48" x2="-172.72" y2="25.4" width="0.1524" layer="91"/>
<pinref part="GND44" gate="1" pin="GND"/>
<pinref part="IC5" gate="G$1" pin="AIN11"/>
<wire x1="-205.74" y1="40.64" x2="-203.2" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-203.2" y1="40.64" x2="-203.2" y2="30.48" width="0.1524" layer="91"/>
<junction x="-203.2" y="30.48"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="C53" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND30" gate="1" pin="GND"/>
<pinref part="R56" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND84" gate="1" pin="GND"/>
<pinref part="C52" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="-378.46" y1="213.36" x2="-378.46" y2="223.52" width="0.1524" layer="91"/>
<pinref part="IC13" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C54" gate="G$1" pin="2"/>
<wire x1="-297.18" y1="139.7" x2="-294.64" y2="139.7" width="0.1524" layer="91"/>
<pinref part="GND85" gate="1" pin="GND"/>
<wire x1="-294.64" y1="139.7" x2="-294.64" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J5" gate="1" pin="1"/>
<pinref part="GND86" gate="1" pin="GND"/>
<wire x1="-335.28" y1="162.56" x2="-342.9" y2="162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND70" gate="1" pin="GND"/>
<wire x1="-66.04" y1="83.82" x2="-66.04" y2="86.36" width="0.1524" layer="91"/>
<pinref part="C56" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND87" gate="1" pin="GND"/>
<wire x1="-63.5" y1="223.52" x2="-63.5" y2="226.06" width="0.1524" layer="91"/>
<pinref part="C55" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND80" gate="1" pin="GND"/>
<wire x1="-259.08" y1="15.24" x2="-259.08" y2="17.78" width="0.1524" layer="91"/>
<pinref part="J21" gate="1" pin="2"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<wire x1="-246.38" y1="76.2" x2="-243.84" y2="76.2" width="0.1524" layer="91"/>
<pinref part="P+2" gate="VCC" pin="VCC"/>
<wire x1="-243.84" y1="76.2" x2="-241.3" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-241.3" y1="76.2" x2="-241.3" y2="78.74" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="IOVDD"/>
<wire x1="-241.3" y1="76.2" x2="-241.3" y2="73.66" width="0.1524" layer="91"/>
<junction x="-241.3" y="76.2"/>
<pinref part="IC5" gate="G$1" pin="DVDD"/>
<wire x1="-243.84" y1="73.66" x2="-243.84" y2="76.2" width="0.1524" layer="91"/>
<junction x="-243.84" y="76.2"/>
<pinref part="C19" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="-251.46" y1="99.06" x2="-254" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-254" y1="99.06" x2="-254" y2="101.6" width="0.1524" layer="91"/>
<pinref part="P+19" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R45" gate="G$1" pin="1"/>
<pinref part="P+13" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="P+16" gate="VCC" pin="VCC"/>
<wire x1="-172.72" y1="251.46" x2="-172.72" y2="254" width="0.1524" layer="91"/>
<pinref part="J6" gate="1" pin="2"/>
</segment>
<segment>
<wire x1="-96.52" y1="93.98" x2="-99.06" y2="93.98" width="0.1524" layer="91"/>
<pinref part="P+18" gate="VCC" pin="VCC"/>
<wire x1="-99.06" y1="96.52" x2="-99.06" y2="93.98" width="0.1524" layer="91"/>
<pinref part="J16" gate="1" pin="2"/>
</segment>
<segment>
<wire x1="-93.98" y1="233.68" x2="-96.52" y2="233.68" width="0.1524" layer="91"/>
<pinref part="P+5" gate="VCC" pin="VCC"/>
<wire x1="-96.52" y1="236.22" x2="-96.52" y2="233.68" width="0.1524" layer="91"/>
<pinref part="J11" gate="1" pin="2"/>
</segment>
<segment>
<pinref part="P+3" gate="VCC" pin="VCC"/>
<wire x1="-383.54" y1="154.94" x2="-383.54" y2="149.86" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="P+12" gate="VCC" pin="VCC"/>
<wire x1="-386.08" y1="254" x2="-386.08" y2="251.46" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="J24" gate="G$1" pin="2"/>
<pinref part="P+6" gate="VCC" pin="VCC"/>
<wire x1="-287.02" y1="241.3" x2="-287.02" y2="243.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<wire x1="-309.88" y1="129.54" x2="-304.8" y2="129.54" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="SCLK/SCL"/>
<label x="-304.8" y="129.54" size="0.8128" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-165.1" y1="203.2" x2="-167.64" y2="203.2" width="0.1524" layer="91"/>
<label x="-167.64" y="203.2" size="0.8128" layer="95" rot="R180" xref="yes"/>
<pinref part="J7" gate="1" pin="1"/>
</segment>
<segment>
<wire x1="-165.1" y1="134.62" x2="-165.1" y2="132.08" width="0.1524" layer="91"/>
<pinref part="J10" gate="1" pin="1"/>
<label x="-165.1" y="132.08" size="0.8128" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="N$125" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="CON1" gate="G$1" pin="1"/>
<wire x1="-142.24" y1="88.9" x2="-132.08" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$126" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="CON1" gate="G$1" pin="2"/>
<wire x1="-142.24" y1="86.36" x2="-132.08" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$127" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="-152.4" y1="88.9" x2="-160.02" y2="88.9" width="0.1524" layer="91"/>
<pinref part="Z1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$128" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-152.4" y1="86.36" x2="-160.02" y2="86.36" width="0.1524" layer="91"/>
<pinref part="Z3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$129" class="0">
<segment>
<wire x1="-170.18" y1="86.36" x2="-185.42" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="86.36" x2="-185.42" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="88.9" x2="-182.88" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-200.66" y1="86.36" x2="-185.42" y2="86.36" width="0.1524" layer="91"/>
<junction x="-185.42" y="86.36"/>
<pinref part="IC5" gate="G$1" pin="AIN1"/>
<wire x1="-205.74" y1="66.04" x2="-200.66" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-200.66" y1="66.04" x2="-200.66" y2="86.36" width="0.1524" layer="91"/>
<pinref part="Z3" gate="G$1" pin="1"/>
<pinref part="C24" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$130" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="-142.24" y1="83.82" x2="-132.08" y2="83.82" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$131" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="-142.24" y1="81.28" x2="-132.08" y2="81.28" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="4"/>
</segment>
</net>
<net name="N$132" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="-152.4" y1="83.82" x2="-160.02" y2="83.82" width="0.1524" layer="91"/>
<pinref part="Z5" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$133" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="-152.4" y1="81.28" x2="-160.02" y2="81.28" width="0.1524" layer="91"/>
<pinref part="Z7" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$134" class="0">
<segment>
<wire x1="-170.18" y1="81.28" x2="-185.42" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="81.28" x2="-185.42" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="83.82" x2="-182.88" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-198.12" y1="81.28" x2="-185.42" y2="81.28" width="0.1524" layer="91"/>
<junction x="-185.42" y="81.28"/>
<pinref part="IC5" gate="G$1" pin="AIN2"/>
<wire x1="-205.74" y1="63.5" x2="-198.12" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-198.12" y1="63.5" x2="-198.12" y2="81.28" width="0.1524" layer="91"/>
<pinref part="Z7" gate="G$1" pin="1"/>
<pinref part="C25" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$135" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="-142.24" y1="78.74" x2="-132.08" y2="78.74" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$136" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="-142.24" y1="76.2" x2="-132.08" y2="76.2" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="6"/>
</segment>
</net>
<net name="N$137" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="-152.4" y1="78.74" x2="-160.02" y2="78.74" width="0.1524" layer="91"/>
<pinref part="Z8" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$138" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="-152.4" y1="76.2" x2="-160.02" y2="76.2" width="0.1524" layer="91"/>
<pinref part="Z9" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$139" class="0">
<segment>
<wire x1="-170.18" y1="76.2" x2="-185.42" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="76.2" x2="-185.42" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="78.74" x2="-182.88" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-195.58" y1="76.2" x2="-185.42" y2="76.2" width="0.1524" layer="91"/>
<junction x="-185.42" y="76.2"/>
<pinref part="IC5" gate="G$1" pin="AIN3"/>
<wire x1="-205.74" y1="60.96" x2="-195.58" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-195.58" y1="60.96" x2="-195.58" y2="76.2" width="0.1524" layer="91"/>
<pinref part="Z9" gate="G$1" pin="1"/>
<pinref part="C26" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$140" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="-142.24" y1="73.66" x2="-132.08" y2="73.66" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="7"/>
</segment>
</net>
<net name="N$141" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="-142.24" y1="71.12" x2="-132.08" y2="71.12" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="8"/>
</segment>
</net>
<net name="N$142" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="-152.4" y1="73.66" x2="-160.02" y2="73.66" width="0.1524" layer="91"/>
<pinref part="Z10" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$143" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="-152.4" y1="71.12" x2="-160.02" y2="71.12" width="0.1524" layer="91"/>
<pinref part="Z11" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$144" class="0">
<segment>
<wire x1="-170.18" y1="71.12" x2="-185.42" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="71.12" x2="-185.42" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="73.66" x2="-182.88" y2="73.66" width="0.1524" layer="91"/>
<junction x="-185.42" y="71.12"/>
<pinref part="IC5" gate="G$1" pin="AIN4"/>
<wire x1="-205.74" y1="58.42" x2="-193.04" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-193.04" y1="58.42" x2="-193.04" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-193.04" y1="71.12" x2="-185.42" y2="71.12" width="0.1524" layer="91"/>
<pinref part="Z11" gate="G$1" pin="1"/>
<pinref part="C27" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$145" class="0">
<segment>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="-142.24" y1="68.58" x2="-132.08" y2="68.58" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="9"/>
</segment>
</net>
<net name="N$146" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="-142.24" y1="66.04" x2="-132.08" y2="66.04" width="0.1524" layer="91"/>
<pinref part="CON1" gate="G$1" pin="10"/>
</segment>
</net>
<net name="N$147" class="0">
<segment>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="-152.4" y1="68.58" x2="-160.02" y2="68.58" width="0.1524" layer="91"/>
<pinref part="Z12" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$148" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="-152.4" y1="66.04" x2="-160.02" y2="66.04" width="0.1524" layer="91"/>
<pinref part="Z13" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$149" class="0">
<segment>
<wire x1="-170.18" y1="66.04" x2="-185.42" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="66.04" x2="-185.42" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="68.58" x2="-182.88" y2="68.58" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="AIN5"/>
<wire x1="-205.74" y1="55.88" x2="-187.96" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="55.88" x2="-187.96" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="66.04" x2="-185.42" y2="66.04" width="0.1524" layer="91"/>
<junction x="-185.42" y="66.04"/>
<pinref part="Z13" gate="G$1" pin="1"/>
<pinref part="C28" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$153" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="1"/>
<wire x1="-152.4" y1="53.34" x2="-160.02" y2="53.34" width="0.1524" layer="91"/>
<pinref part="Z15" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$154" class="0">
<segment>
<wire x1="-170.18" y1="53.34" x2="-185.42" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="53.34" x2="-185.42" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="55.88" x2="-182.88" y2="55.88" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="AIN6"/>
<wire x1="-205.74" y1="53.34" x2="-185.42" y2="53.34" width="0.1524" layer="91"/>
<junction x="-185.42" y="53.34"/>
<pinref part="Z15" gate="G$1" pin="1"/>
<pinref part="C29" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$158" class="0">
<segment>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="-152.4" y1="48.26" x2="-160.02" y2="48.26" width="0.1524" layer="91"/>
<pinref part="Z17" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$159" class="0">
<segment>
<wire x1="-170.18" y1="48.26" x2="-185.42" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="48.26" x2="-185.42" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="50.8" x2="-182.88" y2="50.8" width="0.1524" layer="91"/>
<junction x="-185.42" y="50.8"/>
<pinref part="IC5" gate="G$1" pin="AIN7"/>
<wire x1="-205.74" y1="50.8" x2="-185.42" y2="50.8" width="0.1524" layer="91"/>
<pinref part="Z17" gate="G$1" pin="1"/>
<pinref part="C30" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$163" class="0">
<segment>
<pinref part="R30" gate="G$1" pin="1"/>
<wire x1="-152.4" y1="43.18" x2="-160.02" y2="43.18" width="0.1524" layer="91"/>
<pinref part="Z19" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$164" class="0">
<segment>
<wire x1="-170.18" y1="43.18" x2="-185.42" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="43.18" x2="-185.42" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="45.72" x2="-182.88" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-193.04" y1="45.72" x2="-185.42" y2="45.72" width="0.1524" layer="91"/>
<junction x="-185.42" y="45.72"/>
<pinref part="IC5" gate="G$1" pin="AIN8"/>
<wire x1="-205.74" y1="48.26" x2="-193.04" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-193.04" y1="48.26" x2="-193.04" y2="45.72" width="0.1524" layer="91"/>
<pinref part="Z19" gate="G$1" pin="1"/>
<pinref part="C31" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$168" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="1"/>
<wire x1="-152.4" y1="38.1" x2="-160.02" y2="38.1" width="0.1524" layer="91"/>
<pinref part="Z21" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$169" class="0">
<segment>
<wire x1="-170.18" y1="38.1" x2="-185.42" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="38.1" x2="-185.42" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="40.64" x2="-182.88" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-195.58" y1="40.64" x2="-185.42" y2="40.64" width="0.1524" layer="91"/>
<junction x="-185.42" y="40.64"/>
<pinref part="IC5" gate="G$1" pin="AIN9"/>
<wire x1="-205.74" y1="45.72" x2="-195.58" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-195.58" y1="45.72" x2="-195.58" y2="40.64" width="0.1524" layer="91"/>
<pinref part="Z21" gate="G$1" pin="1"/>
<pinref part="C32" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$173" class="0">
<segment>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="-152.4" y1="33.02" x2="-160.02" y2="33.02" width="0.1524" layer="91"/>
<pinref part="Z23" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$174" class="0">
<segment>
<wire x1="-170.18" y1="33.02" x2="-185.42" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="33.02" x2="-185.42" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="35.56" x2="-182.88" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-198.12" y1="35.56" x2="-185.42" y2="35.56" width="0.1524" layer="91"/>
<junction x="-185.42" y="35.56"/>
<pinref part="IC5" gate="G$1" pin="AIN10"/>
<wire x1="-205.74" y1="43.18" x2="-198.12" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-198.12" y1="43.18" x2="-198.12" y2="35.56" width="0.1524" layer="91"/>
<pinref part="Z23" gate="G$1" pin="1"/>
<pinref part="C33" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$175" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="AIN0"/>
<wire x1="-205.74" y1="68.58" x2="-203.2" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-203.2" y1="68.58" x2="-203.2" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="-203.2" y1="96.52" x2="-205.74" y2="96.52" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="-203.2" y1="96.52" x2="-200.66" y2="96.52" width="0.1524" layer="91"/>
<junction x="-203.2" y="96.52"/>
</segment>
</net>
<net name="N$176" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="REFOUT"/>
<wire x1="-218.44" y1="73.66" x2="-218.44" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="-218.44" y1="91.44" x2="-218.44" y2="96.52" width="0.1524" layer="91"/>
<wire x1="-218.44" y1="96.52" x2="-215.9" y2="96.52" width="0.1524" layer="91"/>
<wire x1="-213.36" y1="88.9" x2="-213.36" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-213.36" y1="91.44" x2="-218.44" y2="91.44" width="0.1524" layer="91"/>
<junction x="-218.44" y="91.44"/>
<pinref part="C22" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$178" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="AVDD"/>
<wire x1="-226.06" y1="73.66" x2="-226.06" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-226.06" y1="91.44" x2="-226.06" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-226.06" y1="99.06" x2="-228.6" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-231.14" y1="88.9" x2="-231.14" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-231.14" y1="91.44" x2="-226.06" y2="91.44" width="0.1524" layer="91"/>
<junction x="-226.06" y="91.44"/>
<pinref part="Z24" gate="G$1" pin="2"/>
<pinref part="C21" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$179" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="-238.76" y1="99.06" x2="-241.3" y2="99.06" width="0.1524" layer="91"/>
<pinref part="Z24" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DAC_OUT" class="0">
<segment>
<wire x1="-345.44" y1="137.16" x2="-378.46" y2="137.16" width="0.1524" layer="91"/>
<pinref part="TP4" gate="G$1" pin="TP"/>
<wire x1="-378.46" y1="137.16" x2="-363.22" y2="137.16" width="0.1524" layer="91"/>
<label x="-375.92" y="137.16" size="1.778" layer="95"/>
<pinref part="IC2" gate="G$1" pin="VOUT"/>
</segment>
<segment>
<pinref part="IC7" gate="G$1" pin="IN1+"/>
<wire x1="-264.16" y1="139.7" x2="-251.46" y2="139.7" width="0.1524" layer="91"/>
<label x="-264.16" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="GATE" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="IC7" gate="G$1" pin="OUT1"/>
<wire x1="-269.24" y1="152.4" x2="-251.46" y2="152.4" width="0.1524" layer="91"/>
<wire x1="-251.46" y1="152.4" x2="-251.46" y2="144.78" width="0.1524" layer="91"/>
<wire x1="-269.24" y1="152.4" x2="-274.32" y2="152.4" width="0.1524" layer="91"/>
<junction x="-269.24" y="152.4"/>
<pinref part="TP5" gate="G$1" pin="TP"/>
<label x="-274.32" y="152.4" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="-358.14" y1="86.36" x2="-358.14" y2="83.82" width="0.1524" layer="91"/>
<label x="-358.14" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="G"/>
<wire x1="-226.06" y1="187.96" x2="-215.9" y2="187.96" width="0.1524" layer="91"/>
<label x="-226.06" y="187.96" size="1.778" layer="95"/>
<pinref part="R56" gate="G$1" pin="1"/>
<wire x1="-215.9" y1="187.96" x2="-210.82" y2="187.96" width="0.1524" layer="91"/>
<junction x="-215.9" y="187.96"/>
</segment>
</net>
<net name="GND1" class="0">
<segment>
<pinref part="R43" gate="G$1" pin="1"/>
<pinref part="R42" gate="G$1" pin="2"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="-325.12" y1="218.44" x2="-325.12" y2="228.6" width="0.1524" layer="91"/>
<wire x1="-317.5" y1="231.14" x2="-317.5" y2="218.44" width="0.1524" layer="91"/>
<wire x1="-317.5" y1="218.44" x2="-325.12" y2="218.44" width="0.1524" layer="91"/>
<wire x1="-337.82" y1="218.44" x2="-332.74" y2="218.44" width="0.1524" layer="91"/>
<junction x="-325.12" y="218.44"/>
<pinref part="R51" gate="G$1" pin="1"/>
<wire x1="-332.74" y1="218.44" x2="-325.12" y2="218.44" width="0.1524" layer="91"/>
<junction x="-332.74" y="218.44"/>
<junction x="-325.12" y="228.6"/>
<pinref part="R50" gate="G$1" pin="2"/>
<wire x1="-332.74" y1="228.6" x2="-325.12" y2="228.6" width="0.1524" layer="91"/>
<wire x1="-337.82" y1="218.44" x2="-337.82" y2="228.6" width="0.1524" layer="91"/>
<pinref part="IC13" gate="G$1" pin="FB"/>
<wire x1="-337.82" y1="228.6" x2="-350.52" y2="228.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC2" class="0">
<segment>
<wire x1="-345.44" y1="223.52" x2="-345.44" y2="238.76" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="-342.9" y1="238.76" x2="-345.44" y2="238.76" width="0.1524" layer="91"/>
<junction x="-345.44" y="238.76"/>
<pinref part="L1" gate="G$1" pin="4"/>
<wire x1="-353.06" y1="238.76" x2="-345.44" y2="238.76" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="-353.06" y1="241.3" x2="-353.06" y2="238.76" width="0.1524" layer="91"/>
<junction x="-353.06" y="238.76"/>
<pinref part="IC13" gate="G$1" pin="LX"/>
<wire x1="-345.44" y1="223.52" x2="-350.52" y2="223.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="D"/>
<wire x1="-203.2" y1="208.28" x2="-203.2" y2="198.12" width="0.1524" layer="91"/>
<wire x1="-203.2" y1="208.28" x2="-208.28" y2="208.28" width="0.1524" layer="91"/>
<pinref part="R49" gate="G$1" pin="2"/>
<wire x1="-213.36" y1="215.9" x2="-208.28" y2="215.9" width="0.1524" layer="91"/>
<wire x1="-208.28" y1="215.9" x2="-208.28" y2="208.28" width="0.1524" layer="91"/>
<junction x="-208.28" y="208.28"/>
<pinref part="R47" gate="G$1" pin="1"/>
</segment>
</net>
<net name="I_SENS" class="0">
<segment>
<wire x1="-226.06" y1="241.3" x2="-220.98" y2="241.3" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="OUT"/>
<pinref part="TP6" gate="G$1" pin="TP"/>
<wire x1="-220.98" y1="241.3" x2="-218.44" y2="241.3" width="0.1524" layer="91"/>
<junction x="-220.98" y="241.3"/>
<label x="-218.44" y="241.3" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<wire x1="-142.24" y1="53.34" x2="-137.16" y2="53.34" width="0.1524" layer="91"/>
<label x="-137.16" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="U_TEG" class="0">
<segment>
<pinref part="CON2" gate="G$1" pin="1"/>
<wire x1="-243.84" y1="190.5" x2="-246.38" y2="190.5" width="0.1524" layer="91"/>
<wire x1="-243.84" y1="190.5" x2="-243.84" y2="200.66" width="0.1524" layer="91"/>
<wire x1="-243.84" y1="200.66" x2="-243.84" y2="208.28" width="0.1524" layer="91"/>
<wire x1="-243.84" y1="208.28" x2="-226.06" y2="208.28" width="0.1524" layer="91"/>
<wire x1="-243.84" y1="200.66" x2="-241.3" y2="200.66" width="0.1524" layer="91"/>
<pinref part="R49" gate="G$1" pin="1"/>
<wire x1="-220.98" y1="215.9" x2="-226.06" y2="215.9" width="0.1524" layer="91"/>
<wire x1="-226.06" y1="215.9" x2="-226.06" y2="208.28" width="0.1524" layer="91"/>
<pinref part="TP7" gate="G$1" pin="TP"/>
<junction x="-243.84" y="208.28"/>
<junction x="-243.84" y="200.66"/>
<junction x="-226.06" y="208.28"/>
<label x="-241.3" y="200.66" size="1.778" layer="95"/>
<pinref part="R48" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-317.5" y1="86.36" x2="-317.5" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R29" gate="G$1" pin="2"/>
<label x="-317.5" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="GATE_SENS" class="0">
<segment>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="-358.14" y1="71.12" x2="-358.14" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R25" gate="G$1" pin="1"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="-350.52" y1="71.12" x2="-350.52" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-350.52" y1="73.66" x2="-358.14" y2="73.66" width="0.1524" layer="91"/>
<junction x="-358.14" y="73.66"/>
<label x="-350.52" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-142.24" y1="43.18" x2="-137.16" y2="43.18" width="0.1524" layer="91"/>
<pinref part="R30" gate="G$1" pin="2"/>
<label x="-137.16" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="U_SENS" class="0">
<segment>
<wire x1="-317.5" y1="71.12" x2="-317.5" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R31" gate="G$1" pin="1"/>
<pinref part="R29" gate="G$1" pin="1"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="-309.88" y1="71.12" x2="-309.88" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-309.88" y1="73.66" x2="-317.5" y2="73.66" width="0.1524" layer="91"/>
<junction x="-317.5" y="73.66"/>
<label x="-309.88" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R28" gate="G$1" pin="2"/>
<wire x1="-137.16" y1="48.26" x2="-142.24" y2="48.26" width="0.1524" layer="91"/>
<label x="-137.16" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="BOOST_SENS" class="0">
<segment>
<wire x1="-355.6" y1="25.4" x2="-355.6" y2="22.86" width="0.1524" layer="91"/>
<pinref part="R44" gate="G$1" pin="1"/>
<pinref part="R33" gate="G$1" pin="1"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="-347.98" y1="22.86" x2="-347.98" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-347.98" y1="25.4" x2="-355.6" y2="25.4" width="0.1524" layer="91"/>
<junction x="-355.6" y="25.4"/>
<label x="-347.98" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="-142.24" y1="38.1" x2="-137.16" y2="38.1" width="0.1524" layer="91"/>
<label x="-137.16" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="VCC_SENS" class="0">
<segment>
<wire x1="-325.12" y1="25.4" x2="-325.12" y2="22.86" width="0.1524" layer="91"/>
<pinref part="R46" gate="G$1" pin="1"/>
<pinref part="R45" gate="G$1" pin="2"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="-317.5" y1="22.86" x2="-317.5" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-317.5" y1="25.4" x2="-325.12" y2="25.4" width="0.1524" layer="91"/>
<junction x="-325.12" y="25.4"/>
<label x="-317.5" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="-142.24" y1="33.02" x2="-137.16" y2="33.02" width="0.1524" layer="91"/>
<label x="-137.16" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="IC7" gate="G$1" pin="IN2+"/>
<wire x1="-223.52" y1="144.78" x2="-218.44" y2="144.78" width="0.1524" layer="91"/>
<label x="-218.44" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="IC7" gate="G$1" pin="IN1-"/>
<wire x1="-269.24" y1="142.24" x2="-251.46" y2="142.24" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<junction x="-269.24" y="142.24"/>
<wire x1="-269.24" y1="142.24" x2="-276.86" y2="142.24" width="0.1524" layer="91"/>
<wire x1="-276.86" y1="142.24" x2="-276.86" y2="139.7" width="0.1524" layer="91"/>
<pinref part="C53" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="IC7" gate="G$1" pin="IN2-"/>
<wire x1="-223.52" y1="142.24" x2="-218.44" y2="142.24" width="0.1524" layer="91"/>
<pinref part="IC7" gate="G$1" pin="OUT2"/>
<wire x1="-223.52" y1="139.7" x2="-218.44" y2="139.7" width="0.1524" layer="91"/>
<wire x1="-218.44" y1="142.24" x2="-218.44" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="1V8" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="OUT"/>
<wire x1="-132.08" y1="220.98" x2="-129.54" y2="220.98" width="0.1524" layer="91"/>
<wire x1="-129.54" y1="220.98" x2="-121.92" y2="220.98" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="220.98" x2="-114.3" y2="220.98" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="215.9" x2="-121.92" y2="220.98" width="0.1524" layer="91"/>
<junction x="-121.92" y="220.98"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="-114.3" y1="218.44" x2="-114.3" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="-129.54" y1="215.9" x2="-129.54" y2="220.98" width="0.1524" layer="91"/>
<junction x="-129.54" y="220.98"/>
<pinref part="C13" gate="G$1" pin="1"/>
<label x="-114.3" y="220.98" size="0.8128" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-149.86" y1="193.04" x2="-149.86" y2="200.66" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="NCS"/>
<label x="-149.86" y="200.66" size="0.8128" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="IC6" gate="G$1" pin="VDD"/>
<wire x1="-127" y1="162.56" x2="-124.46" y2="162.56" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="-124.46" y1="162.56" x2="-114.3" y2="162.56" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="162.56" x2="-111.76" y2="162.56" width="0.1524" layer="91"/>
<junction x="-114.3" y="162.56"/>
<pinref part="C11" gate="G$1" pin="1"/>
<junction x="-124.46" y="162.56"/>
<label x="-111.76" y="162.56" size="0.8128" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-152.4" y1="144.78" x2="-152.4" y2="139.7" width="0.1524" layer="91"/>
<wire x1="-152.4" y1="139.7" x2="-154.94" y2="139.7" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="VDDIO"/>
<label x="-154.94" y="139.7" size="0.8128" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="REGOUT"/>
<wire x1="-139.7" y1="137.16" x2="-147.32" y2="137.16" width="0.1524" layer="91"/>
<wire x1="-147.32" y1="137.16" x2="-147.32" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="IC6" gate="G$1" pin="AD0/SDO"/>
<wire x1="-149.86" y1="139.7" x2="-149.86" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="I2C_INT" class="0">
<segment>
<wire x1="-139.7" y1="142.24" x2="-142.24" y2="142.24" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="INT"/>
<wire x1="-142.24" y1="142.24" x2="-142.24" y2="144.78" width="0.1524" layer="91"/>
<pinref part="J23" gate="1" pin="2"/>
</segment>
<segment>
<pinref part="J23" gate="1" pin="1"/>
<wire x1="-129.54" y1="142.24" x2="-127" y2="142.24" width="0.1524" layer="91"/>
<label x="-127" y="142.24" size="0.8128" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="PR_DA"/>
<wire x1="-170.18" y1="162.56" x2="-170.18" y2="144.78" width="0.1524" layer="91"/>
<pinref part="J9" gate="1" pin="2"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="RP_CL"/>
<wire x1="-165.1" y1="144.78" x2="-154.94" y2="144.78" width="0.1524" layer="91"/>
<pinref part="J10" gate="1" pin="2"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="Z16" gate="G$1" pin="1"/>
<wire x1="-172.72" y1="241.3" x2="-172.72" y2="238.76" width="0.1524" layer="91"/>
<pinref part="J6" gate="1" pin="1"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="IC8" gate="G$1" pin="IN"/>
<wire x1="-160.02" y1="223.52" x2="-157.48" y2="223.52" width="0.1524" layer="91"/>
<pinref part="IC8" gate="G$1" pin="EN"/>
<wire x1="-157.48" y1="218.44" x2="-160.02" y2="218.44" width="0.1524" layer="91"/>
<wire x1="-160.02" y1="218.44" x2="-160.02" y2="223.52" width="0.1524" layer="91"/>
<junction x="-160.02" y="223.52"/>
<wire x1="-160.02" y1="228.6" x2="-160.02" y2="223.52" width="0.1524" layer="91"/>
<pinref part="C35" gate="G$1" pin="1"/>
<pinref part="Z16" gate="G$1" pin="2"/>
<wire x1="-172.72" y1="226.06" x2="-172.72" y2="228.6" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="228.6" x2="-160.02" y2="228.6" width="0.1524" layer="91"/>
<junction x="-172.72" y="228.6"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<wire x1="-20.32" y1="58.42" x2="-15.24" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="SIO_3/AIN/MOSI"/>
<pinref part="TP20" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<wire x1="-15.24" y1="55.88" x2="-20.32" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="SIO_17/MISO"/>
<pinref part="TP23" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<wire x1="-20.32" y1="53.34" x2="-15.24" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="SIO_00/CLK"/>
<pinref part="TP22" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="Z18" gate="G$1" pin="1"/>
<wire x1="-86.36" y1="93.98" x2="-83.82" y2="93.98" width="0.1524" layer="91"/>
<pinref part="J16" gate="1" pin="1"/>
</segment>
</net>
<net name="VCC_LORA1" class="0">
<segment>
<wire x1="-63.5" y1="93.98" x2="-66.04" y2="93.98" width="0.1524" layer="91"/>
<pinref part="Z18" gate="G$1" pin="2"/>
<pinref part="C36" gate="G$1" pin="2"/>
<wire x1="-66.04" y1="93.98" x2="-71.12" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="93.98" x2="-73.66" y2="93.98" width="0.1524" layer="91"/>
<junction x="-71.12" y="93.98"/>
<pinref part="C56" gate="G$1" pin="2"/>
<junction x="-66.04" y="93.98"/>
<label x="-63.5" y="93.98" size="0.8128" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="-78.74" y1="48.26" x2="-86.36" y2="48.26" width="0.1524" layer="91"/>
<junction x="-78.74" y="48.26"/>
<wire x1="-71.12" y1="48.26" x2="-78.74" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="VCC_BLE"/>
<label x="-86.36" y="48.26" size="0.8128" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-30.48" y1="93.98" x2="-33.02" y2="93.98" width="0.1524" layer="91"/>
<pinref part="SL2" gate="G$1" pin="1"/>
<label x="-30.48" y="93.98" size="0.8128" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-20.32" y1="48.26" x2="-15.24" y2="48.26" width="0.1524" layer="91"/>
<pinref part="C34" gate="G$1" pin="1"/>
<wire x1="-20.32" y1="48.26" x2="-20.32" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="40.64" x2="-25.4" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="VCC_LORA"/>
<junction x="-20.32" y="48.26"/>
<label x="-25.4" y="40.64" size="0.8128" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="Z2" gate="G$1" pin="1"/>
<wire x1="-83.82" y1="233.68" x2="-81.28" y2="233.68" width="0.1524" layer="91"/>
<pinref part="J11" gate="1" pin="1"/>
</segment>
</net>
<net name="VCC_LORA2" class="0">
<segment>
<wire x1="-55.88" y1="233.68" x2="-63.5" y2="233.68" width="0.1524" layer="91"/>
<pinref part="Z2" gate="G$1" pin="2"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="-63.5" y1="233.68" x2="-68.58" y2="233.68" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="233.68" x2="-71.12" y2="233.68" width="0.1524" layer="91"/>
<junction x="-68.58" y="233.68"/>
<pinref part="C55" gate="G$1" pin="2"/>
<junction x="-63.5" y="233.68"/>
<label x="-55.88" y="233.68" size="0.8128" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="VDD1"/>
<wire x1="-68.58" y1="205.74" x2="-71.12" y2="205.74" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="205.74" x2="-71.12" y2="208.28" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="VDD0"/>
<wire x1="-71.12" y1="208.28" x2="-68.58" y2="208.28" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="208.28" x2="-81.28" y2="208.28" width="0.1524" layer="91"/>
<junction x="-71.12" y="208.28"/>
<label x="-81.28" y="208.28" size="0.8128" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-27.94" y1="228.6" x2="-22.86" y2="228.6" width="0.1524" layer="91"/>
<pinref part="SL1" gate="G$1" pin="1"/>
<label x="-22.86" y="228.6" size="0.8128" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="-73.66" y1="185.42" x2="-68.58" y2="185.42" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="SPI_MISO"/>
<pinref part="TP13" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="-68.58" y1="187.96" x2="-73.66" y2="187.96" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="SPI_MOSI"/>
<pinref part="TP12" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="-73.66" y1="190.5" x2="-68.58" y2="190.5" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="SPI_SCK"/>
<pinref part="TP10" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<wire x1="-68.58" y1="193.04" x2="-73.66" y2="193.04" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="SPI_NSS"/>
<pinref part="TP9" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="SL2" gate="G$1" pin="3"/>
<wire x1="-33.02" y1="99.06" x2="-7.62" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="99.06" x2="-7.62" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="73.66" x2="-20.32" y2="73.66" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="NC/SWCLK"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="SL2" gate="G$1" pin="2"/>
<wire x1="-33.02" y1="96.52" x2="-10.16" y2="96.52" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="96.52" x2="-10.16" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="71.12" x2="-20.32" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="NRESET/SWDIO"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="SWCLK"/>
<wire x1="-68.58" y1="142.24" x2="-68.58" y2="137.16" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="137.16" x2="-12.7" y2="137.16" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="137.16" x2="-12.7" y2="233.68" width="0.1524" layer="91"/>
<pinref part="SL1" gate="G$1" pin="3"/>
<wire x1="-12.7" y1="233.68" x2="-27.94" y2="233.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="SL1" gate="G$1" pin="2"/>
<wire x1="-27.94" y1="231.14" x2="-25.4" y2="231.14" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="SWDIO"/>
<wire x1="-25.4" y1="231.14" x2="-25.4" y2="213.36" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="213.36" x2="-27.94" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="-20.32" y1="60.96" x2="-15.24" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="SIO_4/AIN"/>
<pinref part="TP21" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="I2C0_SCL"/>
<wire x1="-73.66" y1="147.32" x2="-68.58" y2="147.32" width="0.1524" layer="91"/>
<pinref part="TP16" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="I2C0_SDA"/>
<wire x1="-68.58" y1="144.78" x2="-73.66" y2="144.78" width="0.1524" layer="91"/>
<pinref part="TP17" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<wire x1="-73.66" y1="175.26" x2="-68.58" y2="175.26" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="UART0_RX"/>
<pinref part="TP15" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<wire x1="-68.58" y1="177.8" x2="-73.66" y2="177.8" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="UART0_TX"/>
<pinref part="TP14" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="ADC-SCK" class="0">
<segment>
<wire x1="-256.54" y1="43.18" x2="-269.24" y2="43.18" width="0.1524" layer="91"/>
<label x="-269.24" y="43.18" size="0.8128" layer="95" rot="R180" xref="yes"/>
<pinref part="IC5" gate="G$1" pin="SCLK"/>
</segment>
</net>
<net name="ADC1-/CS" class="0">
<segment>
<label x="-269.24" y="45.72" size="0.8128" layer="95" rot="R180" xref="yes"/>
<wire x1="-256.54" y1="45.72" x2="-269.24" y2="45.72" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="/CS"/>
</segment>
</net>
<net name="ADC-MISO" class="0">
<segment>
<label x="-269.24" y="40.64" size="0.8128" layer="95" rot="R180" xref="yes"/>
<wire x1="-256.54" y1="40.64" x2="-269.24" y2="40.64" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="DOUT//DRDY"/>
</segment>
</net>
<net name="ADC-MOSI" class="0">
<segment>
<label x="-269.24" y="38.1" size="0.8128" layer="95" rot="R180" xref="yes"/>
<wire x1="-256.54" y1="38.1" x2="-269.24" y2="38.1" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="DIN"/>
</segment>
</net>
<net name="ADC-/RESET" class="0">
<segment>
<wire x1="-256.54" y1="58.42" x2="-259.08" y2="58.42" width="0.1524" layer="91"/>
<label x="-259.08" y="58.42" size="0.8128" layer="95" rot="R180" xref="yes"/>
<pinref part="IC5" gate="G$1" pin="START/SYNC"/>
</segment>
</net>
<net name="ADC-START" class="0">
<segment>
<wire x1="-256.54" y1="55.88" x2="-259.08" y2="55.88" width="0.1524" layer="91"/>
<label x="-259.08" y="55.88" size="0.8128" layer="95" rot="R180" xref="yes"/>
<pinref part="IC5" gate="G$1" pin="/RESET"/>
</segment>
</net>
<net name="ADC1-/DRDY" class="0">
<segment>
<wire x1="-256.54" y1="53.34" x2="-259.08" y2="53.34" width="0.1524" layer="91"/>
<label x="-259.08" y="53.34" size="0.8128" layer="95" rot="R180" xref="yes"/>
<pinref part="IC5" gate="G$1" pin="/DRDY"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="IN+"/>
<wire x1="-220.98" y1="236.22" x2="-226.06" y2="236.22" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="-220.98" y1="226.06" x2="-220.98" y2="231.14" width="0.1524" layer="91"/>
<wire x1="-220.98" y1="231.14" x2="-220.98" y2="236.22" width="0.1524" layer="91"/>
<junction x="-220.98" y="231.14"/>
<pinref part="R48" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="IC3" gate="G$1" pin="IN-"/>
<wire x1="-226.06" y1="238.76" x2="-213.36" y2="238.76" width="0.1524" layer="91"/>
<wire x1="-213.36" y1="238.76" x2="-213.36" y2="231.14" width="0.1524" layer="91"/>
<junction x="-213.36" y="231.14"/>
<wire x1="-213.36" y1="231.14" x2="-213.36" y2="226.06" width="0.1524" layer="91"/>
<pinref part="R47" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<wire x1="-251.46" y1="236.22" x2="-251.46" y2="231.14" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="VS"/>
<wire x1="-251.46" y1="231.14" x2="-259.08" y2="231.14" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="-259.08" y1="231.14" x2="-266.7" y2="231.14" width="0.1524" layer="91"/>
<junction x="-259.08" y="231.14"/>
<pinref part="C51" gate="G$1" pin="1"/>
<wire x1="-266.7" y1="233.68" x2="-266.7" y2="231.14" width="0.1524" layer="91"/>
<pinref part="Z4" gate="G$1" pin="2"/>
<wire x1="-271.78" y1="231.14" x2="-266.7" y2="231.14" width="0.1524" layer="91"/>
<junction x="-266.7" y="231.14"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<wire x1="-370.84" y1="149.86" x2="-373.38" y2="149.86" width="0.1524" layer="91"/>
<pinref part="Z6" gate="G$1" pin="1"/>
<pinref part="J3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="IC13" gate="G$1" pin="!SHDN"/>
<wire x1="-378.46" y1="228.6" x2="-381" y2="228.6" width="0.1524" layer="91"/>
<pinref part="IC13" gate="G$1" pin="LIM"/>
<wire x1="-350.52" y1="226.06" x2="-347.98" y2="226.06" width="0.1524" layer="91"/>
<wire x1="-347.98" y1="226.06" x2="-347.98" y2="233.68" width="0.1524" layer="91"/>
<wire x1="-347.98" y1="233.68" x2="-381" y2="233.68" width="0.1524" layer="91"/>
<wire x1="-381" y1="233.68" x2="-381" y2="228.6" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="IC13" gate="G$1" pin="VCC"/>
<wire x1="-378.46" y1="226.06" x2="-381" y2="226.06" width="0.1524" layer="91"/>
<wire x1="-381" y1="226.06" x2="-386.08" y2="226.06" width="0.1524" layer="91"/>
<wire x1="-386.08" y1="226.06" x2="-386.08" y2="223.52" width="0.1524" layer="91"/>
<junction x="-386.08" y="226.06"/>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="-386.08" y1="238.76" x2="-386.08" y2="241.3" width="0.1524" layer="91"/>
<wire x1="-386.08" y1="238.76" x2="-386.08" y2="226.06" width="0.1524" layer="91"/>
<junction x="-386.08" y="238.76"/>
<wire x1="-373.38" y1="238.76" x2="-386.08" y2="238.76" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="3"/>
<junction x="-373.38" y="238.76"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="-373.38" y1="241.3" x2="-373.38" y2="238.76" width="0.1524" layer="91"/>
<wire x1="-381" y1="228.6" x2="-381" y2="226.06" width="0.1524" layer="91"/>
<junction x="-381" y="228.6"/>
<junction x="-381" y="226.06"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<wire x1="-345.44" y1="139.7" x2="-347.98" y2="139.7" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="VDD"/>
<wire x1="-347.98" y1="139.7" x2="-347.98" y2="149.86" width="0.1524" layer="91"/>
<pinref part="Z6" gate="G$1" pin="2"/>
<wire x1="-347.98" y1="149.86" x2="-353.06" y2="149.86" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="-353.06" y1="149.86" x2="-360.68" y2="149.86" width="0.1524" layer="91"/>
<wire x1="-360.68" y1="134.62" x2="-360.68" y2="149.86" width="0.1524" layer="91"/>
<junction x="-360.68" y="149.86"/>
<pinref part="R8" gate="G$1" pin="1"/>
<junction x="-353.06" y="149.86"/>
<wire x1="-335.28" y1="157.48" x2="-337.82" y2="157.48" width="0.1524" layer="91"/>
<wire x1="-337.82" y1="157.48" x2="-337.82" y2="149.86" width="0.1524" layer="91"/>
<wire x1="-337.82" y1="149.86" x2="-347.98" y2="149.86" width="0.1524" layer="91"/>
<junction x="-347.98" y="149.86"/>
<pinref part="J4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="U_BOOST" class="0">
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="R42" gate="G$1" pin="1"/>
<wire x1="-317.5" y1="238.76" x2="-325.12" y2="238.76" width="0.1524" layer="91"/>
<wire x1="-317.5" y1="238.76" x2="-309.88" y2="238.76" width="0.1524" layer="91"/>
<wire x1="-309.88" y1="238.76" x2="-309.88" y2="231.14" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="-337.82" y1="238.76" x2="-332.74" y2="238.76" width="0.1524" layer="91"/>
<junction x="-325.12" y="238.76"/>
<wire x1="-332.74" y1="238.76" x2="-325.12" y2="238.76" width="0.1524" layer="91"/>
<junction x="-309.88" y="238.76"/>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="R50" gate="G$1" pin="1"/>
<junction x="-332.74" y="238.76"/>
<pinref part="TP3" gate="G$1" pin="TP"/>
<junction x="-317.5" y="238.76"/>
<pinref part="J2" gate="G$1" pin="2"/>
<label x="-325.12" y="238.76" size="0.8128" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<wire x1="-355.6" y1="35.56" x2="-355.6" y2="38.1" width="0.1524" layer="91"/>
<pinref part="R33" gate="G$1" pin="2"/>
<label x="-355.6" y="38.1" size="0.8128" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="SPI2C"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="-353.06" y1="139.7" x2="-353.06" y2="129.54" width="0.1524" layer="91"/>
<wire x1="-353.06" y1="129.54" x2="-345.44" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<pinref part="IC2" gate="G$1" pin="!SYNC!/A0"/>
<wire x1="-312.42" y1="149.86" x2="-307.34" y2="149.86" width="0.1524" layer="91"/>
<wire x1="-307.34" y1="149.86" x2="-307.34" y2="132.08" width="0.1524" layer="91"/>
<wire x1="-307.34" y1="132.08" x2="-309.88" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="C54" gate="G$1" pin="1"/>
<pinref part="IC2" gate="G$1" pin="VREFIO"/>
<wire x1="-304.8" y1="139.7" x2="-309.88" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="-325.12" y1="162.56" x2="-325.12" y2="157.48" width="0.1524" layer="91"/>
<wire x1="-325.12" y1="157.48" x2="-325.12" y2="149.86" width="0.1524" layer="91"/>
<wire x1="-325.12" y1="149.86" x2="-322.58" y2="149.86" width="0.1524" layer="91"/>
<pinref part="J5" gate="1" pin="2"/>
<pinref part="J4" gate="G$1" pin="2"/>
<junction x="-325.12" y="157.48"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<wire x1="-71.12" y1="55.88" x2="-83.82" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="SIO_29/SCL"/>
<pinref part="TP18" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<wire x1="-71.12" y1="53.34" x2="-83.82" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="SIO_30/SDA"/>
<pinref part="TP19" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="UART4_CTS" class="0">
<segment>
<label x="-91.44" y="68.58" size="0.8128" layer="95" rot="R180" xref="yes"/>
<wire x1="-83.9724" y1="68.58" x2="-91.44" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="UART4_RTS" class="0">
<segment>
<label x="-91.44" y="66.04" size="0.8128" layer="95" rot="R180" xref="yes"/>
<wire x1="-83.9724" y1="66.04" x2="-91.44" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="UART4_RX" class="0">
<segment>
<label x="-91.44" y="73.66" size="0.8128" layer="95" rot="R180" xref="yes"/>
<wire x1="-83.9724" y1="73.66" x2="-91.44" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="UART4_TX" class="0">
<segment>
<label x="-91.44" y="71.12" size="0.8128" layer="95" rot="R180" xref="yes"/>
<wire x1="-83.9724" y1="71.12" x2="-91.44" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<wire x1="-309.88" y1="134.62" x2="-304.8" y2="134.62" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="SDIN/SDA"/>
<label x="-304.8" y="134.62" size="0.8128" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-165.1" y1="198.12" x2="-167.64" y2="198.12" width="0.1524" layer="91"/>
<label x="-167.64" y="198.12" size="0.8128" layer="95" rot="R180" xref="yes"/>
<pinref part="J8" gate="1" pin="2"/>
</segment>
<segment>
<wire x1="-170.18" y1="134.62" x2="-170.18" y2="132.08" width="0.1524" layer="91"/>
<pinref part="J9" gate="1" pin="1"/>
<label x="-170.18" y="132.08" size="0.8128" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="SDA/SDI"/>
<wire x1="-154.94" y1="198.12" x2="-154.94" y2="193.04" width="0.1524" layer="91"/>
<pinref part="J8" gate="1" pin="1"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="SCL/SCLK"/>
<wire x1="-154.94" y1="203.2" x2="-152.4" y2="203.2" width="0.1524" layer="91"/>
<wire x1="-152.4" y1="203.2" x2="-152.4" y2="193.04" width="0.1524" layer="91"/>
<pinref part="J7" gate="1" pin="2"/>
</segment>
</net>
<net name="UART1_RX" class="0">
<segment>
<wire x1="-88.9" y1="182.88" x2="-91.44" y2="182.88" width="0.1524" layer="91"/>
<pinref part="J12" gate="1" pin="2"/>
<label x="-91.44" y="182.88" size="0.8128" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="UART1_TX" class="0">
<segment>
<wire x1="-91.44" y1="180.34" x2="-88.9" y2="180.34" width="0.1524" layer="91"/>
<pinref part="J13" gate="1" pin="2"/>
<label x="-91.44" y="180.34" size="0.8128" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="J12" gate="1" pin="1"/>
<pinref part="U$2" gate="G$1" pin="UART1_TX"/>
<wire x1="-78.74" y1="182.88" x2="-68.58" y2="182.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="UART1_RX"/>
<pinref part="J13" gate="1" pin="1"/>
<wire x1="-68.58" y1="180.34" x2="-78.74" y2="180.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="UART1_RTS" class="0">
<segment>
<wire x1="-15.24" y1="208.28" x2="-7.62" y2="208.28" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="208.28" x2="-7.62" y2="210.82" width="0.1524" layer="91"/>
<label x="-7.62" y="210.82" size="0.8128" layer="95" rot="R90" xref="yes"/>
<pinref part="J15" gate="1" pin="1"/>
</segment>
</net>
<net name="UART1_CTS" class="0">
<segment>
<wire x1="-10.16" y1="210.82" x2="-15.24" y2="210.82" width="0.1524" layer="91"/>
<label x="-10.16" y="210.82" size="0.8128" layer="95" rot="R90" xref="yes"/>
<pinref part="J14" gate="1" pin="1"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="UART1_RTS"/>
<wire x1="-25.4" y1="210.82" x2="-27.94" y2="210.82" width="0.1524" layer="91"/>
<pinref part="J14" gate="1" pin="2"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="UART1_CTS"/>
<wire x1="-27.94" y1="208.28" x2="-25.4" y2="208.28" width="0.1524" layer="91"/>
<pinref part="J15" gate="1" pin="2"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="SIO_23/UART_RTS"/>
<wire x1="-71.12" y1="68.58" x2="-73.5076" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="SIO_24/UART_CTS"/>
<wire x1="-71.12" y1="66.04" x2="-73.5076" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="SIO_21/UART_TX"/>
<wire x1="-71.12" y1="73.66" x2="-73.5076" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="SIO_22/UART_RX"/>
<wire x1="-71.12" y1="71.12" x2="-73.5076" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CLK-ADC1-OUT1" class="0">
<segment>
<wire x1="-256.54" y1="30.48" x2="-259.08" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-259.08" y1="30.48" x2="-261.62" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-259.08" y1="27.94" x2="-259.08" y2="30.48" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="CLK"/>
<pinref part="J21" gate="1" pin="1"/>
<pinref part="J22" gate="G$1" pin="2"/>
</segment>
</net>
<net name="CLK-ADC1-OUT" class="0">
<segment>
<wire x1="-271.78" y1="30.48" x2="-274.32" y2="30.48" width="0.1524" layer="91"/>
<pinref part="J22" gate="G$1" pin="1"/>
<label x="-274.32" y="30.48" size="0.8128" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="VCC_BOOST" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="-299.72" y1="238.76" x2="-297.18" y2="238.76" width="0.1524" layer="91"/>
<label x="-294.64" y="228.6" size="1.778" layer="95" rot="R270"/>
<wire x1="-297.18" y1="238.76" x2="-297.18" y2="220.98" width="0.1524" layer="91"/>
<label x="-297.18" y="220.98" size="0.8128" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="IC7" gate="G$1" pin="V+"/>
<wire x1="-223.52" y1="137.16" x2="-220.98" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C52" gate="G$1" pin="2"/>
<wire x1="-220.98" y1="137.16" x2="-218.44" y2="137.16" width="0.1524" layer="91"/>
<junction x="-220.98" y="137.16"/>
<label x="-218.44" y="137.16" size="0.8128" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="J24" gate="G$1" pin="1"/>
<pinref part="Z4" gate="G$1" pin="1"/>
<wire x1="-287.02" y1="231.14" x2="-281.94" y2="231.14" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
