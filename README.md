# Extension board
We use a existing controller board with microcontroller, flash and power supply and build a extension board for it. Most communication between the boards happens over SPI and I2C. The supply voltage provided is 3V.

![](figures/extension_board.png)
IMU and Lora Modules are not included in the diagram.

The 12 provided pins for extension boards are located on main boards header JP2:

- P1.08 (DIN)
- P1.09 (DOUT)
- P0.27 (SCLK)
- P0.26 (CS_ADC)
- P1.05 (CS_IMU)
- P1.07 (free)
- P0.25 (free)
- P0.24 (free)
- SCL
- SDA
- GND
- VCC (regulated 3V)

## ADC

ADS114S08
- SPI (DIN, DOUT, SCLK, CS)
- Analog Supply: Unipolar (2.7 V to 5.25 V) or Bipolar (±2.5 V)
- Digital Supply: 2.7 V to 3.6 V
- 12 AIN pins
- 16-bit sigma-delta ADC
- 2.5 SPS to 4 kSPS
- Internal Reference: 2.5 V
- Programmable Gain: 1 to 128 (range: 0 - 2.5 V to 0 - 19.53 mV) 
- Internal Temperature Sensor (can be used for cold-junction compensation? Otherwise use NTC 10k with 2.2 kΩ.
- Operating Temperature: –50°C to +125°C

ADC-Pin assignment:
- AIN0 for NTC cold-junction compensation
- AIN1 for TEG-voltage measurement
- AIN2 for TEG-current measurement 
- AIN3 - AIN4 for thermocouple-sensors
- (AIN5 for gate voltage measurement)
    - 6 pins total

[Datasheet](https://www.ti.com/lit/ds/symlink/ads114s08.pdf?ts=1631465589843&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FADS114S08)

[Mouser](https://www.mouser.at/ProductDetail/Texas-Instruments/ADS114S08IRHBT?qs=sGAEpiMZZMv0NwlthflBix0u6alvYvpzmqDvGU7CtX8%3D)

Voltage dividers
- ADC ref: 2.5V
- R2: 180kOhm

```
Uout = 2.5V
R2 = 180k 
R1 = R2 * (Uin / Uout - 1) 

Uin = 12V
R1 = 180k * (12/2.5 - 1) = 684k ~= 680k

Uin = 5V
R1 = 180 * (5/2.5 - 1) = 180k
```

## Mosfet as adjustable load

Transistor as adjustable load for peltier cell. Needs adjustble gate voltage source.

## Gate voltage
Creating the mosfet gate voltage.

Requirements:
- 3V input
- 0 - 10V output
- stable
- very low current

DAC with output voltages above 5V are very uncommon. Therefore we amplify the output of the 0-3V DAC with an OP-Amp.
[using this as reference](https://store.ncd.io/product/1-channel-0-10v-dac-digital-analog-converter-i%C2%B2c/)

### DAC

DAC80501ZDGST
- I2C interface
- 16 bit
- 1 mA at 5.5 V

[Mouser](https://www.mouser.at/ProductDetail/Texas-Instruments/DAC80501ZDGST?qs=%252B6g0mu59x7JqdHgN5ZkaXA==)

[Datasheet](https://www.ti.com/lit/ds/symlink/dac80501.pdf?ts=1638266801893&ref_url=https%253A%252F%252Fwww.mouser.es%252F)

#### Not in stock
MCP4725
- 12-Bit
- Voltage supply: 2.7V to 5.5V
- I2C interface
- Range with Vin = 3 V: 0 - 3 V
- Settling Time: 6 µs 
- Extended Temperature Range: -40°C to +125°C
- 6-lead SOT-23 package

[Mouser](https://www.mouser.at/ProductDetail/Microchip-Technology/MCP4725A0T-E-CH?qs=vkMqrgFpRajeQECZ3goc0A==)

[Datasheet](https://www.mouser.at/datasheet/2/268/22039d-9676.pdf)

### Amplifier
TLV9102IDDFR
- Wide supply: ±1.35 V to ±8 V, 2.7 V to 16 V
- low power operation (115 µA, typ) 

To amplify the 0 - 3 V DAC voltage to 0 - 10V, I choose any common rail-to-rail opamp and use a non-inverting amplifier circuit.

```
    Gain of 10/3 = 3.33
    U2 = 3V
    U1 = 10V - 3V
    R2 = 200kΩ
    
    U1 / U2 = R1 / R2
    R1 = R2 * U1 / U2 
    R1 = 200kΩ * (10V-3V) / 3V = 466.666kΩ

```

[Mouser](https://www.mouser.at/ProductDetail/Texas-Instruments/TLV9102IDDFR?qs=IS%252B4QmGtzzroGAqUkTxJrA==)

[Datasheet](https://www.ti.com/lit/ds/symlink/tlv9102.pdf?HQS=dis-mous--mousermode-dsf-pf--wwe&DCM=yes&ref_url=https%3A%2F%2Fwww.mouser.com%2F&distId=26)

### Boost converter
To generate a 10V source for the OP-Amp.

TPS61040DDCR

- 1.8 V - 6 V supply
- Output up to 28 V 
- 250-mA Internal Switch Current 
- 28-μA Typical No-Load Quiescent Current
- Passives need to be calculated (inductor, diode, 3x capacitors, 2x resistors)
- SOT-23-Thin-5 package

[Mouser 250mA version](https://www.mouser.at/ProductDetail/Texas-Instruments/TPS61041DBVR?qs=sGAEpiMZZMsKEdP9slC0YcJKWAcv5ceP)

[Datasheet](https://www.ti.com/lit/ds/symlink/tps61040.pdf?HQS=dis-mous-null-mousermode-dsf-pf-null-wwe&ts=1631716080703&ref_url=https%253A%252F%252Fwww.mouser.it%252F)

Passives

- L1 [10uH 760mA 230 mohms](https://www.mouser.at/ProductDetail/Sumida/CR32NP-100KC?qs=%2Fha2pyFadugw9EqBgpBQAuhHV0U5whTirm0g6Xo9Fik%3D)
- D1 shotky [MBR0520LT1G](https://www.mouser.at/ProductDetail/onsemi/MBR0520LT1G?qs=3JMERSakebqBWTiPUQp0nw%3D%3D) 
- C_out [0805 25V 4.7uF X5R](https://www.mouser.at/ProductDetail/TDK/C2012X5R1E475K125AB?qs=LcTL%2F5vFEzEfIWXeqp5s%252BQ%3D%3D) 
- C_in same as in ADC design
- C_ff 10pF
- R1: 1.5MOhm (calculated: 1.571824 MOhm)
- R2: 180kOhm

Output of python script:

```
---- inputs
vin:   3 V
vout:  12 V
ip:    0.25 A
L:     1e-05 H
r2:    180000.0 Ohm

---- outputs
switching freq:           899999 Hz (must be lower than 1MHz:  OK )
incuctor peak current:    7.499999999999999 mA (inductor must handle this)
max load current:         21.875 mA
resistor r1:              1571824 Ohm (must be lower than 2.2 MOhm:  OK )
cff:                      2.250108583180053 pF (recommended value 10 pF)

```


## Current sense amplifier for high side measurement

INA190

- 65 µA of supply current
- Low supply voltage, VS: 1.7 V to 5.5 V
- 25 V/V, 50 V/V, 100 V/V, 200 V/V, or 500 V/V
- common-mode voltages from –0.2 V to +40 V
- Low input bias currents: 500 pA (typ)
- Temperature –40 °C to +125 °C
- enable pin 

[Datasheet](https://www.ti.com/lit/ds/symlink/ina190.pdf?ts=1635992834147)

### Calculate Gain and shunt resistor
- maximal current while testing TEG: 1.5A
- choosing maximal expected current: 5A

Script output: (current_sens_passives.py)
```
---- inputs:
supply voltage:    3 V [OK]
max current:       5 A
max power over R:  0.25 W
gain:              50 V/V
vref:              0 V ( 0V for unidirectional )
exteral ADC ref:   2.5 V
unidirectional

---- outputs:
Rsens is 0.01 Ohm ( 0.25 W at 5 A )
Maximal output range from 0.001 V to 2.96 V
Full output range is  0.002 A to  5.92 A

Range is 0.002 A ( 0.001 V ) to 5 A ( 2.5 V )
Using  100.0 % of ADC resolution
    min output bigger than min output range:  [OK]
    max output smaller than max output range: [OK]
    max output smaller than ext. ADC range:   [OK]
```
Using A2 Variant with 50V/V gain: INA190A2-Q1
[Mouser](https://www.mouser.at/ProductDetail/Texas-Instruments/INA190A2IDDFR?qs=wnTfsH77Xs4o%2FqYxJnHUvw%3D%3D)


Shunt resistor
- SMD 2512 0.01ohm 1% Shunt Res AEC-Q200
- 3 W
[Mouser](https://www.mouser.at/ProductDetail/ROHM-Semiconductor/GMR100HTBFA10L0?qs=chTDxNqvsylQYSdLz9rpug%3D%3D)

C_BYPASS
- recommended 0.1uF 

Optional: 
- Lowpass filter
- pass a small vref voltage to accurately represent 0A. Currently the output voltage range is only from 0.001 V to 2.96 V. 



### Worse alternatives:

INA190-Q1, same as INA190 except
- AEC-Q100 qualified for automotive applications (not necessary)
- without enable pin
[Datasheet](https://www.ti.com/lit/ds/symlink/ina190-q1.pdf?ts=1635969017542&ref_url=https%253A%252F%252Fwww.mouser.at%252F)

INA280-Q1
- 370-μA supply current

INA293-Q1
- **1.5 mA of supply current** too much current
- Supply Voltage: 2.7V - 20 V
- Common-mode input range –4 to 110 V
- Available gains: 
    - INA293A1-Q1, INA293B1-Q1 : 20 V/V
    - INA293A2-Q1, INA293B2-Q1 : 50 V/V
    - INA293A3-Q1, INA293B3-Q1 : 100 V/V
    - INA293A4-Q1, INA293B4-Q1 : 200 V/V
    - INA293A5-Q1, INA293B5-Q1 : 500 V/V
- Temperature –40 °C to +125 °C
- needs a shunt resitor

[Mouser](https://www.mouser.at/ProductDetail/Texas-Instruments/INA293A3QDBVRQ1?qs=T94vaHKWudQpVen%252BZfuwMw%3D%3D)

[Datasheet](https://www.ti.com/lit/ds/symlink/ina293-q1.pdf?ts=1631676781299&ref_url=https%253A%252F%252Fwww.ti.com%252Famplifier-circuit%252Fcurrent-sense%252Foverview.html%253FkeyMatch%253DCURRENT%2BSENSE%2BAMPLIFIERS)


## IMU
  
#### TDK InvenSense ICM-20789

- 7 DOF (3-axis gyroscope & 3-axis accelerometer & barometer)
- 8kHz
- 16-bit ADCs
- SPI & I2C (for barometer)
- -40°C - 80°C

[Mouser](https://eu.mouser.com/ProductDetail/TDK-InvenSense/ICM-20789?qs=%2Fha2pyFadujrMbzlgf633axxztCeYrB1Yso1nP3p7pg%3D)

[Datasheet](https://product.tdk.com/system/files/dam/doc/product/sensor/mortion-inertial/imu/data_sheet/ds-000169-icm-20789-typ-v1.4.pdf)

For I2C implementation:

[LDO 1.8 V 300mA](https://www.mouser.at/ProductDetail/Texas-Instruments/TLV70218DBVT?qs=zQ2yIHQ6k3yCTbRGDS6zAA%3D%3D)



## Lora Modul
Requirements:
- at least -40°C to 85°C operating temperature
- for european frequency band 863 MHz to 870 MHz

### 1. RM186-SM-02

- 2.1V – 3.5V supply voltage
- -40 ˚C to +85 ˚C
- BLE
- range up to 15 km
- I2C, SPI and Full UART

[Digikey](https://www.digikey.at/product-detail/de/laird-connectivity-inc/RM186-SM-02/RM186-SM-02-ND/6109663)

[Datasheet](https://connectivity-staging.s3.us-east-2.amazonaws.com/2019-02/CS-DS-RM1xx%20v1_9.pdf)

[Datasheet](https://docs.rs-online.com/b459/A700000007211715.pdf)

### 2. MTXDOT-EU1-A01-100

- 2.4 to 3.57 V supply voltage
- up to 10 miles (15 km) line of sight 1 - 3 miles (2km) into buildings
- -40° C to +85° C
- I2C, SPI, UART

[Digikey](https://www.digikey.at/product-detail/de/multi-tech-systems-inc/MTXDOT-EU1-A01-100/591-1314-ND/6237016)

[Datasheet](https://www.multitech.com/documents/publications/data-sheets/86002182.pdf)

[Datasheet of dev-board](https://www.multitech.com/documents/publications/quick-start-guides/82101900l.pdf)

[Developer guide](https://www.multitech.com/documents/publications/developer-guides/S000645--xDOT-Developer-Guide.pdf)


(Habe bei ersten Versuchen das Modul E220-900T22D mit Chip LLCC68 verwendet, und hat bis 100°C funktioniert. Dieses ist aber bis jetzt nur in Ostasien erhältlich)


## Connections to main board

```mermaid
graph TB
IN(NRF53 Mainbord) 
IN -- VCC --- V(3V supply)
V-- DCDC --- VV(10V supply)
IN -- SPI --- ADC
IN -- I2C --- DAC 
IN -- SPI/I2C --- IMU
IN -- SPI or I2C or UART  --- Lora-Modules
```


# Existing controller board
## Microcontroller
#### NRF5340
- -40°C to 105°C
- Low Energy Bluetooth Stack
- 1 MB flash, 512 kB RAM, up to 128 MHz
- bereits vorhanden

[Datasheet](https://infocenter.nordicsemi.com/pdf/nRF5340_PS_v1.1.pdf)

## Power Supply
Buck converter ADP5302
- Vout = 3V

USB charger MCP73831
- 100mA


## RAM (or is it flash?)
2 x 512kB with load switch(TPS22860) to disable the ram to save energy.
- connected via SPI

## Lora module
SX1261/2 with passive components directly on board.
- connected via SPI

## Temperature/humidity sensor
HDC1080DMBR
- connected via I2C

## 2 x microphone connectors
- not used by this project
