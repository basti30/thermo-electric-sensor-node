vin = 3
vout = 10
ip = 250e-3 # 400 mA or 250 mA
L = 10e-6 #  2.2 μH to 47 μH

r2 = 180e3 #R2 of ≤200 kΩ and a maximum value for R1 of 2.2 MΩ


print('---- inputs')
print('vin:  ', vin, 'V')
print('vout: ', vout, 'V')
print('ip:   ', ip, 'A')
print('L:    ', L, 'H')
print('r2:   ', r2, 'Ohm')

print('\n---- outputs')
fs = (vin*(vout-vin))/(ip*L*vout)
print('switching freq:          ', int(fs), 'Hz (must be lower than 1MHz: ', 'OK'if fs<1e6 else 'ERROR', ')')

i_peak = ip * 100e-9 * vin / L 
print('incuctor peak current:   ', i_peak*1e3, 'mA (inductor must handle this)')

i_load_max = 0.7 * (ip**2 * L * fs)/(2 * (vout - vin))
print('max load current:        ', i_load_max*1e3, 'mA')

r1 = r2 * (vout/1.233 - 1)
print('resistor r1:             ', int(r1),'Ohm (must be lower than 2.2 MOhm: ', 'OK'if r1<2.2e6 else 'ERROR', ')')

import math
cff = 1/(2*math.pi*r1*fs/20)
print('cff:                     ', cff*1e12, 'pF (recommended value 10 pF)')